# ParTCP Vote

ParTCP Vote ist eine Anwendung, mit der Sie an Abstimmungen, Umfragen und anderen Online-Veranstaltungen teilnehmen können, bei denen Daten an eine zentrale Stelle zu übermitteln sind. Die Anwendung ist Teil der Open-source-Plattform ParTCP, bei deren Entwicklung besonderer Wert gelegt wurde auf Dezentralität, Anonymität und den Schutz personenbezogener Daten.

ParTCP Vote kann ohne Installation in allen modernen Webbrowsern ausgeführt werden und steht für bestimmte Betriebssysteme auch als installierbare App zur Verfügung. Wenn Sie Ihre eigenen Abstimmungen oder Umfragen durchführen möchten, besuchen Sie die Webseite partcp.org.

Aktuelle Version: https://vote.partcp.org

Quellcode: https://codeberg.org/ParTCP/partcp_client




---

**Lizenzhinweis**

Copyright © 2025 ParTCP Working Group

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.

