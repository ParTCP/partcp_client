// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:get/get_utils/src/platform/platform.dart';

class Constants {

  static const XDG_DESKTOP_NAME = "partcp";            // for linux gdx-open
  static const APP_HOST = "https://app.partcp.org";
  static const APP_HOME = "https://app.partcp.org/";

  static const CLIENT_APPDATA_DIR_NAME = "PartcpClient";    /// need for Windows. In ParTcpRunner must be exactly the same

  static const LOT_CODES_DIR_NAME = "Lot-Codes";
  static const EXPORT_DIR_NAME = "Exports";
  static const APK_DIR_NAME = "Apk";

  static const APP_LOCK_FILE_NAME = "app.lock";
  static const CLIENT_UNI_LINK_FILE = "uniLink";


  static String get APP_REV_URL {
    String appClient = "";
    if (GetPlatform.isWeb) {
      appClient = "web";
    }
    else if (GetPlatform.isDesktop && GetPlatform.isLinux) {
      appClient = "linux";
    }
    else if (GetPlatform.isDesktop && GetPlatform.isWindows) {
      appClient = "windows";
    }
    else if (GetPlatform.isAndroid) {
      appClient = "apk";
    }
    return "${APP_HOME}revision/index.yaml.php?appClient=$appClient";
  }


  static String fileNameRegExp(String fileName) {
    return fileName.replaceAll(RegExp(r'[^a-zA-Z0-9.]'), '-');

  }
  static String fileDirRegExp(String fileDir) {
    return fileDir.replaceAll(RegExp(r'[^a-zA-Z0-9.-/_]'), '-');
  }

  static const WALLET_FILE_NAME = "ParTCP-{n}-{d}.txt";

  static const ASSET_SERVER_LIST_FILE_NAME = "assets/server_list.txt";

  static const SNACK_BAR_DURATION_LONG = Duration(seconds: 3, milliseconds: 500);
  static const SNACK_BAR_DURATION_SERVER_ERROR = Duration(seconds: 10);


  // static Future<Directory?> get internalStorageRootDir async {
  //   return GetPlatform.isMobile && !GetPlatform.isWeb
  //       ? await pathProvider.getApplicationDocumentsDirectory()
  //       : null;
  // }
  //
  // static Future<Directory?> get externalStorageRootDir async {
  //   return GetPlatform.isMobile && !GetPlatform.isWeb
  //       ? (await pathProvider.getExternalStorageDirectories())![0]  /// -> /storage_storage/Android/data/APP_NAME/
  //       : null;
  // }

  // static Future<Directory?> get appDirExternal async {
  //   if (MainController.to.isDesktopOrWeb) return null;
  //
  //   String appPath =  path.join((await externalStorageRootDir)!.path,CLIENT_APPDATA_DIR_NAME);
  //   Directory appDir = Directory(appPath);
  //
  //   if (await Permissions.requestPermission(
  //       Permissions.group(MyPermissionGroups.ExternalStorage))) {
  //     if (!appDir.existsSync()) {
  //       appDir.createSync(recursive: true);
  //     }
  //   }
  //   return appDir;
  // }

  // static Future<Directory?> get appDirInternal async {
  //   if (MainController.to.isDesktopOrWeb) return null;
  //
  //   String appPath = path.join((await internalStorageRootDir)!.path,CLIENT_APPDATA_DIR_NAME);
  //   Directory appDir = Directory(appPath);
  //
  //   if (!appDir.existsSync()) {
  //     appDir.createSync(recursive: true);
  //   }
  //   return appDir;
  // }

  // static Future<Directory?> get appExportDir async {
  //   if (MainController.to.isDesktopOrWeb) return null;
  //
  //   String _path =  path.join((await appDirExternal)!.path, EXPORT_DIR_NAME);
  //   Directory appDir = Directory(_path);
  //
  //   if (await Permissions.requestPermission(
  //       Permissions.group(MyPermissionGroups.ExternalStorage))) {
  //     if (!appDir.existsSync()) {
  //       appDir.createSync(recursive: true);
  //     }
  //   }
  //   return appDir;
  // }

  // static Future<Directory?> get appApkDir async {
  //   if (MainController.to.isDesktopOrWeb) return null;
  //
  //   String _path =  path.join((await appDirExternal)!.path, APK_DIR_NAME);
  //   Directory appDir = Directory(_path);
  //
  //   if (await Permissions.requestPermission(
  //       Permissions.group(MyPermissionGroups.ExternalStorage))) {
  //     if (!appDir.existsSync()) {
  //       appDir.createSync(recursive: true);
  //     }
  //   }
  //   return appDir;
  // }

  ///////////////////////////////////////////////////////////////////////
  /// IM
  /// http error codes
  static const int CODE__OK                = 1;
  static const int CODE__CANCEL_BY_USER    = 60;
  static const int CODE__NO_INTERNET       = 100;
  static const int CODE__IO_EXCEPTION      = 110;
  static const int CODE__ERROR_ON_CHUNKS   = 5010;
  static const int CODE__CWS__NOT_CONNECTED= 101;
  static const int CODE__CWS__EXCEPTION    = 5100;
  static const int CODE__CWS__ERROR        = 501;
  static const int CODE__SERVER_ERROR_500  = 500;
  static const int CODE__WRONG_ACTION_401  = 401;
  static const int CODE__FILE_NOT_FOUND_404= 404;

  static const int APP_REV = 8;

  static const URL_WEB_SOCKET			 	= "im.partcp.ogx.de";
  // static const URL_WEB_SOCKET			 	= "im.concryptum.com";
  static const URL_WEB_SOCKET_PUSH  = "push.im.partcp.ogx.de";

  static const String APP_ID_QR_CODE_VERSION = "4";
  static const int USER_ID_LEN = 8;

  static const int AES_PWD_LENGTH = 32;
  static Duration AES_KEY_MAX_AGE = const Duration(days: 10);
  static const String WSOCKET_ADMIN_USER_ID = "[admin ]";


  static final Duration WEBSOCKET_DISCONNECTION_TIME =  const Duration(minutes: 1); /// 5
  static final Duration COMPOSING_INTERVAL_SEND = const Duration(seconds: 2);
  static final Duration COMPOSING_INTERVAL_RECEIVE = const Duration(seconds: 2, milliseconds: 200);

  static final Duration pageRouteHeroAnimDuration = const Duration(milliseconds: 300);
  static final Duration pageRouteFadeAnimDuration = const Duration(milliseconds: 200);
  static final Duration pageRouteSlideAnimDuration = const Duration(milliseconds: 150);

  static final Duration barsAnimationDuration = const Duration(milliseconds: 200);

  static final int avatarFadeOutDuration = 400;
  static final int avatarFadeInDuration = 700;

  static const String dbName = "im_db.db";

}