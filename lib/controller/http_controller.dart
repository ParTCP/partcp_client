// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved


import 'dart:async';
import 'dart:convert';

import 'package:dio/dio.dart' as d;
import 'package:get/get.dart';
import '/objects/account.dart';
import '/objects/server_response.dart';
import '/objects/api_server.dart';
import '/utils/crypt/my_ec_crypt.dart';
import '/utils/log.dart';
import '/utils/yaml_converter.dart';

import 'main_controller.dart';

const _HTTP_STATUS = "HTTP_STATUS";
const _HTTP_BODY = "HTTP_BODY";
const _SIGNATURE = "Signature";
const _PUBLIC_KEY = "Public-Key";
const _FROM = "From";

// const HTTP_BODY_MAP = "HTTP_BODY_MAP";

class GetXProvider extends GetConnect {
  @override
  void onInit() {
    httpClient.timeout = const Duration(seconds: 5);
    httpClient.followRedirects = false;
    httpClient.maxAuthRetries = 3;

  }
}

enum ServerRequest {
  ping,
  keySubmission,
  votingListRequest,
}



// class HttpController extends GetxController {
class HttpController {

  final _TAG = "HttpController";
  bool test = false;
  // static HttpController to = Get.find<HttpController>();

  GetXProvider getXProvider = GetXProvider()..timeout = (const Duration(seconds: 15));
  d.CancelToken? dioCancelToken;

  final YamlConverter _yamlConverter = YamlConverter();

  /// SEND TO SERVER
  Future<ServerResponse> sendToServer(String url, String body, {bool post = true, String loc = "", bool useGetX = true, StreamController<int>? streamController, Function(int, d.CancelToken?)? progressCallback}) async {
    return await useGetX
        ? _sendToServerViaGetX(url, body, post: post, loc: loc, streamController: streamController)
        : _sendToServerViaDio(url, body, post: post, loc: loc, streamController: streamController, progressCallback: progressCallback);

//     String _debugBody = body.length > 1024 ? "TRUNCATED for debug (1024) \n${body.substring(0, 1024)}" : body;
//     Log.spacer();
//     Log.d(_TAG, "---------------------------------------------------------------------");
//     Log.d(_TAG, "_sendToServer: url: $url");
//     Log.d(_TAG, "_sendToServer: body: \n$_debugBody");
//     Log.spacer();
//
//     ServerResponse serverResponse = ServerResponse()
//       ..receivingData = true
//       ..request = body;
//
//     Map<String, String>? httpHeader = {};
//     httpHeader['X-Partcp-Version'] = '1.0';
//
//     if (MainController.to.useShaHash) {
//       httpHeader['X-Partcp-Kx-Method'] = '1';
//     }
//
//     // Response _response;    /// getX
//
//     d.Dio _dio = d.Dio();
//     d.Response _response;     /// Dio
//
//
//     var t1 = DateTime.now().millisecondsSinceEpoch;
//     var t2 = DateTime.now().millisecondsSinceEpoch;
//
//
//     if (post) {
//       if (streamController != null) {
//         streamController.sink.add(0);
//       }
//
//       int _lastProgress = 0;
//
//       /// DIO
//       _response = await _dio.post(
//         url,
//         data: body,
//         onSendProgress: (int sent, int total) {
//           int progress = (sent / total * 100).round();
//           if (progress > _lastProgress) {
//             _lastProgress = progress;
//
//             streamController?.sink.add(progress);
//             if (progressCallback != null) {
//               progressCallback(progress);
//             }
//             // print("$sent,  $total, $progress%");
//           }
//         },
//       );
//
//
//       /// altes geuX
//       /*
//       _response = await getXProvider.post(
//           url, body, contentType: "test/plain", headers: httpHeader,
//           uploadProgress: (double? progress) {
//
//             if (progress != null) {
//               int progressRound = progress.round();
//               if (progress > _lastProgress) {
//                 _lastProgress = progressRound;
//
//                 streamController?.sink.add(progressRound);
//                 if (progressCallback != null) {
//                   progressCallback(progressRound);
//                 }
//                 // print("$sent,  $total, $progress%");
//               }
//             }
//           }
//       );
//       */
//
//     }
//     else {
//       // _response = await getXProvider.get(url);
//       _response = await _dio.get(url);
//     }
//
//     var t3 = DateTime.now().millisecondsSinceEpoch;
//     Log.d(_TAG, "dt ($loc): upload: ${t2-t1}ms, gesammt: ${t3-t1}ms");
//
//
//     serverResponse.receivingData = false;
//     Map<String, dynamic> defMap = {};
//
//
//     /// DIO
//
//     if (_response.statusCode != 200) {
//       var respTxt = "${_response.statusMessage}";
//       if (_response.data != null) {
//         respTxt += " : ${_response.data}";
//       }
//       serverResponse
//         ..statusCode = _response.statusCode
//         ..statusText = _response.statusMessage
//         ..body = _response.data.toString()
//         ..bodyMap = defMap
//         ..errorMessage = respTxt;
//     } else {
//       serverResponse
//         ..statusCode = _response.statusCode
//         ..statusText = _response.statusMessage
//         ..body = _response.data
//         ..responseTime = DateTime.now()
//         ..bodyMap = serverResponse.fillBodyMap(_response.data);
//
//       //to fill errorMessage, if necessary with Message-Type - info
//       serverResponse.isOk(loc);
//     }
//
//
//
//     /// altes GetXProvider
// /*
//     if (_response.hasError) {
//       var respTxt = "${_response.statusText}";
//       if (_response.bodyString != null) {
//         respTxt += " : ${_response.bodyString}";
//       }
//       serverResponse
//         ..statusCode = _response.statusCode
//         ..statusText = _response.statusText
//         ..body = _response.bodyString
//         ..bodyMap = defMap
//         ..errorMessage = respTxt;
//     } else {
//       serverResponse
//         ..statusCode = _response.statusCode
//         ..statusText = _response.statusText
//         ..body = _response.bodyString
//         ..responseTime = DateTime.now()
//         ..bodyMap = serverResponse.fillBodyMap(_response.bodyString);
//
//       //to fill errorMessage, if necessary with Message-Type - info
//       serverResponse.isOk(loc);
//     }
//  */
//
//     Log.d(_TAG, "_sendToServer -> statusCode: ${serverResponse.statusCode}");
//     Log.d(_TAG, "_sendToServer -> statusText: ${serverResponse.statusText}");
//     Log.d(_TAG, "_sendToServer -> response: \n${serverResponse.body}");
//
//     Log.d(_TAG, "---------------------------------------------------------------------");
//
//     Log.spacer();
//     return serverResponse;
  }



  /// /////////////////////////////////////////////////
  /// GETX
  Future<ServerResponse> _sendToServerViaGetX(String url, String body, {bool post = true, String loc = "", StreamController<int>? streamController}) async {
    String _debugBody = body.length > 1024 ? "TRUNCATED for debug (1024) \n${body.substring(0, 1024)}" : body;
    Log.spacer();
    Log.d(_TAG, "---------------------------------------------------------------------");
    Log.d(_TAG, "_sendToServer: url: $url");
    Log.d(_TAG, "_sendToServer: body: \n$_debugBody");
    Log.spacer();

    ServerResponse serverResponse = ServerResponse()
      ..receivingData = true
      ..request = body;

    Map<String, String>? httpHeader = {};
    httpHeader['X-Partcp-Version'] = '1.0';

    if (MainController.to.useShaHash) {
      httpHeader['X-Partcp-Kx-Method'] = '1';
    }

    Response _response;    /// getX

    var t1 = DateTime.now().millisecondsSinceEpoch;
    var t2 = DateTime.now().millisecondsSinceEpoch;


    if (post) {
      if (streamController != null) {
        streamController.sink.add(0);
      }

      int _lastProgress = 0;

      _response = await getXProvider.post(
          url, body, contentType: "test/plain", headers: httpHeader,
          // uploadProgress: (double? progress) {
          //
          //   if (progress != null) {
          //     int progressRound = progress.round();
          //     if (progress > _lastProgress) {
          //       _lastProgress = progressRound;
          //
          //       streamController?.sink.add(progressRound);
          //       if (progressCallback != null) {
          //         progressCallback(progressRound);
          //       }
          //       // print("$sent,  $total, $progress%");
          //     }
          //   }
          // }
      );

    }
    else {
      _response = await getXProvider.get(url);
    }

    var t3 = DateTime.now().millisecondsSinceEpoch;
    Log.d(_TAG, "dt ($loc): upload: ${t2-t1}ms, gesammt: ${t3-t1}ms");


    serverResponse.receivingData = false;
    Map<String, dynamic> defMap = {};


    if (_response.hasError) {
      var respTxt = "${_response.statusText}";
      if (_response.bodyString != null) {
        respTxt += " : ${_response.bodyString}";
      }
      serverResponse
        ..statusCode = _response.statusCode
        ..statusText = _response.statusText
        ..body = _response.bodyString
        ..bodyMap = defMap
        ..errorMessage = respTxt;
    } else {
      serverResponse
        ..statusCode = _response.statusCode
        ..statusText = _response.statusText
        ..body = _response.bodyString
        ..responseTime = DateTime.now()
        ..bodyMap = serverResponse.fillBodyMap(_response.bodyString);

      //to fill errorMessage, if necessary with Message-Type - info
      serverResponse.isOk(loc);
    }


    Log.d(_TAG, "_sendToServer -> statusCode: ${serverResponse.statusCode}");
    Log.d(_TAG, "_sendToServer -> statusText: ${serverResponse.statusText}");
    Log.d(_TAG, "_sendToServer -> response: \n${serverResponse.body}");

    Log.d(_TAG, "---------------------------------------------------------------------");

    Log.spacer();
    return serverResponse;
  }


  /// /////////////////////////////////////////////////
  /// DIO
  Future<ServerResponse> _sendToServerViaDio(String url, String body, {bool post = true, String loc = "", StreamController<int>? streamController, Function(int, d.CancelToken?)? progressCallback}) async {
    String _debugBody = body.length > 1024 ? "TRUNCATED for debug (1024) \n${body.substring(0, 1024)}" : body;
    Log.spacer();
    Log.d(_TAG, "---------------------------------------------------------------------");
    Log.d(_TAG, "_sendToServer: url: $url");
    Log.d(_TAG, "_sendToServer: body: \n$_debugBody");
    Log.spacer();

    ServerResponse serverResponse = ServerResponse()
      ..receivingData = true
      ..request = body;

    Map<String, String>? httpHeader = {};
    httpHeader['X-Partcp-Version'] = '1.0';

    if (MainController.to.useShaHash) {
      httpHeader['X-Partcp-Kx-Method'] = '1';
    }


    final baseOptions = d.BaseOptions(
      // contentType: d.Headers.jsonContentType,
      validateStatus: (int? status) {
        // Return true for all status codes to consider all requests as successful
        // return status != null;
        Log.d(_TAG, "Dio BaseOptions status: $status");
        return true;
        // Alternatively, you can specify a range of status codes to consider as successful
        // return status != null && status >= 200 && status < 300;
      },
      headers: httpHeader
    );

    dioCancelToken = d.CancelToken();
    d.Dio _dio = d.Dio(baseOptions);
    d.Response? _response;     /// Dio

    var t1 = DateTime.now().millisecondsSinceEpoch;
    var t2 = DateTime.now().millisecondsSinceEpoch;


    if (post) {
      if (streamController != null) {
        streamController.sink.add(0);
      }

      int _lastProgress = 0;

      /// DIO
      try {
        _response = await _dio.post(
          url,
          data: body,
          cancelToken: dioCancelToken,

          onSendProgress: (int sent, int total) {
            int progress = (sent / total * 100).round();
            if (progress > _lastProgress) {
              _lastProgress = progress;

              // streamController?.sink.add(progress);
              if (progressCallback != null) {
                progressCallback(progress, dioCancelToken);
              }
              // print("$sent,  $total, $progress%");
            }
          },
        );
      }
      catch (e) {
        ServerResponse _responseCanceled = ServerResponse()..canceled = true;
        return _responseCanceled;
      }

    }
    else {
      _response = await _dio.get(url);
    }


    var t3 = DateTime.now().millisecondsSinceEpoch;
    Log.d(_TAG, "dt ($loc): upload: ${t2-t1}ms, gesammt: ${t3-t1}ms");


    serverResponse.receivingData = false;
    Map<String, dynamic> defMap = {};


    /// DIO

    if (_response != null) {
      if (_response.statusCode != 200) {
        var respTxt = "${_response.statusMessage}";
        if (_response.data != null) {
          respTxt += " : ${_response.data}";
        }
        serverResponse
          ..statusCode = _response.statusCode
          ..statusText = _response.statusMessage
          ..body = _response.data.toString()
          ..bodyMap = defMap
          ..errorMessage = respTxt;
      } else {
        serverResponse
          ..statusCode = _response.statusCode
          ..statusText = _response.statusMessage
          ..body = _response.data
          ..responseTime = DateTime.now()
          ..bodyMap = serverResponse.fillBodyMap(_response.data);

        serverResponse.isOk(loc);
      }
    }

    Log.d(_TAG, "_sendToServer -> statusCode: ${serverResponse.statusCode}");
    Log.d(_TAG, "_sendToServer -> statusText: ${serverResponse.statusText}");
    Log.d(_TAG, "_sendToServer -> response: \n${serverResponse.body}");

    Log.d(_TAG, "---------------------------------------------------------------------");

    Log.spacer();
    return serverResponse;
  }


  /// CREATE SIGNED SERVER REQUEST, SEND IT AND VERIFY RESPONSE
  Future<ServerResponse> serverRequestSigned(Account account, Map<String, dynamic> messageMap, ApiServer server, {bool useGetX = true, StreamController<int>? streamController, Function(int, d.CancelToken?)? progressCallback, String loc = ""}) async {
    messageMap["To"] = server.name;

    final String message = await _createServerRequest(server, account, messageMap);
    final ServerResponse serverResponse = await sendToServer(server.url, message, loc: loc, streamController: streamController, progressCallback: progressCallback, useGetX: useGetX);

    if (serverResponse.isOk("serverRequestSigned")) {
      serverResponse.signatureIsCorrect = await _verifyServerResponse(server, account, serverResponse.bodyMap!);
    }
    return serverResponse;
  }



  /// CREATE UNSIGNED SERVER REQUEST, SEND IT AND VERIFY RESPONSE
  Future<ServerResponse> serverRequest(Map<String, dynamic> messageMap, Account? account, ApiServer server, {bool useGetX = true}) async {
    if (account != null && account.userId != null) {
      messageMap = _addSenderToMessage(server, account, messageMap);
    }
    final message = _yamlConverter.toYaml(messageMap);
    return await sendToServer(server.url, message, useGetX: useGetX);
  }

  String _addSignatureToMessage(String message, String sig) {
    return "$_SIGNATURE: '$sig'" + "\n" + message;
  }

  String _mapToYaml(Map<String, dynamic> map) {
    return _yamlConverter.toYaml(map);
  }

  Future<bool> _verifyServerResponse(ApiServer server, Account account, Map<dynamic, dynamic> responseMap) async {
    final yamlMessage = responseMap[_HTTP_BODY];
    if (responseMap[_HTTP_STATUS] == 200) {
      return await _verifyResponseSignature(server, account, yamlMessage);
    }
    return false;
  }



  Future<ServerResponse> idProviderRequestSigned({required Account account, required Map<String, dynamic> messageMap, required ApiServer idProvider, required ApiServer host, String loc = "", bool useGetX = true}) async {
    messageMap["To"] = host.name;

    final String message = await _createIdProviderRequest(account: account,  messageMap: messageMap, idProvider: idProvider, host: host);
    final ServerResponse serverResponse = await sendToServer(host.url, message, loc: loc, useGetX: useGetX);

    if (serverResponse.isOk("serverRequestSigned")) {
      serverResponse.signatureIsCorrect = await _verifyServerResponse(idProvider, account, serverResponse.bodyMap!);
    }
    return serverResponse;
  }






  /// CREATE (REGULAR) SERVER REQUEST
  Future<String> _createServerRequest(ApiServer server, Account account, Map<String, dynamic> messageMap) async {
    if (account.userId != null) {
      messageMap = _addSenderToMessage(server, account, messageMap);
      messageMap[_PUBLIC_KEY] = await account.publicKeysConcat;
    }

    String messageYaml = _mapToYaml(messageMap).trim();

    if (account.userId != null) {
      var sig = await MyEcCrypt.createSignatureString(messageYaml, account.keyPairEd!);
      return _addSignatureToMessage(messageYaml, sig);
    }
    else {
      return messageYaml;
    }
  }

  Future<String> _createIdProviderRequest({required ApiServer idProvider, required ApiServer host, required Account account, required Map<String, dynamic> messageMap}) async {
    messageMap = _addSenderToMessage(idProvider, account, messageMap);
    messageMap[_PUBLIC_KEY] = await account.publicKeysConcat;

    String messageYaml = _mapToYaml(messageMap).trim();

    if (account.userId != null) {
      var sig = await MyEcCrypt.createSignatureString(messageYaml, account.keyPairEd!);
      return _addSignatureToMessage(messageYaml, sig);
    }
    else {
      return messageYaml;
    }
  }


  Map<String, dynamic> _addSenderToMessage(ApiServer server, Account account, Map<String, dynamic> messageMap) {
    messageMap[_FROM] = account.userId!.contains("@")
      ? account.userId
      : "${account.userId}@${server.name}";
    return messageMap;
  }





  Future<bool> _verifyResponseSignature(ApiServer server, Account account, String yamlMessage) async {

    String yamlConverted = yamlMessage.replaceAll("\r\n", "\n");
    yamlConverted = yamlConverted.replaceAll("\r", "\n");
    yamlConverted = yamlConverted.trim();

    final splitted = yamlConverted.split("\n");
    // Log.d(_TAG, "verifyResponseSignature; splitted: $splitted");

    if (splitted[0].startsWith(_SIGNATURE)) {
      Map<String, dynamic> map = _yamlConverter.toMap(yamlConverted);
      String message = _extractMessageFromResponseBody(yamlConverted, splitted[0]);
      String sigB64 = (map[_SIGNATURE]).trim();
      // List<int> sigSeed = base64Decode(sigB64);

      Log.d(_TAG, "_verifyResponseSignature; sigB64: $sigB64");
      // Log.d(_TAG, "_verifyResponseSignature; sigSeed: $sigSeed");
      // Log.d(_TAG, "_verifyResponseSignature; message: $message");

      bool isCorrect = await MyEcCrypt.verifySignature(message, sigB64, server.publicKeyEd!);
      Log.d(_TAG, "_verifyResponseSignature; isCorrect: $isCorrect");

      return isCorrect;
    }
    return false;
  }

  String _extractMessageFromResponseBody(String yamlMessage, String signatureString) {
    String message = yamlMessage.replaceAll("$signatureString\n", "");
    // Log.d(_TAG, "_extractMessageFromResponseBody; message: $message");
    return message;
  }
}
