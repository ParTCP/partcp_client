import 'package:flutter/material.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';

import '../utils/log.dart';

enum LifeCycleState {
  detached,
  inactive,
  paused,
  resumed,
  close,
  na
}

class LifeCycleController extends SuperController {
  final String _TAG = "LifeCycleController";


  AppLifecycleState? appState;


  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);
    Log.d(_TAG, "didChangeAppLifecycleState: $state");
    appState = state;
  }

  @override
  void onClose() {
    super.onClose();
    // Log.d(_TAG, "onClose");
  }

  @override
  void onDetached() {
    // Log.d(_TAG, "onDetached");
  }

  @override
  void onInactive() {
    // Log.d(_TAG, "onInactive");
  }

  @override
  void onPaused() {
    // Log.d(_TAG, "onPaused");
  }

  @override
  void onResumed() {
    // Log.d(_TAG, "onResumed");
  }

  @override
  void onHidden() {
    // TODO: implement onHidden
  }
}