// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved


import 'dart:async';
import 'dart:io';
import 'package:adaptive_theme/adaptive_theme.dart';
import 'package:flutter/material.dart';
import 'package:universal_html/html.dart' as js;

import 'package:flutter_window_close/flutter_window_close.dart';
import 'package:get/get.dart';
import 'package:hive/hive.dart';
import 'package:package_info_plus/package_info_plus.dart';
import '../listener/uni_link_listener.dart';
import '../styles/styles.dart';
import '/objects/_lot_code.dart';
import '/objects/lot_code_from_deposit.dart';
import '/objects/participant.dart';
import '/objects/storage/app_box.dart';
import '/objects/url_params.dart';
import '/objects/voting_event.dart';
import '/objects/api_server.dart';
import '/objects/wallet.dart';
import '/ui/home/index_controller.dart';
import '/ui/home/index_page.dart';
import '/utils/crypt/my_ec_crypt.dart';
import '/utils/log.dart';

import 'package:intl/intl.dart';
import '/objects/storage/user_box.dart';
import '/utils/path_provider.dart';
import '/utils/register_app.dart';
import 'package:uni_links/uni_links.dart';
import '../constants.dart';
import 'lifecycle_controller.dart';
import 'ws_client/ws_controller.dart';
import 'package:path/path.dart' as path;
import 'package:watcher/watcher.dart';


class MainController extends GetxController {
  final _TAG = "MainController";
  late PackageInfo packageInfo;
  var appVersion = ''.obs;

  /// DEBUG OUTPUT ///
  static const enableLogD = false;

  String unsecureWalletPwd = "";

  double _textScaleFactor = 1.0;
  double _textScaleFactorDefault = 1;

  double get screenScaleFactor {
    return _textScaleFactor;
  }
  void changeScreenScaleFactor(double value) {
    if ((value < 0 && _textScaleFactor < 0.8) || (value > 0 &&  _textScaleFactor > 1.5)) {
      return;
    }
    _textScaleFactor += value;
    appBox!.setValue(AppBoxEnum.textScaleValue, _textScaleFactor);
    _setTextTheme();
  }

  void resetScreenScaleFactor() {
    _textScaleFactor = 1;
    appBox!.setValue(AppBoxEnum.textScaleValue, _textScaleFactorDefault);
    _setTextTheme();
  }

  void _setTextTheme () {
    AdaptiveTheme.of(Get.context!).setTheme(
        light: ThemeData(
            // colorSchemeSeed: AppColors.colorMain,
            textTheme: Styles.textTheme
        ),
        dark: ThemeData()
    );
  }

  /// for better handling of Wallet updates
  /// 1: 2024-09-27
  static const int accountRevision = 1;

  String? appDataPath;
  FullLifeCycleController lifeCycleController =  Get.put(LifeCycleController());
  UniLinkListener? uniLinkListener;


  Future<void> initController() async {
    appVersion.value = 'Version wird geladen...';

    packageInfo = await PackageInfo.fromPlatform();
    appVersion.value = '${packageInfo.version}';

    if (!GetPlatform.isWeb) {
      appDataPath = await PathProvider().getAppDataPath();
      if (appDataPath != null) {
        Directory _dir = Directory(appDataPath!);
        if (!_dir.existsSync()) {
          _dir.createSync(recursive: true);
        }
      }
    }
    Log.d(_TAG, "initController() -> appDataPath: $appDataPath");

    await initBoxen();
    if (!boxError) {
      var isSlow = getSlowMachine();
      isSlowMachine = isSlow ?? GetPlatform.isWeb && GetPlatform.isMobile;
      
      _textScaleFactor = appBox!.getValue(AppBoxEnum.textScaleValue, defValue: _textScaleFactor);

      getExtendedAppMode();

      // await _initWsController();
      await lockFileCreate();

      /// Desktop only
      if (GetPlatform.isDesktop && !GetPlatform.isWeb) {
        FlutterWindowClose.setWindowShouldCloseHandler(() async {
          await lockFileDelete();
          return true;
        });
      }

      /// UniLink
      if (!GetPlatform.isWeb) {
        uniLinkListener = Get.put(UniLinkListener());
        await uniLinkListener!.initListener();
      }

      /// WEB
      if (GetPlatform.isWeb) {
        uniLinkListener = Get.put(UniLinkListener());
        await uniLinkListener!.initListener();

        var uri = Uri.dataFromString(js.window.location.href);

        Map<String, String> params = uri.queryParameters;
        Log.d(_TAG, "_getUrlParams; params: $params");

        if (params.isNotEmpty) {
          String param = params.values.first;
          Log.d(_TAG, "_getUrlParams; param: $param");
          UrlParams? urlParams = UrlParams.fromString(param);
          uniLinkListener!.onChange(urlParams);
        }
      }
    }
  }


  static MainController to = Get.find<MainController>();
  // WsController? wsController;

  /// nur zur Testzwecken, kann später wech
  bool useShaHash = true;

  static const MIN_KEY_PWD_LEN = 0;
  static const USER_BOX_NAME = "userBox";
  static const APP_BOX_NAME = "appBox";
  static const WALLETS_BOX_NAME = "walletsBox";
  static const FILE_DATE_FORMAT = "yy-MM-dd-HHmm";


  String get walletFileName {
    String dateString = DateFormat(FILE_DATE_FORMAT).format(DateTime.now());
    String fileName = Constants.WALLET_FILE_NAME.replaceFirst("{d}", dateString);
    if (wallet != null && wallet!.walletAccount != null) {
      fileName = fileName.replaceFirst("{n}", Constants.fileNameRegExp(wallet!.walletAccount!.name));
    }
    return fileName;
  }


  bool? get pubKeysOnServerValid {
    if (wallet == null) {
      return null;
    }
    if (wallet!.walletAccount == null) {
      return null;
    }
    return wallet!.walletAccount!.pubKeysOnServerValid;
  }


  bool cryptoLibWorks = false;
  bool isFullScreen = false;
  bool isInForeground = false;

  /// query params passed trough the url: https://domain.com?a=1&b=2
  // Map<String, dynamic> urlQueryParams = {};

  // Wallets wallets = Wallets();

  /// wallet with main account and server accounts
  Wallet? wallet;
  Map<String, Wallet> walletMap = {};

  /// vars Storage
  UserBox? userBox;
  AppBox? appBox;
  Box? walletsBox;
  bool boxError = false;


  /// wrong LotCode list
  List<LotCodeFromDeposit>? wrongLotCodesFromDeposit;

  /// for notifications
  bool walletWasChanged = false;
  bool walletNotification = false;

  String? appRev;

  List<String> appStartArgs = [];

  bool extendedAppMode = false;

  bool get isDesktopOrWeb => GetPlatform.isWeb || GetPlatform.isDesktop;

  String get currentWalletTitle => wallet != null
      ? wallet!.walletAccount!.title
      : "";

  String get currentWalletAppBarTitle => wallet != null && extendedAppMode
      ? "(${wallet!.walletAccount!.title})"
      : "";

  Future<String> getAppRev() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    return packageInfo.version;
  }

  // bool get isSlowMachine => GetPlatform.isWeb && GetPlatform.isMobile;
  bool isSlowMachine = false;


  set moderatorTools (bool? val) {
    if (val != null && userBox!= null) {
      userBox!.setValue(UserBoxEnum.moderator, val);
    }
  }

  // Future<void> _initWsController() async {
  //   wsController = await Get.put(WsController(), permanent: true);
  // }


  Future<void> _initUserBox() async {
    Log.d(_TAG, "_initMainBox()");
    if (wallet != null && wallet!.pwdBytes != null) {
      Log.d(_TAG, "_initMainBox(); wallet != null && wallet!.pwdBytes != null => initBox");
      userBox = UserBox();
      await userBox!.initBox(await wallet!.walletAccount!.publicKeyX, await wallet!.pwdForStringsHashed);
    }
  }


  // void setExtendedAppMode (bool isExtended) {
  //   extendedAppMode = isExtended;
  //   userBox!.setValue(UserBoxEnum.extendedAppMode, isExtended);
  // }

  void getExtendedAppMode() {
    extendedAppMode = appBox!.getValue(AppBoxEnum.extendedAppMode, defValue: false);
  }
//Todo Aleks? ist das hier an der richtigen Stelle? NBT 20240520
  String getExtendedAppModeHint(){
    getExtendedAppMode();
    if (!extendedAppMode){
      return "Soll dieses Gerät von mehreren Personen genutzt werden, "
          "navigiere zurück zur Startseite und aktiviere im "
          "Funktionsmenü oben rechts (≡) den Mehr-Benutzer-Modus. "
          "Du kannst dann ein neues Benutzerprofil erstellen und unter "
          "diesem Profil einen weiteren Teilnahmecode einlösen.";
    }
    return "Bitte lege ein neues Benutzerprofil an oder wechsle zu einem "
        "anderen bestehenden Benutzerprofil, um diesen "
        "Teilnahmecode einzulösen.";
  }

  Future<void> setExtendedAppMode(bool val) async {
    extendedAppMode = val;
    appBox!.setValue(AppBoxEnum.extendedAppMode, val);
  }

  dynamic getSlowMachine() {
    return appBox!.getValue(AppBoxEnum.slowMachine, defValue: null);
  }

  Future<void> setSlowMachine(bool val) async {
    isSlowMachine = val;
    appBox!.setValue(AppBoxEnum.slowMachine, val);
  }


  // Future<void> _initWrongLotCodes() async {
  //   String? wrongLotCodes = box!.getValue(MyStorageEnum.wrongLotCodes);
  //   wrongLotCodesFromDeposit = [];
  //   if (wrongLotCodes != null) {
  //     try{
  //       List<dynamic> list = jsonDecode(wrongLotCodes);
  //       for (var jsonData in list) {
  //         LotCodeFromDeposit lc = LotCodeFromDeposit.fromJson(jsonData);
  //         wrongLotCodesFromDeposit!.add(lc);
  //       }
  //     }
  //     catch(e) {
  //       wrongLotCodesFromDeposit = [];
  //       await box!.setValue(MyStorageEnum.wrongLotCodes, jsonEncode(wrongLotCodesFromDeposit));
  //     }
  //   }
  // }


  // Future<void> updateWrongLotCodes() async {
  //   await box!.setValue(MyStorageEnum.wrongLotCodes, jsonEncode(wrongLotCodesFromDeposit));
  // }
  //
  // List<LotCodeFromDeposit> wrongLotCodeForEvent(VotingServer server, VotingEvent event) {
  //   List<LotCodeFromDeposit>? test = [];
  //   for (LotCodeFromDeposit lc in wrongLotCodesFromDeposit!) {
  //     if ((lc.server != null && lc.server == server.name) && (lc.eventId != null && lc.eventId == event.id)) {
  //       test.add(lc);
  //     }
  //   }
  //   return test;
  // }



  // Future<Box<LotCode>>getEventBoxLotCodes(VotingEvent event) async {
  //   if (!Hive.isAdapterRegistered(1)) {
  //     Hive.registerAdapter(LotCodeAdapter());
  //   }
  //   String spKey = event.eventBoxLotCodes;
  //   List<int> boxPwd = await wallet!.pwdForStrings;
  //   String boxId = MyEcCrypt.hashFromPublicKeyX(await wallet!.walletAccount!.publicKeyX);
  //   String boxName = "$spKey-$boxId";
  //   Log.d(_TAG, "getEventBoxLotCodes; boxName: $boxName");
  //   return await Hive.openBox<LotCode>(boxName, encryptionCipher: HiveAesCipher(boxPwd), path: appDataPath);
  // }

  // Future<Box<Participant>>getEventBoxParticipants(VotingEvent event) async {
  //   if (!Hive.isAdapterRegistered(2)) {
  //     Hive.registerAdapter(ParticipantAdapter());
  //   }
  //   String spKey = event.eventBoxParticipants;
  //   List<int> boxPwd = await wallet!.pwdForStrings;
  //   String boxId = MyEcCrypt.hashFromPublicKeyX(await wallet!.walletAccount!.publicKeyX);
  //   String boxName = "$spKey-$boxId";
  //   Log.d(_TAG, "getEventBoxParticipants; boxName: $boxName");
  //   return await Hive.openBox(boxName, encryptionCipher: HiveAesCipher(boxPwd), path: appDataPath);
  // }


  Future<ApiServer?> getLastServer() async {
    String? lastUrl = userBox!.getValue(UserBoxEnum.userLastServer);
    if (lastUrl != null) {
      for(ApiServer server in wallet!.serverList) {
        if (server.url == lastUrl) {
          return server;
        }
      }
    }
    return null;
  }



  /////////// WALLET ///////////////

  Future<void> onWalletDecrypted(Wallet wallet) async {
    this.wallet = wallet;
    this.wallet!.walletAccount!.dateU = DateTime.now();

    await _saveWalletInBox();
    await appBox!.setValue(AppBoxEnum.lastWallet, await this.wallet!.walletAccount!.walledId);
    await _initUserBox();
    // extendedAppMode = userBox!.getValue(UserBoxEnum.extendedAppMode, ifNotExists: false);
    // wsController?.initWs();
  }

  bool walletIsDecrypted () {
    bool _keysLoaded = wallet != null && wallet!.walletAccount != null && wallet!.walletAccount!.keyPairX != null && wallet!.walletAccount!.keyPairEd != null;
    // Log.d(_TAG, "keysLoaded: $_keysLoaded");
    return _keysLoaded;
  }


  bool encryptedWalletExists () {
    bool _exists = wallet != null && wallet!.walletAccount != null && wallet!.walletAccount!.keyPairX == null;
    return _exists;
  }


  List<Wallet> get walletsInBox {
    var yamlWallets = walletsBox!.values;
    List<Wallet> wallets = [];
    for (String yamlWallet in yamlWallets) {
      Wallet? wallet = Wallet.fromYaml(yamlWallet);
      if (wallet != null) {
        wallets.add(wallet);
      }
    }
    wallets.sort((a, b) => b.walletAccount!.dateU!.compareTo(a.walletAccount!.dateU!));
    return wallets;
  }


  void onWalletDownloaded() {
    walletWasChanged = false;
    walletNotification = false;
  }


  Future<void> onWalletChanged({bool updateNotification = true}) async {
    wallet!.walletAccount!.dateU = DateTime.now();
    if (updateNotification) {
      walletNotification = true;
    }
    walletWasChanged = true;
    await _saveWalletInBox();
  }


  Future<void> initBoxen() async {
    if (walletsBox == null) {
      try {
        walletsBox = await Hive.openBox(WALLETS_BOX_NAME, path: appDataPath);
      }
      catch (e) {
        boxError = true;
        Log.w(_TAG, "Cannot open $WALLETS_BOX_NAME in ${MainController.to.appDataPath}");
        return;
      }
      Log.d(_TAG, "initBoxen(); walletsBox.isOpen: ${walletsBox!.isOpen}; walletsBox.path: ${walletsBox!.path}");
    }

    if (appBox == null) {
      appBox = AppBox();
      await appBox!.initBox(APP_BOX_NAME);
    }
  }

  Future<bool> loadWallet({String? walletId}) async {
    /// load last used
    walletId ??= appBox!.getValue(AppBoxEnum.lastWallet);

    if (walletId != null) {
      String? yamlWallet = getYamlWallet(walletId: walletId);
      if (yamlWallet != null) {
        wallet = Wallet.fromYaml(yamlWallet);
        if (wallet != null) {
          return true;
        }
      }
    }
    return false;
  }


  String? getYamlWallet({String? walletId}) {
    var wallets = walletsBox!.toMap();
    // Log.d(_TAG, "getYamlWallet: $wallets");
    String? yamlWallet = walletsBox!.get(walletId);
    return yamlWallet;
  }


  Future<bool> _saveWalletInBox() async {
    Log.d(_TAG, "_saveWalletInBox()");
    String yamlWallet = await wallet!.toYaml();
    String walledId = await wallet!.walletAccount!.walledId;
    await walletsBox!.put(walledId, yamlWallet);
    await walletsBox!.flush();
    await walletsBox!.compact();
    Log.d(_TAG, "_saveWalletInBox; yamlWallet: $yamlWallet");
    return true;
  }


  Future<void> deleteWallet({String? walletId}) async {
    if (walletId == null && wallet != null) {
      walletId = await wallet!.walletAccount!.walledId;
      wallet = null;
    }
    if (walletId != null && walletsBox != null) {
      await walletsBox!.delete(walletId);
      await walletsBox!.flush();
      await walletsBox!.compact();
    }
  }


  Future<void> lockWallet() async {
    wallet = null;
    // await wsController?.closeWs();
  }


  Future<void> lockFileDelete() async {
    if (GetPlatform.isDesktop && !GetPlatform.isWeb) {
      File file = File(path.join(appDataPath!, Constants.APP_LOCK_FILE_NAME));
      await file.delete();
    }
  }
  Future<void> lockFileCreate() async {
    if (GetPlatform.isDesktop && !GetPlatform.isWeb) {
      File file = File(path.join(appDataPath!, Constants.APP_LOCK_FILE_NAME));
      await file.writeAsString("");
    }
  }

}
