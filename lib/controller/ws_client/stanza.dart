import 'dart:convert';
import 'package:enum_to_string/enum_to_string.dart';
import '/utils/log.dart';

const _TAG = "Stanza";


enum StanzaType {
  ack,            // IN, OUT
  auth,           // IN, OUT
  authenticated,  // IN
  info,           // IN, OUT
  msg,            // IN, OUT
  welcome,        // IN, OUT
}

class Stanza {
  late final StanzaType type;
  final String? from;
  final String? to;
  late final String stanzaId;
  final Map<String, dynamic>? data;

  Stanza({
    required this.type,
    this.from,
    this.to,
    required this.stanzaId,
    this.data
  });

  static Stanza? fromRaw(String rawStanza) {
    Map<String, dynamic> map = jsonDecode(rawStanza);

    try {
      Stanza stanza = Stanza(
          type: EnumToString.fromString(StanzaType.values, map["t"])!,
          from: map.containsKey("from") ? map["from"] : null,
          to: map.containsKey("to") ? map["to"] : null,
          stanzaId: map["s"],
          data: map.containsKey("d") ? map["d"] : null
      );
      return stanza;
    }
    catch(e, s) {
      Log.e(_TAG, "OutStanza.fromRaw: $e; $s");
    }
    return null;
  }


  @override
  String toString() {

    final Map<String, dynamic> map = {
      "t": EnumToString.convertToString(type),
      "s": stanzaId
    };

    if (from != null) {
      map["from"] = from;
    }

    if (to != null) {
      map["to"] = to;
    }

    if (data != null) {
      map["d"] = data;
    }

    String res = jsonEncode(map);
    Log.d(_TAG, res);
    return res;
  }
}
