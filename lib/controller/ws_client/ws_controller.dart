// import 'dart:async';
// import 'dart:io';
//
// import 'package:get/get.dart';
// import '/controller/main_controller.dart';
// import '/objects/_ws_server.dart';
// import '/utils/log.dart';
// import 'package:web_socket_channel/io.dart';
// import 'package:web_socket_channel/web_socket_channel.dart';
// import 'package:web_socket_channel/status.dart' as status;
//
// import 'stanza.dart';
// import 'stanza_listener.dart';
//
// enum WsState { CONNECTED, AUTHENTICATED, CONNECTING, DISCONNECTING, DISCONNECTED, WAITING_AFTER_ERROR, ERROR }
//
// class WsController extends GetxController {
//   final _TAG = "WsController";
//
//   MainController mainController = MainController.to;
//   dynamic wsClient;
//   late WsServer wsServer;
//   WsState wsState = WsState.DISCONNECTED;
//
//
//   // dynamic get wsClient => GetPlatform == GetPlatform.isWeb
//   //   ? ""
//   //   : "!";
//
//
//   bool get accountExistsAndValid =>
//       mainController.wallet != null &&
//       mainController.wallet!.walletAccount != null &&
//       mainController.pubKeysOnServerValid != null &&
//       mainController.pubKeysOnServerValid!;
//
//
//   int? get wsCloseCode => wsClient?.closeCode;
//   String? get wsCloseReason => wsClient?.closeReason;
//
//   Future<void> initWs() async {
//     // Log.d(_TAG, "initWs; wsState: $wsState");
//     //
//     // Uri url = Uri.parse('ws://192.168.1.236:1026');
//     //
//     // if (wsState == WsState.DISCONNECTED && accountExistsAndValid) {
//     //   Log.d(_TAG, "initWs => connect...");
//     //
//     //   wsState = WsState.CONNECTING;
//     //   wsClient = GetPlatform == GetPlatform.isWeb
//     //       ? WebSocketChannel.connect(url)
//     //       : IOWebSocketChannel.connect(url);
//     //
//     //   wsState = WsState.CONNECTED;
//     //   Log.d(_TAG, "initWs => connected");
//     //
//     //   wsClient?.stream.listen(_onData, onError: (error) => _onError(error), onDone: () => _onDone(), cancelOnError: false);
//     // }
//   }
//
//   Future<void> _handleLostConnection() async {
//     Log.i(_TAG, "_handleLostConnection: wsCloseCode: $wsCloseCode, wsCloseReason: $wsCloseReason");
//
//     if (accountExistsAndValid) {
//       bool serverReachable = await checkWsServer();
//
//       if (serverReachable) {
//         Future.delayed(const Duration(seconds: 3)).then((value) {
//           initWs();
//         });
//       }
//     }
//   }
//
//   void _onData(dynamic rawStanza) {
//     Log.d(_TAG, "_onData: $rawStanza");
//
//     Stanza? inStanza = Stanza.fromRaw(rawStanza);
//
//     if (inStanza == null) {
//       return;
//     }
//
//     final type = inStanza.type;
//
//     switch (type) {
//
//       /// welcome message
//       case StanzaType.welcome:
//         sendWelcomeResponse(inStanza);
//         break;
//
//       case StanzaType.authenticated:
//         wsState = WsState.AUTHENTICATED;
//         break;
//
//       case StanzaType.ack:
//         StanzaListener().onStanza(inStanza);
//         break;
//
//       case StanzaType.auth:
//         // ignore, Server only
//         break;
//       case StanzaType.info:
//         //
//         break;
//       case StanzaType.msg:
//       //
//         break;
//     }
//   }
//
//   Future<void> _onError(var error) async {
//     Log.d(_TAG, "_onError: $error");
//
//     _handleLostConnection();
//   }
//
//   Future<void> _onDone() async {
//     Log.d(_TAG, "_onDone: disconnected");
//     wsState = WsState.DISCONNECTED;
//     _handleLostConnection();
//   }
//
//   Future<void> sendWelcomeResponse(Stanza inStanza) async {
//     var bla = inStanza.from;
//
//     wsServer = WsServer(publicKeyConcatB64: inStanza.from);
//
//     Stanza outStanza = await wsServer.createWelcomeResponse(account: MainController.to.wallet!.walletAccount!, inStanza: inStanza);
//     Log.d(_TAG, "sendWelcomeResponse: $outStanza");
//
//     await send(outStanza);
//   }
//
//   Future<void> send(Stanza stanza) async {
//     wsClient?.sink.add("$stanza");
//   }
//
//   Future<void> closeWs() async {
//     Log.d(_TAG, "closeWs => disconnecting...");
//     wsState = WsState.DISCONNECTING;
//     await wsClient?.sink.close(status.goingAway);
//     wsState = WsState.DISCONNECTED;
//     Log.d(_TAG, "closeWs => disconnected");
//   }
//
//   Future<bool> checkWsServer() async {
//     bool success = false;
//     try {
//       final result = await InternetAddress.lookup('partcp.org');
//       if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
//         success = true;
//       }
//     } on SocketException catch (_) {
//       // print('not connected');
//     }
//
//     Log.d(_TAG, "checkWsServer: $success");
//     return success;
//   }
// }
