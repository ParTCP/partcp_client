import 'dart:async';
import 'dart:collection';
import 'dart:io';

import 'package:get/get.dart';
import 'package:uni_links/uni_links.dart';
import 'package:watcher/watcher.dart';
import 'package:path/path.dart' as path;

import '../constants.dart';
import '../objects/url_params.dart';
import '../utils/log.dart';
import '../utils/path_provider.dart';


const _TAG = "DesktopUniLinkWatcher";

class UniLinkListener extends GetxController {

  static UniLinkListener to = Get.find<UniLinkListener>();

  UrlParams? urlParams = UrlParams();
  Map<dynamic, Function(UrlParams)> _map = new HashMap();

  
  /// On desktop uniLinks are saved in a file by the PartcpRunner,
  /// [FileWatcher] acts as observer
  /// 
  /// On Mobile we use the [uriLinkStream] Listener
  
  Future<void> initListener() async {
    /// Desktop
    if (GetPlatform.isDesktop && !GetPlatform.isWeb) {
      String uniLinkPath = await _uniLinkWatcherFilePath();
      var watcher = FileWatcher(uniLinkPath);
      watcher.events.listen(_onDesktopUniLink);
    }
    /// Mobile
    else if (!GetPlatform.isWeb){
      StreamSubscription sub = uriLinkStream.listen((Uri? uri) async {
        Log.d(_TAG, "handleIncomingLinks: got uri: $uri");
        if (uri != null) {
          UrlParams? uniLink = UrlParams.fromString(uri.toString());
          onChange(uniLink);
        }
      }, onError: (Object err) {
        Log.e(_TAG, "handleIncomingLinks err: $err");
      });
    }
    await _onStartup();
  }

  Future<void> _onStartup() async {
    /// Desktop
    if (GetPlatform.isDesktop && !GetPlatform.isWeb) {
      handleDesktopUniLink();
    }
    /// Mobile
    else if (!GetPlatform.isWeb) {
      Uri? uri = await getInitialUri();
      if (uri != null) {
        String params = uri.toString();
        Log.d(_TAG, "_onStartup params: $params");
        UrlParams? uniLink = UrlParams.fromString(params);
        onChange(uniLink);
      }
    }
  }

  UrlParams? listenerAdd({required dynamic id, required Function(UrlParams) callback}) {
    /// add listener
    _map[id] = callback;
    /// return last saved
    return urlParams;
  }

  void listenerRemove({required dynamic id}) {
    _map.remove(id);
  }

  void onChange(UrlParams? uniLink) {
    this.urlParams = uniLink;
    if (uniLink != null) {
      _map.forEach((id, callback) {
        callback(uniLink);
      });
    }
  }

  void _onDesktopUniLink(Object? object) {
    handleDesktopUniLink();
  }

  Future<void>handleDesktopUniLink() async {
    if (GetPlatform.isDesktop && !GetPlatform.isWeb) {
      String uniLinkPath = await _uniLinkWatcherFilePath();
      File file = File(uniLinkPath);
      if (!file.existsSync()) {
        file.writeAsStringSync("");
      }
      String contents = await file.readAsString();
      if (contents.isNotEmpty) {
        await file.writeAsString("");
        UrlParams? uniLink = UrlParams.fromString(contents);
        onChange(uniLink);
      }
    }
  }


  Future<String> _uniLinkWatcherFilePath() async {
    String? appDataPath = await PathProvider().getAppDataPath();
    String uniLinkPath =  path.join(appDataPath!, Constants.CLIENT_UNI_LINK_FILE);
    Log.d(_TAG, "uniLinkWatcherFilePath: $uniLinkPath");
    return uniLinkPath;
  }

}