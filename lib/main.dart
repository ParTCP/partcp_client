// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:adaptive_theme/adaptive_theme.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:partcp_client/styles/styles.dart';
import 'package:window_manager/window_manager.dart';
import '/controller/main_controller.dart';
import '/ui/home/index_page.dart';

// import 'package:fastor_app_ui_widget/fastor_app_ui_widget.dart' as fastor;

import 'package:flutter_localizations/flutter_localizations.dart';
import 'ui/home/index_error_page.dart';
import 'utils/log.dart';

const _TAG = "main";
bool isInBackground = false;

/// background test
/// Be sure to annotate your callback function to avoid issues in release mode on Flutter >= 3.3.0
@pragma('vm:entry-point')
void _backgroundTest() {}

void main(List<String> args) async {
  WidgetsFlutterBinding.ensureInitialized();

  if (GetPlatform.isDesktop && !GetPlatform.isWeb) {
    await windowManager.ensureInitialized();
    await WindowManager.instance.ensureInitialized();
    /// Desktop Window-Title are in
    ///   windows/runner/main.cpp
    ///   linux/my_application.cc
    // windowManager.waitUntilReadyToShow().then((_) async {
    //   await windowManager.setTitle('ParTCP Vote');
    // });
  }

  // await fastor.Fastor.initializeApp( );

  Log.d(_TAG, "main");

  await Hive.initFlutter();
  Get.put(MainController(), permanent: true);
  await MainController.to.initController();

  if (args.isNotEmpty) {
    MainController.to.appStartArgs = args;
  }

  runApp(MyApp());

  /*
  if (GetPlatform.isAndroid) {
    final int helloAlarmID = 0;
    await AndroidAlarmManager.periodic(
      const Duration(minutes: 6),
      helloAlarmID,
      _backgroundTest,
      wakeup: true,
      rescheduleOnReboot: true
    );
  }
  */
}

class MyApp extends StatelessWidget {

  // Map<int, Color> color = {
  //   50: Color.fromRGBO(255, 92, 87, .1),
  //   100: Color.fromRGBO(255, 92, 87, .2),
  //   200: Color.fromRGBO(255, 92, 87, .3),
  //   300: Color.fromRGBO(255, 92, 87, .4),
  //   400: Color.fromRGBO(255, 92, 87, .5),
  //   500: Color.fromRGBO(255, 92, 87, .6),
  //   600: Color.fromRGBO(255, 92, 87, .7),
  //   700: Color.fromRGBO(255, 92, 87, .8),
  //   800: Color.fromRGBO(255, 92, 87, .9),
  //   900: Color.fromRGBO(255, 92, 87, 1),
  // };

  Map<int, Color> color = {
    50: Color.fromRGBO(97, 97, 97, .1),
    100: Color.fromRGBO(97, 97, 97, .2),
    200: Color.fromRGBO(97, 97, 97, .3),
    300: Color.fromRGBO(97, 97, 97, .4),
    400: Color.fromRGBO(97, 97, 97, .5),
    500: Color.fromRGBO(97, 97, 97, .6),
    600: Color.fromRGBO(97, 97, 97, .7),
    700: Color.fromRGBO(97, 97, 97, .8),
    800: Color.fromRGBO(97, 97, 97, .9),
    900: Color.fromRGBO(97, 97, 97, 1),
  };



  @override
  Widget build(BuildContext context) {
    final scrollbarTheme = ScrollbarThemeData(
      thumbVisibility: MaterialStateProperty.all(true),
    );

    MaterialColor colorCustom = MaterialColor(0xff1976D2, color);

    return AdaptiveTheme(

      light: ThemeData(
        // fontFamily: 'BunueloCleanPro',
        // colorSchemeSeed: AppColors.colorMain,
        useMaterial3: true,
        textTheme: Styles.textTheme,
      ),

      dark: ThemeData.dark(useMaterial3: true),

      initial:  AdaptiveThemeMode.light,


      builder: (ThemeData light, ThemeData dark) {
        return GetMaterialApp(
          initialRoute: '/',
          onGenerateRoute: (settings) {
            return MaterialPageRoute(
              builder: (context) => IndexPage(),
            );
          },

          theme: ThemeData.light(useMaterial3: true).copyWith(
            // visualDensity: VisualDensity.adaptivePlatformDensity,

            scrollbarTheme: scrollbarTheme,

            scaffoldBackgroundColor: const Color.fromRGBO(249, 249, 249, 1),

            colorScheme: ColorScheme.light().copyWith(
              primary: Colors.blue,
              secondary: Colors.blueAccent,
              surface: const Color.fromRGBO(255, 255, 255, 1),
              surfaceTint: const Color.fromRGBO(150, 150, 150, 1),
            ),

            textTheme: Styles.textTheme,

            // colorScheme: ColorScheme.fromSwatch(
            //   // primarySwatch: colorCustom,
            //   primarySwatch: colorCustom,
            //   // backgroundColor: const Color.fromRGBO(0, 255, 0, 1),
            //   cardColor: const Color.fromRGBO(255, 255, 255, 1),
            // ),


            appBarTheme: const AppBarTheme(
              backgroundColor: Color.fromARGB(10, 140, 240, 240),
              // foregroundColor: Colors.white,
              toolbarTextStyle: TextStyle(color: Colors.black87, fontSize: 20),
              actionsIconTheme: IconThemeData(color: Colors.black54),
              iconTheme: IconThemeData(color: Colors.black54),
            ),

          ),

          // darkTheme: ThemeData.dark().copyWith(scrollbarTheme: scrollbarTheme),

          defaultTransition: GetPlatform.isMobile && !GetPlatform.isWeb
              ? Transition.rightToLeftWithFade
              : Transition.noTransition,

          transitionDuration: GetPlatform.isMobile && !GetPlatform.isWeb
              ? const Duration(milliseconds: 200)
              : const Duration(milliseconds: 0),

          localizationsDelegates: GlobalMaterialLocalizations.delegates,
          supportedLocales: [
            const Locale('de', 'DE'),
            const Locale('en', 'US'),
          ],
          // locale: Locale('de', 'DE'),

          locale: Get.deviceLocale,
          fallbackLocale: Locale('de', 'DE'),

          title: 'Partcp Vote',
          // home: const MyApp(),
          home: MainController.to.boxError ? IndexErrorPage() : IndexPage(),
        );
      },

    );
  }

/*
    return AdaptiveTheme(
      light: ThemeData(
        fontFamily: 'Roboto',
        brightness: Brightness.light,

        // primarySwatch: Colors.indigo,
        primaryColor: Colors.indigo,


        /*
        primarySwatch: MaterialColor(
          0xff0126bf,
          <int, Color>{
            50: Color(0xff0126bf),
            100: Color(0xff0126bf),
            200: Color(0xff0126bf),
            300: Color(0xff0126bf),
            400: Color(0xff0126bf),
            500: Color(0xff0126bf),
            600: Color(0xff0126bf),
            700: Color(0xff0126bf),
            800: Color(0xff0126bf),
            900: Color(0xff0126bf)
          },
        ),
        */
        // accentColor: Colors.deepPurple,

        // appBarTheme: AppBarTheme(
        //   backgroundColor: Colors.white,
        //   foregroundColor: Colors.black87,
        //   toolbarTextStyle: TextStyle(
        //     color: Colors.black87,
        //     fontSize: 20
        //   ),
        //   actionsIconTheme: IconThemeData(color: Colors.black54),
        //   iconTheme: IconThemeData(color: Colors.black54),
        // ),

        // textTheme: TextTheme(
        //   bodyText1: TextStyle(fontSize: 18.0),
        //   bodyText2: TextStyle(fontSize: 16.0, color: Colors.black54),
        //   button: TextStyle(fontSize: 15.0),
        // ),

      ),
      dark: ThemeData(
        fontFamily: 'Roboto',
        brightness: Brightness.dark,
        primarySwatch: Colors.red,
        // accentColor: Colors.amber,
      ),
      initial: AdaptiveThemeMode.light,

      builder: (theme, darkTheme) => GetMaterialApp(

        builder: (context, child) =>
          MediaQuery(data: MediaQuery.of(context).copyWith(alwaysUse24HourFormat: true), child: child!,),

        theme: theme,
        darkTheme: darkTheme,

        defaultTransition: GetPlatform.isMobile ? Transition.rightToLeftWithFade : Transition.noTransition ,

        initialRoute: '/',
        onGenerateRoute: (settings) {
          return MaterialPageRoute(
            builder: (context) => IndexPage(),
          );
        },

        title: 'ParTCP Vote',
        enableLog: true,

        localizationsDelegates: GlobalMaterialLocalizations.delegates,

        supportedLocales: [
          const Locale('en', 'US'),
          const Locale('de', 'DE'),
        ],
        // locale: Locale('de', 'DE'),
        locale: Get.deviceLocale,

        home: IndexPage(),
      ),
    );
  }
 */
}

class ConstantScrollBehavior extends ScrollBehavior {
  const ConstantScrollBehavior();

  @override
  Widget buildScrollbar(BuildContext context, Widget child, ScrollableDetails details) => child;

  @override
  Widget buildOverscrollIndicator(BuildContext context, Widget child, ScrollableDetails details) => child;

  // @override
  // TargetPlatform getPlatform(BuildContext context) => TargetPlatform.macOS;

  // @override
  // ScrollPhysics getScrollPhysics(BuildContext context) =>
  //     const BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics());
}
