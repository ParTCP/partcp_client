// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import '/objects/_client_data.dart';
import '/objects/group.dart';
import '/objects/voting_event.dart';
import '/utils/s_utils.dart';
import '/utils/s_util__date.dart';

//
// Map<String, dynamic> eventDefinition(Group group, VotingEvent event) {
//
//   Map<String, dynamic> namingRules = {
//     "prefix": event.credentialRules.prefix,
//     "counter_width": event.credentialRules.counterWidth,
//     "group_length": event.credentialRules.groupLength,
//     "group_separator": event.credentialRules.groupSeparator,
//     "crc_length": event.credentialRules.crcLength,
//   };
//
//   Map<String, dynamic> lotCodeRules = {
//     "char_list": event.lotCodeRules.charList,
//     "final_length": event.lotCodeRules.finalLength,
//     "crc_length": event.lotCodeRules.crcLength,
//     "group_length": event.lotCodeRules.groupLength,
//     "group_separator": event.lotCodeRules.groupSeparator,
//   };
//
//   Map<String, dynamic> credentialRules = {
//     "char_list": event.namingRules.charList,
//     "final_length": event.namingRules.finalLength,
//     "crc_length": event.namingRules.crcLength,
//     "group_length": event.namingRules.groupLength,
//     "group_separator": event.namingRules.groupSeparator,
//   };
//
//
//   Map<String, dynamic> eventData = {};
//   eventData["name"] = event.name;
//   eventData["date"] = SUtils.dateToYaml(event.date);
//   eventData["estimated_turnout"] = event.estimatedTurnout;
//   eventData["naming_rules"] = namingRules;
//
//   // var eventClientDataMap = event.clientData!.toMap();
//   /*
//   if (eventClientDataMap.isNotEmpty) {
//     eventData[ClientData.CLIENT_DATA]=  eventClientDataMap;
//   }
//   */
//   String eventClientDataString =  event.clientData!.toYaml();
//   eventData[ClientData.CLIENT_DATA]=  eventClientDataString;
//
//
//   if (event.paperLots) {
//     eventData["credential_rules"] = credentialRules;
//   }
//   if (event.lotCodes) {
//     eventData["lot_code_rules"] = lotCodeRules;
//   };
//
//   Map<String, dynamic> map = {
//     "Message-Type": "event-definition",
//     "Group-Id": group.id,
//     "Event-Data": eventData,
//     "Date": SUtilDate.convertToMessageDate(DateTime.now())
//   };
//   return map;
// }