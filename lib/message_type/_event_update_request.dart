// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import '/message_type/_event_definition.dart';
import '/objects/group.dart';
import '/objects/voting_event.dart';
import '/utils/s_util__date.dart';

// Map<String, dynamic> eventUpdateRequest(Group group, VotingEvent event) {
//
//   Map<String, dynamic> map = eventDefinition(group, event);
//   map["Message-Type"] = "event-update-request";
//   map["Event-Id"] = event.id;
//   map["Date"] = SUtilDate.convertToMessageDate(DateTime.now());
//
//   if (map.containsKey("Group-Id")) {
//     map.remove("Group-Id");
//   }
//   return map;
// }