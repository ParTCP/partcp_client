// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

// import '/objects/_client_data.dart';
// import '/objects/voting_event.dart';
// import '/utils/s_util__date.dart';
//
// Map<String, dynamic> eventUpdateRequestVotingSortOrder(VotingEvent event) {
//
//   Map<String, dynamic> eventData = {};
//   Map<String, dynamic> eventClientDataMap = {
//     ClientData.VOTING_SORT_ORDER: event.clientData!.votingSortOrder.toList(growable: false)
//   };
//
//   if (eventClientDataMap.isNotEmpty) {
//     eventData[ClientData.CLIENT_DATA] =  eventClientDataMap;
//   }
//
//   Map<String, dynamic> map = {
//     "Message-Type": "event-update-request",
//     "Event-Id": event.id,
//     "Event-Data": eventData,
//     "Date": SUtilDate.convertToMessageDate(DateTime.now())
//   };
//   return map;
// }