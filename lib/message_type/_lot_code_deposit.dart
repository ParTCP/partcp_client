// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

/*
  https://demo01.partcp.org/modules/events/doc/lot-code-deposit.txt
 */

// import '/utils/s_util__date.dart';
//
// Map<String, dynamic> magTypeLotCodeDeposit({
//   required participantId,
//   required String lotCodeEnc,
//   required eventId,
// }) {
//   Map<String, dynamic> map = {
//     "Message-Type": "lot-code-deposit",
//     "Participant-Id": participantId,
//     "Event-Id": eventId,
//     "Lot-Code~": lotCodeEnc,
//     "Date": SUtilDate.convertToMessageDate(DateTime.now())
//   };
//   return map;
// }