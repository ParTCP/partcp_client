// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import '/objects/_lot_code.dart';
import '/objects/voting_event.dart';
import '/utils/s_util__date.dart';


// Map<String, dynamic> lotInvalidation({required List<LotCode> lotCodes, required VotingEvent event}) {
//   Map<String, dynamic> map = {};
//   map["Message-Type"] = "lot-invalidation";
//   map["Lot-Codes"] = lotCodes.cast();
//   map["Event-Id"] = event.id;
//   map["Date"] = SUtilDate.convertToMessageDate(DateTime.now());
//   return map;
// }

