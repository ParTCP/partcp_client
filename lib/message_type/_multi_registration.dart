// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

// import '/objects/voting_event.dart';
// import '/utils/s_util__date.dart';
//
// Map<String, dynamic> multiRegistration({
//   required int count,
//   required VotingEvent event,
// }) {
//
//   Map<String, dynamic> map = {
//     "Message-Type": "multi-registration",
//     "Event-Id": event.id,
//     "Count": count,
//     "Date": SUtilDate.convertToMessageDate(DateTime.now())
//   };
//   return map;
// }