// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

// import '/utils/s_util__date.dart';
//
// /*#
//   https://demo01.partcp.org/modules/local_id/doc/registration.txt
//  */
//
// Map<String, dynamic> registration({
//   required String participantId,
//   required String credential,
// }) {
//
//   Map<String, dynamic> map = {
//     "Message-Type": "registration",
//     "Participant-Id": participantId,
//     "Credential": credential,
//     "Date": SUtilDate.convertToMessageDate(DateTime.now())
//   };
//   return map;
// }