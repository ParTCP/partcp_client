// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

// import '/objects/voting.dart';
// import '/objects/voting_event.dart';
// import '/utils/s_util__date.dart';
//
// Map<String, dynamic> votingStartDeclaration({
//   required VotingEvent event,
//   required Voting voting
// }) {
//   Map<String, dynamic> map = {};
//   map["Message-Type"] = "voting-start-declaration";
//   map["Date"] = SUtilDate.convertToMessageDate(DateTime.now());
//   map["Event-Id"] = event.id;
//   map["Voting-Id"] = voting.id;
//   return map;
// }