// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved


import '../objects/voting_option.dart';
import '../utils/s_util__date.dart';
import '/objects/voting.dart';
import '/objects/voting_event.dart';

/*

Name: ballot
Type: request
Purpose: >
  A ballot containing votes for a particular voting
Response: receipt
Inherits: message
Elements:
  Signature: { required: true }
  From: { required: true }
  Date: null
  Event-Id: { required: true, type: identifier, class: event }
  Voting-Id: { required: true, type: identifier, class: voting }
  Votes: { required: true, type: list, max_count: 100, item_type: object, item_class: vote }
  Comment: { type: string, max_length: 1024 }
Rejection-Codes:
  31: Sender is not entitled to participate in this voting
  41: Unknown voting
  42: Voting has not been started yet
  43: Voting has already been closed
  95: Ballot contains invalid option
  96: Mandatory option is missing in ballot
  97: Ballot contains invalid comment
  98: Comment is too long
  99: Mandatory comment is missing

Das "Comment"-Element wird benutzt, wenn EIN Kommentar pro Stimmzettel erlaubt ist (acceptance = 1). Wenn ein Kommtar pro Option erlaubt ist (acceptance = 2), werden die Kommentare in das "Votes"-Element mit eingebunden. Neben "id" und "vote" gibt es dann noch das zusätzliche Attribut "comment".

=>

Signature: ...
Date: 2021-04-02T07:32:55+02:00
From: martin@reg.virthos.net
To: reg.virthos.net
Message-Type: ballot
Voting-Id: abstimmung-01
Votes:
    - { id: 1, vote: 1, comment: "Comment 1" }
    - { id: 2, vote: 0, comment: "Comment 2" }
    - { id: 3, vote: -1 }
    - { id: 4, vote: 1 }
    - { id: 5, vote: 0 }
 */

Map<String, dynamic> msgTypeBallot({required VotingEvent event, required Voting voting, DateTime? dateTime}) {
  List<Map<String, dynamic>> votes = [];
  for (VotingOption vote in voting.votingOptions) {
    votes.add(voting.votingOptionToMap(vote));
  }

  Map<String, dynamic> map = {};
  map["Message-Type"] = "ballot";
  map["Event-Id"] = event.id;
  map["Voting-Id"] = voting.id;
  map["Votes"] = votes;

  String? ballotVotingComment = voting.ballotVotingComment;
  if (ballotVotingComment != null) {
    map["Comment"] = ballotVotingComment;
  }

  if (dateTime != null) {
    map["Date"] = SUtilDate.convertToMessageDate(dateTime);
  }

  return map;
}
