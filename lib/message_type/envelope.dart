// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import '/utils/s_util__date.dart';
/*
  https://demo01.partcp.org/modules/core/doc/envelope.txt
*/

Map<String, dynamic> msgTypeEnvelope({required List<String> messageList}) {

  String content = "";

  messageList.forEach((String e) {
    content += "$e\n---\n";
  });

  Map<String, dynamic> map = {
    "Message-Type": "envelope",
    "Content": content,
    "Date": SUtilDate.convertToMessageDate(DateTime.now())
  };
  return map;
}