// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import '/objects/voting_event.dart';
import '/utils/s_util__date.dart';


/*
https://codeberg.org/ParTCP/partcp_client/issues/55

Wenn bei einer Abstimmung das neue Attribut prerequisites gesetzt ist, nimmt der Server einen Stimmzettel für diese Abstimmung nur an,
wenn der Teilnehmer zuvor an den Abstimmungen teilgenommen hat, die in dem Attribut aufgelistet sind. Der Client sollte Abstimmungen,
bei denen die Voraussetzungen für ein Absenden des Stimmzettels noch nicht erfüllt sind, ausgegraut darstellen.

Um herauszufinden, bei welchen Abstimmungen ein Teilnehmer bereits seine Stimme abgegeben hat,
kann der event-details-request mit Include-Completed-Votings: true erweitert werden.
In der Antwort wird dann in dem neuen Element Completed-Votings die Liste der betreffenden Abstimmungen mitgeliefert.
*/


Map<String, dynamic> msgTypeEventDetailsRequest(VotingEvent event, {bool? includeCompletedVotings}) {
  Map<String, dynamic> map = {};
  map["Message-Type"] = "event-details-request";
  map["Event-Id"] = event.id;
  map["Date"] = SUtilDate.convertToMessageDate(DateTime.now());

  if (includeCompletedVotings != null && includeCompletedVotings) {
    map["Include-Completed-Votings"] = true;
  }
  return map;
}


