// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import '/objects/group.dart';
import '/utils/s_util__date.dart';

Map<String, dynamic> msgTypeEventListRequest(Group group,
    {required bool includeVotingCount, required bool includeAdminInfo, required bool includeOpenVotings}) {
  Map<String, dynamic> map = {
    "Message-Type": "event-list-request",
    "Group-Id": group.id,
    "Include-Voting-Count": true,
    "Include-Open-Votings": includeOpenVotings,
    "Include-Admin-Info": includeAdminInfo,
    "Date": SUtilDate.convertToMessageDate(DateTime.now())
  };
  return map;
}