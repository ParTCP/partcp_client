// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import '/utils/s_util__date.dart';

Map<String, dynamic> msgTypeGroupListRequest(bool includeAdminInfo) {
  Map<String, dynamic> map = {
    "Message-Type": "group-list-request",
    "Include-Admin-Info": includeAdminInfo,
    "Include-Subgroups": true,
    "Date": SUtilDate.convertToMessageDate(DateTime.now())
  };
  return map;
}
