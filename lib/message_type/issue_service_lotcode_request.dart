import 'package:partcp_client/objects/api_server.dart';
import 'package:partcp_client/objects/voting_event.dart';

import '/utils/s_util__date.dart';

Map<String, dynamic> msgTypeIssueServiceLotCodeRequest({
  required VotingEvent event,
  required ApiServer eventServer,
  // required eventId,
}) {
  Map<String, dynamic> map = {
    "Message-Type": "issue-service-lot-code-request",
    // "Event-Server": "demo02.partcp.org",
    // "Event-Id": "gvdw/20240910-bupa-2024-test",
    // "Event-Server": eventServer.name,
    "Event-Server": event.server.name,
    // event.issueService!.hostServer!
    "Event-Id": event.id,
    "Date": SUtilDate.convertToMessageDate(DateTime.now())
  };
  return map;
}

