// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import '/utils/s_util__date.dart';

/*
  https://demo01.partcp.org/modules/local_id/doc/key-submission.txt
 */


Map<String, dynamic> msgTypeKeySubmission({String? credentialEnc, String? eventId, String? credential}) {
  Map<String, dynamic> map = {};
  map["Message-Type"] = "key-submission";

  if (credentialEnc != null) {
    map["Credential~"] = credentialEnc;
  }
  if (eventId != null) {
    map["Event-Id"] = eventId;
  }
  else
  {
    map["Date"] = SUtilDate.convertToMessageDate(DateTime.now());
  }
  if (credential != null) {
    map["Credential"] = credential;
  }
  return map;
}




