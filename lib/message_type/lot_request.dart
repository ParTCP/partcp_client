// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:flutter/foundation.dart';
import '/utils/s_util__date.dart';

Map<String, dynamic> msgTypeLotRequest({
  @required eventId,
  @required lotCodeEnc,
  lotCode,
}){
  Map<String, dynamic> map = {};
  map["Message-Type"] = "lot-request";
  map["Event-Id"] = eventId;
  map["Date"] = SUtilDate.convertToMessageDate(DateTime.now());

  map["Lot-Code~"] = lotCodeEnc;

  if (lotCode != null) {
    map["Lot-Code"] = lotCode;
  }

  return map;
}

