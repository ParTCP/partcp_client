
import '/utils/s_util__date.dart';

Map<String, dynamic> msgTypeParticipantDetailsRequest({
  required String participantId
}) {

  Map<String, dynamic> map = {
    "Message-Type": "participant-details-request",
    "Participant-Id": participantId,
    "Date": SUtilDate.convertToMessageDate(DateTime.now())
  };
  return map;
}