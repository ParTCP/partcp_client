// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import '/utils/s_util__date.dart';

Map<String, dynamic> msgTypePing() {
  Map<String, dynamic> map = {};
  map["Message-Type"] = "ping";
  map["Date"] = SUtilDate.convertToMessageDate(DateTime.now());
  return map;
}