// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import '/utils/s_util__date.dart';
/*
  https://demo01.partcp.org/modules/server/doc/server-details-request.txt
 */

Map<String, dynamic> msgTypeServerDetailsRequest(bool includeEvents, bool includeSubgroups) {
  Map<String, dynamic> map = {
    "Message-Type": "server-details-request",
    "Include-Events": includeEvents,
    "Include-Subgroups": includeSubgroups,
    "Date": SUtilDate.convertToMessageDate(DateTime.now())
  };
  return map;
}

