// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import '/objects/voting.dart';
import '/objects/voting_event.dart';
import '/utils/s_util__date.dart';

Map<String, dynamic> msgTypeVotingDetailsRequest({
  required VotingEvent event,
  required Voting voting,
  // bool includeSubgroups = true
  }) {

  Map<String, dynamic> map = {
    "Message-Type": "voting-details-request",
    "Event-Id": event.id,
    "Voting-Id": voting.id,
    // "Include-Subgroups": includeSubgroups,
    "Date": SUtilDate.convertToMessageDate(DateTime.now())
  };
  return map;
}

