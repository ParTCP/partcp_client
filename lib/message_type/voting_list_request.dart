// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved


import 'package:flutter/foundation.dart';
import '/objects/voting.dart';
import '/utils/s_util__date.dart';

// enum VotingStatusEnum {
//   all, idle, open, closed
// }

Map<String, dynamic> msgTypeVotingListRequest({
  VotingStatusEnum votingStatus = VotingStatusEnum.open,
  required String votingEventId
  }) {
  Map<String, dynamic> map = {};
  map["Message-Type"] = "voting-list-request";
  map["Date"] = SUtilDate.convertToMessageDate(DateTime.now());
  map["Event-Id"] = votingEventId;
  map["Voting-Status"] = "${describeEnum(votingStatus)}";
  return map;
}

