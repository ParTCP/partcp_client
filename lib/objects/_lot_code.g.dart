// GENERATED CODE - DO NOT MODIFY BY HAND

/*
part of '_lot_code.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class LotCodeAdapter extends TypeAdapter<LotCode> {
  @override
  final int typeId = 1;

  @override
  LotCode read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return LotCode(
      lotCode: fields[0] as String?,
      status: fields[1] as int?,
    );
  }

  @override
  void write(BinaryWriter writer, LotCode obj) {
    writer
      ..writeByte(2)
      ..writeByte(0)
      ..write(obj.lotCode)
      ..writeByte(1)
      ..write(obj.status);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is LotCodeAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}


 */