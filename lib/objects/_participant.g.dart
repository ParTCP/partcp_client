// // GENERATED CODE - DO NOT MODIFY BY HAND
//
// part of 'participant.dart';
//
// // **************************************************************************
// // TypeAdapterGenerator
// // **************************************************************************
//
// class ParticipantAdapter extends TypeAdapter<Participant> {
//   @override
//   final int typeId = 2;
//
//   @override
//   Participant read(BinaryReader reader) {
//     final numOfFields = reader.readByte();
//     final fields = <int, dynamic>{
//       for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
//     };
//     return Participant(
//       userId: fields[0] as String,
//       participantId: fields[1] as String?,
//       publicKeyConcatB64: fields[2] as String?,
//       lotCodeKey: fields[3] as dynamic,
//       lotCodeDelivered: fields[4] as bool,
//     );
//   }
//
//   @override
//   void write(BinaryWriter writer, Participant obj) {
//     writer
//       ..writeByte(5)
//       ..writeByte(0)
//       ..write(obj.userId)
//       ..writeByte(1)
//       ..write(obj.participantId)
//       ..writeByte(2)
//       ..write(obj.publicKeyConcatB64)
//       ..writeByte(3)
//       ..write(obj.lotCodeKey)
//       ..writeByte(4)
//       ..write(obj.lotCodeDelivered);
//   }
//
//   @override
//   int get hashCode => typeId.hashCode;
//
//   @override
//   bool operator ==(Object other) =>
//       identical(this, other) ||
//       other is ParticipantAdapter &&
//           runtimeType == other.runtimeType &&
//           typeId == other.typeId;
// }
