// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'dart:convert';

import 'package:cryptography/cryptography.dart' as ecLib;
import 'package:get/get.dart';
import 'package:partcp_client/controller/main_controller.dart';
import 'package:partcp_client/objects/defaults.dart';
import '/controller/http_controller.dart';
import '/message_type/message_type.dart';
import '/objects/credential.dart';
import '/objects/server_response.dart';
import '/objects/voting.dart';
import '/objects/voting_event.dart';
import '/objects/api_server.dart';
import '/objects/_ws_server.dart';
import '/utils/crypt/my_aes_crypt.dart';
import '/utils/crypt/my_ec_crypt.dart';
import '/utils/log.dart';
import '/utils/s_utils.dart';
import '/utils/yaml_converter.dart';

import '/constants.dart';
import 'participant.dart';
import '_participant_data.dart';
import 'wallet.dart';


const _TAG = "Account";

/// we create for each votingEvent one account
enum Mode {
  event,
  wallet,
}


class Account {

  Account (this.mode);

  /// account revision, to better handle updates
  int rev = MainController.accountRevision;

  /// for wallet account.
  WalletProtection protection = WalletProtection.pwd;

  Participant? participant;
  String? _participantEnc;

  String? userId;
  String? userIdEnc;

  String name = "";
  Mode mode = Mode.event;

  String get title {
    if (name.isNotEmpty) {
      return name;
    }
    else {
      return userId != null
          ? userId!
          : "";
    }
  }


  String? privateKeyXB64Enc;
  String? privateKeyEdB64Enc;

  ecLib.SimpleKeyPair? keyPairX;
  ecLib.SimpleKeyPair? keyPairEd;

  bool? pubKeysOnServerValid;

  // String? votingEventId;
  // bool votingEventIsNonAnonymous = false;

  VotingEvent? votingEvent;

  late ApiServer apiServer;
  ApiServer? keySubmissionServer;

  String? lotCodeEnc;
  String? lotCodeEncHash;


  /// list of [Voted] to find past votings
  // List<dynamic> votedList = [];

  DateTime? dateC;
  DateTime? dateU;

  /// [votingAllowed] 0: without credentials (passive); 1: with credential (active account, voting allowed)
  int votingAllowed = 1;

  String lotCodesSubDir(VotingEvent event) {
    return "${Constants.LOT_CODES_DIR_NAME}/${event.id}";
  }


  Future<String> get publicKeysConcat async => MyEcCrypt.concatPublicKeys(
    pubKeyEd: await publicKeyEd,
    pubKeyX: await publicKeyX,
  );

  Future<ecLib.SimplePublicKey> get publicKeyX async => await keyPairX!.extractPublicKey();
  Future<ecLib.SimplePublicKey> get publicKeyEd async => await keyPairEd!.extractPublicKey();

  Future<String> get walledId async => MyEcCrypt.hashFromPublicKeyX(await publicKeyX);


  /// CREATE NEW KEYS
  Future<bool> createNewKeys() async{
    try {
      Log.d(_TAG, "createNewKeys ...");
      keyPairX = await MyEcCrypt.createKeyPairX();
      keyPairEd = await MyEcCrypt.createKeyPairEd();

      Log.d(_TAG, "createNewKeys OK");
      // Log.s(_TAG, "createNewKeys PrivateKeyXBytes: ${await keyPairX!.extractPrivateKeyBytes()}");

      dateC = DateTime.now();
      dateU = DateTime.now();
      if (keyPairX == null || keyPairEd == null) {
        return false;
      }
    }
    catch(e,s) {
      Log.e(_TAG, "createNewKeys error: $e, stack: $s");
      return false;
    }
    return true;
  }

  /// ENCRYPT KEYS
  /// don't use this directly
  /// this method will be called in [Wallet.addAccount]
  Future<bool> encryptKeys(List<int> pwdBytes) async {
    try {
      final List<int> privKeyXEnc = await MyAesCrypt.encryptKey(
          await keyPairX!.extractPrivateKeyBytes(),
          pwdBytes
      );
      privateKeyXB64Enc = base64Encode(privKeyXEnc);
      Log.d(_TAG, "encryptKeys: X: $privateKeyXB64Enc");

      final List<int> privKeyEdEnc = await MyAesCrypt.encryptKey(
          await keyPairEd!.extractPrivateKeyBytes(),
          pwdBytes
      );
      privateKeyEdB64Enc = base64Encode(privKeyEdEnc);
      // Log.d(_TAG, "encryptKeys: Ed: $privateKeyEdB64Enc");
      Log.d(_TAG, "encryptKeys: OK -> return true");
      return true;
    }
    catch (e, s) {
      Log.e(_TAG, "encryptKeys; error:$e; stack: $s");
      return false;
    }
  }


  /// DECRYPT KEYS
  Future<bool> decryptKeys(List<int> pwdBytes) async {
    if (privateKeyXB64Enc != null) {
      try {
        final List<int> privKeyXSeed = await MyAesCrypt.decryptKey(
            base64Decode(privateKeyXB64Enc!),
            pwdBytes
        );
        keyPairX = await MyEcCrypt.createKeyPairXFromSeed(privKeyXSeed);

        final List<int> privKeyEdSeed = await MyAesCrypt.decryptKey(
            base64Decode(privateKeyEdB64Enc!),
            pwdBytes
        );
        keyPairEd = await MyEcCrypt.createKeyPairEdFromSeed(privKeyEdSeed);

        // Log.s(_TAG, "decryptKeys : privateKeyX: ${await keyPairX!.extractPrivateKeyBytes()}");
        // Log.s(_TAG, "decryptKeys: privateKeyEd: ${await keyPairEd!.extractPrivateKeyBytes()}");
        //
        // Log.s(_TAG, "decryptKeys: privateKeyX B64: ${base64Encode(await keyPairX!.extractPrivateKeyBytes())}");
        // Log.s(_TAG, "decryptKeys: privateKeyEd B64: ${base64Encode(await keyPairEd!.extractPrivateKeyBytes())}");
        Log.d(_TAG, "decryptKeys for $userId: OK");

        return true;
      }
      catch (e, s) {
        Log.e(_TAG, "encryptKeys; error:$e; stack: $s");
        return false;
      }
    }
    return false;
  }



  /// ENCRYPT userId
  Future<void> encryptSecretValues(List<int> pwdBytes) async {
    if (userId != null) {
      userIdEnc = base64Encode(await MyAesCrypt.encryptKey(
          utf8.encode(userId!),
          pwdBytes
      ));
    }

    if (participant != null) {
      _participantEnc = base64Encode(await MyAesCrypt.encryptKey(
          utf8.encode(participant!.toJson()),
          pwdBytes
      ));
    }
  }

  /// DECRYPT USER_ID
  Future<bool> decryptSecretValues(List<int> pwdBytes) async {
    if (userIdEnc != null) {
      userId = utf8.decode(await MyAesCrypt.decryptKey(
          base64Decode(userIdEnc!),
          pwdBytes)
      );
    }

    if (_participantEnc != null) {
      String? test = utf8.decode(await MyAesCrypt.decryptKey(
          base64Decode(_participantEnc!),
          pwdBytes)
      );
      participant = Participant.fromMap(jsonDecode(test));
    }

    return true;
  }



  /// ENCRYPT SERVER MESSAGE
  Future<String?> encryptServerMessage({
    required ApiServer server, required String message}) async {

    Log.d(_TAG,"encryptServerMessage remotePubKey: ${server.publicKeyConcatB64}, myPubKey: ${await publicKeysConcat}, keyPairX: ${keyPairX.toString()}");

    return await MyAesCrypt.encryptMessage(
      message: message,
      keyPairX: keyPairX!,
      remotePublicKeyX: server.publicKeyX!,
    );
  }



  /// DECRYPT SERVER MESSAGE
  Future<String?> decryptServerMessage({
    required ApiServer server, required String messageEnc}) async {

    Log.d(_TAG,"decryptServerMessage remotePubKey: ${server.publicKeyConcatB64}, myPubKey: ${await publicKeysConcat}, keyPairX: ${keyPairX.toString()}");
    messageEnc = messageEnc.replaceAll("\n", "");
    return await MyAesCrypt.decryptMessage(
        message: messageEnc,
        keyPairX: keyPairX!,
        remotePublicKeyX: server.publicKeyX!
    );
  }


  /// ENCRYPT WS SERVER MESSAGE
  // Future<String?> encryptWsServerMessage({
  //   required WsServer server, required String message}) async {
  //   return await MyAesCrypt.encryptMessage(
  //     message: message,
  //     keyPairX: keyPairX!,
  //     remotePublicKeyX: server.publicKeyX!,
  //   );
  // }
  //
  //
  // /// DECRYPT WS SERVER MESSAGE
  // Future<String?> decryptWsServerMessage({
  //   required WsServer server, required String messageEnc}) async {
  //
  //   messageEnc = messageEnc.replaceAll("\n", "");
  //   return await MyAesCrypt.decryptMessage(
  //       message: messageEnc,
  //       keyPairX: keyPairX!,
  //       remotePublicKeyX: server.publicKeyX!
  //   );
  // }




  Future<ServerResponse> keySubmission({String? credential} ) async {

    ApiServer server = keySubmissionServer ?? apiServer;
    ServerResponse serverResponse = ServerResponse();

    String? usedCredential;
    if (credential != null) {
      usedCredential = credential;
    }
    else if (participant!= null && participant!.credentialDec!= null) {
      usedCredential = participant!.credentialDec!;
    }

    Log.d(_TAG, "usedCredential: $usedCredential");


    if (usedCredential == null) {
      serverResponse.errorMessage = "Credential may not be null";
    }
    else {
      String? credentialEnc = await encryptServerMessage(
        message: usedCredential,
        server: server,
      );
      // final Map<String, dynamic> messageMap = msgTypeKeySubmission(
      //     credentialEnc: credentialEnc,
      //     eventId: votingEventIsNonAnonymous ? null : votingEventId
      // );
      final Map<String, dynamic> messageMap = msgTypeKeySubmission(
          credentialEnc: credentialEnc,
          eventId: votingEvent!.isNonAnonymous ? null : votingEvent!.id
      );

      serverResponse = await HttpController().serverRequestSigned(this, messageMap, server, loc: "keySubmission");
      // Log.d(_TAG, "submitId; resultMap: $resultMap");

    }
    return serverResponse;
  }


  /// checks users publicKey on server
  Future<bool> checkUsersPubKeyOnServer() async {
    if (userId != null) {
      ServerResponse? response = await participantDetailsRequest(forPublicKeys: true);

      /// if request fails, server returns Rejection-Code: 71
      if (response != null && response.isOk("checkUsersPubKeyOnServer")) {
        if (response.object != null && response.object is Participant) {
          Participant participant = response.object;

          if (participant.publicKey != null && (await publicKeysConcat) == participant.publicKey) {
            return true;
          }
        }
      }
    }
    return false;
  }

  // Future<ServerResponse?> requestParticipantDetails(ApiServer keySubmissionServer) async {
  Future<ServerResponse?> participantDetailsRequest({required bool forPublicKeys}) async {
    ServerResponse? _serverResponse;
    ApiServer server = keySubmissionServer ?? apiServer;

    if (userId != null) {

      final messageMap = msgTypeParticipantDetailsRequest(participantId: userId!);

      if (forPublicKeys) {
        _serverResponse = await HttpController().serverRequest(messageMap, this, server);
        if (_serverResponse.isOk("checkUsersPubKeyOnServer") && _serverResponse.bodyMap!.containsKey("Participant-Data")) {
          Participant participant = Participant.fromMap(_serverResponse.bodyMap!["Participant-Data"]);
          _serverResponse.object = participant;
        }
      }
      else {
        _serverResponse = await HttpController().serverRequestSigned(this, messageMap, server, loc: "checkUsersPubKeyOnServer");

        if (_serverResponse.isOk("checkUsersPubKeyOnServer") && _serverResponse.bodyMap!.containsKey("Participant-Data~")) {
          String messageEnc = _serverResponse.bodyMap!["Participant-Data~"];
          String? message = await decryptServerMessage(server: server, messageEnc: messageEnc);
          Log.d(_TAG, "checkUsersPubKeyOnServer, message: $message ");

          if (message != null) {
            var map = jsonDecode(message);
            Participant participant = Participant.fromMap(map);
            _serverResponse.object = participant;
          }
        }
      }
    }
    return _serverResponse;
  }


  static Account? fromMap({required Map<String, dynamic> map, ApiServer? apiServer }) {
    var serverMap = SUtils.mapValue(map, SERVER, null);
    if (serverMap == "") serverMap = null;

    String modeString = SUtils.mapValue(map, MODE, "");
    Mode mode = Mode.values.firstWhere((mode) => mode.toString() == modeString,
        orElse: () => Mode.event);

    Account account = Account(mode)
      ..userIdEnc = SUtils.mapValue(map, USER_ID_ENC, null)
      ..userId = SUtils.mapValue(map, USER_ID, null)
      ..name = SUtils.mapValue(map, NAME, "")
      ..privateKeyXB64Enc = SUtils.mapValue(map, PRIVATE_KEY_X_ENC, null)
      ..privateKeyEdB64Enc = SUtils.mapValue(map, PRIVATE_KEY_ED_ENC, null)
      // ..votingEventId = SUtils.mapValue(map, EVENT_ID, null)
      ..lotCodeEnc = SUtils.mapValue(map, LOT_CODE_ENC, null)
      ..lotCodeEncHash = SUtils.mapValue(map, LOT_CODE_HASH, null)
      ..rev = SUtils.mapValue(map, REV, 0)
      .._participantEnc = SUtils.mapValue(map, PARTICIPANT_ENC, null)
    ;

    int status = SUtils.mapValue(map, VOTING_ALLOWED, -1);
    if (account.rev < 1) {
      account.votingAllowed = 1;
    }
    else {
      account.votingAllowed = status;
    }

    /// Wallet
    if (mode == Mode.wallet) {
      account.userId = SUtils.mapValue(map, USER_ID, null);
      String? protectionType = SUtils.mapValue(map, PROTECTION, null);
      if (protectionType != null) {
        WalletProtection? type = WalletProtection.values.firstWhereOrNull ( (e) => e.name == protectionType);
        if (type != null) {
          account.protection = type;
        }
        else {
          account.protection = WalletProtection.simple;
        }
      }
      else {
        account.protection = WalletProtection.simple;
      }
    }
    /// Event Account
    else if (mode == Mode.event) {
      account.apiServer = apiServer!;
      // account.votingEventIsNonAnonymous= SUtils.mapValue(map, NONE_ANONYMOUS_EVENT, false);

      if (map.containsKey(EVENT)) {
        account.votingEvent = VotingEvent.fromWalletMap(
            map: map[EVENT], apiServer: apiServer
        );
      }
    }

    String? _dateCIso = SUtils.mapValue(map, DATE_C, null);
    String? _dateUIso = SUtils.mapValue(map, DATE_U, null);

    if (_dateCIso != null) {
      account.dateC = _createDateTimeFromIsoString(_dateCIso);
    }
    if (_dateUIso != null) {
      account.dateU = _createDateTimeFromIsoString(_dateUIso);
    }

    // if (map.containsKey(VOTINGS) && map[VOTINGS] != null) {
    //   account.votedList.addAll(map[VOTINGS]);
    // }

    if (map.containsKey(KEY_SUBMISSION_SERVER)) {
      account.keySubmissionServer = ApiServer.fromMap(map[KEY_SUBMISSION_SERVER]);
    }

    return account;
  }


  static _createDateTimeFromIsoString (String isoDate) {
    return DateTime.parse(isoDate);
  }


  Future<Map<String, dynamic>> toMap(bool isWalletAccount, List<int>? pwdBytes) async {
    if (isWalletAccount) {
      return {
        PRIVATE_KEY_X_ENC: privateKeyXB64Enc,
        PRIVATE_KEY_ED_ENC: privateKeyEdB64Enc,
        USER_ID: userId,
        NAME: name,
        DATE_C: _convertDateToIsoString(dateC),
        DATE_U: _convertDateToIsoString(dateU),
        MODE: mode,
        REV: rev,
        PROTECTION: protection.name,
      };
    }
    else {
      await encryptSecretValues(pwdBytes!);

      var map = {
        USER_ID_ENC: userIdEnc,
        PRIVATE_KEY_X_ENC: privateKeyXB64Enc,
        PRIVATE_KEY_ED_ENC: privateKeyEdB64Enc,
        DATE_C: _convertDateToIsoString(dateC),
        DATE_U: _convertDateToIsoString(dateU),
        MODE: mode,
        LOT_CODE_HASH: lotCodeEncHash,
        VOTING_ALLOWED: votingAllowed,
        REV: rev,
      };

      if (votingEvent != null) {
        map[EVENT] = votingEvent!.toWalletMap();
      }

      // if (votingEventId != null) {
      //   map[EVENT_ID] = votingEventId;
      // }
      // if (votedList.isNotEmpty) {
      //   map[VOTINGS] = votedList;
      // }
      // if (votingEventIsNonAnonymous) {
      //   map[NONE_ANONYMOUS_EVENT] = true;
      // }


      if (participant != null) {
        map[PARTICIPANT_ENC] = _participantEnc;
      }
      if (keySubmissionServer != null) {
        map[KEY_SUBMISSION_SERVER] = keySubmissionServer!.toMap();
      }
      return map;
    }
  }


  String? _convertDateToIsoString (DateTime? dateTime) {
    return dateTime != null
        ? dateTime.toIso8601String()
        : null;
  }


  Future<bool> compareLotCode(String lotCode) async {
    if (lotCodeEncHash != null) {
      String lotCodeHashB64 = await MyEcCrypt.hashSha256AsB64(lotCode.codeUnits);
      return lotCodeHashB64 == lotCodeEncHash;
    }
    return false;
  }

  Future<void> setLotCode(String lotCode) async {
    lotCodeEnc = lotCode;
    lotCodeEncHash = await MyEcCrypt.hashSha256AsB64(lotCode.codeUnits);
  }


  bool isValidAccount() {
    bool _isValid =
        (keyPairX != null && keyPairEd != null) ||
            (privateKeyXB64Enc != null && privateKeyEdB64Enc != null);

    Log.d(_TAG, "isValidAccount: $_isValid");
    return _isValid;
  }


  /// ADD VOTED item
  // void saveVotedByObj(Voting voting) {
  //   if (!votedList.contains(voting.id)) {
  //     votedList.add(voting.id);
  //   }
  //   dateU = DateTime.now();
  // }
  //
  // void saveVotedById({required String votingId}) {
  //   if (!votedList.contains(votingId)) {
  //     votedList.add(votingId);
  //   }
  //   dateU = DateTime.now();
  // }
  //
  //
  // /// CHECK if already voted
  // bool isVoted(Voting voting) {
  //   return votedList.contains(voting.id);
  // }


  /// server respond:
  /// Lot-Content~: >
  ///   wsoo3iKDqKABvWCL0WP+pw==:Y1BYUTJzS1BscFIrZjdrQW96OHNwdzNYZGZJSGN6aWFpZWk5eTJSVEZ0T0UwMVpwVUYvaUdnRWo3UzI1Y1JzPQ==
  ///        IV Base64          :           encrypted Yaml with "participant_id" and "credential" keys
  Future<Credential> decryptCredentialFromLotContent(ApiServer server, String lotContent) async {
    try {
      String? yamlString = (await MyAesCrypt.decryptMessage(
          message:  lotContent,
          keyPairX: keyPairX!,
          remotePublicKeyX: server.publicKeyX!
      ));



      if (yamlString != null) {
        yamlString = yamlString.trim();

        Log.s(_TAG,
            "decryptCredentialFromLotContent: yamlString (decrypted): $yamlString");

        Log.w(_TAG,
            "decryptCredentialFromLotContent: yamlString (decrypted): $yamlString");

        /// YamlConverter converts a large number, eg. 4393685097626700267668050536035 to 4.3936850976267e+30
        /// We replace the String "credential: " with "credential: A"
        yamlString = yamlString.replaceFirst("credential: ", "credential: A");
        Map<String, dynamic> yamlMap = YamlConverter().toMap(yamlString);
        String tempParticipantId = '${yamlMap["participant_id"]}'.trim();
        String credential = '${yamlMap["credential"]}'.trim();

        if (credential.substring(0, 1) == "A") {
          credential = credential.substring(1);
        }

        ///  Bei nicht-anonymen Events kommen Teilnehmerkennungen zum Einsatz, die globalen Charakter haben, also nicht nur auf einen Event bezogen sind.
        ///  Daher sind die zugehörigen Schlüssel global zu speichern, so als hätte sich der Teilnehmer manuell mit einem Server verbunden.

        ///  Dementsprechend kann es sein, dass beim Einlösen des Teilnahmecodes ( lot-request) eine participant_id zurückgeliefert wird,
        ///  die sich auf einen anderen Server bezieht als den Event-Server (erkennbar an der Serveradresse hinter dem @-Zeichen).


        /// todo-test
        var tempParticipantIdSplitted = tempParticipantId.split("@");
        // String participantId = tempParticipantIdSplitted[0];
        String participantId = tempParticipantId;



        ApiServer? lotServer;
        if (tempParticipantIdSplitted.length > 1) {
          String lotServerName = tempParticipantIdSplitted[1];
          lotServer = ApiServer(lotServerName);
          await lotServer.serverDetailsRequest();
        }
        return Credential(participantId, credential, lotServer);
      }
      else {
        return Credential(null, null, null);
      }
    }
    catch(e) {
      return Credential.error("$e");
    }
  }




  static const USER_ID = "USER_ID";
  static const USER_ID_ENC = "USER_ID~";

  static const NAME = "NAME";
  static const PRIVATE_KEY_X_ENC = "PRIVATE_KEY_X_ENC";
  static const PRIVATE_KEY_ED_ENC = "PRIVATE_KEY_ED_ENC";
  static const KEY_PAIR_X = "KEY_PAIR_X";
  static const KEY_PAIR_ED = "KEY_PAIR_ED";
  static const PUBLIC_KEY_X = "PUBLIC_KEY_X";
  static const PUBLIC_KEY_ED = "PUBLIC_KEY_ED";
  static const DATE_C = "DATE_C";
  static const DATE_U = "DATE_U";
  static const MODE = "MODE";
  static const LAST_GROUP_ID = "Last-Group-Id";
  static const LOT_CODE_ENC = "LOT_CODE~";
  static const LOT_CODE_HASH = "LOT_CODE_HASH";

  static const SERVER = "SERVER";

  // static const VOTINGS = "VOTINGS";
  //
  // static const EVENT_ID = "EVENT_ID";
  // static const EVENT_NAME = "EVENT_NAME";

  static const EVENT = "EVENT";

  /// new 27.09.24
  static const VOTING_ALLOWED = "VOTING_ALLOWED";
  static const REV = "REV";

  /// new 13.01.25
  static const PARTICIPANT_ENC = "PARTICIPANT~";
  static const PROTECTION = "PROTECTION";

  static const KEY_SUBMISSION_SERVER = "KEY_SUBMISSION_SERVER";
  // static const NONE_ANONYMOUS_EVENT = "NONE_ANONYMOUS_EVENT";
}




