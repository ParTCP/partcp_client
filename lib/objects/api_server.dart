// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'dart:convert';

import 'package:collection/src/iterable_extensions.dart';
import 'package:cryptography/cryptography.dart' as ecLib;
import 'package:flutter/services.dart';
import '../controller/main_controller.dart';
import '../message_type/issue_service_lotcode_request.dart';
import '/controller/http_controller.dart';
import '/message_type/event_list_request.dart';
import '/message_type/message_type.dart';
import '/objects/account.dart';
import '/objects/server_response.dart';
import '/objects/voting_event.dart';
import '/utils/crypt/my_ec_crypt.dart';
import '/utils/log.dart';
import '/utils/s_utils.dart';

import '../constants.dart';
import 'group.dart';

const _TAG = "ApiServer";

class ApiServer {

  String name;

  String description = "";
  String? publicKeyConcatB64;
  String shortCode = "";

  String keyManagerUrl = "";
  List<Account> accounts = [];

  bool isAssetServer = false;
  bool lotCodesReceived = false;

  /// once we get events from server, we cache it in the map
  Map<String, List<VotingEvent>> groupEventsMap = {};

  List<Group> serverGroups = [];

  ecLib.SimplePublicKey? publicKeyX;
  ecLib.SimplePublicKey? publicKeyEd;

  bool get registrationRequired => keyManagerUrl.isNotEmpty;

  // String get name => Uri.parse(this.url).host;
  String get url => "https://$name";

  // ApiServer(this.url);
  ApiServer(this.name);

  static ApiServer? fromWalletMap(Map<String, dynamic>? map) {
    if (map == null) {
      return null;
    }

    ApiServer server = ApiServer(SUtils.mapValue(map, NAME, null));
    // server.name = SUtils.mapValue(map, NAME, "");
    server.description = SUtils.mapValue(map, COMMUNITY, "");
    server.publicKeyConcatB64 = SUtils.mapValue(map, PUBLIC_KEY_CONCAT_B64, null);
    server.shortCode = SUtils.mapValue(map, SHORT_CODE, "");

    /// Create Keys from concat Ed and X key
    Map<String, dynamic> keyMap = server.createPublicKeysFromConcat();
    if (keyMap.containsKey("X") && keyMap.containsKey("Ed")) {
      server.publicKeyX = keyMap["X"];
      server.publicKeyEd = keyMap["Ed"];
    }

    /// accounts
    final List<dynamic>? yamlAccounts = map[ACCOUNTS];
    if (yamlAccounts != null) {
      for (var i = 0; i < yamlAccounts.length; i++) {
        final Account? account = Account.fromMap(map: yamlAccounts[i], apiServer: server);
        // if (account != null && account.isValidAccount()) {
        if (account != null) {
          server.accounts.add(account);
        }
      }
    }
    return server;
  }

  Future<Map<String, dynamic>> toWalletMap(List<int> pwdBytes) async {
    /// accounts list
    List<dynamic> accountsList = [];
    for (var i = 0; i < accounts.length; i++) {
      accountsList.add(await accounts[i].toMap(false, pwdBytes));
    }
    return {
      NAME: name,
      SHORT_CODE: shortCode,
      COMMUNITY: description,
      PUBLIC_KEY_CONCAT_B64: publicKeyConcatB64,
      ACCOUNTS: accountsList};
  }


  /// used to store KeySubmission Server in Account
  static ApiServer? fromMap(Map<String, dynamic>? map) {
    if (map == null) {
      return null;
    }

    ApiServer vs = ApiServer(SUtils.mapValue(map, NAME, null));
    vs.description = SUtils.mapValue(map, COMMUNITY, "");
    vs.publicKeyConcatB64 = SUtils.mapValue(map, PUBLIC_KEY_CONCAT_B64, null);

    /// Create Keys from concat Ed and X key
    Map<String, dynamic> keyMap = vs.createPublicKeysFromConcat();
    if (keyMap.containsKey("X") && keyMap.containsKey("Ed")) {
      vs.publicKeyX = keyMap["X"];
      vs.publicKeyEd = keyMap["Ed"];
    }
    return vs;
  }

  /// used to load KeySubmission Server in Account
  Map<String, dynamic> toMap()  {
    /// accounts list
    return {
      NAME: name,
      PUBLIC_KEY_CONCAT_B64: publicKeyConcatB64
    };
  }


  void addAccount(Account account) {
    for (var i = 0; i < accounts.length; i++) {
      if (accounts[i].votingEvent!.id == account.votingEvent!.id) {
        accounts.removeAt(i);
        break;
      }
    }
    accounts.add(account);
  }


  dynamic createPublicKeyXFromB64Seed(String publicKeyXB64) {
    publicKeyX = MyEcCrypt.createPublicKeyXFromSeed(base64Decode(publicKeyXB64));
    Log.d(_TAG, "createPublicKeyXFromB64Seed; publicKeyX: ${publicKeyX!.bytes}");
    return publicKeyX;
  }


  dynamic createPublicKeyEdFromB64Seed(String publicKeyEdB64) {
    publicKeyEd = MyEcCrypt.createPublicKeyXFromSeed(base64Decode(publicKeyEdB64));
    Log.d(_TAG, "createPublicKeyEdFromB64Seed; publicKeyEd: ${publicKeyEd!.bytes}");
    return publicKeyEd;
  }


  /// Create Keys from concat Ed and X key
  dynamic createPublicKeysFromConcat() {
    if (publicKeyConcatB64 == null) return null;

    List<int> pubKeyConcatSeed = base64.decode(publicKeyConcatB64!.trim());
    Map<String, dynamic> keyMap = MyEcCrypt.createPublicKeysFromConcat(pubKeyConcatSeed);

    if (keyMap.containsKey("X") && keyMap.containsKey("Ed")) {
      publicKeyX = keyMap["X"];
      publicKeyEd = keyMap["Ed"];
    }
    Log.d(_TAG, "createPublicKeysFromConcat; publicKeyX: ${publicKeyX!.bytes}");
    Log.d(_TAG, "createPublicKeysFromConcat; publicKeyEd: ${publicKeyEd!.bytes}");
    return keyMap;
  }


  Account? getVotingAccountForEvent(VotingEvent event) {
    return accounts.firstWhereOrNull((account) => account.votingEvent!.id == event.id && account.mode == Mode.event);
  }


  Account? getAccountByMode(Mode mode) {
    //-// return accounts.firstWhere((account) => account.mode == mode, orElse: () => null);
    return accounts.firstWhereOrNull((account) => account.mode == mode);
  }


  /// Request events from server or get it from cache
  /// on force == true, we always request a list from server
  Future<ServerResponse> eventsByGroup(Account? account, Group group, {
    bool? force = false, bool includeAdminInfo = false, bool includeVotingCount = false, bool includeOpenVotings = false
  }) async {
    ServerResponse serverResponse = ServerResponse();
    if (groupEventsMap.containsKey(group.id) && force != null && !force && groupEventsMap[group.id]!.isNotEmpty) {
      serverResponse
          ..object = groupEventsMap[group.id]
          ..receivingData = false;
      return serverResponse;
    } else {
      ServerResponse serverResponse = await _sEventListRequest(
          account, group,
          includeVotingCount: includeVotingCount,
          includeAdminInfo: includeAdminInfo,
          includeOpenVotings: includeOpenVotings
      );
      groupEventsMap[group.id] = serverResponse.object;
      return serverResponse;
    }
  }


  Group? getGroupById(String? id) {
    if (id == null || id.isEmpty) {
      return null;
    }
    return serverGroups.firstWhereOrNull((element) => element.id == id);
  }


  Future<void> decryptAccountsUserId(List<int> pwdBytes) async {
    for (Account account in accounts) {
      await account.decryptSecretValues(pwdBytes);
    }
  }


  VotingEvent? getEventById(Group group, String eventId) {
    var list = groupEventsMap[group.id];
    Log.d(_TAG, "getEventById ($eventId) => ${list!.firstWhereOrNull((element) => element.id == eventId)}");
    return list.firstWhereOrNull((element) => element.id == eventId);
  }


  bool isValid() {
    return publicKeyX != null && publicKeyEd != null;
  }


  // String get idHashCode {
  //   var _url = Uri.parse(url);
  //   // Log.d(_TAG, "idHashCode url: $url, host:${_url.host}, authority:${_url.authority}, scheme:${_url.scheme}");
  //   String hash = "${_url.scheme}-${_url.host}".hashCode.toString();
  //   return hash;
  // }


  Future<ServerResponse> issueServiceLotCodeRequest({required Account account, required VotingEvent event}) async {
    final messageMap = msgTypeIssueServiceLotCodeRequest(eventServer: this, event: event);
    ServerResponse response = await HttpController().idProviderRequestSigned(
        account: account,
        messageMap: messageMap,
        idProvider: this,
        host: event.issueService!.hostServer!,
        loc: "$_TAG, requestServerDetails"
    );
    return response;
  }

  /// Server-Details-Request
  Future<ServerResponse> serverDetailsRequest({Account? account}) async {
    final messageMap = msgTypeServerDetailsRequest(true, true);

    ServerResponse response = (account == null || !account.isValidAccount())
        ? await HttpController().serverRequest(messageMap, null, this)
        : await HttpController().serverRequestSigned(account, messageMap, this, loc: "$_TAG, requestServerDetails");

    Map<String, dynamic>? bodyMap = response.bodyMap;

    /// if Error
    if (!response.isOk("$_TAG, requestServerInfo")) {
      return response;
    }

    /// NO PUBLIC KEY
    if (!bodyMap!.containsKey("Public-Key")) {

      if(bodyMap.containsKey("Rejection-Reason")){
        response.errorMessage = bodyMap["Rejection-Reason"];
      }
      else {
        response.errorMessage = "Missing PublicKey";
      }
      return response;
    }

    /// NO Server-Data
    if (!bodyMap.containsKey("Server-Data")) {
      response.errorMessage = "Wrong Server, can not talk with him";
      return response;
    }

    Map<String, dynamic> serverDataMap = bodyMap["Server-Data"];
    String pubKeyConcat = bodyMap["Public-Key"];

    Log.d(_TAG, "pubKeyConcat: $pubKeyConcat");
    publicKeyConcatB64 = pubKeyConcat.trim();
    createPublicKeysFromConcat();

    /// keys error
    if (publicKeyX == null || publicKeyEd == null) {
      response.errorMessage = "Can not create Public Keys";
      return response;
    }

    // name = SUtils.mapValue(serverDataMap, "name", "NoNameDefined");
    description = SUtils.mapValue(serverDataMap, "community", "");
    keyManagerUrl = SUtils.mapValue(serverDataMap, "key_manager", "");

    /// SERVER GROUPS
    serverGroups.clear();
    _fillGroupList(bodyMap);
    
    return response;
  }


  Future<ServerResponse> _sEventListRequest(Account? account, Group group,
      {required bool includeAdminInfo, required bool includeVotingCount, required bool includeOpenVotings}) async {
    final _messageMap = msgTypeEventListRequest(group, includeAdminInfo: includeAdminInfo, includeVotingCount: includeVotingCount, includeOpenVotings: includeOpenVotings);
    final response = await HttpController().serverRequest(_messageMap, account, this);
    List<VotingEvent> _events = [];

    final Map<String, dynamic>? bodyMap = response.bodyMap;

    if (response.isOk("_sEventListRequest") && bodyMap != null && bodyMap.containsKey("Events")) { //old: if (response.statusCode == 200)
      List<dynamic> eventsList = bodyMap["Events"];
      for (var i = 0; i < eventsList.length; i++) {
        VotingEvent? event = await VotingEvent.fromMap(eventsList[i], null, this);
        _events.add(event);
      }
    }
    response.object = _events;
    return response;
  }


  Future<ServerResponse> groupListRequest(Account? account, {bool includeAdminInfo = false}) async {
    final _messageMap = msgTypeGroupListRequest(includeAdminInfo);
    final ServerResponse response = await HttpController().serverRequest(_messageMap, account, this);

    final Map<String, dynamic>? bodyMap = response.bodyMap;
    _fillGroupList(bodyMap!);
    return response;
  }

  void _fillGroupList(Map<String, dynamic> bodyMap) {
    serverGroups.clear();

    if (bodyMap["Groups"] is List<dynamic>) {
      List<dynamic> groupList = bodyMap["Groups"];

      for (var i = 0; i < groupList.length; i++) {
        serverGroups.add(Group.fromMap(groupList[i]));
      }
    }
  }


  Future<void> loadAccountsFromWallet() async {
    ApiServer? walletServer = MainController.to.wallet!.getServerByUrl(url);
    if (walletServer != null) {
      /// todo: compare PublicKey
      accounts = walletServer.accounts;

      /// decrypt accounts userId
      for (Account account in accounts){
        await account.decryptSecretValues(MainController.to.wallet!.pwdBytes!);
      }
    }
    var bla = "";
  }




  static Future<List<ApiServer>> createServerListFromAssetUrl() async {
    List<ApiServer> serverList = [];

    final String serverListData = await rootBundle.loadString(Constants.ASSET_SERVER_LIST_FILE_NAME);
      Log.d(_TAG, "serverData from asset: $serverListData");
      var data = json.decode(serverListData);

      final ServerResponse serverResponse = await HttpController().sendToServer(SUtils.mapValue(data, ApiServer.URL, ""), "");
      if(serverResponse.isOk("createServerListFromAssetUrl") && serverResponse.bodyMap != null) {
        for(Map<String, dynamic> map in serverResponse.bodyMap!["Server-List"]) {
            ApiServer apiServer = ApiServer("${map["name"]}")
              ..shortCode = SUtils.mapValue(map, "short_code", "") //neu NBT [2023-07-02]
              ..description = SUtils.mapValue(map, "description", "")
              ..keyManagerUrl = SUtils.mapValue(map, "key_manager", "")
              ..isAssetServer = true;
            serverList.add(apiServer);
        }
      }
      return serverList;
  }





  static const URL = "URL";
  static const NAME = "NAME";
  static const COMMUNITY = "COMMUNITY";
  static const IS_LOCAL = "IS_LOCAL";
  static const PUBLIC_KEY_X = "PUBLIC_KEY_X";
  static const PUBLIC_KEY_ED = "PUBLIC_KEY_ED";
  static const PUBLIC_KEY_CONCAT_B64 = "PUBLIC_KEY_CONCAT_B64";
  static const SHORT_CODE = "SHORT_CODE";

  static const ACCOUNTS = "ACCOUNTS";
}


