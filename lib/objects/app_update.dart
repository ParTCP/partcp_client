// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import '/utils/s_utils.dart';

class AppUpdate {
  String version = "0";
  String appDir;
  String appFilename;

  AppUpdate({
    required this.version,
    required this.appDir,
    required this.appFilename
  });

  static AppUpdate fromMap(Map<String, dynamic> map) {
    return AppUpdate(
      version: SUtils.mapValue(map, "App-Revision", 0),
      appFilename: SUtils.mapValue(map, "App-Filename", ""),
      appDir: SUtils.mapValue(map, "App-Dir", ""),
    );
  }
}
