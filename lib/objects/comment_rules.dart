import '../utils/log.dart';
import '../utils/s_utils.dart';

const _TAG = "CommentRules";

class CommentRules {
  /// acceptance integer
  /// 0 = Kommentare sind nicht erlaubt
  /// 1 = ein Kommentar pro Stimmzettel ist erlaubt
  /// 2 = ein Kommentar pro Option ist erlaubt

  /// min_length integer
  /// Mindestlänge von Kommentaren, wenn sie verpflichtend sind   => obsolete

  /// max_length integer
  /// maximale Länge eines Kommentars (Bytes)

  /// mandate_type integer
  /// 0 = Kommentare sind freiwillig
  /// 1 = alle erlaubten Kommentare sind verpflichtend
  /// 2 = optionsbezogene Kommentare sind verpflichtend bei bestimmten Werten

  /// mandate_values array
  /// Liste der Werte, bei denen Kommentare verpflichtend sind

  late int acceptance;
  // late int minLength;
  late int maxLength;
  late int mandateType;
  List<int> mandateValues = [];

  CommentRules(
    {this.acceptance = -1, this.maxLength = 299, this.mandateType = -1});

  static CommentRules? fromMap(Map<String, dynamic> map) {
    Log.d(_TAG, "fromMap: $map");

    CommentRules rules = CommentRules()
      ..acceptance = int.tryParse((SUtils.mapValue(map, "acceptance", "-1").toString())) ?? -1
      // ..minLength = int.tryParse((SUtils.mapValue(map, "min_length", "-1").toString())) ?? -1
      ..maxLength = int.tryParse((SUtils.mapValue(map, "max_length", "-1").toString())) ?? 0
      ..mandateType = int.tryParse((SUtils.mapValue(map, "mandate_type", "-1").toString())) ?? -1;

    if (rules.maxLength == 0) {
      rules.acceptance = 0;
    }

    List<dynamic> list = SUtils.mapValue(map, "mandate_values", []);
    for (var item in list) {
      rules.mandateValues.add(item);
    }
    return rules;
  }
}
