// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

/*
Bei nicht-anonymen Events kommen Teilnehmerkennungen zum Einsatz, die globalen Charakter haben, also nicht nur auf einen Event bezogen sind.
Daher sind die zugehörigen Schlüssel global zu speichern, so als hätte sich der Teilnehmer manuell mit einem Server verbunden.

Dementsprechend kann es sein, dass beim Einlösen des Teilnahmecodes ( lot-request) eine participant_id zurückgeliefert wird,
die sich auf einen anderen Server bezieht als den Event-Server (erkennbar an der Serveradresse hinter dem @-Zeichen).
 */

import 'package:partcp_client/objects/api_server.dart';

class Credential {
  String? participantId;
  String? credential;
  ApiServer? lotServer;
  String? errorMsg;

  Credential(this.participantId, this.credential, this.lotServer);
  Credential.error(this.errorMsg);
}