// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import '/utils/s_utils.dart';

/// for [multiRegistration]
/// Rules to generate [VotingEvent.lotCodes] or [VotingEvent.paperLots]
/// [VotingEvent] can accept one or both

enum CredentialTypeEnum {
  lotCode,
  paperLots
}

const Map<CredentialTypeEnum, String> credentialRulesMap = {
  CredentialTypeEnum.lotCode: "Anonyme Lot-Codes",
  CredentialTypeEnum.paperLots: "Papierlose"
};

class LotCodeRules {
  late String charList;
  late int finalLength;
  late int crcLength;
  late int groupLength;
  late String groupSeparator;
  late String pattern;

  static LotCodeRules fromMap(Map<String, dynamic> map) {
    LotCodeRules defaults = LotCodeDefaults();
    LotCodeRules values = LotCodeRules()
      ..charList = (SUtils.mapValue(map, "char_list", defaults.charList)).toString()
      ..finalLength = SUtils.mapValue(map, "final_length", defaults.finalLength)
      ..crcLength = SUtils.mapValue(map, "crc_length", defaults.crcLength)
      ..groupLength = SUtils.mapValue(map, "group_length", defaults.groupLength)
      ..groupSeparator = SUtils.mapValue(map, "group_separator", defaults.groupSeparator)
      ..pattern = SUtils.mapValue(map, "pattern", "");
    return values;
  }
}

class NamingRules {
  late String charList;
  late int finalLength;
  late int crcLength;
  late int groupLength;
  late String groupSeparator;
  late String pattern;

  static NamingRules fromMap(Map<String, dynamic> map) {
    NamingRules defaults = NamingRulesDefaults();
    NamingRules values = NamingRules()
      ..charList = (SUtils.mapValue(map, "char_list", defaults.charList)).toString()
      ..finalLength = SUtils.mapValue(map, "final_length", defaults.finalLength)
      ..crcLength = SUtils.mapValue(map, "crc_length", defaults.crcLength)
      ..groupLength = SUtils.mapValue(map, "group_length", defaults.groupLength)
      ..groupSeparator = SUtils.mapValue(map, "group_separator", defaults.groupSeparator)
      ..pattern = SUtils.mapValue(map, "pattern", "");
    return values;
  }
}


class CredentialRules{
  late String prefix;
  late int counterWidth;
  late int crcLength;
  late int groupLength;
  late String groupSeparator;
  late String pattern;

  static CredentialRules fromMap(Map<String, dynamic> map) {
    CredentialRulesDefaults defaults = CredentialRulesDefaults();
    CredentialRules values = CredentialRules()
      ..prefix = (SUtils.mapValue(map, "prefix", defaults.prefix)).toString()
      ..counterWidth = SUtils.mapValue(map, "counter_width", defaults.counterWidth)
      ..crcLength = SUtils.mapValue(map, "crc_length", defaults.crcLength)
      ..groupLength = SUtils.mapValue(map, "group_length", defaults.groupLength)
      ..groupSeparator = SUtils.mapValue(map, "group_separator", defaults.groupSeparator)
      ..pattern = SUtils.mapValue(map, "pattern", "");
    return values;
  }
}


/// for anonymous paper_lots request
class LotCodeDefaults implements LotCodeRules {
  String charList = "ABCDEFGHJKLMNPQRSTUVWXYabcdefghijklmnopqrstuvwxyzZ123456789";
  int finalLength = 24;
  int crcLength = 1;
  int groupLength = 4;
  String groupSeparator = "-";
  String pattern = "";
}

/// credential lot as part of paper_lots
class NamingRulesDefaults implements NamingRules{
  String charList = "ABCDEFGHJKLMNPQRSTUVWXYZ123456789";
  int finalLength = 16;
  int crcLength = 1;
  int groupLength = 4;
  String groupSeparator = "-";
  String pattern = "";
}

/// credential naming (user-id) as part of paper_lots
class CredentialRulesDefaults implements CredentialRules{
  String prefix = "p";
  int counterWidth = 4;
  int crcLength = 1;
  int groupLength = 4;
  String groupSeparator = "-";
  String pattern = "";
}