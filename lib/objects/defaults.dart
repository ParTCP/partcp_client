// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

const Map<String, dynamic> defaultMap = {};
const List<String> defaultStringList = [];
