// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import '/utils/s_utils.dart';
import '/utils/yaml_converter.dart';


class Description {

  String description;
  String shortDescription;
  String linkName;
  String linkUrl;

  Description({
    this.shortDescription = "",
    this.description = "",
    this.linkName = "",
    this.linkUrl = "",
  });


  static Description fromJson (Map<String, dynamic> map) {

    return Description()
      ..shortDescription = SUtils.mapValue(map, SHORT_DESCRIPTION, "")
      ..description = SUtils.mapValue(map, DESCRIPTION, "")
      ..linkName = SUtils.mapValue(map, LINK_NAME, "")
      ..linkUrl = SUtils.mapValue(map, LINK_URL, "")
    ;
  }

  Map<String, dynamic> get toJson => {
    DESCRIPTION: description,
    SHORT_DESCRIPTION: shortDescription,
    LINK_URL: linkUrl,
    LINK_NAME: linkName
  };
  

  /// YAML description
  static const SHORT_DESCRIPTION = "short_description";
  static const DESCRIPTION = "description";
  static const LINK_URL = "link_url";
  static const LINK_NAME = "link_name";

}