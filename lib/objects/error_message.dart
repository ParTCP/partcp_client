
import '../utils/s_util.dart';
import 'server_response.dart';

class ErrorMessage{
  final ServerResponse serverResponse;

  String message = "";
  int code = 0;
  String reason = "";
  String rawBody = "";

  ErrorMessage(this.serverResponse) {
    if(serverResponse.errorMessage != null) {

      if (serverResponse.rejectionCode != 0) {
        code = serverResponse.rejectionCode;
      }

      if (serverResponse.rejectionReason.isNotEmpty) {
        reason = serverResponse.rejectionReason;
      }
      else {
        reason = serverResponse.errorMessage!;
      }

      message = reason;
      if (code != 0) {
        message = "$message (Code: $code)";
      }
    }

    if (serverResponse.body != null) {
      rawBody = serverResponse.body!;
    }
  }
}