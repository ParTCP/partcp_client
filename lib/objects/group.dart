// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import '/utils/s_utils.dart';

class Group {
  final String id;
  final String name;
  final bool administrable;

  // List<VotingEvent> events = [];

  const Group ({
    required this.id,
    required this.name,
    required this.administrable,
  });

  static Group fromMap(Map<String, dynamic> map) {
    return Group(
        id: map["id"],
        name: SUtils.mapValue(map, "name", "n.a.").toString(),
        administrable: SUtils.mapValue(map, "administrable", false)
    );
  }

}