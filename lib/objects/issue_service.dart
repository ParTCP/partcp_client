
/*
flutter:   issue_service:
flutter:     host: dev01.partcp.org
flutter:     id_provider: partcp.diebasis.team
flutter:     terms:
flutter:       Gliederung: /^04/
 */

import 'package:partcp_client/utils/s_utils.dart';

import 'api_server.dart';

class IssueService {

  String? host;
  String? idProvider;

  ApiServer? hostServer;
  ApiServer? idProviderServer;

  IssueService({this.host, this.idProvider});

  static IssueService fromMap(Map<String, dynamic> map) {
    return IssueService(
      host: SUtils.mapValue(map, "host", null),
      idProvider: SUtils.mapValue(map, "id_provider", null),
    );
  }

}