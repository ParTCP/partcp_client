// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import '/utils/s_utils.dart';

/*
  Register on Key-Cloak
 */

class KeyData {
  String? participantId;
  String? id;
  String? credential;
  
  /// SESSION DATA
  String? participantStatus;
  String? urlStatus;
  String? urlLogout;
  String? urlRegister;
  String? urlRenewal;
  bool emailVerified = false;
  String? email;

  static KeyData? fromMap(Map<String, dynamic> map) {
    KeyData km = KeyData()
      ..participantId = SUtils.mapValue(map, "participant_id", null)
      ..participantStatus = SUtils.mapValue(map, "participant_status", null)
      ..urlLogout = SUtils.mapValue(map, "url_logout", null)
      ..urlRegister = SUtils.mapValue(map, "url_register", null)
      ..urlRenewal = SUtils.mapValue(map, "url_renewal", null);

    Map<String, dynamic>? profile = SUtils.mapValue(map, "profile", null);
    if (profile != null) {
      km.id = SUtils.mapValue(profile, "id", null).toString();
      km.emailVerified = SUtils.mapValue(map, "email_verified", false);
      km.email = SUtils.mapValue(map, "email", null);
    }

    return km.participantId != null && km.id != null
        ? km
        : null;
  }

  static const STATUS_UNREGISTERED = "unregistered";
  static const STATUS_INCOMPLETE = "incomplete";
  static const STATUS_COMPLETE = "complete";
}

