// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2024. All rights reserved

import 'dart:convert';

import 'package:cryptography/cryptography.dart' as ecLib;
import 'package:hive/hive.dart';
import '/utils/crypt/my_aes_crypt.dart';
import '/utils/crypt/my_ec_crypt.dart';
import '/utils/s_utils.dart';

import '../utils/log.dart';
import 'account.dart';

const _TAG = "Participant";

/*
participant-details-request, response
{
  "id":"l67826cb64417eb177",
  "credential":"6ca13d52ca70c883e0f0bb101e425a89e8624de51db2d2392593af6a84118090",
  "attributes":{
    "first_name":"Klaus",
    "last_name":"Munck",
    "zip":71334,
    "city":"Waiblingen"
  },
  "credential_permanence":true,
  "public_key":"LA\/\/H0kLrxyP2qsd2jgeBQvjj4eaza9+QUV9rp9YajFZ4xwij\/2b7D+pc2aD04KOalgWyQzjMlMqgddU79ICPA=="
}
 */


class Participant {

  String? id;
  String? credentialHash;
  String? credentialDec;
  bool? credentialPermanence;
  String? publicKey;

  _Attributes? attributes;


  Participant({
    this.id,
    this.credentialHash,
    this.credentialDec,
    this.credentialPermanence,
    this.publicKey,
    this.attributes
  });

  factory Participant.fromMap(Map<String, dynamic> map) {
    Log.d(_TAG, "fromMap: ${map}");

    _Attributes? attributes;
    if (map.containsKey("attributes")) {
      Map<String, dynamic>? attrib = SUtils.mapValue(map, "attributes", null);
      if (attrib != null) {
        attributes = _Attributes.fromMap(map["attributes"]);
      }
    }

    Participant m = Participant(
      id: SUtils.mapValue(map, "id", "na"),
      credentialHash: SUtils.mapValue(map, "credential", null),
      credentialDec: SUtils.mapValue(map, "credentialDec", null),
      credentialPermanence: SUtils.mapValueBool(map, "credential_permanence", false),
      publicKey: SUtils.mapValue(map, "public_key", false),
      attributes: attributes
    );
    return m;
  }

  String toJson() {
    Map<String, dynamic> map =  {
      "id": id,
      "credential": credentialHash,
      "credentialDec": credentialDec,
      "credential_permanence": credentialPermanence,
      "public_key": publicKey,
    };
    if (attributes != null) {
      map["attributes"] = attributes!.toMap();
    }
    return json.encode(map);
  }

}

class _Attributes {
  String firstName;
  String lastName;
  String zip;
  String city;
  String domainCode;
  String placeBuilding;
  String placeStreet;
  String placePostalCode;
  String placeCity;

  _Attributes ({
    required this.firstName,
    required this.lastName,
    required this.zip,
    required this.city,
    required this.domainCode,
    required this.placeBuilding,
    required this.placeStreet,
    required this.placePostalCode,
    required this.placeCity,
  });

  factory _Attributes.fromMap(Map<String, dynamic> map) {

    String fnKey = "first_name";
    String lnKey = "last_name";

    if (map.containsKey("firstname")) {
      fnKey = "firstname";
      lnKey = "lastname";
    }

    return _Attributes(
      // firstName: SUtils.mapValue(map, "first_name", ""),
      // lastName: SUtils.mapValue(map, "last_name", ""),
      firstName: SUtils.mapValue(map, fnKey, ""),
      lastName: SUtils.mapValue(map, lnKey, ""),
      zip: (SUtils.mapValue(map, "zip", "")).toString(),
      city: (SUtils.mapValue(map, "city", "")).toString(),
      domainCode: (SUtils.mapValue(map, "domain_code", "")).toString(),
      placeBuilding: (SUtils.mapValue(map, "place_building", "")).toString(),
      placeStreet: (SUtils.mapValue(map, "place_street", "")).toString(),
      placePostalCode: (SUtils.mapValue(map, "place_postal_code", "")).toString(),
      placeCity: (SUtils.mapValue(map, "place_city", "")).toString(),
    );
  }

  Map<String, dynamic> toMap() {
    return {
      "first_name": firstName,
      "last_name": lastName,
      "zip": zip,
      "city": city,

      "domain_code": domainCode,
      "place_building": placeBuilding,
      "place_street": placeStreet,
      "place_postal_code": placePostalCode,
      "place_city": placeCity,
    };
  }
}
