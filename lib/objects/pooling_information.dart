// Author: Aleksander Lorenz
// partcp-client@ogx.de


import '/utils/s_utils.dart';

class PoolingInformation {
  final int changeCount;
  final int pollingInterval;
  final String pollingUrl;


  const PoolingInformation ({
    required this.changeCount,
    required this.pollingInterval,
    required this.pollingUrl,
  });

  static PoolingInformation fromMap(Map<String, dynamic> map, {String? knowUrl}) {
    return PoolingInformation(
        changeCount: map["change_count"],
        pollingInterval: map["polling_interval"],
        pollingUrl: knowUrl != null ? knowUrl : map["polling_url"],
    );
  }

}