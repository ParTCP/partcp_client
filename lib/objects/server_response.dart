// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import '../utils/s_util.dart';
import '/utils/log.dart';
import '/utils/yaml_converter.dart';

class ServerResponse {

  ServerResponse(){
    uuid = SUtil.createUuid();
  }

  String uuid = "";
  String? request;
  int? statusCode;
  String? statusText;
  String? body;
  String? publicKey;
  Map<String, dynamic>? bodyMap;
  bool canceled = false;

  String? _errorMessage;
  DateTime? responseTime;

  bool signatureIsCorrect = false;
  bool receivingData = false;

  final _TAG = "ServerResponse";

  int rejectionCode = 0;
  String rejectionReason = "";

  set errorMessage(String? message) {
    uuid = SUtil.createUuid();
    _errorMessage = message;
    statusCode ??= message == null
        ? null
        : 400;
  }

  String? get errorMessage => _errorMessage != null
      ? "[ERROR]: $_errorMessage"
      : null;

  // isOk(...) checks if the 'serverResponse' is Ok according to the intention of
  // the issue #29 in https://codeberg.org/ParTCP/partcp_client/issues/29
  // The optional parameter 'loc' (for location) is used for debug purposes only (may be removed later on).
  bool isOk([String loc = ""]){

    if (statusCode == null){
      errorMessage = "statusCode is Null!";
      Log.w(_TAG, "ServerResponse.isOk() - statusCode is Null! - loc:$loc");
      return false;
    }
    if (statusCode != 200){ //ToDo: statusCode within a range?
      errorMessage = "statusCode is: $statusCode!";
      Log.w(_TAG, "ServerResponse.isOk() - statusCode is: $statusCode! - loc:$loc");
      return false;
    }

    if (body == null){
      errorMessage = "body is Null!";
      Log.w(_TAG, "ServerResponse.isOk() - body is Null! - loc:$loc");
      return false;
    }

    // fillBodyMap(body); //throws an Exception in case of an error, result: bodyMap is null

    if (bodyMap == null){
      errorMessage = "bodyMap is Null!";
      Log.w(_TAG, "ServerResponse.isOk() - bodyMap is Null! - loc:${loc}");
      return false;
    }

    // if (errorMessage != null) {
    //   Log.w(_TAG, "$errorMessage");
    //   return false;
    // }

    if(bodyMap!.containsKey('Message-Type') == true){

      if(bodyMap!['Message-Type'] == 'rejection-notice'){
        rejectionCode = bodyMap!['Rejection-Code'];
        rejectionReason = bodyMap!['Rejection-Reason'];
        errorMessage = "Server response: $rejectionReason ($rejectionCode)";
        Log.w(_TAG, "Rejection-Reason: $rejectionReason ($rejectionCode)");
        return false;
      }

      if(bodyMap!['Message-Type'] == 'failure-notice'){
        errorMessage = "ServerResponse: Failure-Description: ${bodyMap!['Failure-Description']}";
        Log.w(_TAG, "ServerResponse: .isOk() - Message-Type: 'failure-notice' found! - Description: ${bodyMap!['Failure-Description']} - loc:$loc");
        return false;
      }
    }
    else{
      String msg = "No Message-Type found... - loc:${loc}";
      Log.w(_TAG, "${msg}");
    }
    return true;
  }

  // fillBodyMap(...) decodes the raw 'bodyString' to a Map or list and
  // stores the result in bodyMap
  //ToDo: alter return value to void?
  Map<String, dynamic>? fillBodyMap(String? bodyString){

    bodyMap = null;

    try {
      YamlConverter yamlConverter = YamlConverter();
      dynamic result = yamlConverter.toDynamicUnCatch(bodyString!);
      // dynamic result = yamlConverter.toMap(bodyString!);
      if (result is Map) {
        bodyMap = result as Map<String, dynamic>;
        if (bodyMap!.containsKey("Public-Key")){
          publicKey = bodyMap!["Public-Key"];
        }
      }
      else if (result is List) {
        bodyMap = {"list": result};
      }
      else {
        throw Exception('Wrong Server Response, ist weder Liste noch Map');
      }
    } catch (e, s) {
      errorMessage = "Could not convert server's YAML message:\n $e.toString(); s: $s";
      statusCode = 999;
    }
    return bodyMap;
  }

  /// we can append some data to it
  dynamic object;

}


