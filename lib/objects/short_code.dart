import '../utils/log.dart';

const _TAG = "ShortCode";

class ShortCode {
  String shortCode = "";

  String serverCode = "";
  String serverUrl = "";
  String eventCode = "";
  String lotCode = "";

  int shortCodeRev = 1; /// 1: old, 2: ab 28.02.24


  ShortCode (String code) {
    code = code.trim();

    /// Lot Code
    if (code.contains("@")){
      var splitted = code.split("@");
      lotCode = splitted.first;
      code = splitted.last;
    }

    /// Event and Server
    if (code.isNotEmpty) {

      /// Neue Schreibweise lang
      /// 49NM-RV5K-YY7F-SWPN@demo.partcp.org/demo/20240222-demoveranstaltung

      /// Neue Schreibweise kurz:
      /// 49NM-RV5K-YY7F-SWPN@H/gNf

      /// Alte Schreibweise:
      /// 49NM-RV5K-YY7F-SWPN@gNf.H

      /// Neue Schreibweise
      if (code.contains("/")) {
        var splitted = code.split("/");
        /// server url

        /// Neue Schreibweise kurz
        if (splitted.length == 2) {
          serverCode = splitted[0];
          eventCode = "#${splitted[1]}";
          Log.d(_TAG, "Neue Schreibweise kurz; serverCode:$serverCode, eventCode:$eventCode");
        }
        /// Neue Schreibweise lang
        else if (splitted.length == 3) {
          serverUrl = "https://${splitted[0]}";
          eventCode = "${splitted[1]}/${splitted[2]}";
          Log.d(_TAG, "Neue Schreibweise lang; serverUrl:$serverUrl, eventCode:$eventCode");
        }
      }
      /// Alte Schreibweise
      else if (code.contains(".")) {
        var splitted = code.split(".");
        eventCode = "#${splitted[0]}";
        serverCode = splitted[1];
        Log.d(_TAG, "Alte Schreibweise; serverCode:$serverCode, eventCode:$eventCode");
      }

    }
  }

}