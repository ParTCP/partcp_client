// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:hive/hive.dart';
import '/controller/main_controller.dart';

import '/utils/log.dart';

enum AppBoxEnum {
  lastWallet,
  // multiUserMode,
  extendedAppMode,
  slowMachine,
  textScaleValue,
  showTextScaleWidget,
}

class AppBox {
  static const _TAG = "AppBox";
  late Box _box;


  Future<void> setValue(AppBoxEnum key, dynamic value) async {
    await _box.put(key.toString(), value);
  }

  dynamic getValue(AppBoxEnum key, {dynamic defValue}) {
    var result = _box.get(key.toString());

    return result ?? defValue;
  }

  Future<void> initBox(String boxName) async{
    Log.d(_TAG, "initBox; boxName: $boxName");
    try {
      _box = await Hive.openBox(boxName, path: MainController.to.appDataPath);
    }
    catch(e) {
      Log.w(_TAG, "Cannot open $boxName in ${MainController.to.appDataPath}");
    }
  }
}