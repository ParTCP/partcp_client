// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'dart:convert';

import '/utils/s_utils.dart';

import '../utils/log.dart';

const _TAG = "UrlParams";

class UrlParams {
  String shortCode = "";

  UrlParams();

  /// partcp://partcp.org?c=C32TQA7NVQWDVA6684GTX2TTM7TV22HR@aic.d

  static UrlParams? fromString(String uriParams) {
    String params = uriParams.trim();
    Log.d(_TAG, "UrlParams fromUri: $params");

    /// on web uriParams may contains additional signs like %23/ or #/
    params = _cleanUriParams(params);
    
    Uri uri = Uri.dataFromString(params);
    
    Map<String, String> paramsMap = uri.queryParameters;
    UrlParams urlParams = UrlParams()
      ..shortCode = SUtils.mapValueUrlDecoded(paramsMap, "c", "");

    /// on Desktop
    if (urlParams.shortCode.isEmpty) {
      urlParams.shortCode = params;
    }

    Log.i(_TAG, "UrlParams: $urlParams");
    return urlParams;
  }
  
  static String _cleanUriParams (String uriParams) {
    String params = uriParams;
    params = _removeFromParamsBy(params, "%23/");
    params = _removeFromParamsBy(params, "#/");
    return params;
  }
  
  static String _removeFromParamsBy(String uriParams, String toRemove) {
    return uriParams.split(toRemove)[0];
  }

  @override
  String toString() {
    return {
      "shortCode": shortCode
    }.toString() ;
  }
}