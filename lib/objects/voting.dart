// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:flutter/material.dart';
import 'package:partcp_client/objects/description.dart';
import '/controller/http_controller.dart';
import '/message_type/message_type.dart';
import '/message_type/_voting_definition.dart';
import '/objects/account.dart';
import '/objects/server_response.dart';
import '/objects/_client_data.dart';
import '/objects/voting_event.dart';
import '/objects/voting_option.dart';
import '/objects/voting_result.dart';
import '/objects/api_server.dart';
import '/objects/voting_type.dart';
import '/utils/log.dart';
import '/utils/s_utils.dart';
import '/utils/yaml_converter.dart';

import 'package:collection/collection.dart';

import 'comment_rules.dart';
import 'defaults.dart';
import 'voting_comment.dart';

class Voting {
  static const _TAG = "Voting";

  static const int NO_VOTE_VALUE = -100;


  String? id;
  String name = "";
  String title = "";
  String status = "";

  Description description = Description();
  String division = "";

  VotingType votingType = VotingType(VotingTypeEnum.n_a);

  List<VotingOption> votingOptions = [];

  DateTime? createdOn;
  DateTime? modifiedOn;
  String createdBy = "";
  String modifiedBy = "";

  CommentRules? commentRules;
  VotingComment? votingComment;

  String? confirmationText;


  DateTime? periodStart;
  DateTime? periodEnd;

  bool singleVoteOnly = false;

  /// nur bei anicht-anonymen Votings (???)
  /// Liste der Votings (id's), welche zuvor erledigt seinen müssen, bevor dieses Voting gesendet werden darf
  /// Um herauszufinden, bei welchen Abstimmungen ein Teilnehmer bereits seine Stimme abgegeben hat,
  /// kann der event-details-request mit Include-Completed-Votings: true erweitert werden.
  /// In der Antwort wird dann in dem neuen Element Completed-Votings die Liste der betreffenden Abstimmungen mitgeliefert.
  var confirmSegmentResults;
  VotingResult? votingResult;
  List<String> prerequisites = [];

  late String votingListTitle;


  // List<Vote> votes = [];

  // String get typeTitle => VotingType.title(type);

  String get statusText {
    if (status == STATUS_OPEN) {
      return "aktiv";
    } else if (status == STATUS_CLOSED) {
      return "beendet";
    } else if (status == STATUS_FINISHED) {
      return "ausgezählt";
    } else {
      return "vorbereitet";
    }
  }

  Icon get statusIcon {
    IconData data;
    Color color;

    if (status == STATUS_OPEN) {
      data = Icons.stop_circle_outlined;
      color = Colors.red;
    }

    /// closed and waiting for generating voting stat
    else if (status == STATUS_CLOSED) {
      data = Icons.check_circle_outline;
      color = Colors.green;
    }

    /// closed and waiting for generating voting stat
    else if (status == STATUS_FINISHED) {
      data = Icons.insert_chart_outlined;
      // color = Colors.green;
      color = Colors.black54;
    }

    /// IDLE
    else if (status == STATUS_IDLE) {
      data = Icons.play_circle_fill;
      color = Colors.black45;
    }

    /// unknown
    else {
      data = Icons.device_unknown;
      color = Colors.black45;
    }

    return Icon(
      data,
      color: color,
    );
  }

  static Voting fromMap(Map<String, dynamic> map) {

    VotingType? votingType;
    String? typeString = SUtils.mapValue(map, "type", null);
    VotingTypeEnum? typeTest = VotingType.typeTest(typeString);
    votingType = typeTest != null
        ? VotingType(typeTest)
        : VotingType(VotingTypeEnum.n_a);

    Voting voting = Voting()
      ..id = SUtils.mapValue(map, ID, "").trim()
      ..name = (SUtils.mapValue(map, NAME, "")).toString()
      ..title = (SUtils.mapValue(map, TITLE, "n.a")).toString()
      ..status = (SUtils.mapValue(map, STATUS, "n.a")).toString()
      ..votingType = votingType
      ..description = Description.fromJson(map)
      ..createdBy = (SUtils.mapValue(map, "created_by", "n.a")).toString()
      ..modifiedBy = (SUtils.mapValue(map, "modified_by", "n.a")).toString()
      ..votingOptions = []
      ..periodStart = SUtils.mapParseDateTime(map, PERIOD_START, null)
      ..periodEnd = SUtils.mapParseDateTime(map, PERIOD_END, null)
      ..createdOn = SUtils.mapParseDateTime(map, "created_on", null)
      ..modifiedOn = SUtils.mapParseDateTime(map, "modified_on", null)
      ..confirmationText = (SUtils.mapValue(map, "confirmation_text", null))
      ..singleVoteOnly = SUtils.mapValueBool(map, "single_vote_only", false)

      ..confirmSegmentResults = (SUtils.mapValue(map, "confirm_segment_results", null))
      ..votingResult = VotingResult.fromMap(SUtils.mapValue(map, "voting_result", defaultMap))

    ;

    /// OPTIONS
    /// server can response not all options for a voting
    if (map.containsKey(VotingOption.OPTIONS)) {
      if (map[VotingOption.OPTIONS] is List<dynamic>) {
        List<dynamic> optionsList = map[VotingOption.OPTIONS];
        for (var i = 0; i < optionsList.length; i++) {
          try {
            voting.votingOptions.add(VotingOption.fromMap(optionsList[i], votingType));
          } catch (e, s) {
            Log.e(_TAG, "Voting fromMap -> create votingOptions -> error: $e: Stack: $s");
          }
        }
      }
    }

    if (map.containsKey("comment_rules")) {
      voting.commentRules = CommentRules.fromMap(map["comment_rules"]);
      voting._initComments();
    }

    if (map.containsKey("prerequisites")) {
      List<dynamic> items = map["prerequisites"];
      for (String item in items) {
        voting.prerequisites.add(item);
      }

    }

    return voting;
  }


  /// LOAD FROM STRING
  static Voting? fromYamlString(String votingYaml) {
    try {
      Map<String, dynamic> votingMap = YamlConverter().toMap(votingYaml);

      if (votingMap.containsKey(VOTING_DATA)) {
        Voting _voting = Voting.fromMap(votingMap[VOTING_DATA]);
        _voting.id = SUtils.mapValue(votingMap, "Voting-Id", null);
        if (_voting.id != null) {
          _voting.id!.trim();
        }
        return _voting;
      }
    } catch (e, s) {
      Log.e(_TAG, "createFromYamlString; error: $e; stack:$s");
    }
    return null;
  }


  /// NEW OR UPDATE
  // Future<ServerResponse> sendDefinitionOrUpdateRequest(ApiServer server, Account account, VotingEvent event) async {
  //   final Map<String, dynamic> messageMap =
  //       id == null ? votingDefinition(event: event, voting: this) : votingUpdateRequest(event: event, voting: this);
  //
  //   ServerResponse serverResponse = await HttpController().serverRequestSigned(account, messageMap, server, "sendDefinitionOrUpdateRequest");
  //   return serverResponse;
  // }


  /// SEND STATUS CHANGE REQUEST
  // Future<ServerResponse> sendStatusChangeRequest(ApiServer server, VotingEvent event, Account account) async {
  //   Map<String, dynamic>? messageMap;
  //
  //   if (status == STATUS_IDLE) {
  //     messageMap = votingStartDeclaration(event: event, voting: this);
  //   } else if (status == STATUS_OPEN) {
  //     messageMap = votingEndDeclaration(event: event, voting: this);
  //   } else if (status == STATUS_CLOSED) {
  //     messageMap = voteCountRequest(event: event, voting: this);
  //   }
  //
  //   assert(messageMap != null);
  //   ServerResponse serverResponse = await HttpController().serverRequestSigned(account, messageMap!, server, "sendStatusChangeRequest");
  //
  //   // if (status == STATUS_CLOSED) {
  //   //   votingResult = VotingResult.fromMap(SUtils.mapValue(serverResponse.bodyMap, "Voting-Result", defaultMap));
  //   // }
  //   return serverResponse;
  // }


  /// if for [Voting] or [VotingOption] are Comments allowed, we add a defined [VotingComment] to the Object
  /// for oll other are Comments not allowed
  void _initComments() {
    if (commentRules != null) {
      switch (commentRules!.acceptance) {
      /// no comments allowed
        case 0:
          return;

      /// ony for event
        case 1:
          _addVotingCommentToEvent();
          break;

      /// only for options
        case 2:
          _addVotingCommentToOptions();
          break;

      /// voting and options
        case 3:
          _addVotingCommentToEvent();
          _addVotingCommentToOptions();
          break;
      }
    }
  }

  void _addVotingCommentToEvent() {
    votingComment = VotingComment()..textController = TextEditingController();
  }

  void _addVotingCommentToOptions() {
    /// loop over VotingOptions
    for (VotingOption option in votingOptions) {
      VotingComment optionComment = VotingComment()..textController = TextEditingController();
      option.votingComment = optionComment;
      option.votingComment!.commentRequired = commentRules!.mandateType == 1;
      option.votingComment!.boxIsOpen = option.votingComment!.commentRequired;
    }
  }



  int? vote(VotingOption option) {
    return votingOptions.firstWhereOrNull((VotingOption element) => element.id == option.id)?.voteValue;
  }

  void clearOptionsData() {
    for (VotingOption option in votingOptions) {
      option.clearClientVote();
    }
  }


  // /// voting for consensus, yes-no
  // void setVoteValue(VotingOption option, int value) {
  //  votingOptions.firstWhere((element) => element.id == id).voteValue = value;
  //  option.votingComment!.commentRequired = _isCommentRequiredForOption(option);
  //
  // }
  //
  // bool _isCommentRequiredForOption (VotingOption option) {
  //   /// [CommentRules].mandateType == 2 && [VotingOption].value in [CommentRules].mandateValues
  //   if (commentRules!.mandateType == 2) {
  //     int? voteValue = (votingOptions.firstWhereOrNull((element) => element == option))?.voteValue;
  //     if (voteValue != null) {
  //       bool result = commentRules!.mandateValues.contains(voteValue);
  //       Log.d(_TAG, "isCommentRequiredForOption: $result");
  //       return result;
  //     }
  //   }
  //   return false;
  // }

  /// voting for multiple-choice
  bool setVoteMulti(VotingOption option, bool selected) {
    bool success = false;
    if (selected) {
      Iterable<VotingOption> _votes = votingOptions.where((element) => element.voteValue == 1);
      Log.d(_TAG, "setVoteMulti; selected: $selected; votes: ${_votes.length};");

      // if (_votes.length < VotingType.getMultipleChoiceCountFromType(type!)) {
      if (_votes.length < votingType.multipleChoiceCount) {
        option.voteValue = 1;
        success = true;
      } else {
        success = false;
      }
    } else {
      VotingOption? vote = votingOptions.firstWhereOrNull((element) => element.id == id);
      if (vote != null) {
        vote.voteValue = 0;
      }
      success = true;
    }
    return success;
  }


  /// voting for single choice
  void setVoteSingle(VotingOption option) {
    for (VotingOption vote in votingOptions) {
      vote.voteValue = 0;
    }
    option.voteValue = 1;
  }


  /// /////////////////////////////////////////////////////////////
  /// ballot
  Map<String, dynamic> votingOptionToMap (VotingOption option) {
    Map<String, dynamic> vote = {
      "id": option.id,
      "vote": option.voteValue
    };

    /// for text, photo, attachment, etc
    if (option.voteData != null) {
      vote["data"] = option.voteData;
    }


    if (commentRules != null && commentRules!.acceptance >= 2) {
      vote["comment"] = option.votingComment!.boxIsOpen ? option.votingComment!.textController!.text.trim() : "";
    }
    return vote;
  }

  String? get ballotVotingComment {
    if (commentRules != null && commentRules!.acceptance == 1 || commentRules != null && commentRules!.acceptance == 3) {
      if (votingComment!.boxIsOpen) {
        return votingComment!.textController!.text.trim();
      }
    }
    return null;
  }




  /// List Key from [msgTypeVotingListRequest]
  static const VOTINGS = "Votings";

  /// List Key from [msgTypeVotingDetailsRequest]
  static const VOTING_DATA = "Voting-Data";

  static const ID = "id";
  static const NAME = "name";
  static const TITLE = "title";
  static const STATUS = "status";
  static const DIVISION = "division";
  static const TYPE = "type";
  static const PERIOD_START = "period_start";
  static const PERIOD_END = "period_end";

  static const STATUS_IDLE = "idle";
  static const STATUS_OPEN = "open";
  static const STATUS_CLOSED = "closed";
  static const STATUS_FINISHED = "finished";

  /// none anonymous
  static const CONFIRM_SEGMENT_RESULTS = "confirm_segment_results";
  static const VOTING_RESULT = "voting_result";
}

enum VotingStatusEnum { open, closed, idle, all }
