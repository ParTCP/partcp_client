// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

class VotingAdmin {
  final String admin;

  VotingAdmin(this.admin);

  static const ADMINS = "admins";

}