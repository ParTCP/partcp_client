import 'package:flutter/cupertino.dart';

import '../utils/log.dart';
import 'comment_rules.dart';
import 'voting_option.dart';

const _TAG = "VotingComment";

class VotingComment {

  TextEditingController? textController;

  bool boxIsOpen = false;
  bool commentRequired = false;

  bool commentRequiredForOption (CommentRules rules, VotingOption option) {
    /// [CommentRules].mandateType == 2 && [VotingOption].value in [CommentRules].mandateValues
    if (rules.mandateType == 2 && option.voteValue != null) {
      bool result = rules.mandateValues.contains(option.voteValue);
      // Log.d(_TAG, "isCommentRequiredForOption: $result");
      return result;
    }
    return false;
  }

}