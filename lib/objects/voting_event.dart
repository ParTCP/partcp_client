// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved


import 'package:darq/darq.dart';
import 'package:partcp_client/objects/pooling_information.dart';

import '/controller/http_controller.dart';
import '/message_type/message_type.dart';
import '/objects/_client_data.dart';
import '/objects/credential_rules.dart';
import '/objects/short_code.dart';
import '/objects/voting.dart';
import '/objects/api_server.dart';
import '/utils/log.dart';
import '/utils/s_utils.dart';

import 'package:collection/collection.dart';
import 'account.dart';
import 'defaults.dart';
import 'description.dart';
import 'issue_service.dart';
import 'server_response.dart';
import 'voting_count.dart';

/// will be build from server info, is not stored in wallet

enum VotingStringEnum{
  voting_list_title
}

class VotingEvent {
  static const _TAG = "VotingEvent";

  /// Event [id] will be overwritten when the event is created with a [ShortCode.eventId]
  String id;
  String name = "";
  DateTime? date = DateTime.now();

  DateTime? createdOn;
  DateTime? modifiedOn;
  bool isNonAnonymous = false;

  int estimatedTurnout = -1;

  late NamingRules namingRules;
  late LotCodeRules lotCodeRules;

  late CredentialRules credentialRules; /// todo wird das noch benötigt?

  Description description;
  // List<String> admins = [];

  /// from event-details-request [Votings]
  List<Voting> votings = [];

  /// from event-details-request [Participants]
  int lotsCreated = 0;
  int lotsRedeemed = 0;
  int keysSubmitted = 0;
  bool lotCodeDeposited = false;

  ApiServer server;

  late VotingCount votingCount;

  PoolingInformation? poolingInformation;
  IssueService? issueService;

  /// for none anonymous Events
  /// Um herauszufinden, bei welchen Abstimmungen ein Teilnehmer bereits seine Stimme abgegeben hat,
  /// kann der event-details-request mit Include-Completed-Votings: true erweitert werden.
  /// In der Antwort wird dann in dem neuen Element Completed-Votings die Liste der betreffenden Abstimmungen mitgeliefert.
  /// AL, 2025-02-01
  List<String> completedVotings = [];


  Map<String, dynamic> strings = {};

  VotingEvent(this.id, this.server, {this.isNonAnonymous = false, required this.description});


  String get group => id.split("/").first;
  String get idWoGroup => id.split("/").last;



  /// ////////////////////////////////////////////////////////
  /// from Wallet Map
  static VotingEvent fromWalletMap ({required Map<String, dynamic> map, required ApiServer apiServer}) {
    VotingEvent votingEvent = VotingEvent(
        (SUtils.mapValue(map, ID, "")).toString(),
        apiServer,
        description: Description()
    );

    votingEvent.name = map[NAME];

    if (map.containsKey(COMPLETED_VOTINGS) && map[COMPLETED_VOTINGS] != null) {
      List<dynamic> items = map[COMPLETED_VOTINGS];
      for(var item in items) {
        votingEvent.completedVotings.add(item.toString());
      }
    }

    if (map.containsKey(NONE_ANONYMOUS_EVENT)) {
      votingEvent.isNonAnonymous = map[NONE_ANONYMOUS_EVENT];
    }
    return votingEvent;
  }


  String string (VotingStringEnum votingStringEnum, {String? ifEmpty}) {
    if (strings.containsKey(votingStringEnum.name)) {
      return strings[votingStringEnum.name];
    }

    return ifEmpty ?? "";
  }



  /// ////////////////////////////////////////////////////////
  /// to Wallet Map
  Map<String, dynamic> toWalletMap () {
    Map<String, dynamic> map = {
      ID: id,
      NAME: name,
      COMPLETED_VOTINGS: completedVotings
    };

    if (isNonAnonymous) {
      map[NONE_ANONYMOUS_EVENT] = isNonAnonymous;
    }
    return map;
  }




  static Future<VotingEvent> fromMap(Map<String, dynamic> map, VotingEvent? event, ApiServer server) async {
    if (event == null) {
      event = VotingEvent(
          map["id"].trim(), 
          server,
          description: Description()
      );
    }
    event
      ..id = (SUtils.mapValue(map, "id", "")).toString()
      ..name = (SUtils.mapValue(map, "name", "")).toString()
      ..estimatedTurnout = SUtils.mapValue(map, "estimated_turnout", 0)
      ..lotCodeDeposited = SUtils.mapValue(map, "lot_code_deposited", false)
      ..date = SUtils.mapParseDateTime(map, "date", null)
      ..createdOn = SUtils.mapParseDateTime(map, "created_on", null)
      ..modifiedOn = SUtils.mapParseDateTime(map, "modified_on", null)
      ..namingRules = NamingRules.fromMap(SUtils.mapValue(map, "naming_rules", defaultMap))
      ..lotCodeRules = LotCodeRules.fromMap(SUtils.mapValue(map, "lot_code_rules", defaultMap))
      ..credentialRules = CredentialRules.fromMap(SUtils.mapValue(map, "credential_rules", defaultMap))
      ..votingCount = VotingCount.fromMap(SUtils.mapValue(map, "voting_count", defaultMap))
      ..issueService = IssueService.fromMap(SUtils.mapValue(map, "issue_service", defaultMap))
      ..isNonAnonymous = SUtils.mapValueBool(map, "is_non_anonymous", false)
      ..description = Description.fromJson(map)
    ;

    if (map.containsKey("strings")) {
      event.strings = SUtils.mapValue(map, "strings", defaultMap);
    }


    if (event.issueService != null && event.issueService!.host != null) {
      event.issueService!.hostServer = ApiServer("${event.issueService!.host!}");
      await event.issueService!.hostServer!.serverDetailsRequest();
    }

    if (event.issueService != null && event.issueService!.idProvider != null) {
      event.issueService!.idProviderServer = ApiServer("${event.issueService!.idProvider!}");
      await event.issueService!.idProviderServer!.serverDetailsRequest();
    }

    /// we get "open_votings" key, when we call [eventListRequest] with includeOpenVotings = true
    List<dynamic>? openVotings = SUtils.mapValue(map, "open_votings", null);
    if (openVotings != null) {
      Log.d(_TAG, "openVotings: $openVotings");
      for (Map<String, dynamic> map in openVotings ) {
        Voting v = Voting.fromMap(map);
        event.votings.add(v);
      }
    }

    // try {
    //   event.clientData = ClientData.fromYaml(SUtils.mapValue(map, ClientData.CLIENT_DATA, null));
    // } catch (e, s) {
    //   Log.w(_TAG, "fromMap; error: $e => Setting clientData = ClientData(), s: $s");
    //   event.clientData = ClientData();
    // }

    /// ADMINS
    // var tmpMap = SUtils.mapValue(map, "admins", defaultStringList);
    // if (tmpMap is List<String>) {
    //   event.admins.addAll(tmpMap);
    // }

    return event;
  }

  // void _sortVotingList() {
  //   if (votings.isNotEmpty) {
  //     for (var newIndex = 0; newIndex < clientData!.votingSortOrder.length; newIndex++) {
  //       Voting? item = votings.firstWhereOrNull((element) => element.id == clientData!.votingSortOrder[newIndex]);
  //       if (item != null) {
  //         int oldIndex = votings.indexOf(item);
  //
  //         if (newIndex > oldIndex) {
  //           newIndex -= 1;
  //         }
  //         votings.removeAt(oldIndex);
  //         votings.insert(newIndex, item);
  //       }
  //     }
  //     updateClientDataVotingOrder();
  //   }
  // }


  List<String>  unmetPrerequisites(Voting voting) {
    List<String> items = [];
    for (String id in voting.prerequisites) {
      // if ( (completedVotings != null && !completedVotings!.contains(id) ) || completedVotings == null ) {
      if ( !completedVotings.contains(id)  ) {
        items.add(id);
      }
    }
    return items.distinct().toList();
  }

  bool votingIsBlocked (Voting voting) {
    if (unmetPrerequisites(voting).isNotEmpty) {
      return true;
    }
    if (voting.singleVoteOnly && completedVotings.contains(voting.id)) {
      return true;
    }
    return false;
  }

  // void updateClientDataVotingOrder() {
  //   List<String> votingList = [];
  //   for (var element in votings) {
  //     votingList.add(element.id!);
  //   }
  //   clientData!.votingSortOrder = votingList;
  // }

  /// UPDATE CLIENT-DATA (voting sort order)
  // Future<ServerResponse> sendUpdateRequestClientData(
  //     ApiServer server, Account account, VotingEvent event) async {
  //
  //   final Map<String, dynamic> messageMap = eventUpdateRequestVotingSortOrder(this);
  //
  //   ServerResponse serverResponse =
  //   await HttpController().serverRequestSigned(account, messageMap, server, loc: "sendUpdateRequestClientData");
  //   return serverResponse;
  // }


  /// EVENT DETAILS REQUEST
  Future<ServerResponse> eventDetailsRequest(ApiServer server, Account account, {bool? includeCompletedVotings = false}) async {
    final _messageMap = msgTypeEventDetailsRequest(this, includeCompletedVotings: includeCompletedVotings);
    final ServerResponse serverResponse = await HttpController().serverRequest(_messageMap, account, server);

    if (!serverResponse.isOk("sEventDetailsRequest")) {
      return serverResponse;
    }

    Map<String, dynamic> eventDataMap = {};

    final Map<String, dynamic>? bodyMap = serverResponse.bodyMap;
    if (bodyMap != null && bodyMap.containsKey("Event-Data")) {
      eventDataMap = bodyMap["Event-Data"];
      await VotingEvent.fromMap(eventDataMap, this, server);
    }
    else {
      Log.e(_TAG, "sEventDetailsRequest -> Missing Key 'Event-Data'");
      serverResponse.errorMessage = "Missing Key 'Event-Data' in serverResponse.bodyMap";
      return serverResponse;
    }

    final Map<String, dynamic> participants = SUtils.mapValue(bodyMap, "Participants", defaultMap);
    lotsCreated = SUtils.mapValue((participants), "lots_created", 0);
    lotsRedeemed = SUtils.mapValue((participants), "lots_redeemed", 0);
    keysSubmitted = SUtils.mapValue((participants), "keys_submitted", 0);

    if (bodyMap.containsKey("Polling-Information")) {
      final Map<String, dynamic> poolingMap = SUtils.mapValue(bodyMap, "Polling-Information", defaultMap);
      poolingInformation = PoolingInformation.fromMap(poolingMap);
    }

    if (bodyMap.containsKey("Completed-Votings")) {
      List<dynamic> _items = bodyMap["Completed-Votings"];
      Log.d(_TAG, "eventDetailsRequest => Completed-Votings: $_items");
      for (String votingId in _items) {
        // account.saveVotedById(votingId: votingId);
        addCompletedVoting(votingId: votingId);
      }
    }



    /// Votings
    votings.clear();
    List<dynamic>? _votings = SUtils.mapValue(bodyMap, "Votings", []);
    if (_votings != null) {
      for (var i = 0; i < _votings.length; i++) {
        Log.d(_TAG, "sEventDetailsRequest: votings: ${_votings.length}");

        Voting v = Voting.fromMap(_votings[i]);
        if (v.status == Voting.STATUS_OPEN) {
          votingCount.open ++;
        }
        else if (v.status == Voting.STATUS_IDLE) {
          votingCount.idle ++;
        }
        else if (v.status == Voting.STATUS_CLOSED) {
          votingCount.closed ++;
        }
        else if (v.status == Voting.STATUS_FINISHED) {
          votingCount.finished ++;
        }
        votings.add(v);
      }
      // _sortVotingList();
    } else {
      Log.e(_TAG, "sEventDetailsRequest: bodyMap[Voting.VOTINGS_KEY] == null");
    }
    return serverResponse;
  }


  void addCompletedVoting ({required String votingId}) {
    if (!completedVotings.contains(votingId)) {
      completedVotings.add(votingId);
    }
  }


  /// VOTING DETAIL REQUEST
  Future<ServerResponse> votingListRequest(ApiServer server, Account? account, VotingStatusEnum status) async {
    votings.clear();

    final Map<String, dynamic> messageMap = msgTypeVotingListRequest(
      votingEventId: id,
      votingStatus: status,
    );
    ServerResponse serverResponse = await HttpController().serverRequest(messageMap, account, server);

    if (!serverResponse.isOk("sVotingListRequest")) {
      return serverResponse;
    }

    final Map<String, dynamic>? bodyMap = serverResponse.bodyMap;
    if (bodyMap != null && bodyMap.containsKey(Voting.VOTINGS)) {
      final List<dynamic> _votingsList = SUtils.mapValue(bodyMap, Voting.VOTINGS, defaultMap);

      Log.d(_TAG, "sVotingListRequest; _votingsList: ${_votingsList.length}");

      for (var i = 0; i < _votingsList.length; i++) {
        Voting voting = Voting.fromMap(_votingsList[i]);
        votings.add(voting);
      }
    }
    return serverResponse;
  }

  /// returns true when updateRequest required
  Future<bool> poolingInformationRequest() async {
    bool result = false;
    if (poolingInformation == null) {
      throw "poolingInformation is null";
    }
    ServerResponse response = await HttpController().sendToServer(poolingInformation!.pollingUrl, "", post: false);
    if (response.bodyMap != null) {
      PoolingInformation poolingResult = PoolingInformation.fromMap(response.bodyMap!, knowUrl: poolingInformation!.pollingUrl);
      if (poolingResult.changeCount != poolingInformation!.changeCount) {
        result = true;
      }
      poolingInformation = poolingResult;
    }
    return result;
  }

  static const NONE_ANONYMOUS_EVENT = "NONE_ANONYMOUS_EVENT";
  static const ID = "ID";
  static const NAME = "NAME";
  static const COMPLETED_VOTINGS = "COMPLETED_VOTINGS";
}

