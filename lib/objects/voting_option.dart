// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved



import 'package:partcp_client/objects/description.dart';
import 'package:partcp_client/objects/voting_type.dart';

import '/objects/_client_data.dart';
import '/utils/s_utils.dart';
import 'package:uuid/uuid.dart';

import 'voting_comment.dart';

class VotingOption {

  String id;
  String name;
  VotingType? votingType;
  String? linkUrl;

  /// for Attachments in MB
  int? maxSize;

  int? maxLength;
  int? maxCount;
  int? rows;

  /// only for [Voting.votingType.type == VotingTypeEnum.mixed]
  bool mandatory;


  /// only for
  String? confirmationText;
  bool confirmSegmentResults;

  Description description = Description();

  VotingComment? votingComment;

  /// vote MUST be set: 0 or 1;
  int voteValue;
  dynamic voteData;

  VotingOption({
    this.id = "",
    this.votingType,
    this.name = "",
    this.linkUrl,
    this.maxSize,
    this.maxLength,
    this.maxCount,
    this.mandatory = false,
    this.rows,
    this.confirmationText,
    this.voteValue = 0,
    required this.description,
    this.confirmSegmentResults = false,
  });


  static VotingOption fromMap(Map<String, dynamic> map, VotingType parentVotingType) {
    Map<String, dynamic> defMap = {};

    VotingType? votingType;
    String? typeExists = SUtils.mapValue(map, "type", null);
    if (typeExists != null) {
      VotingTypeEnum? typeTest = VotingType.typeTest(typeExists);
      if (typeTest != null) {
        votingType = VotingType(typeTest);
      }
    }
    else if (parentVotingType != VotingTypeEnum.mixed) {
      votingType = parentVotingType;
    }


    return VotingOption(
      id: map[ID].toString(),
      votingType: votingType,
      name: SUtils.mapValue(map, NAME, "~"),
      linkUrl: SUtils.mapValue(map, "link_url", null),
      maxSize: SUtils.mapValue(map, "max_size", null),
      maxLength: SUtils.mapValue(map, "max_length", null),
      maxCount: SUtils.mapValue(map, "max_count", null),
      mandatory: SUtils.mapValueBool(map, "mandatory", false),
      rows: SUtils.mapValue(map, "rows", null),
      confirmationText: SUtils.mapValue(map, CONFIRMATION_TEXT, null),
      description: Description.fromJson(map),
      confirmSegmentResults: SUtils.mapValueBool(map, "confirm_segment_results", false)
    );
  }


  bool get visible => name != "~";

  void clearClientVote () {
    voteValue = 0;
    voteData = null;
  }

  static String createUuid() {
    return const Uuid().v1();
  }

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = {
      ID: id,
      NAME: name.trim(),
    };

    map[Description.DESCRIPTION] = description.toJson;
    return map;
  }


  Map<String, dynamic> get voteToMap => {
    "id": id,
    "vote": voteValue
  };

  /// Server Array keys
  static const OPTIONS = "options";
  static const ID = "id";
  static const NAME = "name";
  // static const LINK_URL = "link_url";
  // static const MAX_SIZE = "max_size";
  // static const MAX_LENGTH = "max_length";
  // static const MAX_COUNT = "max_count";
  // static const MANDATORY = "mandatory";
  // static const ROWS = "rows";
  static const CONFIRMATION_TEXT = "confirmation_text";



}