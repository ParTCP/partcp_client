// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:collection/src/iterable_extensions.dart';
import '../utils/log.dart';
import '/objects/vote.dart';
import '/utils/s_utils.dart';
import 'voting.dart';

class VoteResult {
  final dynamic voteKey;
  final dynamic value;

  const VoteResult({
    required this.voteKey,
    required this.value
  });
}


const _TAG = "VotingResult";

class VotingResult {
  int participants = 0;
  int invalid = 0;
  Map<String, dynamic> resultSet = {};

  /// <option.id, VoteResult>
  Map<String, List<VoteResult>> options = {};

  static VotingResult fromMap(Map<String, dynamic> map) {

    VotingResult votingResult = VotingResult()
      ..participants = SUtils.mapValue(map, "participants", 0)
      ..invalid = SUtils.mapValue(map, "invalid", 0);


    /// Danke Martin für den Quatsch, AL 2025-02-05
    var test = SUtils.mapValue(map, "result_sets", null);
    if (test is Map<String, dynamic>) {
      votingResult.resultSet = test;
    }



    /// optionsList
    /*
   voting_result:
     result_sets:
       1:
       - l678211a5bb1a9a0ed@demo01.partcp.org
       2:
       - q67824633100eb577e@demo01.partcp.org
     options:
       option:0:
         set:1: 1
         set:2: 2
       option:1:
         set:1: 1
         set:2: 2
       option:2:
         set:1: 1
         set:2: 22
     */

    dynamic _options = SUtils.mapValue(map, "options", []);

    if (_options is Map<String, dynamic>) {
      _options = SUtils.mapValue(map, "options", []);
      _options.keys.forEach((option) {

        String optionId = _parseKey(option);
        votingResult.options[optionId] = [];

        Map<String, dynamic> _votesMap = SUtils.mapValue(_options, option, null);

        var bla = map;

        _votesMap.forEach((key, value) {
          if (key != 'comment') {
            VoteResult voteResult = VoteResult(
                voteKey: _parseValue(key),
                value: value
            );
            votingResult.options[optionId]!.add(voteResult);
          }
        });
      });
    }

    return votingResult;
  }



  /// todo AL 2025-02-03
  double resistance (String id) {
    // List<VoteResult>? items = results[id];
    // if (items == null) {
    //   return 0;
    // }
    // int sum = 0;
    // int count = 0;
    //
    // items.forEach((VoteResult voteResult) {
    //   if (voteResult.voteKey != Voting.NO_VOTE_VALUE) {
    //     sum += voteResult.voteKey * voteResult.value;
    //     count += voteResult.value;
    //   }
    // });
    // return sum/count;
    return 0;
  }


  int valueByVote(String id, int vote) {
    List<VoteResult>? items = options[id];
    if (items == null) {
      return 0;
    }
    VoteResult? voteResult = items.firstWhereOrNull((voteResult) => voteResult.voteKey == vote);
    return voteResult != null ? voteResult.value : 0;
  }

  int highestConsensusCount(String id) {
    List<VoteResult>? items = options[id];
    if (items == null) {
      return 0;
    }
    int max = 0;
    items.forEach((VoteResult voteResult) {
      if (voteResult.value > max) {
        max = voteResult.value;
      }
    });
    return max;
  }

  int highestOptionCount() {
    int count = 0;
    options.forEach((key, List<VoteResult> voteResults) {
      VoteResult voteResult = voteResults[0];
      if (voteResult.value > count) {
        count = voteResult.value;
      }
    });
    return count;
  }

  int votingsCount(String id) {
    List<VoteResult>? items = options[id];
    if (items == null) {
      return 0;
    }
    int count = 0;

    /// todo AL 2025-02-03
    // items.forEach((VoteResult voteResult) {
    //   count += voteResult.value;
    // });
    return count;
  }

  static String _parseKey(String option) {
    return option.split(":").last.toString();
  }

  static dynamic _parseValue(String option) {
    List<dynamic> split = option.trim().split(":");
    // int? result = int.tryParse(option.trim().split(":").last)!;
    var lastSplit  = split.last;
    if (lastSplit.toString().isEmpty) {
      return 0;
    }
    return split.last;
  }



}