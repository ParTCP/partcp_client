// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022-2025. All rights reserved


import 'package:get/get.dart';

enum VotingTypeEnum {
  n_a,
  yes_no_rating,
  simple_10_0_10,
  consensus_3,
  consensus_5,
  consensus_10,
  single_choice,
  multiple_choice_2,
  multiple_choice_3,
  multiple_choice_4,
  multiple_choice_all,
  mixed,
  photo,
  text,
  file,
  count,
  confirmation,
  info,
  confirmationRejection,    /// app intern only
}


class VotingType {

  VotingTypeEnum type;
  String name = "";
  int multipleChoiceCount = 0;
  
  VotingType (this.type){
    _init();
  }
    
  void _init() {
    switch (type) {
      
      case VotingTypeEnum.n_a:
        name = "Fehler, Typ nicht implementiert";
        break;

      case VotingTypeEnum.yes_no_rating:
        name = "Ja-Nein Abstimmung";
        break;
        
      case VotingTypeEnum.simple_10_0_10:
        name = "simple_10_0_10";
        break;

      case VotingTypeEnum.consensus_3:
        name = "Konsensierung (3 Punkte)";
        break;

      case VotingTypeEnum.consensus_5:
        name = "Konsensierung (5 Punkte)";
        break;

      case VotingTypeEnum.consensus_10:
        name = "Konsensierung (10 Punkte)";
        break;

      case VotingTypeEnum.single_choice:
        name = "Single Choice";
        break;

      case VotingTypeEnum.multiple_choice_2:
        name = "Multiple Choice (2)";
        multipleChoiceCount = 2;
        break;

      case VotingTypeEnum.multiple_choice_3:
        name = "Multiple Choice (3)";
        multipleChoiceCount = 3;
        break;

      case VotingTypeEnum.multiple_choice_4:
        name = "Multiple Choice (4)";
        multipleChoiceCount = 4;
        break;

      case VotingTypeEnum.multiple_choice_all:
        name = "Multiple Choice (alle)";
        multipleChoiceCount = 999;
        break;

      case VotingTypeEnum.mixed:
        name = "mixed";
        break;

      case VotingTypeEnum.photo:
        name = "photo";
        break;

      // case VotingTypeEnum.vote_count:
      //   name = "vote_count";
      //   break;

      case VotingTypeEnum.count:
        name = "Count";
        break;

      case VotingTypeEnum.text:
        name = "Text";
        break;

      case VotingTypeEnum.file:
        name = "Dateianhang";
        break;

      case VotingTypeEnum.confirmation:
        name = "confirmation";
        break;

      case VotingTypeEnum.info:
        name = "info";
        break;

      case VotingTypeEnum.confirmationRejection:
        name = "";
    }

  }

  bool get isMultipleChoice {
    return type.name.startsWith("multiple-choice");
  }

  bool get isConsensus {
    return type.name.startsWith("consensus");
  }

  bool get isYesNo {
    return type == VotingTypeEnum.yes_no_rating;
  }


  /// test, if VotingTypeEnum exists
  static VotingTypeEnum? typeTest (String? type) {
    if (type == null) {
      return null;
    }
    /// replace "-" with "_"
    String typeTest = type.replaceAll("-", "_");
    VotingTypeEnum? test = VotingTypeEnum.values.firstWhereOrNull((element) => element.name == typeTest);
    return test;
  }

  

  
  /// ///////////////////////////////////////////////
  /// altes Zeugs

  /*
  static const values = [
    YES_NO_RATING,
    CONSENSUS_3,
    CONSENSUS_5,
    CONSENSUS_10,
    SINGLE_CHOICE,
    MULTIPLE_CHOICE_2,
    MULTIPLE_CHOICE_3,
    MULTIPLE_CHOICE_4,
    MULTIPLE_CHOICE_ALL,
  ];




  // static String title(String? type) {
  //   if (type == null) return "null";
  //
  //   return _titleMap.containsKey(type)
  //     ? _titleMap[type]!
  //     : "n/a";
  // }


  // static bool isConsensus(String type) {
  //   return type.startsWith("consensus");
  // }

  // static bool isMultipleChoice(String type) {
  //   return type.startsWith("multiple-choice");
  // }

  static int getMultipleChoiceCountFromType(String type) {
    if (type.startsWith("multiple-choice-")) {
      var splitted = type.split("multiple-choice-");
      if (splitted[1] == "all") {
        return 9999;
      }
      else {
        var test = int.tryParse(splitted[1]);
        return test != null
            ? test
            : -1;
      }
    }
    else {
      return -1;
    }
  }

  static const Map<String, String> _titleMap = {
    YES_NO_RATING: "Ja-Nein Abstimmung",
    CONSENSUS_3: "Konsensierung (3 Punkte)",
    CONSENSUS_5: "Konsensierung (5 Punkte)",
    CONSENSUS_10: "Konsensierung (10 Punkte)",
    SINGLE_CHOICE: "Single Choice",
    MULTIPLE_CHOICE_2: "Multiple Choice (2)",
    MULTIPLE_CHOICE_3: "Multiple Choice (3)",
    MULTIPLE_CHOICE_4: "Multiple Choice (4)",
    MULTIPLE_CHOICE_ALL: "Multiple Choice (alle)",
  };


  static const YES_NO_RATING = "yes-no-rating";
  static const SIMPLE_10_0_10 = "simple-10-0-10";
  static const CONSENSUS_3 = "consensus-3";
  static const CONSENSUS_5 = "consensus-5";
  static const CONSENSUS_10 = "consensus-10";
  static const SINGLE_CHOICE = "single-choice";
  static const MULTIPLE_CHOICE_2 = "multiple-choice-2";
  static const MULTIPLE_CHOICE_3 = "multiple-choice-3";
  static const MULTIPLE_CHOICE_4 = "multiple-choice-4";
  static const MULTIPLE_CHOICE_ALL = "multiple-choice-all";
   */

}


/*

 */