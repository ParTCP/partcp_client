// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:get/get.dart';
import 'package:partcp_client/objects/error_message.dart';
import 'package:partcp_client/objects/participant.dart';
import '/controller/http_controller.dart';
import '/controller/main_controller.dart';
import '/message_type/lot_request.dart';
import '/objects/credential.dart';
import '/objects/server_response.dart';
import '/objects/voting_event.dart';
import '/objects/api_server.dart';
import '/utils/crypt/my_aes_crypt.dart';
import '/utils/log.dart';
import '/utils/yaml_converter.dart';
import 'package:cryptography/cryptography.dart' as ecLib;
import 'package:uuid/uuid.dart';

import '../utils/crypt/my_ec_crypt.dart';
import 'account.dart';

const _TAG = "Wallet";

YamlConverter _yamlConverter = YamlConverter();


enum WalletProtection {
  simple, // blank pwd on linux
  pwd,
  store
}

class Wallet {

  List<ApiServer> serverList = [];

  Account? walletAccount;
  List<int>? pwdBytes;

  static WalletProtection get defaultProtectionByOs {
    if (GetPlatform.isWeb) {
      return WalletProtection.simple;
    }
    return WalletProtection.pwd;
  }


  /// ENCRYPT KEYS + CREATE FILE CONTENT
  static Future<dynamic> createAndEncryptWallet({required List<int> pwdCodeUnits, required String walletName, WalletProtection protection = WalletProtection.pwd}) async {

    String _pass = "";
    try {
      _pass += "Create Wallet..";
      Wallet wallet = Wallet();
      _pass += "\nCreate Wallet: OK";
      wallet.pwdBytes = pwdCodeUnits;

      /// create wallet keys
      _pass += "\nCreate Account..";
      wallet.walletAccount = Account(Mode.wallet);
      wallet.walletAccount!.name = walletName;
      wallet.walletAccount!.protection = protection;
      _pass += "\nCreate Account: OK";
      _pass += "\nCreate Keys...";

      if (await wallet.walletAccount!.createNewKeys()) {
        _pass += "\nCreate Keys: OK";
        _pass += "\nEncrypt Keys...";
        await wallet.walletAccount!.encryptKeys(wallet.pwdBytes!);
        _pass += "\nEncrypt Keys: OK";

        _pass += "\nEncrypt onWalletChanged...";
        await MainController.to.onWalletDecrypted(wallet);
        await MainController.to..onWalletChanged(updateNotification: false);
        _pass += "\nEncrypt onWalletChanged: OK";

        return wallet;
      }
    }
    catch(e, s) {
      _pass += "\n\nerror: $e; \nstack: $s";
      Log.e(_TAG, "_createAndEncryptWallet: $_pass");
      // fatalErrorCreateKeys = true;
      // fatalErrorMessage = "$_pass";
      return null;
    }
  }


  /// for Box-Encryption we use a hashed PrivateKey
  Future<List<int>> get pwdForStringsHashed async {
    final key = await pwdForStrings;
    final ecLib.Hash hash = await MyEcCrypt.hashSha256(key);
    return hash.bytes;
  }

  /// for [encryptString] and [decryptString}
  /// we use as pwd the pure PrivateKey from keyPairX
  /// and hash the key in [encryptKey] and [decryptKey}
  Future<List<int>> get pwdForStrings async {
    return await walletAccount!.keyPairX!.extractPrivateKeyBytes();
  }

  Future<bool> addAccount(ApiServer apiServer, Account account) async {
    await account.encryptKeys(pwdBytes!);
    addServer(apiServer);
    serverList.firstWhere((element) => element.publicKeyConcatB64 == apiServer.publicKeyConcatB64).accounts.add(account);
    return true;
  }

  bool addServer(ApiServer server) {
    for (var i=0; i<serverList.length; i++) {
      if (serverList[i].url == server.url) {
        return false;
      }
    }
    serverList.add(server);
    updateWalletDateU();
    return true;
  }

  bool deleteAccountByEventId(ApiServer apiServer, String eventId) {
    Account? account = accountForEvent(apiServer, eventId, Mode.event);
    if (account != null) {
      serverList
          .firstWhere((element) => element.publicKeyConcatB64 == apiServer.publicKeyConcatB64)
          .accounts
          .remove(account);
      return true;
    }
    else {
      return false;
    }
  }
  


  ApiServer? getServerByUrl(String url) {
    if (url.trim().isEmpty) return null;

    for (var i=0; i<serverList.length; i++) {
      if (serverList[i].url == url) {
        // Log.d(_TAG, "getServerByHash: server exists");
        return serverList[i];
      }
    }
    // Log.d(_TAG, "getServerByHash: server not exists");
    return null;
  }

  static Wallet? fromYaml (String? yamlString) {
    Wallet wallet = Wallet();

    try {
      if (yamlString != null) {
        final Map<String, dynamic> yamlMap = _yamlConverter.toMap(yamlString);

        /// main wallet account
        final Map<String, dynamic>? walletAccountMap = yamlMap[WALLET];
        if (walletAccountMap != null) {
          wallet.walletAccount = Account.fromMap(map: walletAccountMap);
        }

        /// ApiServer
        if (yamlMap.containsKey(SERVER)) {
          final List<dynamic>? yamlServer = yamlMap[SERVER];
          if (wallet.walletAccount != null && yamlServer != null) {
            for (var i = 0; i < yamlServer.length; i++) {
              final ApiServer? server = ApiServer.fromWalletMap(yamlServer[i]);
              if (server != null) {
                wallet.serverList.add(server);
              }
            }
          }
        }
      }
    }
    catch(e, s) {
      Log.e(_TAG, "fromYaml error: $e, $s");
    }

    return wallet.walletAccount == null
      ? null
      : wallet;
  }


  Future<bool> decryptAccounts (List<int>? pwdBytes) async {
    List<int> _pwdBytes = pwdBytes != null
      ? pwdBytes
      : this.pwdBytes!;

    bool isSuccess = false;

    isSuccess = await walletAccount!.decryptKeys(_pwdBytes);
    if (!isSuccess) {
      return false;
    }

    this.pwdBytes = _pwdBytes;

    /// loop over server
    for (var i = 0; i<serverList.length; i++) {
      /// loop over accounts
      for(var a = 0; a<serverList[i].accounts.length; a++) {
        Log.d(_TAG, "decryptAccount for ${serverList[i].accounts[a].userIdEnc}");
        await serverList[i].accounts[a].decryptKeys(_pwdBytes);
        await serverList[i].accounts[a].decryptSecretValues(_pwdBytes);
      }
    }
    return true;
  }


  Account? accountForEvent(ApiServer server, String eventId, Mode mode, {bool keyPairMustExists = false}) {
    for (var i=0; i<server.accounts.length; i++) {
      Account _account = server.accounts[i];

      if (_account.votingEvent != null && _account.votingEvent!.id == eventId) {
        Log.d(_TAG, "accountForEvent ($eventId); userId~: ${server.accounts[i].userIdEnc}");
        if (keyPairMustExists && _account.privateKeyEdB64Enc == null) {
          Log.i(_TAG, "accountForEvent ($eventId): ${server.accounts[i].userId} -> keyPairMustExists but is null");
        }
        else {
          return server.accounts[i];
        }
      }

    }
    Log.d(_TAG, "accountForEvent ($eventId): null");
    return null;
  }


  Future<Account> createAccountWithoutCredential({required VotingEvent event, required ApiServer server}) async {
    Account account = Account(Mode.event);
    await account.createNewKeys();

    var uuid = const Uuid();
    account.userId = uuid.v4();
    // account.votingEvent.id = event.id;
    account.votingEvent = event;
    account.votingAllowed = 0;

    await addAccount(server, account);
    await MainController.to.onWalletChanged(updateNotification: true);
    return account;
  }


  /// ////////////////////////////////////////////////////////////////
  /// request anonymous userId and tan with lotCode for Voting
  Future<CredentialResponse> requestCredentialsFromLot({
    required String lotCode,
    required Mode mode,
    required VotingEvent event,
    required ApiServer server
  }) async {

    CredentialResponse credentialResponse = CredentialResponse();

    /// we create a temp account with temp keys to decrypt the credentials
    Account account = Account(mode);
    account.apiServer = server;

    await account.createNewKeys();

    /// Generate a v4 (random) id as userId
    var uuid = const Uuid();
    account.userId = uuid.v4();

    String? lotCodeEnc = await account.encryptServerMessage(
      message: lotCode,
      server: server,
    );

    final Map<String, dynamic> messageMap = msgTypeLotRequest(
        lotCodeEnc: lotCodeEnc,
        eventId: event.id
    );
    ServerResponse serverResponse = await HttpController().serverRequestSigned(account, messageMap, server, loc: "requestVotingCredentialsFromLot");
    credentialResponse.lotRequestServerResponse = serverResponse;

    if (!serverResponse.isOk("requestVotingCredentialsFromLot")) {
      credentialResponse.lotRequestSuccess = false;
      return credentialResponse;
    }
    else {
      /// server should response IV  and encrypted yaml credentials
      /// Lot-Content~: >
      ///   wsoo3iKDqKABvWCL0WP+pw==:Y1BYUTJzS1BscFIrZjdrQW96OHNwdzNYZGZJSGN6aWFpZWk5eTJSVEZ0T0UwMVpwVUYvaUdnRWo3UzI1Y1JzPQ==

      if (serverResponse.bodyMap!.containsKey("Lot-Content~")) {
        String lotContent = (serverResponse.bodyMap!["Lot-Content~"]).trim();
        // Log.d(_TAG, "_requestUserCredentialsFromLot: Lot-Content~: $lotContent");

        Credential credential = await account.decryptCredentialFromLotContent(server, lotContent);

        /// we got credential
        if (credential.participantId != null && credential.credential != null) {
          credentialResponse.lotRequestSuccess = true;

          await keySubmission(
              credentialResponse: credentialResponse,
              lotCode: lotCode,
              credential: credential,

              event: event,
              eventServer: server,
              keySubmissionServer: credential.lotServer ?? server,

              mode: mode
          );

          return credentialResponse;
        }

        /// something goes wrong
        else {
          serverResponse.errorMessage = credential.errorMsg != null
              ? credential.errorMsg
              : "Unexpected error";

          credentialResponse
            ..lotRequestSuccess = false
            ..lotRequestServerResponse = serverResponse;

          return credentialResponse;
        }
      }
      else if(serverResponse.bodyMap!.containsKey("Rejection-Reason")) {
        serverResponse.errorMessage = (serverResponse.bodyMap!["Rejection-Reason"]).trim();

        credentialResponse
          ..lotRequestSuccess = false
          ..lotRequestServerResponse = serverResponse;

        return credentialResponse;
      }

      else {
        serverResponse.errorMessage = "Unexpected error";

        credentialResponse
          ..lotRequestSuccess = false
          ..lotRequestServerResponse = serverResponse;

        return credentialResponse;

      }
    }
  }


  /// on paperLots or Admin accounts we have userId and credential
  /// and can send it directly
  Future<CredentialResponse> keySubmission ({
    required CredentialResponse credentialResponse,

    required Credential credential,

    required String lotCode,
    required Mode mode,
    required VotingEvent event,
    required ApiServer eventServer,
    required ApiServer keySubmissionServer,
  }) async {

    Account? account;

    /// first check, if account for this event already exists (without keyPair)
    if (mode == Mode.event) {
      account = accountForEvent(eventServer, event.id, mode);
    }

    account ??= Account(mode);

    account.userId = credential.participantId;
    // account.votingEvent.isNonAnonymous = event.isNonAnonymous;
    account.votingEvent = event;

    await account.createNewKeys();

    if (account.mode == Mode.event) {
      // account.votingEvent.id = event.id;
    }

    account.keySubmissionServer = keySubmissionServer;
    ServerResponse serverResponse = await account.keySubmission(credential: credential.credential);
    credentialResponse.keySubmissionServerResponse = serverResponse;

    if (!serverResponse.isOk("keySubmission") ) {
      credentialResponse.keySubmissionSuccess = false;
    }
    else {

      /// save [Participant] in account
      if (credentialResponse.keySubmissionServerResponse!.bodyMap!.containsKey("Participant-Data")){
        Participant participant = Participant.fromMap(credentialResponse.keySubmissionServerResponse!.bodyMap!["Participant-Data"]);
        account.participant = participant;
        /// [event.isNonAnonymous], we need to save decrypted credential in Wallet
        if (event.isNonAnonymous) {
          account.participant!.credentialDec = credential.credential!;
          Log.w(_TAG, "????????????????? credential:  ${account.participant!.credentialDec}");
        }
      }


      /// save hashed LotCode in Account
      await account.setLotCode(lotCode);
      account.votingAllowed = 1;
      // account.keySubmissionServer = keySubmissionServer;

      /// add account to Wallet
      await addAccount(eventServer, account);

      /// save wallet in shared prefs
      await MainController.to.onWalletChanged(updateNotification: true);
      credentialResponse.keySubmissionSuccess = true;
      credentialResponse.account = account;
    }
    return credentialResponse;
  }
  ////////////////////////////////////////////////////////////////////


  /// ENCRYPT STRING
  Future<String?> encryptString(String content) async {
    return base64Encode(await MyAesCrypt.encryptKey(content.codeUnits, await pwdForStrings));
  }


  Future<String?> computeEncryptString(String content) async {
    Map data = {
      "content": content,
      "pwdBytes": await pwdForStrings,
    };
    return await compute(_computeEncryptString, data);
  }



  static Future<String> _computeEncryptString(Map data) async {
    String content = data["content"];
    var pwdBytes = data["pwdBytes"];
    return base64Encode(await MyAesCrypt.encryptKey(content.codeUnits, pwdBytes));
  }


  /// DECRYPT STRING
  Future<String?> decryptString(String contentEnc) async {
    try {
      var test = base64.normalize(contentEnc);
      Log.d(_TAG, "base64 normalize: $test");
    }
    on FormatException catch(e) {
      return null;
    }
    catch(e) {
      Log.e(_TAG, "decryptString error: $e");
      return null;
    }
    return const Utf8Codec().decode(
        await MyAesCrypt.decryptKey(base64Decode(contentEnc), await pwdForStrings)
    );
  }


  void updateWalletDateU() {
    walletAccount!.dateU = DateTime.now();
  }


  Future<String> toYaml() async {
    Map<String, dynamic> map = {};

    /// wallet account
    map[WALLET] = await walletAccount!.toMap(true, null);

    /// accounts
    List<dynamic> sList = [];
    for (var i=0; i<serverList.length; i++) {
      sList.add(await serverList[i].toWalletMap(pwdBytes!));
    }
    map[SERVER] = sList;

    String yamlString = _yamlConverter.toYaml(map);
    return yamlString;
  }

  static const SERVER = "SERVER";
  static const WALLET = "WALLET";
}


class CredentialResponse{
  bool? lotRequestSuccess;
  ServerResponse? lotRequestServerResponse;

  bool? keySubmissionSuccess;
  ServerResponse? keySubmissionServerResponse;

  Account? account;
}