// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:flutter/material.dart';
import 'package:partcp_client/controller/main_controller.dart';


enum TextStyleEnum {
  H1,
  H2,
  H3,
  normal,
  title,
  subtitle,
  info,
  button
}

class Styles {

  static const Color buttonColor = Color(0xff0b2ebf);

  static const Color textColorDefault = Color.fromARGB(255, 10, 10, 10);
  static const Color textColorSubtitle = Color.fromARGB(255, 80, 80, 80);

  static const Color textColorDisabled = Color.fromARGB(255, 120, 120, 120);

  // static const Color selectedTileColor = Color.fromARGB(100, 220, 220, 220);
  static const Color selectedTileColor = Color.fromARGB(100, 225, 245, 254);


  static const dividerColor = Colors.black26;
  static Color waitingColor = Colors.green.withOpacity(1);

  static const Widget listDivider = Divider(
    thickness: 1,
    color: dividerColor,
  );

  static double fontSize (TextStyleEnum styleEnum) {
    double scaleFactor = MainController.to.screenScaleFactor;
    switch (styleEnum) {

      case TextStyleEnum.H1:
        return 22 * scaleFactor;

      case TextStyleEnum.H2:
        return 20 * scaleFactor;

      case TextStyleEnum.H3:
        return 19 * scaleFactor;

      case TextStyleEnum.normal:
        return 17 * scaleFactor;

      case TextStyleEnum.title:
        return 19 * scaleFactor;

      case TextStyleEnum.subtitle:
        return 17 * scaleFactor;

      case TextStyleEnum.info:
        return 15 * scaleFactor;

      case TextStyleEnum.button:
        return 16 * scaleFactor;
    }
  }

  static TextStyle get H1 => TextStyle(fontSize: Styles.fontSize(TextStyleEnum.H1), color: textColorDefault);
  static TextStyle get H2 => TextStyle(fontSize: Styles.fontSize(TextStyleEnum.H2), color: textColorDefault);
  static TextStyle get H3 => TextStyle(fontSize: Styles.fontSize(TextStyleEnum.H3), color: textColorDefault);

  static TextStyle get listTileTextMain => TextStyle(fontSize: Styles.fontSize(TextStyleEnum.title), color: textColorDefault);
  static TextStyle get listTileTextMainBold => TextStyle(fontSize: Styles.fontSize(TextStyleEnum.title), color: textColorDefault, fontWeight: FontWeight.bold);

  static TextStyle get listTileTextSub => TextStyle(fontSize: Styles.fontSize(TextStyleEnum.subtitle), color: textColorSubtitle);
  static TextStyle get listTileTextSubError => TextStyle(fontSize: Styles.fontSize(TextStyleEnum.subtitle), color: Colors.red);


  static Divider get divider => Divider(color: Color.fromARGB(80, 150, 150, 150), thickness: 1,);


  static TextTheme get textTheme => TextTheme(
    /// buttons
    labelLarge: TextStyle(
        fontSize: Styles.fontSize(TextStyleEnum.button),
        color: Styles.textColorDefault
    ),

    /// alert dialog title
    headlineSmall: TextStyle(
        fontSize: Styles.fontSize(TextStyleEnum.H1),
        color: Styles.textColorDefault
    ),

    /// alert dialog Text
    bodyMedium: TextStyle(
        fontSize: Styles.fontSize(TextStyleEnum.normal),
        color: Styles.textColorDefault
    ),
  );

}