// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved


import 'package:flutter/material.dart';
import 'package:dio/dio.dart';
import '/utils/log.dart';

import 'transferable_typedef.dart';

class FileDownload {
  static const _TAG = "FileDownload";

  final TransferableOnFinishCallback onFinish;
  final TransferableOnErrorCallback onError;
  final TransferableProgressCallback onProgress;

  final String fileUrl;
  final String savePath;

  /////////////////////////////////
  /// download
  FileDownload({
    required this.fileUrl,
    required this.savePath,
    required this.onFinish,
    required this.onError,
    required this.onProgress,
  }) {

    _download();
  }

  Dio _dio = Dio();
  CancelToken _cancelToken = CancelToken();
  late Response _response;

  bool _canceledByUser = false;
  int _lastProgress = 0;


  /////////////////////////////////
  /// cancel
  void cancel() {
    debugPrint(_TAG + "cancel()");
    _canceledByUser = true;
    _cancelToken.cancel();
    _dio.close(force: true);
  }

  /// CALLBACK ON FINISH
  void _callbackOnFinish() {
    if (_canceledByUser) return;
    onFinish();
  }

  /// CALLBACK ON ERROR
  void _callbackOnError(int status, String errorMessage) {
    if (_canceledByUser) return;
    onError(status, errorMessage);
  }

  /// CALLBACK ON PROGRESS
  void _callbackProgress(int progress) {
    if (_canceledByUser) return;
    onProgress(progress);
  }


  void _download() async {

    Log.d(_TAG, "_download; fileUrl: $fileUrl");
    Log.d(_TAG, "_download; savePath: $savePath");

    try {
      _response = await _dio.download(
        fileUrl,
        savePath,
        cancelToken: _cancelToken,
        // data: {'aa': 'bb' * 22},
        onReceiveProgress: (receive, currentTotal) => _onProgress(receive, currentTotal),
          options: Options(
            responseType: ResponseType.bytes,
            followRedirects: false,
            // validateStatus: (status) {
            //   _httpStatus = status;
            //   return status == 200;
            // }),
          )
      );

      if (_response.statusCode == 200) {
        _callbackOnFinish();
      }
    }
    on DioError catch (e) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx and is also not 304.
      if (e.response != null) {
        Log.e(_TAG, "DioError: ${e.response!.data}");
        Log.e(_TAG, "DioError: ${e.response!.headers}");
        Log.e(_TAG, "DioError: ${e.response!.statusCode}");
        _callbackOnError(e.response!.statusCode!, e.response!.data.toString());
      } else {
        // Something happened in setting up or sending the request that triggered an Error
        Log.e(_TAG, "DioError: ${e.message}");

        //[NBT2023-06-02]: The argument type 'String?' can't be assigned to the parameter type 'String'. -> "!" added.
        _callbackOnError(500, e.message!);
      }
    }
  }


  void _onProgress(int currentReceive, int currentTotal) {
    if (_canceledByUser) return;

    double progress = currentReceive / currentTotal * 100;

    if (progress.round() > _lastProgress) {
      _lastProgress = progress.round();
      _callbackProgress(progress.round());
    }
  }
}