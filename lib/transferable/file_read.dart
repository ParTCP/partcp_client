// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:dio/dio.dart';
import '/utils/log.dart';
import 'transferable_typedef.dart';

class FileRead {

  static const _TAG = "FileRead";

  final TransferableOnResponseCallback onResponse;
  final TransferableOnErrorCallback onError;

  final String fileUrl;

  Dio _dio = Dio();
  late Response _response;
  CancelToken _cancelToken = CancelToken();
  bool _canceledByUser = false;

  FileRead({
    required this.fileUrl,
    required this.onResponse,
    required this.onError,
  }) {
    _download();
  }

  /////////////////////////////////
  /// cancel
  void cancel() {
    Log.d(_TAG, "cancel()");
    _canceledByUser = true;
    _cancelToken.cancel();
    _dio.close(force: true);
  }

  /// CALLBACK ON ERROR
  void _callbackOnError(int status, String errorMessage) {
    if (_canceledByUser) return;
    onError(status, errorMessage);
  }

  void _download() async {
    Log.d(_TAG, "_download url: $fileUrl");
    try {
      _response = await _dio.get(
          fileUrl,
          // data: {'id': 12, 'name': 'wendu'}
          options: Options(
            headers: {
              "Access-Control-Allow-Origin": "*", // Required for CORS support to work
              "Access-Control-Allow-Credentials": true, // Required for cookies, authorization headers with HTTPS
              "Access-Control-Allow-Headers": "Origin,Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token,locale",
              "Access-Control-Allow-Methods": "POST, OPTIONS",
            },
          ));
      if (_response.statusCode == 200) {
        if (_canceledByUser) return;
        // Log.d(_TAG, "_response.data: ${_response.data}");
        onResponse(_response.data);
      }
    }
    on DioError catch (e) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx and is also not 304.
      if (e.response != null) {
        Log.e(_TAG, "DioError: ${e.response!.data}");
        Log.e(_TAG, "DioError: ${e.response!.headers}");
        Log.e(_TAG, "DioError: ${e.response!.statusCode}");
        _callbackOnError(e.response!.statusCode!, e.response!.data.toString());
      } else {
        // Something happened in setting up or sending the request that triggered an Error
        Log.e(_TAG, "DioError: ${e.message}");

        //[NBT2023-06-02]: The argument type 'String?' can't be assigned to the parameter type 'String'. -> "!" added.
        _callbackOnError(500, e.message!);
      }
    }
  }

}