// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved




import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../styles/styles.dart';
import '/controller/main_controller.dart';
import '/objects/server_response.dart';
import '/objects/api_server.dart';
import '/objects/wallet.dart';
import '/ui/home/index_controller.dart';
import '/ui/home/index_page.dart';
import '/ui/server_user_manager/server_user_manager_page.dart';
import '/utils/log.dart';
import '/widgets/app_bar.dart';
import '/widgets/card_widget.dart';
import '/widgets/scroll_body.dart';
import '/widgets/error_message_snackbar.dart';
import '/widgets/widgets.dart';




class ApiServerController extends GetxController {
  final _TAG = "ApiServerController";

  List<ApiServer> menuServerList = [];
  
  ServerResponse serverResponse = ServerResponse();
  ApiServer? apiServer;

  Size dropDownMenuSize = Size(100, 80);

  TextEditingController urlController = new TextEditingController();
  MainController mainController = MainController.to;

  Wallet get wallet => mainController.wallet!;
  int serverItemPos = 0;

  Future<void> initController({ApiServer? apiServer}) async {
    serverResponse.errorMessage = null;

    if (apiServer != null) {
      onServerSelected(apiServer);
    }
    else {
      await _createServerList();
      updateMe();
    }
  }


  Future<void> _createServerList() async {
    menuServerList.clear();
    serverResponse.receivingData = true;
    updateMe();

    List<ApiServer> serverList = await ApiServer.createServerListFromAssetUrl();
    /// update short codes in wallet.serverList - standard procedure, 'no shortcode' usage
    if (serverList.isNotEmpty && wallet.serverList.isNotEmpty) { //NBT-NEU 2023-08-22
      for (ApiServer server in serverList) {
        for (ApiServer walletServer in wallet.serverList) {
          if (server.shortCode.isNotEmpty && server.url == walletServer.url) {
            walletServer.shortCode = server.shortCode;
            continue;
          }
        }
      }
    }
    
    /// last saved server on the top
    if (mainController.wallet!.serverList.isNotEmpty) { //NBT-NEU 2023-08-16
      menuServerList.clear();
    }
    menuServerList.addAll(wallet.serverList);


    for (ApiServer server in serverList) {
      if (mainController.wallet!.getServerByUrl(server.url) == null) {
        menuServerList.add(server);
      }
    }
    serverResponse.receivingData = false;
  }


  //NBT
  bool get urlCodeSubmitAllowed => true;


  void onServerSelected(ApiServer server) async {
    Log.d(_TAG, "onServerSelected()");

    serverResponse = ServerResponse()
      ..receivingData = true;
    updateMe();

    ///Server-Details-Request
    serverResponse = await server.serverDetailsRequest(account: MainController.to.wallet!.walletAccount);

    if (!serverResponse.isOk("$_TAG, onServerSelected")) {
      updateMe();
      return;
    }

    await server.loadAccountsFromWallet();

    IndexController.to.apiServer = server;
    IndexController.to.updateMe();
    updateMe();

    if (server.registrationRequired && (mainController.wallet!.walletAccount!.userId == null || mainController.pubKeysOnServerValid == null || !mainController.pubKeysOnServerValid!)) {
      Get.to(() => ServerUserManagerPage(server))!
          .then((_) => Get.offAll(() => IndexPage()
      ));
    }
    else {
      Get.back();
    }
  }


  Future<bool> onWillPop() async {
    return true;
  }

  void updateMe() {
    update();
  }
}



class ApiServerPage extends StatelessWidget {
  final _TAG = "ApiServerPage";

  final ApiServerController controller = Get.put(ApiServerController());
  final ApiServer? apiServer;

  ApiServerPage({this.apiServer});

  final spacer = Container(
    height: 10,
  );

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      controller.initController();
    });

    return WillPopScope(
      onWillPop: controller.onWillPop,
      child: OrientationBuilder(
        builder: (_, __) => GetBuilder<ApiServerController>(
            // initState: (_) => controller.initController(),
            builder: (_) => Scaffold(
                  appBar: MyAppBar(
                    title: const Text("Mit Server verbinden"),
                  ),
                  body: Widgets.loaderBox(
                      loading: controller.serverResponse.receivingData,
                      child: ScrollBody(
                        children: _body
                      )
                  ),
                )),
      ),
    );
  }

  List<Widget> get _body {
    List<Widget> list = [];

    list.add(CardWidget(
      titleListTile: CardWidget.listTileTitle(
        // title: "Server Übersicht",
        subtitle: const Text("Wähle den Server aus, mit dem Du arbeiten möchtest"),
      ),
      child: Column(
        children: [
          ListView(
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            children: _serverList,
          ),
        ],
      ),
    ));

    list.add(ErrorMessageSnackBar(serverResponse: controller.serverResponse));
    return list;
  }


  List<Widget> get _serverList {
    List<Widget> items = [];

    for (var i = 0; i < controller.menuServerList.length; i++) {
      items.add(_serverItem(controller.menuServerList[i], i));
    }
    return items;
  }

  Widget _serverItem(ApiServer server, int pos) {
    return Column(
      children: [
        ListTile(
          isThreeLine: true,
          leading: server.isAssetServer
              ? const Icon(Icons.link, color: Colors.black54)
              : const Icon(Icons.link, color: Colors.black54),
          title: server.isAssetServer
              ? Text(server.name)
              : Text("${server.name}\nAccounts: ${server.accounts.length}"),

          subtitle: Text(server.description),
          // selected: GetPlatform.isMobile && server == controller.currentServer,
          contentPadding: const EdgeInsets.all(0),
          onTap: () => controller.onServerSelected(server),
        ),
        Styles.listDivider
      ],
    );
  }

}
