// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:flutter/material.dart';
import 'package:flutter_linkify/flutter_linkify.dart';
import 'package:get/get.dart';
import '../../styles/styles.dart';

import '/utils/launch_url.dart';
import '/utils/permissions.dart';

import '/widgets/button_sub.dart';
import '/widgets/scroll_body.dart';
import '/widgets/error_message_snackbar.dart';

import 'package:r_upgrade/r_upgrade.dart' as updater;

import 'app_update_controller.dart';

class AppUpdatePage extends StatelessWidget {
  final AppUpdateController controller = Get.put(AppUpdateController());

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      controller.onPostFrameCallback();
    });

    return OrientationBuilder(
      builder: (_, __) => GetBuilder<AppUpdateController>(
        initState: (_) => controller.initController(),
        builder: (_) => Scaffold(
          appBar: AppBar(
            title: const Text('Aktualisierung'),
          ),
          body: ScrollBody(
            children: <Widget>[
              ErrorMessageSnackBar(errorMessage: controller.errorMessage),

              // _infoTile('Installed App name', controller.packageInfo.appName),
              //_infoTile('Package name', controller.packageInfo.packageName),
              _infoTile('Installierte Version', controller.packageInfo.version),
              // _infoTile('Installed Build number', controller.packageInfo.buildNumber),
              // _infoTile('Installed Build signature', controller.packageInfo.buildSignature),
              // _infoTile('App Directory', controller.appDirectory),

              const Divider(color: Styles.dividerColor),

              _infoTile('Verfügbare Version', controller.appRev),
              //_infoTile('Available App URL', controller.appUrl),

              const Divider(color: Styles.dividerColor),

              _buttonList,

              const Divider(color: Styles.dividerColor),

            ],
          ),
        ),
      ),
    );
  }

  Widget get _buttonList {
    List<Widget> _items = [];

    _items.add(
      ButtonSub(
        child: Text("Herunterladen und installieren"),
        //onPressed: controller.downloading ? null : () => controller.onClickDownloadAndInstallApk(), //ToDo implement...
      ),
    );

    _items.add(
      ButtonSub(
        child: Text("Auf Aktualisierungen prüfen"),
        onPressed: controller.downloading ? null : () => controller.getAppRev(),
      ),
    );

    // if (controller.appUpdate != null && !controller.fileReady && controller.allowUpdate) {
    //   _items.add(controller.downloading
    //       ? ButtonSub(
    //           child: Text("Cancel (${controller.progress}%)"),
    //           onPressed: () => controller.cancelDownload(),
    //         )
    //       : ButtonSub(
    //           child: Text("Download App"),
    //           onPressed: () => controller.downloadApp(),
    //         ));
    // }

    // if (controller.fileReady) {
    //   _items.add(ButtonSub(
    //     child: Text("Update App"),
    //     onPressed: () => onClickInstallApk(),
    //   ));
    // }

    return ButtonBar(alignment: MainAxisAlignment.start, children: _items);
  }

  Widget _infoTile(String title, String? subtitle) {
    return ListTile(
      title: Text(title),
      subtitle: Linkify(
        text: subtitle != null && subtitle.isEmpty ? 'Not set' : subtitle!,
        onOpen: (element) => launchURL(element.url),
      ),
    );
  }

  // void _launchURL(String url) async => await canLaunch(url) ? await launch(url) : throw 'Could not launch $url';

  Future<void> onClickInstallApk() async {
    if ((await controller.appPath)!.isEmpty) {
      print('make sure the apk file is set');
      return;
    }

    if (await Permissions.requestPermission(Permissions.group(MyPermissionGroups.ExternalStorage))) {
      updater.RUpgrade.installByPath(await controller.appPath!).then((result) {
        print('install apk $result');
      }).catchError((error) {
        print('install apk error: $error');
      });
    } else {
      print('Permission request fail!');
    }

    // InstallPlugin.installApk(await controller.appPath, Constants.APP_ID).then((result) {
    //     print('install apk $result');
    //   }).catchError((error) {
    //     print('install apk error: $error');
    //   });
    // } else {
    //   print('Permission request fail!');
    // }
  }

// void onClickGotoAppStore(String url) {
//   url = url.isEmpty
//       ? 'https://itunes.apple.com/cn/app/%E5%86%8D%E6%83%A0%E5%90%88%E4%BC%99%E4%BA%BA/id1375433239?l=zh&ls=1&mt=8'
//       : url;
//   InstallPlugin.gotoAppStore(url);
// }
}