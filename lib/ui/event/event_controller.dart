// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../home/index_controller.dart';
import '../voting/voting_list_page_selector.dart';
import '/controller/main_controller.dart';
import '/objects/account.dart';
import '/objects/error_message.dart';
import '/objects/group.dart';
import '/objects/server_response.dart';
import '/objects/voting.dart';
import '/objects/voting_event.dart';
import '/objects/api_server.dart';
import '/objects/wallet.dart';
import '../voting/default/default/voting_list_page.dart';
import '/utils/crc_pass.dart';
import '/utils/log.dart';

import 'event_lot_page.dart';

class EventController extends GetxController {
  final _TAG = "EventController";

  static const minModeratorTanLength = 6;
  static const minParticipantIdLength = 6;

  static EventController to = Get.find<EventController>();
  ServerResponse serverResponse = ServerResponse();
  ErrorMessage? errorMessage;

  late ApiServer _apiServer;
  late Mode mode;

  ApiServer get apiServer {
    // Log.d(_TAG, "event apiServer getter: ${_apiServer.url}");
    return _apiServer;
  }

  set apiServer (ApiServer server) {
    _apiServer = server;
    Log.d(_TAG, "event apiServer setter: ${server.url}");
  }

  Account? account;

  Group? group;
  Group blankGroup = const Group(id: "", name: "", administrable: false);
  VotingEvent? lastSelectedEvent;

  VotingEvent? event;

  List<VotingEvent> _currentGroupEvents = [];

  bool eventFilterClosed = false;
  bool eventFilterOpen = true;
  bool eventFilterWithLotCode = false;

  MainController mainController = MainController.to;

  late TextEditingController lotCodeTextController;

  List<VotingEvent> get activeEventsForGroup {
    List<VotingEvent> list = [];

    for (VotingEvent event in _currentGroupEvents) {
      // if (event.lotCodes || event.paperLots) {

        bool listCandidateOpen = false;
        bool listCandidateClosed = false;
        bool listCandidateWithLotCode = false;

        bool? hasActiveLotCode;
        if (eventFilterWithLotCode) {
          hasActiveLotCode = _hasActiveAccountForEvent(event);
        }

        bool? hasOpenVotings;
        if (eventFilterOpen || eventFilterClosed) {
          hasOpenVotings = _hasOpenVotings(event);
          // Log.d(_TAG, "hasOpenVotings; event: ${event.name} => $hasOpenVotings");
        }

        /// open
        if (eventFilterOpen && ( (hasOpenVotings != null && hasOpenVotings) ) ) {
          listCandidateOpen = true;
        }

        /// closed
        if (eventFilterClosed && ( (hasOpenVotings != null && !hasOpenVotings) ) ) {
          listCandidateClosed = true;
        }

        /// with lotCode
        if ( (eventFilterWithLotCode && hasActiveLotCode != null && hasActiveLotCode) ) {
          listCandidateWithLotCode = true;
        }


        if (listCandidateOpen || listCandidateClosed) {
          if (eventFilterWithLotCode && listCandidateWithLotCode) {
            list.add(event);
          }
          else if (!eventFilterWithLotCode) {
            list.add(event);
          }
        }

      // }
    }
    return list;
  }


  Future<void> initController(ApiServer server, Mode mode) async {
    Log.d(_TAG, "initController; ApiServer: ${server.url}");
    apiServer = server;
    this.mode = mode;

    serverResponse = ServerResponse();

    lotCodeTextController = TextEditingController();

    if (server.getGroupById(MainController.to.userBox!.getLastGroupIdByServer(server)) != null) {
      group = server.getGroupById(MainController.to.userBox!.getLastGroupIdByServer(server));
    } else {
      group = blankGroup;
    }
  }

  bool _hasActiveAccountForEvent(VotingEvent event) {
    final Account? _accountForEvent = accountForEvent(event);
    // if (_accountForEvent != null && _accountForEvent.isValidAccount()) {
    if (_accountForEvent != null) {
      return true;
    }
    return false;
  }

  bool _hasOpenVotings(VotingEvent event) {
    for (Voting voting in event.votings) {
      if (voting.status == Voting.STATUS_OPEN) {
        return true;
      }
    }
    return false;
  }

  bool get votingAllowNext {
    if (serverResponse.receivingData || event == null) {
      return false;
    }
    return _checkLotCodeField || accountForCurrentEvent != null;
  }

  bool get _checkLotCodeField {
    Log.d(
        _TAG,
        "_checkLotCodeField; "
        "lotCode: ${lotCodeTextController.text} "
        "lotCode length: ${lotCodeTextController.text.length} "
        //"event.lotCodeValues.finalLength: ${event.lotCodeValues.finalLength}");
        "event.lotCodeValues.finalLength: ${event!.lotCodeRules.pattern.length}");

    return (!serverResponse.receivingData &&
        crcPass(
            text: lotCodeTextController.text,
            crcLength: event!.lotCodeRules.crcLength,
            //finalLength: event.lotCodeValues.finalLength
            finalLength: event!.lotCodeRules.pattern.length));
  }


  Account? get accountForCurrentEvent {
    Account? account;
    if (event != null) {
      account = accountForEvent(event!);
    }
    return account;
  }


  Future<void> onPostFrameCallback() async {
    // await onRefresh();
    await onGroupsChanges(group!);
  }

  Future<void> onRefresh() async {
    serverResponse.receivingData = true;
    updateMe();

    String lastGroupId = group!.id;

    String? lastEventId;
    if (event != null) {
      lastEventId = event!.id;
    }

    serverResponse = await apiServer.groupListRequest(null);
    Group? testGroup = apiServer.getGroupById(lastGroupId);

    group = testGroup ?? blankGroup;

    serverResponse = await requestEvents(group!, force: true);

    _currentGroupEvents = serverResponse.object;

    if (lastEventId != null) {
      VotingEvent? testEvent = apiServer.getEventById(group!, lastEventId);
      event = testEvent ?? activeEventsForGroup[0];
    }
    updateMe();
  }


  Account? accountForEvent(VotingEvent event) {
    return MainController.to.wallet!.accountForEvent(apiServer, event.id, Mode.event);
  }


  Future<void> onEventLotCodeSubmit() async {
    Log.d(_TAG, "onEventLotCodeSubmit()");
    serverResponse = ServerResponse()
      ..receivingData = true;
    updateMe();

    /// we request CredentialsFromLot and make a KeySubmission
    CredentialResponse credentialResponse = await MainController.to.wallet!.requestCredentialsFromLot(
        lotCode: lotCodeTextController.text,
        event: event!,
        mode: mode,
        server: apiServer
    );

    /// error lotRequest
    if (!credentialResponse.lotRequestSuccess!) {
      serverResponse = credentialResponse.lotRequestServerResponse!;
    }

    /// error keySubmission
    else if (!credentialResponse.keySubmissionSuccess!) {
      serverResponse = credentialResponse.keySubmissionServerResponse!;
    }

    /// success
    else {
      serverResponse.receivingData = false;
      IndexController.to.reRequestEvents();
      updateMe();

      Route<dynamic> route = Get.rawRoute!;
      goToVotingPage(event!);

      /// remove this route
      Get.removeRoute(route);
    }

    updateMe();
  }



  void onEventFilterClosed (bool value) {
    eventFilterClosed = value;
    updateEventFilter();
  }

  void onEventFilterOpen (bool value) {
    Log.d(_TAG, "onEventFilterOpen: $value");
    eventFilterOpen = value;
    updateEventFilter();
  }

  void onEventFilterWithLotCode (bool value) {
    Log.d(_TAG, "onEventFilterWithLotCode: $value");
    eventFilterWithLotCode = value;
    updateEventFilter();
  }


  void updateEventFilter() {
    Log.d(_TAG, "updateEventFilter => eventFilterClosed: $eventFilterClosed; eventFilterOpen: $eventFilterOpen; eventFilterWithLotCode: $eventFilterWithLotCode");
    updateMe();
  }

  Future<void> onGroupsChanges(Group group) async {
    if (group.id != "") {
      this.group = group;
      await MainController.to.userBox!.setLastGroupIdByServer(apiServer, group);

      serverResponse = ServerResponse()..receivingData = true;
      updateMe();

      serverResponse = await requestEvents(group);
      _currentGroupEvents = serverResponse.object;

      // event = activeEventsForGroup.isNotEmpty ? activeEventsForGroup[0] : null;
      updateMe();
    }
  }

  Future<ServerResponse> requestEvents(Group group, {bool? force}) async {
    return await apiServer.eventsByGroup(
        account, group,
        includeOpenVotings: true,
        includeAdminInfo: false,
        force: force
    );
  }

  void onInputChanged() {
    Log.d(_TAG, "onInputChanged()");
    updateMe();
  }

  void updateMe() {
    Log.d(_TAG, "updateMe()");
    update();
  }

  Future<void> goToVotingPageAsGuest(VotingEvent event) async {
    Account account = await mainController.wallet!.createAccountWithoutCredential(event: event, server: apiServer);
    Get.to(() => VotingListPageSelector(
      server: apiServer,
      account: account,
      votingEvent: event,
    ))!
        .then((_) {
      if (votingAllowNext) {
        updateMe();
      } else {
        Get.back();
      }
    });
  }



  Future<void> goToVotingPage(VotingEvent event) async {
    this.event = event;
    account = accountForEvent(event);

    if (account == null) {
      account = await mainController.wallet!.createAccountWithoutCredential(event: event, server: apiServer);
    }
    lastSelectedEvent = event;

    Log.d(_TAG, "goToVotingPage; apiServer: ${apiServer.url}");

    Get.to(() => VotingListPageSelector(
              server: apiServer,
              account: account,
              votingEvent: event,
            ))!
        .then((_) {
      if (votingAllowNext) {
        updateMe();
      } else {
        Get.back();
      }
    });
  }

  void goToVotingLotPage(VotingEvent event) {
    this.event = event;
    Log.d(_TAG, "goToVotingLotPage; event: ${this.event!.id}");
    lastSelectedEvent = event;
    Get.to(() => EventLotPage(this, event))!
        .then((value) => updateMe());
  }
}

