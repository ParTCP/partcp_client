// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:flutter/material.dart';
// import 'package:flutter_multi_formatter/flutter_multi_formatter.dart';
import 'package:get/get.dart';
import '/objects/account.dart';
import '/objects/group.dart';
import '/objects/voting.dart';
import '/objects/voting_event.dart';
import '/objects/api_server.dart';
import '/styles/styles.dart';
import '/utils/field_width.dart';
import '/utils/log.dart';
import '/widgets/app_bar.dart';
import '/widgets/card_widget.dart';
import '/widgets/readmore.dart';
import '/widgets/scroll_body.dart';
import '/widgets/error_message_snackbar.dart';
import '/widgets/text_label.dart';
import '/widgets/widgets.dart';

import 'event_controller.dart';


class EventListPage extends StatelessWidget {
  final ApiServer server;
  EventListPage(this.server);

  final _TAG = "EventListPage";
  final EventController controller = Get.put(EventController());

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      controller.onPostFrameCallback();
    });

    return OrientationBuilder(
      builder: (_, __) => GetBuilder<EventController>(
          initState: (_) => controller.initController(server, Mode.event),
          builder: (_) => Scaffold(
              appBar: MyAppBar(
                title: const Text("Veranstaltungen & Abstimmungen"),
                actions: [
                  /// refresh
                  IconButton(icon: Icon(Icons.refresh), onPressed: () => controller.onRefresh())
                ],
              ),
              // body: ScrollBody(children: _body),
              body: Widgets.loaderBox(
                loading: controller.serverResponse.receivingData,
                child: ScrollBody(
                  children: _body,
                ),
              ))),
    );
  }

  List<Widget> get _body => [
        ErrorMessageSnackBar(serverResponse: controller.serverResponse),
        CardWidget(title: "Bitte wähle eine Gruppe aus", child: _groupDropDownList),
        _eventCardWidget,
      ];

  Widget get _groupDropDownList {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        DropdownButton<Group>(
          value: controller.group,
          items: _groups,
          onChanged: (item) => controller.onGroupsChanges(item!),
          elevation: 1,
        ),
        _eventListFilter,
      ],
    );
  }

  Widget get _eventListFilter {
    return Padding(
      padding: const EdgeInsets.only(left: 40, top: 20),
      child: Column(
        children: [
          TextLabel(
            label: "Veranstaltungsfilter:",
            child: ButtonBar(
              alignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                /// open
                SizedBox(
                  width: 160,
                  child: CheckboxListTile(
                      dense: true,
                      value: controller.eventFilterOpen,
                      onChanged: (val) => controller.onEventFilterOpen(val!),
                      title: const Text("laufende"),
                      controlAffinity: ListTileControlAffinity.leading),
                ),

                /// closed
                SizedBox(
                  width: 160,
                  child: CheckboxListTile(
                      dense: true,
                      value: controller.eventFilterClosed,
                      onChanged: (val) => controller.onEventFilterClosed(val!),
                      title: const Text("beendete"),
                      controlAffinity: ListTileControlAffinity.leading),
                ),

                /// with LotCode
                SizedBox(
                  width: 200,
                  child: CheckboxListTile(
                      dense: true,
                      value: controller.eventFilterWithLotCode,
                      onChanged: (val) => controller.onEventFilterWithLotCode(val!),
                      title: const Text("mit Zugangsdaten"),
                      controlAffinity: ListTileControlAffinity.leading),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget get _noEventsInGroup {
    return controller.serverResponse.receivingData
        ? Container()
        : CardWidget(
            titleListTile: CardWidget.listTileTitle(
                leading: const Icon(
                  Icons.cancel_outlined,
                  color: Colors.red,
                ),
                title: "In dieser Gruppe finden zur Zeit keine Veranstaltung statt.")
          );

  }

  Widget get _eventCardWidget {
    if (controller.activeEventsForGroup.isEmpty) return _noEventsInGroup;

    return CardWidget(
      title: "Bitte wähle die Veranstaltung, an der Du teilnehmen möchten: ",
      // expandChild: true,
      child: Padding(
        padding: const EdgeInsets.only(top: 6, bottom: 6),
        child: _eventList,
      ),
    );
  }

  List<DropdownMenuItem<Group>> get _groups {
    List<DropdownMenuItem<Group>> list = [];
    if (controller.group == null || (controller.group != null && controller.group!.id == "")) {
      list.add(_groupItem(controller.blankGroup));
    }

    for (var i = 0; i < controller.apiServer.serverGroups.length; i++) {
      list.add(_groupItem(controller.apiServer.serverGroups[i]));
    }
    return list;
  }

  Widget get _eventList {
    List<Widget> _items = [];
    for (var i = 0; i < controller.activeEventsForGroup.length; i++) {
      _items.add(_eventItem(controller.activeEventsForGroup[i]));
    }
    return ListView(
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      children: _items,
    );
  }

  Widget _eventItem(VotingEvent event) {
    final Account? accountForEvent = controller.accountForEvent(event);
    bool accountExists = false;
    bool accountIsValid = false;

    if (accountForEvent != null) {
      accountExists = true;
      if (accountForEvent.isValidAccount()) {
        accountIsValid = true;
      }
    }

    Widget leading;
    if (accountExists && accountIsValid) {
      leading = const Icon(Icons.check, color: Colors.green);
    } else if (accountExists && !accountIsValid) {
      leading = const Icon(Icons.new_releases, color: Colors.red);
    } else {
      leading = const Icon(Icons.new_releases, color: Colors.black12);
    }

    Widget? subtitle() {
      if (event.description.shortDescription.isEmpty) return null;

      Widget shortDescription = ReadMoreText(event.description.shortDescription,
          trimLines: 3,
          colorClickableText: Colors.pink,
          trimMode: TrimMode.Line,
          trimCollapsedText: 'Show more',
          trimExpandedText: ' Show less',
          style: const TextStyle(fontSize: 16),
          moreStyle: const TextStyle(fontSize: 16, fontWeight: FontWeight.bold));

      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [shortDescription],
      );
    }

    Widget? _subtitle = subtitle();

    int openVotedCount = 0;
    if (accountForEvent != null) {
      for (Voting voting in event.votings) {
        Log.d(_TAG, "openVotedCount_ : ${voting.id}; voting.status: ${voting.status}");
        // if (voting.status == VotingStatusEnum.open.name && accountForEvent.votedList.contains(voting.id)) {
        if (voting.status == VotingStatusEnum.open.name && accountForEvent.votingEvent != null && accountForEvent.votingEvent!.completedVotings.contains(voting.id)) {
          openVotedCount++;
          Log.d(_TAG, "openVotedCount_ : $openVotedCount");
        }
      }
    }

    final int newOpenVotingsCount = event.votingCount.open - openVotedCount;

    late Text openVotings;

    if (accountIsValid) {
      openVotings = Text("neu: $newOpenVotingsCount (von ${event.votingCount.open})",
        style: TextStyle(
            fontWeight: newOpenVotingsCount > 0 && accountExists ? FontWeight.bold : FontWeight.normal,
            color: Colors.green),
      );
    }
    else {
      openVotings = Text("offen: ${event.votingCount.open}");
    }

    Widget _trailing = Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        openVotings,
        Text("beendet: ${event.votingCount.finished}")
      ],
    );

    return Column(
      children: [
        ListTile(
          tileColor: controller.lastSelectedEvent != null && controller.lastSelectedEvent! == event ? Styles.selectedTileColor : null,
          leading: leading,
          title: Text(
            event.name,
            style: TextStyle(fontWeight: accountExists ? FontWeight.bold : FontWeight.normal),
          ),
          subtitle: _subtitle != null ? Padding(padding: const EdgeInsets.only(top: 6), child: _subtitle) : null,
          trailing:  _subtitle == null ? Padding(padding: const EdgeInsets.only(top: 6), child: _trailing) : _trailing,

          isThreeLine: false, //_subtitle != null,
          contentPadding: EdgeInsets.symmetric(horizontal: GetPlatform.isMobile ? 4 : 20),
          onTap: accountForEvent != null && accountForEvent.isValidAccount()
              ? () => controller.goToVotingPage(event)
              : () => controller.goToVotingLotPage(event),

          // selected: event == controller.event,
        ),
        const Divider(color: Styles.dividerColor),
      ],
    );
  }

  DropdownMenuItem<Group> _groupItem(Group group) {
    bool hasAccounts = false;
    bool hasWrongAccounts = false;

    for (Account account in controller.apiServer.accounts) {
      if (account.votingEvent != null && account.votingEvent!.id.split("/")[0] == group.id) {
        if (account.privateKeyXB64Enc != null) {
          hasAccounts = true;
        } else {
          hasWrongAccounts = true;
        }
      }
    }

    MaterialColor? iconColor;
    if (hasAccounts && !hasWrongAccounts) {
      iconColor = Colors.green;
    } else if (!hasAccounts && hasWrongAccounts) {
      iconColor = Colors.red;
    } else if (hasWrongAccounts && hasWrongAccounts) {
      iconColor = Colors.orange;
    }

    return DropdownMenuItem(
      value: group,
      child: Container(
        constraints: BoxConstraints(minWidth: 100, maxWidth: fieldBoxMaxWidth * 0.9),
        child: ListTile(
          leading: Icon(
            Icons.group,
            color: iconColor,
          ),
          title: Text(group.name),
          contentPadding: const EdgeInsets.all(0),
          selected: group == controller.group,
        ),
      ),
    );
  }
}
