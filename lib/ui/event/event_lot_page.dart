// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:flutter/material.dart';
// import 'package:flutter_multi_formatter/formatters/masked_input_formatter.dart';
import 'package:get/get.dart';
import 'package:partcp_client/ui/server_user_manager/key_manager_page.dart';
import '../../utils/masked_input_formatter.dart';
import '../server_user_manager/server_user_manager_page.dart';
import '/objects/account.dart';
import '/objects/voting_event.dart';
import '/utils/field_width.dart';
import '/utils/log.dart';
import '/widgets/app_bar.dart';
import '/widgets/card_widget.dart';
import '/widgets/s_spacer.dart';
import '/utils/s_util__date.dart';
import '/widgets/scroll_body.dart';
import '/widgets/error_message_snackbar.dart';
import '/widgets/button_submit.dart';
import '/widgets/text_field_widget.dart';
import '/widgets/widgets.dart';

import 'event_controller.dart';

class EventLotPage extends StatelessWidget {
  final VotingEvent event;

  final _TAG = "EventLotPage";
  final EventController controller;

  // final EventController controller = EventController.to;
  /// AL, 23-10-11
  /// Könnte das ein GetX-Bug sein? EventController.to holt eine veraltete, eigentlich nicht mehr existierende Version des Controlers,
  /// sodass der ApiServer still und heimlich geändert wird. Als Fix wird der Controller im Constructor übergeben

  const EventLotPage(this.controller, this.event, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // WidgetsBinding.instance.addPostFrameCallback((_) {
    //   controller.onPostFrameCallback();
    // });

    return OrientationBuilder(
      builder: (_, __) => GetBuilder<EventController>(
          builder: (_) => Scaffold(
              appBar: MyAppBar(
                title: const Text("Anmeldung"),
              ),
              body: Widgets.loaderBox(loading: controller.serverResponse.receivingData, child: ScrollBody(children: _body)))),
    );
  }

  List<Widget> get _body {

    final Account? accountForEvent = controller.accountForEvent(event);
    bool accountExists = false;
    bool accountIsValid = false;

    if (accountForEvent != null) {
      accountExists = true;
      if (accountForEvent.isValidAccount()) {
        accountIsValid = true;
      }
    }

    List<Widget> items = [_head];

    if (accountExists && !accountIsValid) {
      items.add(_wrongLotCodeExists);
    }
    else {
      // items.add(_userIdAndTan);
      // items.add(_userIdAndTan);
      // items.add(_userIdAndTanAndLotCode);

      if (event.issueService != null) {
        items.add(_goToKeyManagerPageWithIssueService);
      }
      // items.add(_lotCode);
      // items.add(_submitLotCodeBtn);
    }
    items.add(_nextWoRegister);
    items.add(ErrorMessageSnackBar(serverResponse: controller.serverResponse));

    return items;
  }

  /// head
  Widget get _head => CardWidget(
      titleListTile: CardWidget.listTileTitle(
        leading: const Icon(Icons.event),
        title: "${SUtilDate.shortDate(dateTime: event.date!, context: Get.context!)}, ${event.name}",
        subtitle: Text(
          "${event.description.shortDescription}",
        ),
        isThreeLine: false,
      ));


  /// userId and Tan
  // Widget get _userIdAndTan {
  //   if (controller.event == null) return Container();
  //
  //   Account? account = controller.accountForCurrentEvent;
  //   if (account != null && account.isValidAccount()) {
  //     return const CardWidget(
  //       title: "Teilnahmecode für dieses Event bereits vorhanden",
  //     );
  //   }
  //
  //   Widget child = Column(
  //     children: [_userId, Sspacer.s10, _tan],
  //   );
  //
  //   return CardWidget(
  //     title: "Bitte gebe Deine User-ID und TAN ein",
  //     child: child,
  //   );
  // }



  Widget get _goToKeyManagerPage => CardWidget(
      child: Column(
        children: [
          ButtonSubmit(
            submitting: controller.serverResponse.receivingData,
            onPressed: () async {
              Get.to(() => KeyManagerPage(apiServer: event.issueService!.idProviderServer!, event: event,));
              } ,
            text: "Identitätsprüfung starten",
          ),
        ],
      )
  );

  Widget get _goToKeyManagerPageWithIssueService => CardWidget(
      titleListTile: CardWidget.listTileTitle(
          title: "Um an der Veranstaltung teilzunehmen, führe bitte die Identitätsprüfung durch, die der Server bereitstellt."),
      child: Column(
        children: [
          // Text("Um an der Veranstaltung teilzunehmen, führe bitte die Identitätsprüfung durch, die der Server bereitstellt. "),
          SizedBox(height: 16,),
          ButtonSubmit(
            submitting: controller.serverResponse.receivingData,
            onPressed: () async {
              Get.to(() => KeyManagerPage(apiServer: event.issueService!.idProviderServer!, event: event,));
            } ,
            text: "Identitätsprüfung starten",
          ),
        ],
      )
  );



  Widget get _submitLotCodeBtn => CardWidget(
      child: Column(
        children: [
          ButtonSubmit(
            submitting: controller.serverResponse.receivingData,
            onPressed: controller.votingAllowNext ? () => controller.onEventLotCodeSubmit() : null,
            text: "Anmelden",
          ),
        ],
      )
  );

  Widget get _nextWoRegister => CardWidget(
    child: TextButton(onPressed: () => controller.goToVotingPageAsGuest(event), child: const Text("Weiter ohne Anmeldung (Abstimmen nicht möglich)")),
  );


  /// NAMING (USER-ID)
  // Widget get _userId {
  //   return Center(
  //     child: TextFieldWidget(
  //       textController: controller.namingTextController,
  //       labelText: "User-ID",
  //       hint: controller.event!.credentialRules.pattern,
  //       inputFormatters: [
  //         // MaskedInputFormatter(controller.event.credentialNamingValues.pattern, anyCharMatcher: RegExp(r'[a-zA-Z0-9\.]+'))
  //         MaskedInputFormatter(controller.event!.credentialRules.pattern)
  //       ],
  //       onTap: () => ({}),
  //       onSubmitted: (_) => ({}),
  //       onChanged: (_) => controller.onInputChanged(),
  //       autoCorrect: false,
  //       isPasswordField: true,
  //       keyboardType: TextInputType.text,
  //       textAlign: TextAlign.start,
  //       maxBoxLength: fieldBoxMaxWidth,
  //       obscuringCharacter: "#",
  //       enabled: controller.idFieldsEnabled,
  //     ),
  //   );
  // }

  /// CREDENTIAL (TAN, PWD)
  // Widget get _tan {
  //   return Center(
  //     child: TextFieldWidget(
  //       textController: controller.credentialTextController,
  //       labelText: "TAN",
  //       hint: controller.event!.namingRules.pattern,
  //       inputFormatters: [MaskedInputFormatter(controller.event!.namingRules.pattern)],
  //       onTap: () => {},
  //       onSubmitted: (_) => {},
  //       onChanged: (_) => controller.onInputChanged(),
  //       autoCorrect: false,
  //       isPasswordField: true,
  //       keyboardType: int.tryParse(controller.event!.namingRules.charList) != null ? TextInputType.number : TextInputType.text,
  //       textAlign: TextAlign.start,
  //       maxBoxLength: fieldBoxMaxWidth,
  //       obscuringCharacter: "#",
  //       enabled: controller.idFieldsEnabled,
  //     ),
  //   );
  // }
  //
  // Widget get _userIdAndTanAndLotCode {
  //   if (controller.event != null) {
  //     return Column(
  //       children: [
  //         Sspacer.s10,
  //         const Text(
  //           "ODER",
  //           style: TextStyle(fontWeight: FontWeight.bold),
  //         ),
  //       ],
  //     );
  //   }
  //   return Container();
  // }


  Widget get _wrongLotCodeExists {
    return CardWidget(
        titleListTile: CardWidget.listTileTitle(
          leading: const Icon(Icons.new_releases, color: Colors.red),
          title: "Timecodes für diese Abstimmung konnte nicht entschlüsselt werden",
          subtitle: const Text("Der Veranstaltungsleiter hat Dir bereits einen verschlüsselten Teilnahmecode hinterlegt, welcher von dieser ParTCP App abgerufen wurde, "
              "die App konnte den Teilnahmecode jedoch nicht entschlüsseln.\n\n"
              "Die wahrscheinichste Ursache dafür ist, dass Du Dich erneut, mit einem anderem Gerät, registriert hast, nachdem der Teilnahmecode verschlüsselt wurde.\n\n"
              "Lösungsvorschläge:\n\n"
              "- Wenn Du den ursprünglichen Benutzerprofil exportiert und gespeichert hast, importiere diesen in diese App.\n\n"
              "- Wenn der ursprünglichen Benutzerprofil sich auf einem anderen Gerät befindet, welchen Du jetzt nutzen kannst, wechsle zu dem anderen Gerät und rufe 'Neue Teilnahmecodes' erneut auf\n\n"
              "- Wende Dich an den Veranstaltungsleiter, damit er für Dich Deinen Teilnahmecode erneut verschlüsseln und auf den Server hinterlassen kann")),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: ButtonSubmit(
            submitting: controller.serverResponse.receivingData,
            onPressed: () => Get.back(),
            text: "Zurück",
          ),
        ),);
  }


  Widget get _lotCode {
    if (controller.event == null) {
      Log.d(_TAG, "_lotCode; event: ${controller.event}");
      Log.d(_TAG, "_lotCode; votingAccountForCurrentEvent: ${controller.accountForCurrentEvent}");
      Log.d(_TAG, "_lotCode; votingAccountForCurrentEvent!.privateKeyXB64Enc: ${controller.accountForCurrentEvent?.privateKeyXB64Enc}");
      return Container();
    }

    Widget child = TextFieldWidget(
      textController: controller.lotCodeTextController,
      labelText: "Teilnahmecode",
      hint: controller.event!.lotCodeRules.pattern,
      inputFormatters: [MaskedInputFormatter(controller.event!.lotCodeRules.pattern)],
      onTap: () => ({}),
      onSubmitted: (_) => controller.onEventLotCodeSubmit(),
      onChanged: (_) => controller.onInputChanged(),
      autoCorrect: false,
      isPasswordField: false,
      keyboardType: int.tryParse(controller.event!.lotCodeRules.charList) != null ? TextInputType.number : TextInputType.text,
      textAlign: TextAlign.start,
      maxBoxLength: fieldBoxMaxWidth,
      obscuringCharacter: "#",
    );

    return CardWidget(
        titleListTile: CardWidget.listTileTitle(
            title: "Bitte gebe Deinen Teilnahmecode ein",
            subtitle: const Text("Um mit abzustimmen, ist ein Teilnahmecode erforderlich. Du erhälst diesen in der Regel vom Veranstaltungsleiter.")),
        child: child);
  }
}
