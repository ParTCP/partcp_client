import 'package:flutter/material.dart';
// import 'package:flutter_multi_formatter/utils/enum_utils.dart';
import 'package:get/get.dart';
import 'package:partcp_client/widgets/list_tile_simple.dart';
import '../../objects/account.dart';
import '../../objects/voting.dart';
import '../../utils/log.dart';
import '/ui/home/index_controller.dart';

import '/styles/styles.dart';
import '/widgets/readmore.dart';

const _TAG = "EventWidgets";

class EventWidgets {


  Widget eventItemIndexPage(EventObj eventObj, IndexController controller) {
    final Account accountForEvent = eventObj.account;
    Widget leading = const Icon(Icons.event);

    Widget _subtitle() {
      List<Widget> items = [];
      if (eventObj.event.description.shortDescription.isNotEmpty) {
        Widget shortDescription = ReadMoreText(eventObj.event.description.shortDescription,
            trimLines: 3,
            colorClickableText: Colors.pink,
            trimMode: TrimMode.Line,
            trimCollapsedText: 'Show more',
            trimExpandedText: ' Show less',
            style: const TextStyle(fontSize: 16),
            moreStyle: const TextStyle(fontSize: 16, fontWeight: FontWeight.bold));

        items.add(shortDescription);
      }

      Widget eventId = Text(
        "${eventObj.server.url} - ${eventObj.event.id}",
        style: TextStyle(fontStyle: FontStyle.italic, fontSize: Styles.fontSize(TextStyleEnum.info)),
      );

      items.add(eventId);

      if (accountForEvent.votingAllowed != 1) {
        Widget infoNoLotCode = Text(
          "Ohne Stimmberechtigung",
          style: TextStyle(color: Colors.red),
        );
        items.add(infoNoLotCode);

        Widget delEventButton = TextButton(
            onPressed: () => controller.onDeleteEvent(server: eventObj.server, event: eventObj.event),
            style: ButtonStyle(
              padding: MaterialStateProperty.all(
                EdgeInsets.symmetric(vertical: 4.0, horizontal: 0.0),
              ),
            ),
            child: const Text("Veranstaltung entfernen"));
        items.add(delEventButton);
      }


      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: items,
      );
    }

    int openVotedCount = 0;

    for (Voting voting in eventObj.event.votings) {
      Log.d(_TAG, "openVotedCount_ : ${voting.id}; voting.status: ${voting.status}");
      // if (voting.status == VotingStatusEnum.open.name && accountForEvent.votedList.contains(voting.id)) {
      if (voting.status == VotingStatusEnum.open.name && accountForEvent.votingEvent != null && accountForEvent.votingEvent!.completedVotings.contains(voting.id)) {
        openVotedCount++;
        Log.d(_TAG, "openVotedCount_ : $openVotedCount");
      }
    }

    int? newOpenVotingsCount;
    Widget? openVotings;

    try {
      if (accountForEvent.votingAllowed == 1) {
        newOpenVotingsCount = eventObj.event.votingCount.open - openVotedCount;
        openVotings = Text(
          "neu: $newOpenVotingsCount (von ${eventObj.event.votingCount.open})",
          style: TextStyle(
              fontWeight: newOpenVotingsCount > 0 ? FontWeight.bold : FontWeight.normal, color: newOpenVotingsCount > 0 ? Colors.green : null,
              fontSize: Styles.fontSize(TextStyleEnum.info
            )
          ),
        );
      } else {
        openVotings = Text(
          "${eventObj.event.votingCount.open}",
          style: TextStyle(
            fontWeight: FontWeight.normal,
          ),
        );
      }
    } catch (err) {
      Log.w(_TAG, "openVotings: $err");
      openVotings = Text("???", style: TextStyle(fontSize: Styles.fontSize(TextStyleEnum.info) ));
      Log.w(_TAG, err);
    }

    Widget? finishedVotings;
    try {
      finishedVotings = Text("beendet: ${eventObj.event.votingCount.finished}", style: TextStyle(fontSize: Styles.fontSize(TextStyleEnum.info) ));
    } catch (err) {
      Log.w(_TAG, "finishedVotings: $err");
      finishedVotings = Text("???", style: TextStyle(fontSize: Styles.fontSize(TextStyleEnum.info) ));
    }

    try {
      Widget trailing =
          Column(crossAxisAlignment: CrossAxisAlignment.end, mainAxisAlignment: MainAxisAlignment.center, children: [openVotings, finishedVotings]);

      return Column(
        children: [
          ListTileSimple(
              // tileColor: controller.lastSelectedEvent != null && controller.lastSelectedEvent! == eventObj.event ? Styles.selectedTileColor : null,
              selected: controller.lastSelectedEvent != null && controller.lastSelectedEvent! == eventObj.event,
              leading: GetPlatform.isAndroid ? null : leading,
              title: Text(
                eventObj.event.name,
                style: const TextStyle(fontWeight: FontWeight.normal),
              ),
              subtitle: Padding(padding: const EdgeInsets.only(top: 6), child: _subtitle()),
              trailing: trailing,
              isDense: true,
              isThreeLine: false,
              //_subtitle != null,
              contentPadding: const EdgeInsets.symmetric(horizontal: 6),
              onTap: eventObj.event.votings.isEmpty ? null : () => controller.goToVotingPage(eventObj)

              // selected: event == controller.event,
              ),
          const Divider(color: Styles.dividerColor),
        ],
      );
    } catch (err) {
      Log.w(_TAG, "Fehler beim Abruf des Events: $err");
      return const Padding(
        padding: EdgeInsets.only(top: 8.0),
        child: Text("Fehler beim Abruf des Events"),
      );
    }
  }
}
