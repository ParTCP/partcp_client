// Author: Aleksander Lorenz

// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'dart:async';

import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:barcode_scan2/model/scan_result.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:partcp_client/ui/home/index_page.dart';
import 'package:partcp_client/ui/voting/voting_list_page_selector.dart';
import 'package:window_manager/window_manager.dart';
import '../../message_type/participant_details_request.dart';
import '../../utils/scanner_web.dart';
import '../event/event_controller.dart';
import '../event/event_lot_page.dart';
import '../info/about_page.dart';
import '../wallet/create_wallet_by_shortcode_page.dart';
import '/controller/http_controller.dart';
import '/controller/main_controller.dart';
import '/controller/result_status_enum.dart';
import '/message_type/lot_code_request.dart';
import '/objects/abstract_handle_error.dart';
import '/objects/account.dart';
import '/objects/error_message.dart';
import '/objects/group.dart';
import '/objects/lot_code_from_deposit.dart';
import '/objects/server_response.dart';
import '/objects/url_params.dart';
import '/objects/voting_event.dart';
import '/objects/api_server.dart';
import '/objects/wallet.dart';
import '/ui/event/event_list_page.dart';
import '/ui/home/shortcode_controller.dart';
import '/ui/server_user_manager/server_user_manager_page.dart';
import '/ui/wallet/create_wallet_page.dart';
import '/utils/check_for_update.dart';
import '/utils/log.dart';

import '/utils/scanner.dart';
import '/ui/api_server/api_server_page.dart';
import '../voting/default/default/voting_list_page.dart';
import '/ui/wallet/decrypt_wallet_page.dart';
import '/ui/wallet/import_wallet_page.dart';

enum WalletTileDialogKey { export, rename, delete }

class EventObj {
  late ApiServer server;
  late Account account;
  late VotingEvent event;

  EventObj({required this.server, required this.account, required this.event}) {}
}

class IndexController extends GetxController implements HandleError {
  final String _TAG = "IndexController";

  static IndexController to = Get.find<IndexController>();

  MainController mainController = MainController.to;

  // ApiServer? apiServer;

  ApiServer? _apiServer;

  ApiServer? get apiServer {
    // Log.d(_TAG, "event apiServer getter: ${_apiServer?.url}");
    return _apiServer;
  }

  set apiServer (ApiServer? server) {
    _apiServer = server;
    Log.d(_TAG, "event apiServer setter: ${server?.url}");
  }


  ShortCodeController shortCodeController = ShortCodeController();
  ServerResponse serverResponse = ServerResponse();
  ErrorMessage? errorMessage;

  bool isInBackground = false;
  String? appRev;
  String? urlParams;
  bool controllerReady = false;
  bool appHasUpdate = false;

  bool receivingLotCodes = false;
  bool controllerInitialized = false;

  bool get allowCheckForNewKeys {
    if (receivingLotCodes) {
      return false;
    }
    if (apiServer == null) {
      Log.w(_TAG, "checkForNewKeys: Not checked; server == null");
      return false;
    }
    if (apiServer!.url.isEmpty) {
      Log.w(_TAG, "checkForNewKeys: Not checked; server url: ${apiServer!.url}");
      return false;
    }
    if (mainController.pubKeysOnServerValid == null) {
      Log.w(_TAG, "checkForNewKeys: Not checked; pubKeysOnServerValid == null");
      return false;
    }
    if (mainController.pubKeysOnServerValid!) {
      Log.w(_TAG, "checkForNewKeys: Not checked; pubKeysOnServerValid != true");
      return false;
    }
    return true;
  }

  Future<void> initController(var urlParams) async {
    if (controllerInitialized) return;

    // shortCodeController = Get.put(ShortCodeController());
    controllerInitialized = true;

    CheckForUpdate().check(onResponse: (isNew) {
      appHasUpdate = isNew;
      updateMe();
      Log.d(_TAG, "CheckForUpdate: $isNew");
    });

    /// register listener and get last UrlParams, if any
    UrlParams? _uniLink = mainController.uniLinkListener?.listenerAdd(id: this, callback: _onUniLink);
    if (_uniLink != null) {
      _onUniLink(_uniLink);
    }

    appRev ??= await _setAppRev();
    controllerReady = true;
    updateMe();

    Future.delayed(const Duration(seconds: 5)).then((value) => _restoreWindow());
  }

  void _onUniLink (UrlParams uniLink) {
    Log.d(_TAG, "onUniLink: $uniLink");
    if (uniLink.shortCode.isNotEmpty) {
      shortCodeController.shortCodeTextController.text = uniLink.shortCode;
      if (controllerInitialized) {
        _restoreWindow();
        updateMe();
      }
    }
  }


  Future<void> _restoreWindow() async {
    if (GetPlatform.isDesktop && !GetPlatform.isWeb) {
      WindowOptions windowOptions = WindowOptions(
        skipTaskbar: false,
      );
      windowManager.waitUntilReadyToShow(windowOptions, () async {
        await windowManager.show();
        await windowManager.focus();
      });
    }
  }


  Future<String> _setAppRev() async {
    if (mainController.appRev == null) {
      return await mainController.getAppRev();
    } else {
      return mainController.appRev!;
    }
  }

  Future<void> lockWallet() async {
    eventObjList = null;
    apiServer = null;
    await mainController.lockWallet();
    updateMe();
  }

  void forgetServer() {
    apiServer = null;
    updateMe();
  }


  /// CREATE WALLET
  void goToCreateWallet() async {
    ResultStatus _result = await Get.to(() => CreateWalletPage());
    Log.d(_TAG, "goToCreateWallet => result: $_result");
    updateMe();
  }

  /// CREATE WALLET BY SHORT CODE
  void goToCreateWalletByShortCode() async {

    final List<Wallet> wallets = mainController.walletsInBox;
    Wallet? wallet = await Wallet.createAndEncryptWallet(pwdCodeUnits: MainController.to.unsecureWalletPwd.codeUnits, walletName: "Teilnehmer ${wallets.length+1}", protection: WalletProtection.simple);
    if (wallet != null) {
      await onShortCodeSubmit();
      updateMe();
    }
  }


  /// IMPORT WALLET
  Future<void> goToImportWallet() async {
    /// we will get back wallet or null
    Wallet wallet = await Get.to(() => ImportWalletPage());
    await _onWalletDecrypted(wallet);
  }

  Future<void> onWalletChoose(Wallet wallet) async {
    Log.d(_TAG, "onWalletChoose()");

    if (wallet.walletAccount != null && wallet.walletAccount!.protection == WalletProtection.simple) {
      bool isSuccess = await wallet.decryptAccounts(MainController.to.unsecureWalletPwd.codeUnits);

      /// Pop decrypted wallet
      if (isSuccess) {
        await _onWalletDecrypted(wallet);
      }
      else {
        Log.e(_TAG, "_decryptKeys: FAILS");
        // decryptionError = "Passwort falsch";
        updateMe();
      }
    }
    else {
      Wallet? curWallet = await Get.to(() => DecryptWalletPage(wallet: wallet));

      if (curWallet != null) {
        await _onWalletDecrypted(curWallet);
      }
    }
  }

  Future<void> _onWalletDecrypted(Wallet? wallet) async {
    if (wallet != null) {
      Log.d(_TAG, "_onWalletDecrypted -> wallet != null -> OK");

      serverResponse = ServerResponse();
      serverResponse.receivingData = true;
      updateMe();

      await mainController.onWalletDecrypted(wallet);
      serverResponse.receivingData = false;
      updateMe();
    }

    /// FATAL ERROR: WALLET == null !!!
    else {
      Log.e(_TAG, "_onWalletDecrypted; FATAL ERROR: wallet == null!");
    }
    updateMe();
  }

  Future<void> _requestServerDetails(ApiServer server) async {
    Log.d(_TAG, "_connectToServer; url: ${server.url}");

    ServerResponse serverInfo = await server.serverDetailsRequest(account: MainController.to.wallet!.walletAccount);
    if (serverInfo.errorMessage == null) {
      await server.decryptAccountsUserId(mainController.wallet!.pwdBytes!);
      await _checkUsersPubKeyOnServer();
      // checkForNewKeys();
    }
  }

  Future<void> _checkUsersPubKeyOnServer() async {
    if (apiServer != null) {
      // ServerResponse? result = await mainController.wallet!.walletAccount!.requestParticipantDetails(apiServer!);
      ServerResponse? result = await mainController.wallet!.walletAccount!.participantDetailsRequest(forPublicKeys: true);
      Log.d(_TAG, "_checkUsersPubKeyOnServer; result:$result");
      if (result != null && result is bool) {

        /// todo: = true ist quatsch
        // mainController.wallet!.walletAccount!.pubKeysOnServerValid = result;
        mainController.wallet!.walletAccount!.pubKeysOnServerValid = true;
      } else {
        mainController.wallet!.walletAccount!.pubKeysOnServerValid = false;
      }
    }
  }

  Future<void> triggerCheckForNewKeys() async {
    if (!allowCheckForNewKeys) {
      Future.delayed(const Duration(seconds: 1)).then((value) => checkForNewKeys());
    }
  }

  /// todo: rename to connectToWsServer
  // Future<void> checkForNewKeys() async {
  //   mainController.wsController?.initWs();
  // }


  /// todo: temp until messaging server is working
  Future<void> checkForNewKeys() async {
    if (!allowCheckForNewKeys) {
      return;
    }

    receivingLotCodes = true;
    updateMe();

    ApiServer server = apiServer!;
    Account walletAccount = mainController.wallet!.walletAccount!;

    server.lotCodesReceived = true;

    await server.groupListRequest(walletAccount);
    Log.d(_TAG, "_checkForNewKeys; groups: ${server.serverGroups.length}");
    for (Group group in server.serverGroups) {
      await server.eventsByGroup(walletAccount, group, force: true);
    }

    for (List<VotingEvent> events in server.groupEventsMap.values.toList()) {
      for (VotingEvent event in events) {
        if (event.lotCodeDeposited && mainController.wallet!.accountForEvent(server, event.id, Mode.event, keyPairMustExists: true) == null) {
          Map<String, dynamic> messageMap = msgTypeLotCodeRequest(eventId: event.id);

          ServerResponse serverResponse = await HttpController().serverRequestSigned(walletAccount, messageMap, server, loc: "14");
          if (serverResponse.errorMessage == null) {
            Log.d(_TAG, "_checkForNewKeys -----------------------------------------------------");
            Log.d(_TAG, "_checkForNewKeys: ${serverResponse.body}");

            LotCodeFromDeposit? lotCodeFromDeposit;
            try {
              lotCodeFromDeposit = await LotCodeFromDeposit.fromMap(serverResponse.bodyMap!, event, apiServer!);
            } catch (e) {
              // var bla = "ignore";
            }

            /// fatal error
            if (lotCodeFromDeposit == null) {
              showOkAlertDialog(
                  context: Get.context!,
                  title: "Fehler",
                  message: 'Du hast für die Veranstaltung "${event.name}" Schlüssel erhalten, '
                      'diese konnten jedoch nicht entschüsselt werden.');
            } else {
              if (await lotCodeFromDeposit.wrongCodeAlreadySaved == false) {
                /// could not be decrypted
                if (lotCodeFromDeposit.lotCode == null) {
                  await lotCodeFromDeposit.setAsWrong();
                  showOkAlertDialog(
                      context: Get.context!,
                      title: "Fehler",
                      message: 'Du hast für die Veranstaltung "${event.name}" Schlüssel erhalten, '
                          'diese konnten jedoch nicht entschüsselt werden.');
                }

                /// ok
                else {
                  Log.d(_TAG, "_checkForNewKeys: lotCodeFromDeposit: ${lotCodeFromDeposit.lotCode}");

                  // CredentialResponse credentialResponse = CredentialResponse();
                  CredentialResponse credentialResponse = await MainController.to.wallet!.requestCredentialsFromLot(
                      lotCode: lotCodeFromDeposit.lotCode!, event: event, mode: Mode.event, server: apiServer!);

                  /// success
                  if (credentialResponse.keySubmissionSuccess != null && credentialResponse.keySubmissionSuccess!) {
                    showOkAlertDialog(
                        context: Get.context!,
                        title: "Neue Zugangsschlüssel",
                        message: 'Du hast für die Veranstaltung "${event.name}" neue Schlüssel erhalten '
                            'und kannst an dieser teilnehmen, sobald die Veranstaltung gestarted wird');
                  }
                }
              }

              /// lotCode already marked as wrong
              else {
                Log.d(_TAG, "_checkForNewKeys: lotCodeFromDeposit: old wrong lotCode:  ${lotCodeFromDeposit.lotCode}");
              }
            }

            Log.d(_TAG, "_checkForNewKeys -----------------------------------------------------");
          }
        }
      }
    }

    receivingLotCodes = false;
    updateMe();
  }

  /// GO TO SERVER PAGE
  Future<void> goToServerPage() async {
    Log.d(_TAG, "goToServerPage()");
    Get.to(() => ApiServerPage())!.then((_) async {
      Log.d(_TAG, "goToServerPage().then");
      updateMe();
      await _checkUsersPubKeyOnServer();
      Log.d(_TAG,
          "goToServerPage().then await _checkUsersPubKeyOnServer: pubKeysOnServerValid==${mainController.pubKeysOnServerValid}, Server: ${apiServer!.url}");
      updateMe();
      checkForNewKeys();
      updateMe();
    });
  }

  /// GO TO ID PAGE
  void goToIdPage() {
    Get.to(() => EventListPage(apiServer!))!.then((_) => updateMe());
  }

  /// GO TO [ServerUserManagerPage]
  void goToServerUserManagerPage() {
    Get.to(() => ServerUserManagerPage(apiServer!))!.then((_) => updateMe());
  }

  /// GO TO [ServerUserManagerPage]
  void goToAboutPage() {
    Get.to(() => AboutPage())!.then((_) => updateMe());
  }


  bool get urlCodeSubmitAllowed => false;

  Future<void> onShortCodeSubmit() async {
    Log.d(_TAG, "onShortCodeSubmit; shortCodeTextController.text: ${shortCodeController.shortCodeTextController.text}");
    Account? _account;

    serverResponse.receivingData = true;
    updateMe();

    errorMessage = await shortCodeController.getShortCutEnvironment();

    if (errorMessage == null) {

      await _requestServerDetails(shortCodeController.currentServer!);

      /// shortCode with event
      if (shortCodeController.event != null) {
        _account = MainController.to.wallet!.accountForEvent(shortCodeController.currentServer!, shortCodeController.event!.id, Mode.event, keyPairMustExists: true);

        /// account exists already
        if (_account != null && shortCodeController.shortCode!.lotCode.isNotEmpty) {
          Log.w(_TAG, "onShortCodeSubmit -> valid account exists");

          /// already saved LotCode submitted
          if (await _account.compareLotCode(shortCodeController.shortCode!.lotCode)) {
            Log.d(_TAG, "onShortCodeSubmit -> same LotCode submitted");
            // await _account.requestParticipantDetails(forPublicKeys: true);
            await MainController.to.onWalletChanged(updateNotification: false);

            /// todo: macht das Sinn??? 2025-01-24
            // if (_account.userId == _account.participant!.id && await _account.publicKeysConcat != _account.participant!.publicKey) {
            //   Log.d(_TAG, "_account.userId == _account.participant!.id && await _account.publicKeysConcat != _account.participant!.publicKey => keySubmission");
            //   await _account.keySubmission();
            //   await MainController.to.onWalletChanged(updateNotification: false);
            // }
            var bla = "";
          }
          /// new LotCode submitted, but account exists -> show Message and ignore LotCode
          else {
            await showOkAlertDialog(
              context: Get.context!,
              title: "Zugangsdaten bereits vorhanden",
              message: "Unter diesem Benutzerprofil wurde bereits ein "
                  "Teilnahmecode für die Veranstaltung '${shortCodeController.event!.name} ' "
                  "eingelöst. Das Einlösen eines weiteren Codes ist nicht möglich.\n\n"
                  "${mainController.getExtendedAppModeHint()} ",
              okLabel: 'OK',
            );
          }
        }

        /// request credential from LotCode and create Account
        else if (_account == null && shortCodeController.shortCode!.lotCode.isNotEmpty) {
          // CredentialResponse credentialResponse = CredentialResponse();
          CredentialResponse credentialResponse = await MainController.to.wallet!.requestCredentialsFromLot(
              lotCode: shortCodeController.shortCode!.lotCode,
              event: shortCodeController.event!,
              mode: Mode.event,
              server: shortCodeController.currentServer!);

          _account = credentialResponse.account;

          /// error lotRequest
          if (!credentialResponse.lotRequestSuccess!) {
            serverResponse = credentialResponse.lotRequestServerResponse!;
            errorMessage = ErrorMessage(serverResponse);
          }
          /// error keySubmission
          else if (!credentialResponse.keySubmissionSuccess!) {
            serverResponse = credentialResponse.keySubmissionServerResponse!;
            errorMessage = ErrorMessage(serverResponse);
          }
          else {
            // await _account!.requestParticipantDetails(_account.keySubmissionServer!);
            // await _account!.requestParticipantDetails(forPublicKeys: false);
            await MainController.to.onWalletChanged(updateNotification: false);
          }
        }
      }

      serverResponse.receivingData = false;
      updateMe();


      if (errorMessage == null) {

        shortCodeController.shortCodeTextController.text = "";
        await reRequestEvents();
        updateMe();

        /// go to EventPage / EventLotPage
        if (_account != null && _account.isValidAccount()) {
          /// no votings defined jet, go to index page
          if (shortCodeController.event!.votings.isEmpty) {
            Route<dynamic> route = Get.rawRoute!;
            Get.to( () => IndexPage());

            /// remove this route
            Get.removeRoute(route);
          }
          else {
            await shortCodeController.currentServer!.decryptAccountsUserId(mainController.wallet!.pwdBytes!);
            Get.to(() =>
                VotingListPageSelector(server: shortCodeController.currentServer!, account: _account, votingEvent: shortCodeController.event!));
          }
        }
        /// we got Server and Event code => Go to the Event
        else if(_account == null && shortCodeController.currentServer != null && shortCodeController.event != null) {
          shortCodeController.shortCodeTextController.text = "";

          EventController eventController = Get.put(EventController());
          await eventController.initController(shortCodeController.currentServer!, Mode.event);

          /// We have a valid account for this Event => Go to EventPage
          if (eventController.accountForCurrentEvent != null) {

            /// no votings defined jet, go to index page
            if (shortCodeController.event!.votings.isEmpty) {
              Route<dynamic> route = Get.rawRoute!;
              Get.to( () => IndexPage());

              /// remove this route
              Get.removeRoute(route);
            }
            /// go to voting
            else {
              Get.to(() =>
                  VotingListPageSelector(server: shortCodeController.currentServer!, account: _account, votingEvent: shortCodeController.event!));
            }
          }
          /// We don't have a valid account for this Event => Go to EventLotPage
          else {
            Get.to(() => EventLotPage(eventController, shortCodeController.event!));
          }
        }
        /// we got Server only code => Go to the Server
        else if(_account == null && shortCodeController.currentServer != null && shortCodeController.event == null) {
          shortCodeController.shortCodeTextController.text = "";
          Get.to(() => EventListPage(shortCodeController.currentServer!));
        }
        else {
          Log.w(_TAG, "onShortCodeSubmit; account==null or account is invalid");
          serverResponse.errorMessage = "Fatal: account==null or account is invalid";
          errorMessage = ErrorMessage(serverResponse);
          updateMe();
        }
      }
    } else {
      Log.w(_TAG, "onShortCodeSubmit_: currentServer==null or event== null");

      serverResponse.receivingData = false;
      updateMe();
    }
  }

  Future<void> onShortCodeScan() async {
    String result = "";

    if (GetPlatform.isMobile && !GetPlatform.isWeb) {
      ScanResult scanResult = await scanner();
      result = scanResult.rawContent;
    }
    else if (GetPlatform.isMobile && GetPlatform.isWeb) {
      result = await Get.to(() => const ScannerWeb("ParTCP QR-Code Scanner"));
    }

    // Log.d(_TAG, "onShortCodeScan: $result");
    shortCodeController.shortCodeTextController.text = result;
    updateMe();
  }

  Future<void> onDeleteEvent({required ApiServer server, required VotingEvent event}) async {
    OkCancelResult result = await showOkCancelAlertDialog(
      context: Get.context!,
      title: "Veranstaltung entfernen",
      message: "Bist Du sicher, dass die Veranstaltung '${event.name}' aus der Liste entfernt werden soll?",
      okLabel: 'Entfernen',
      cancelLabel: "Abbrechen"
    );

    if (result == OkCancelResult.ok) {
      bool result = MainController.to.wallet!.deleteAccountByEventId(server, event.id);
      if (result) {
        await MainController.to.onWalletChanged(updateNotification: false);
        reRequestEvents();
      }
    }

  }


  bool requestingEvents = false;
  VotingEvent? lastSelectedEvent;

  List<EventObj>? eventObjList;


  Future<void>reRequestEvents() async {
    eventObjList = null;
    _requestEventsDetails();
  }

  Future<void>requestEvents() async {
    if (requestingEvents || eventObjList != null) {
      return;
    }
    _requestEventsDetails();
  }

  Future<void>_requestEventsDetails() async {
    requestingEvents = true;
    updateMe();

    eventObjList = [];
    List<ApiServer> serverList = mainController.wallet!.serverList;

    for (ApiServer server in serverList) {
      for (Account account in server.accounts) {
        if (account.votingEvent != null && account.votingEvent!.id.isNotEmpty && account.isValidAccount()) {
          // VotingEvent event =  VotingEvent(account.votingEventId!, server, isNonAnonymous: account.votingEventIsNonAnonymous);
          VotingEvent event =  account.votingEvent!;
          await event.eventDetailsRequest(server, account, includeCompletedVotings: event.isNonAnonymous);
          EventObj eventObj = EventObj(server: server, account: account, event: event);
          eventObjList!.add(eventObj);
        }
      }
    }
    eventObjList = eventObjList!.reversed.toList();


    requestingEvents = false;
    updateMe();
  }

  void goToVotingPage(EventObj eventObj) {
    lastSelectedEvent = eventObj.event;

    // Get.to(() => VotingListPage(
    //   server: eventObj.server,
    //   account: eventObj.account,
    //   votingEvent: eventObj.event,
    // ));

    Get.to(() => VotingListPageSelector(
      server: eventObj.server,
      account: eventObj.account,
      votingEvent: eventObj.event,
    ));
  }

  Future<void> updateMe() async {
    Log.d(_TAG, "updateMe(); apiServer: ${apiServer?.url}");
    update();
  }

  Future<void> onRefresh() async {
    Log.d(_TAG, "onRefresh()");
    update();
  }


  @override
  void onErrorMessageDismiss() {
    Log.d(_TAG, "onErrorMessageDismiss");
    errorMessage = null;
    updateMe();
  }
}