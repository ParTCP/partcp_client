
import 'dart:io';

import 'package:flutter/material.dart';

import '../../widgets/app_bar.dart';
import '../../widgets/button_sub.dart';

class IndexErrorPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: MyAppBar(
        title: Text("ParTCP Vote"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              "Die App kann nicht erneut gestartet werden, solange eine Instanz der App geöffnet ist.",
              textScaleFactor: 1.3,
              textAlign: TextAlign.center,
              style: (TextStyle(

              )),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 12.0, bottom: 50),
              child: ButtonSub(onPressed: () => _closeApp(), child: const Text("App schliessen")),
            ),
          ],
        )
      ),
    );
  }

  void _closeApp() {
    exit(0);
  }

}