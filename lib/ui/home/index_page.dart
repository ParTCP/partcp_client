// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'dart:io';

import 'package:flutter_linkify/flutter_linkify.dart';
import 'package:partcp_client/objects/storage/app_box.dart';
import 'package:partcp_client/widgets/card_widget_simple.dart';
import 'package:partcp_client/widgets/list_tile_simple.dart';
import 'package:window_manager/window_manager.dart';

import '../../styles/styles.dart';
import '../../widgets/button_sub.dart';
import '../../widgets/scroll_body.dart';
import '../../widgets/text_scale_widget.dart';
import '/constants.dart';
import '/objects/wallet.dart';
import '/ui/app_update/app_update_page.dart';
import '/ui/info/info_page.dart';
import '/utils/launch_url.dart';
import '/utils/log.dart';
import '/utils/s_util__date.dart';

import '/controller/main_controller.dart';
import '/widgets/app_bar.dart';
import '/widgets/card_widget.dart';
import '/widgets/widgets.dart';
import '/widgets/text_field_widget.dart';
import '/widgets/error_message_snackbar.dart';
import 'event_widgets.dart';
import '../wallet/export_wallet.dart';
import 'index_controller.dart';

import 'package:move_to_background/move_to_background.dart';
import 'package:getwidget/getwidget.dart';
import "package:universal_html/html.dart" as html;
import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

enum _MenuItems {
  importWallet,
  exportWallet,
  deleteWallet,
  lockWallet,
  fullscreen,
  // extendedAppMode,
  // info,
  registerOnKeyManager,
  appUpdate,
  multiClient,
  // slowMachine,
  textScaleButtons,
  about,
  nfcTest,
}

const buttonPadding = 10.0;

class IndexPage extends StatelessWidget with WidgetsBindingObserver, WindowListener {
  final _TAG = "IndexPage";
  final urlParams;

  IndexPage({Key? key, this.urlParams}) : super(key: key);

  static IndexController to = Get.find<IndexController>();

  final MainController mainController = MainController.to;
  final IndexController controller = Get.put(IndexController());

  final TextEditingController urlController = TextEditingController();

  void _deleteWalletDialog() async {
    /// ask to delete
    final result = await showOkCancelAlertDialog(
      context: Get.context!,
      //title: "Schlüsselbund '${mainController.wallet!.walletAccount!.title}' löschen?",
      title: "Benutzerprofil löschen?",
      isDestructiveAction: true,
      message:
          "Bist Du sicher, dass Du das Benutzerprofil '${mainController.wallet!.walletAccount!.title}' und alle dazugehörigen Daten löschen möchtest?"
          //"\nFalls Du keine Sichernungkopie von dem Schlüsselbund exportiert hast, kannst Du diesen nie wieder verwenden.",
          "\nDies lässt sich nicht rückgängig machen.",
      okLabel: 'LÖSCHEN',
      cancelLabel: 'ABBRECHEN',
    );

    if (result == OkCancelResult.ok) {
      await mainController.deleteWallet();
      Get.back();
      controller.lockWallet();
      controller.initController(null);
      controller.updateMe();
    }
  }

  Future<void> _handleMenuClick(_MenuItems value) async {
    switch (value) {
      case _MenuItems.importWallet:
        //ToDo: ImportWallet().download().then((value) => controller.updateMe());
        controller.goToImportWallet();
        break;

      case _MenuItems.exportWallet:
        ExportWallet().download().then((value) => controller.updateMe());
        break;

      case _MenuItems.deleteWallet:
        _deleteWalletDialog();
        break;

      case _MenuItems.lockWallet:
        controller.lockWallet();
        break;

      case _MenuItems.fullscreen:
        MainController.to.isFullScreen ? html.document.exitFullscreen() : html.document.documentElement!.requestFullscreen();
        MainController.to.isFullScreen = !MainController.to.isFullScreen;
        break;

      //case _MenuItems.info:
      //Get.to(() => InfoPage());
      //  break;

      case _MenuItems.appUpdate:
        Get.to(() => AppUpdatePage());
        break;

      case _MenuItems.multiClient:
        // Get.to(() => AppUpdatePage());
        break;

      case _MenuItems.registerOnKeyManager:
        controller.goToServerUserManagerPage();
        break;

      case _MenuItems.about:
        controller.goToAboutPage();
        break;

      case _MenuItems.nfcTest:
        // Get.to(() => NfcTest());
        break;

      // case _MenuItems.mqttTest:
      //   // runMqtt();
      //   break;
      //
      default:
        break;
    }
  }

  List<PopupMenuItem<_MenuItems>> _popupMenuItems() {
    List<PopupMenuItem<_MenuItems>> list = [];

    // list.add(PopupMenuItem<_MenuItems>(value: _MenuItems.importWallet, child: Text("Schlüsselbund importieren")));

    list.add(PopupMenuItem<_MenuItems>(
        enabled: (mainController.encryptedWalletExists() || mainController.walletIsDecrypted()),
        value: _MenuItems.exportWallet,
        child: const Text("Benutzerprofil exportieren")));

    // list.add(PopupMenuItem<_MenuItems>(enabled: true, value: _MenuItems.createWallet, child: Text("Schlüsselbund erstellen")));

    list.add(PopupMenuItem<_MenuItems>(
        enabled: (mainController.encryptedWalletExists() || mainController.walletIsDecrypted()),
        value: _MenuItems.deleteWallet,
        child: const Text("Benutzerprofil löschen")));

    if (GetPlatform.isMobile && GetPlatform.isWeb) {
      list.add(
          PopupMenuItem<_MenuItems>(value: _MenuItems.fullscreen, child: Text(MainController.to.isFullScreen ? "Vollbild verlassen" : "Vollbild")));
    }

    if ((GetPlatform.isMobile && GetPlatform.isAndroid) || GetPlatform.isDesktop) {
      Widget child = controller.appHasUpdate
          ? Stack(
              children: const [
                Padding(
                  padding: EdgeInsets.only(left: 5),
                  child: Text("Aktualisierung"),
                ),
                Align(
                  alignment: Alignment.topLeft,
                  child: GFBadge(
                    size: 15,
                    shape: GFBadgeShape.circle,
                  ),
                )
              ],
            )
          : Text("Aktualisierung");
      list.add(PopupMenuItem<_MenuItems>(value: _MenuItems.appUpdate, child: child));
    }

    if (mainController.encryptedWalletExists() && mainController.walletIsDecrypted()) {
      list.add(const PopupMenuItem<_MenuItems>(value: _MenuItems.registerOnKeyManager, child: Text("KeyManager")));
    }

    list.add(PopupMenuItem<_MenuItems>(
        value: _MenuItems.multiClient,
        child: CheckboxListTile(
          title: const Text("Mehr-Benutzer-Modus"),
          value: controller.mainController.extendedAppMode,
          contentPadding: const EdgeInsets.all(0),
          onChanged: (val) {
            Log.d(_TAG, "IndexMenu onClick extendedAppMode => new Val: $val");
            controller.mainController.setExtendedAppMode(val ??= false);
            controller.updateMe();
            Get.back();
          },
          // controlAffinity: ListTileControlAffinity.leading
        )));

    list.add(PopupMenuItem<_MenuItems>(
        value: _MenuItems.textScaleButtons,
        child: CheckboxListTile(
          title: const Text("Schriftgröße-Buttons anzeigen"),
          value: controller.mainController.appBox!.getValue(AppBoxEnum.showTextScaleWidget, defValue: false),
          contentPadding: const EdgeInsets.all(0),
          onChanged: (val) {
            Log.d(_TAG, "IndexMenu onClick textScaleButtons => new Val: $val");
            controller.mainController.appBox!.setValue(AppBoxEnum.showTextScaleWidget, val);
            controller.updateMe();
            Get.back();
          },
          // controlAffinity: ListTileControlAffinity.leading
        )));


    // list.add(PopupMenuItem<_MenuItems>(
    //     value: _MenuItems.slowMachine,
    //     child: CheckboxListTile(
    //       title: const Text("Vereinfachte Darstellung (schneller)"),
    //       value: controller.mainController.isSlowMachine,
    //       contentPadding: const EdgeInsets.all(0),
    //       onChanged: (val) {
    //         Log.d(_TAG, "IndexMenu onClick slowMachine => new Val: $val");
    //         controller.mainController.setSlowMachine(val ??= false);
    //         controller.updateMe();
    //         Get.back();
    //       },
    //       // controlAffinity: ListTileControlAffinity.leading
    //     )));

    list.add(PopupMenuItem<_MenuItems>(value: _MenuItems.about, child: const Text("Über ParTCP Vote")));

    list.add(PopupMenuItem<_MenuItems>(
        enabled: (mainController.encryptedWalletExists() || mainController.walletIsDecrypted()),
        value: _MenuItems.lockWallet,
        child: const Text("Anwendung sperren")));

    return list;
  }

  @override
  void onWindowFocus() {
    // Make sure to call once.
    Log.d(_TAG, "onWindowFocus");
    controller.updateMe();
    // do something
  }

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      controller.initController(urlParams);
      //_run();
    });

    return WillPopScope(
      onWillPop: () async {
        if (GetPlatform.isWeb) {
          return false;
        } else if (GetPlatform.isAndroid) {
          MoveToBackground.moveTaskToBack();
          return false;
        } else {
          return true;
        }
      },
      child: OrientationBuilder(
        builder: (_, __) => GetBuilder<IndexController>(
          initState: (_) {
            WidgetsBinding.instance.addObserver(this);
          },
          builder: (context) => Scaffold(
            // backgroundColor: const Color.fromRGBO(240, 240, 240, 1),
            appBar: MyAppBar(
              title: Text("ParTCP Vote ${controller.mainController.currentWalletAppBarTitle}"),
              isStartPage: true,
              actions: [

                if (MainController.to.appBox!.getValue(AppBoxEnum.showTextScaleWidget, defValue: false))
                  textScaleWidget(onUpdate: () => controller.updateMe()),

                PopupMenuButton<_MenuItems>(
                    //icon: Icon(Icons.menu),
                    icon: controller.appHasUpdate
                        ? GFIconBadge(
                            position: GFBadgePosition.topStart(top: 0, start: 0),
                            counterChild: const GFBadge(
                              size: 15,
                              shape: GFBadgeShape.circle,
                            ),
                            child: const Icon(Icons.menu),
                          )
                        : const Icon(Icons.menu),
                    onSelected: _handleMenuClick,
                    itemBuilder: (BuildContext context) => _popupMenuItems()),
              ],
            ),

            body: controller.controllerReady
                // ? ScrollBody(children: _body())
                ? Widgets.loaderBox(
                    child: ScrollBody(
                      shrinkWrap: true,
                      children: _body(),
                    ),
                    loading: controller.serverResponse.receivingData)
                : _splash(),
          ),
        ),
      ),
    );
  }

  Widget _splash() {
    return const Center(
      child: Text("loading..."),
    );
  }

  List<Widget> _body() {
    List<Widget> list = [
      ErrorMessageSnackBar(
        errorMessage: controller.errorMessage,
        onCloseCallback: () => controller.onErrorMessageDismiss(),
      )
    ];
    bool allowCheckNewKeys = false;

    ////////////////////////////////////////////////////
    /// NO WALLET
    if (!mainController.walletIsDecrypted()) {
      final List<Wallet> wallets = mainController.walletsInBox;

      /// CREATE
      if (wallets.isEmpty) {
        // list.add(_welcome());
      }

      /// CHOOSE
      else {
        /// multiUserMode = false => load last wallet
        if (mainController.extendedAppMode) {
          list.add(_decryptedWalletList(wallets));
        } else {
          Wallet wallet = wallets.first;
          controller.onWalletChoose(wallet);
        }
      }

      if (wallets.isNotEmpty && mainController.extendedAppMode) {
        list.add(_createWallet());
        list.add(_importWallet());
      } else if (wallets.isEmpty) {
        list.add(_createWalletByShortCode());
        if (mainController.extendedAppMode) {
          list.add(_createWallet());
          list.add(_importWallet());
        }
      }

      // list.add(_appRev());
    }

    ////////////////////////////////////////////////////
    /// WALLET LOADED
    else {
      list.add(_shortCodeMainPage());

      if (mainController.walletIsDecrypted()) {
        list.add(_myEventList);
      }
    }

    if (controller.appHasUpdate) {
      list.add(_appUpdate);
    }

    return list;
  }

  Widget? get _scanShortCodeWidget {
    if (GetPlatform.isMobile) {
      return InkWell(
          onTap: () => controller.onShortCodeScan(),
          child: const Icon(
            Icons.qr_code,
            color: Colors.black38,
          ));
    }
    return null;
  }

  Widget _shortCodeMainPage() {
    return CardWidgetSimple(
      // title: "Teilnahmecode",
      child: ListTileSimple(
        title: Text("Teilnahmecode"),
        subtitle: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text("Trage hier den Code ein, den Du vom Veranstalter erhalten hast"),

            /// Teilnahmecode / Servercode
            Padding(
              padding: const EdgeInsets.only(top: 18.0),
              child: TextFieldWidget(
                  textController: controller.shortCodeController.shortCodeTextController,
                  labelText: "Teilnahmecode",
                  hint: "",
                  // onTap: () => (_),
                  onSubmitted: (_) => (_),
                  onChanged: (_) => controller.updateMe(),
                  autoCorrect: false,
                  isPasswordField: false,
                  keyboardType: TextInputType.visiblePassword,
                  textAlign: TextAlign.start,
                  maxBoxLength: 550,
                  showBorder: true,
                  suffixIcon: _scanShortCodeWidget),
            ),

            Align(
                alignment: Alignment.centerRight,
                child: ButtonBar(
                  children: [
                    controller.shortCodeController.shortCodeTextController.text.isNotEmpty
                        ? TextButton(
                            onPressed: () {
                              controller.shortCodeController.shortCodeTextController.text = "";
                              controller.updateMe();
                            },
                            child: const Text("Löschen"))
                        : Container(),
                    TextButton(
                      onPressed: controller.shortCodeController.urlCodeSubmitAllowed ? () => controller.onShortCodeSubmit() : null,
                      child: const Text("Weiter"),
                    )
                  ],
                ))
          ],
        ),
      ),
    );
  }

  Widget _welcome() {
    return CardWidgetSimple(
      child: ListTileSimple(
        title: Text("ParTCP Vote ist freie Software, die von einer Gemeinschaft ehrenamtlicher tätiger Menschen entwickelt und betreut wird."),
        subtitle: Text("Auf der Webseite https://partcp.org kannst Du mehr über das Projekt erfahren."),
      ),
    );
  }

  /// CREATE WALLET BY SHORTCODE
  Widget _createWalletByShortCode() {
    return CardWidgetSimple(
        child: ListTileSimple(
      title: Text("Teilnahmecode"),
      // subTitleStyle: Styles.H2,
      subtitle: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text("Trage hier den Code ein, den Du vom Veranstalter erhalten hast"),

          /// Teilnahmecode / Servercode
          Padding(
            padding: const EdgeInsets.only(top: 18.0),
            child: TextFieldWidget(
                textController: controller.shortCodeController.shortCodeTextController,
                labelText: "Teilnahmecode",
                hint: "",
                // onTap: () => (_),
                onSubmitted: (_) => (_),
                onChanged: (_) => controller.updateMe(),
                autoCorrect: false,
                isPasswordField: false,
                keyboardType: TextInputType.visiblePassword,
                textAlign: TextAlign.start,
                maxBoxLength: 550,
                showBorder: true,
                suffixIcon: _scanShortCodeWidget),
          ),

          Align(
              alignment: Alignment.centerRight,
              child: ButtonBar(
                children: [
                  controller.shortCodeController.shortCodeTextController.text.isNotEmpty
                      ? TextButton(
                          onPressed: () {
                            controller.shortCodeController.shortCodeTextController.text = "";
                            controller.updateMe();
                          },
                          child: const Text("Löschen"))
                      : Container(),
                  ElevatedButton(
                    // onPressed: controller.shortCodeController.urlCodeSubmitAllowed ? () => controller.onShortCodeSubmit() : null,
                    onPressed: controller.shortCodeController.urlCodeSubmitAllowed ? () => controller.goToCreateWalletByShortCode() : null,
                    child: const Text("Weiter"),
                  )
                ],
              )),

          const SizedBox(height: 16),

          // ElevatedButton(
          //   child: Text("Ich habe keinen Teilnahmecode erhalten"),
          //   onPressed: () => controller.goToCreateWallet()
          // ),
          //
          // const SizedBox(height: 8),
        ],
      ),
    ));
  }

  //

  /// CREATE WALLET
  Widget _createWallet() {
    return _cardWidgetWithButton(
        title: "Neues Benutzerprofil erstellen",
        text: "Beim ersten Einrichten oder wenn Du Dein "
            "Passwort vergessen hast oder Dein Gerät mit einer anderen Person teilst.",
        onPressed: () => controller.goToCreateWallet(),
        onPressedText: "Erstellen");
  }

  /// IMPORT WALLET
  Widget _importWallet() {
    return _cardWidgetWithButton(
        title: "Benutzerprofil importieren",
        text: "Wenn Du zu einem früheren Zeitpunkt ein Benutzerprofil exportiert hast, kannst Du es hier wieder einlesen.",
        onPressed: () => controller.goToImportWallet(),
        onPressedText: "Importieren");
  }

  /// EXPORT WALLET
  Widget _exportWallet() {
    return mainController.walletWasChanged && GetPlatform.isWeb
        ? _cardWidgetWithButton(
            title: "Benutzerprofil exportieren",
            text: "Dein Benutzerprofil wurde verändert und sollte zur Sicherheit exportiert werden.",
            onPressed: () => ExportWallet()..download().then((value) => controller.updateMe()),
            onPressedText: "exportieren")
        : Container();
  }

  /// DECRYPT WALLET
  Widget _decryptWallet(Wallet wallet) {
    return _cardWidgetWithButton(
        title: "Benutzerprofil entsperren",
        text: "Benutzerprofil ist bereits vorhanden.",
        onPressed: () => controller.onWalletChoose(wallet),
        onPressedText: "entsperren");
  }

  /// USED SERVER
  Widget _usedServer() {
    if (!controller.apiServer!.lotCodesReceived) {
      controller.triggerCheckForNewKeys();
    }

    return _cardWidgetWithButton(
        title: "Verbunden",
        text: "Server: ${controller.apiServer!.name}",
        onPressed: () {
          controller.forgetServer();
          controller.updateMe();
        },
        onPressedText: "trennen");
  }

  /// GO TO SERVER
  Widget _goToServer() {
    return _cardWidgetWithButton(
        title: "Verbinden",
        text: "Dein Benutzerprofil ist bereit, mit Server verbinden",
        onPressed: () => controller.goToServerPage(),
        onPressedText: "verbinden");
  }

  /// GO TO KEY MANAGER PAGE
  Widget _goToRegisterKey() {
    return _cardWidgetWithButton(
        title: "Registrierung erforderlich",
        text: "Eine Registrierung am Server ist erforderlich",
        onPressed: () => controller.goToServerUserManagerPage(),
        onPressedText: "registrieren");
  }

  /// GO TO KEY MANAGER PAGE
  Widget _goToUpdateKey() {
    return _cardWidgetWithButton(
        title: "Wiederholung der Registrierung erforderlich",
        text:
            "Eine erneute Registrierung am Server ist erforderlich, da der am Server hinterlegte öffentliche Schlüssel nicht mit dem Schlüssel dieses Schlüsselbundes übereinstimmt",
        onPressed: () => controller.goToServerUserManagerPage(),
        onPressedText: "registrieren");
  }

  /// GO TO EVENTS
  Widget _goToServerEvents() {
    return _cardWidgetWithButton(
        title: "Veranstaltungen duchstöbern",
        text: "Zu den Veranstaltungen und Abstimmungen",
        onPressed: () => controller.goToIdPage(),
        onPressedText: "stöbern");
  }

  /// CHECK FOR NEW KEYS
  Widget _checkForNewKeys() {
    return _cardWidgetWithButton(
        title: "Neue Teilnahmecodes",
        text: "Prüfen, ob auf dem Server neue Teilnahmecodes für Veranstaltungen für Dich vorliegen",
        onPressed: controller.receivingLotCodes ? null : () => controller.checkForNewKeys(),
        onPressedText: controller.receivingLotCodes ? "prüfe ..." : "prüfen");
  }

  Widget get _myEventList {
    controller.requestEvents();
    Widget w;

    if (controller.requestingEvents) {
      w = Padding(
        padding: const EdgeInsets.all(4.0),
        child: Align(alignment: Alignment.centerLeft, child: SizedBox(width: 35, height: 35, child: Widgets.loaderM)),
      );
    } else if (controller.eventObjList!.isEmpty) {
      w = const Padding(
        padding: EdgeInsets.symmetric(vertical: 8.0),
        child: Text("Wenn Du Teilnahmecodes einlöst, werden hier die betreffenden Veranstaltungen aufgelistet."),
      );
    } else {
      List<Widget> items = [];
      for (var i = 0; i < controller.eventObjList!.length; i++) {
        EventObj eventObj = controller.eventObjList![i];
        items.add(EventWidgets().eventItemIndexPage(eventObj, controller));
      }
      w = ListView(
        shrinkWrap: true,
        physics: const NeverScrollableScrollPhysics(),
        children: items,
      );
    }

    return CardWidgetSimple(
      child: ListTileSimple(
        title: Row(
          children: [
            Text("Meine Veranstaltungen"),
            Padding(
              padding: const EdgeInsets.only(left: 10),
              child: InkWell(
                onTap: () => controller.reRequestEvents(),
                child: const Icon(Icons.refresh),
              ),
            )
          ],
        ),
        subtitle: Padding(
          padding: const EdgeInsets.only(top: 8.0),
          child: w,
        ),
      ),
    );

  }

  /// APP UPDATE
  Widget get _appUpdate {
    return _cardWidgetWithButton(
        title: "Neue Version",
        text: "Eine neue Version ist vorhanden",
        onPressed: () {
          if (GetPlatform.isWeb) {
            launchURL(Constants.APP_HOME);
          } else {
            Get.to(() => AppUpdatePage());
          }
        },
        onPressedText: "update");
  }

  Widget _cardWidgetWithButton({required String title, required String text, required Function()? onPressed, required String onPressedText}) {
    return CardWidgetSimple(
      child: ListTileSimple(
        title: Text(title),
        subtitle: Text(text),
        trailing: TextButton(onPressed: onPressed, child: Text(onPressedText)),
        onTap: onPressed,
      ),
    );

    // return CardWidget(
    //
    //   titleListTile:
    //   CardWidget.listTileTitle(title: title,
    //       subtitle: Padding(
    //         padding: const EdgeInsets.fromLTRB(0, 20, 0, 0), // Adjust the padding as needed
    //         child: Text(
    //           text,
    //           //style: TextStyle(fontSize: 12),
    //         ),
    //       ),
    //       trailing: TextButton(onPressed: onPressed, child: Text(onPressedText))),
    //   onWidgetTap: onPressed,
    // );
  }

  /// CARD WIDGET
  Widget _cardWidget({required String title, required Widget body}) {
    return CardWidget(
      titleListTile: CardWidget.listTileTitle(title: title, subtitle: body),
    );
  }

  /// CARD WIDGET CUSTOM
  Widget _cardWidgetCustom({required Widget title, required Widget body}) {
    return CardWidget(
      titleListTile: CardWidget.listTileTitle(title: title, subtitle: body),
    );
  }

  /// DECRYPTED WALLET LIST
  Widget _decryptedWalletList(List<Wallet> wallets) {
    List<Widget> items = [];
    for (Wallet wallet in wallets) {
      items.add(_walletTile(wallet));
    }
    Widget list = ListView(
      shrinkWrap: true,
      children: items,
    );

    return _cardWidget(title: "Vorhandenes Benutzerprofil auswählen:", body: list);
  }

  Widget _walletTile(Wallet wallet) {
    final String dateC = SUtilDate.shortDate(dateTime: wallet.walletAccount!.dateC!, context: Get.context!);
    final String dateU = SUtilDate.shortDate(dateTime: wallet.walletAccount!.dateU!, context: Get.context!);
    return ListTile(
      onTap: () => controller.onWalletChoose(wallet),
      title: Text(wallet.walletAccount!.title),
      subtitle: Text("Erstellt am $dateC - zuletzt verwendet $dateU"),
      leading: const InkWell(
          // onTap: () => controller.walletTileDialog(wallet),
          // child: Icon(Icons.settings)),
          child: Icon(Icons.wallet)),
    );
  }
}
