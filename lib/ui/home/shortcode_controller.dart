import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:partcp_client/objects/description.dart';
import '/controller/main_controller.dart';
import '/objects/error_message.dart';
import '/objects/server_response.dart';
import '/objects/api_server.dart';
import '/utils/log.dart';
import '/objects/short_code.dart';
import '/objects/voting_event.dart';


// class ShortCodeController extends GetxController {
class ShortCodeController {
  final _TAG = "ShortCodeController";

  // static ShortCodeController to = Get.find<ShortCodeController>();
  MainController mainController = MainController.to;


  ServerResponse serverResponse = ServerResponse();
  TextEditingController shortCodeTextController = TextEditingController();

  VotingEvent? event;
  ApiServer? currentServer;
  ShortCode? shortCode;

  bool get urlCodeSubmitAllowed => shortCodeTextController.text.isNotEmpty;

  Future<void> initController() async {
    Log.d(_TAG, "------------ initController ------------");
    serverResponse.errorMessage = null;
    shortCode = null;
  }


  Future<ErrorMessage?> getShortCutEnvironment() async{

    Log.d(_TAG, "------------ getShortCutEnvironment ------------");
    serverResponse = ServerResponse();

    if (mainController.wallet == null)  {
      serverResponse.errorMessage = "Wallet not encrypted";
      ErrorMessage errorMessage = ErrorMessage(serverResponse);
      return errorMessage;
    }

    try {
      /// parse and create [ShortCode]
      shortCode = ShortCode(shortCodeTextController.text);

      if (shortCode!.serverCode.isEmpty && shortCode!.serverUrl.isEmpty) {
        Log.e(_TAG, "getShortCutEnvironment: serverCode==null => return");

        serverResponse.errorMessage = "Invalid Shortcode";
        ErrorMessage errorMessage = ErrorMessage(serverResponse);
        return errorMessage;
      }

      ApiServer? serverTest;

      if (shortCode!.serverUrl.isNotEmpty) {
        serverTest = ApiServer(shortCode!.serverUrl);
      }
      /// request Server list from Asset Server Url to resolve the server shortCode
      else {
        List<ApiServer> serverList = await ApiServer.createServerListFromAssetUrl(); //enthält die Server
        for (ApiServer server in serverList) {
          if (server.shortCode == shortCode!.serverCode) {
            serverTest = server;
            break;
          }
        }
      }

      ApiServer? walletServer = mainController.wallet!.getServerByUrl(serverTest!.url);
      if (walletServer != null) {
        await walletServer.loadAccountsFromWallet();
        currentServer = walletServer;
      }
      else {
        serverTest.serverDetailsRequest(account: mainController.wallet!.walletAccount!);
        await serverTest.loadAccountsFromWallet();
        currentServer = serverTest;
      }





    /// request Server list from Asset Server Url to resolve the server shortCode
      // List<ApiServer> serverList = await ApiServer.createServerListFromAssetUrl(); //enthält die Server
      // for (ApiServer server in serverList) {
      //   String currSrvUrl = '';
      //   if (server.shortCode.toLowerCase() == shortCode!.serverCode){
      //
      //     ApiServer? walletServer = mainController.wallet!.getServerByUrl(server.url);
      //     if (walletServer != null) {
      //       await walletServer.loadAccountsFromWallet();
      //       currentServer = walletServer;
      //     }
      //     else {
      //       currentServer = server;
      //     }
      //
      //     currSrvUrl = server.url;
      //
      //     server.requestServerDetails(account: mainController.wallet!.walletAccount!);
      //     await server.loadAccountsFromWallet();
      //
      //     Log.d(_TAG, "<ApiServer> ShortCode: ${server.shortCode}");
      //     Log.d(_TAG, "currSrvUrl: ${currSrvUrl}");
      //     break;
      //   }
      // }

      if (currentServer == null){
        Log.e(_TAG, "getShortCutEnvironment: currentServer==null => return");
        serverResponse.errorMessage = "Invalid Shortcode. Could not determine Server";
        ErrorMessage errorMessage = ErrorMessage(serverResponse);
        return errorMessage;
      }

      if (shortCode!.eventCode.isNotEmpty) {
        /// request Event details
        VotingEvent eventTmp = VotingEvent("'${shortCode!.eventCode}'", currentServer!, description: Description());
        serverResponse = await eventTmp.eventDetailsRequest(currentServer!, mainController.wallet!.walletAccount!, includeCompletedVotings: eventTmp.isNonAnonymous);
        if (!serverResponse.isOk("getShortCutEnvironment_sEventDetailsRequest")) {
          ErrorMessage errorMessage = ErrorMessage(serverResponse);
          return errorMessage;
        }
        event = eventTmp;
      }

    }
    catch(e,s) {
      Log.e(_TAG, "_createServerList error: $e, stack: $s");
      serverResponse.errorMessage = "$e";
      ErrorMessage errorMessage = ErrorMessage(serverResponse);
      return errorMessage;
    }
    return null;
  }
}