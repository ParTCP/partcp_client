
import 'dart:io';

//import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:package_info_plus/package_info_plus.dart';

import 'package:about/about.dart';
//import 'package:partcp_client/controller/main_controller.dart';
import 'package:yaml/yaml.dart';

import '../../controller/main_controller.dart';
import '../../widgets/app_bar.dart';
import '../../widgets/scroll_body.dart';
//import '../app_update/app_update_controller.dart';

class AboutController extends GetxController {
  //PackageInfo? packageInfo;

  Future<void> initController() async {
    //packageInfo = await PackageInfo.fromPlatform();
    updateMe();
  }

  void updateMe() {
    update();
  }
}


class AboutPage extends StatelessWidget with WidgetsBindingObserver{

  final AboutController controller = Get.put(AboutController());

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      controller.initController();
    });

    return OrientationBuilder(
      builder: (_,__) => GetBuilder<AboutController>(
        initState: (_) {
          WidgetsBinding.instance.addObserver(this);
        },
        builder: (_) => Scaffold(
          appBar: MyAppBar(
            title: Text("Über ParTCP Vote"),
          ),
          // body: ScrollBody(children: [
          //   _body
          // ],),
          body: _body,
        ),

      ),
    );
  }

  final _body = Padding(
    padding: const EdgeInsets.symmetric(horizontal: 20),
    child: AboutContent(
      applicationName: "ParTCP Vote",
      applicationVersion: MainController.to.appVersion.value,
      applicationIcon: Image.asset('assets/images/icon-192.png',width: 100, height: 100),//const FlutterLogo(size: 100),
      applicationLegalese: 'Copyright © ParTCP Working Group, 2025',
      applicationDescription: Text("ParTCP Vote ist ein Programm, mit dem Du an vertrauenswürdigen Online-Abstimmungen teilnehmen kannst. "
          "Es ist freie Software, die von einer Gemeinschaft ehrenamtlich tätiger Menschen entwickelt und betreut wird. \n"
          "Auf der Webseite partcp.org kannst Du mehr über das Projekt erfahren."),
      values: {
        'version': "Pubspec.version",
        'buildNumber': "Pubspec.versionBuild.toString()",
        'year': DateTime.now().year.toString(),
        'author': "Pubspec.authorsName.join(', ')",
      },
      children: const <Widget>[
        MarkdownPageListTile(
          filename: 'assets/README.md',
          title: Text('View Readme'),
          icon: Icon(Icons.all_inclusive),
        ),
        /* MarkdownPageListTile(
          filename: 'CHANGELOG.md',
          title: Text('View Changelog'),
          icon: Icon(Icons.view_list),
        ), */
        MarkdownPageListTile(
          filename: 'assets/LICENSE.md',
          title: Text('Lizenzhinweise'),
          icon: Icon(Icons.description),
        ),
        /* MarkdownPageListTile(
          filename: 'CONTRIBUTING.md',
          title: Text('Hinweise zum Mitmachen'),
          icon: Icon(Icons.share),
        ),
        MarkdownPageListTile(
          filename: 'CODE_OF_CONDUCT.md',
          title: Text('Verhaltensregeln'),
          icon: Icon(Icons.sentiment_satisfied),
        ), */
        LicensesPageListTile(
          title: Text('Verwendete Bibliotheken'),
          icon: Icon(Icons.favorite),
        ),
      ],
    ),
  );

}