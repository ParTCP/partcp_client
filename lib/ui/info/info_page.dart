// // Author: Aleksander Lorenz
// // partcp-client@ogx.de
// // Copyright 2022. All rights reserved
//
// import 'package:flutter/material.dart';
// import 'package:get/get.dart';
// import 'package:package_info_plus/package_info_plus.dart';
// import '/widgets/app_bar.dart';
// import '/widgets/card_widget.dart';
// import '/widgets/scroll_body.dart';
//
// class InfoPageController extends GetxController {
//
//   PackageInfo packageInfo = PackageInfo(
//     appName: 'Unknown',
//     packageName: 'Unknown',
//     version: 'Unknown',
//     buildNumber: 'Unknown',
//     buildSignature: 'Unknown',
//   );
//
//   Future<void> onPostFrameCallback() async {
//     packageInfo = await PackageInfo.fromPlatform();
//     updateMe();
//   }
//
//   void updateMe() {
//     update();
//   }
// }
//
// class InfoPage extends StatelessWidget {
//
//   final InfoPageController controller = Get.put(InfoPageController());
//
//   @override
//   Widget build(BuildContext context) {
//
//     WidgetsBinding.instance.addPostFrameCallback((_) {
//       controller.onPostFrameCallback();
//     });
//
//     return OrientationBuilder(
//       builder: (_, __) => GetBuilder<InfoPageController>(
//         // initState: (_) => controller.initController(),
//         builder: (_) => Scaffold(
//           appBar: MyAppBar(
//             title: Text("Device Info"),
//           ),
//           body: ScrollBody(children: [_body]),
//         ),
//       ),
//     );
//   }
//
//   Widget get _body {
//     List<Widget> _items = [];
//
//     _items.add(_infoTile('App name', controller.packageInfo.appName));
//     _items.add(_infoTile('Package name', controller.packageInfo.packageName));
//     _items.add(_infoTile('App version', controller.packageInfo.version));
//     _items.add(_infoTile('Build number', controller.packageInfo.buildNumber));
//     _items.add(_infoTile('Build signature', controller.packageInfo.buildSignature));
//
//     return CardWidget(
//       titleListTile: CardWidget.listTileTitle(
//         leading: Icon(Icons.info_outline_rounded),
//         title: "Client Info"
//       ),
//       child: Column(
//         children: _items,
//       ),
//     );
//   }
//
//   Widget _isIcon (bool expression) {
//     return expression
//         ? Icon(Icons.check, color: Colors.green,)
//         : Icon(Icons.clear);
//   }
//
//   Widget _infoTileOs(String title, bool isTrue) {
//     return isTrue
//     ? ListTile(
//       leading: _isIcon(isTrue),
//       title: Text("$title"),
//       onTap: () => {},
//     )
//     : Container();
//   }
//
//   Widget _infoTile(String title, String subtitle) {
//     return ListTile(
//       title: Text(title),
//       subtitle: Text(subtitle.isEmpty ? 'Not set' : subtitle),
//       onTap: () => {}
//     );
//   }
//
// }