import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../objects/api_server.dart';
import '/controller/main_controller.dart';
import '/objects/account.dart';
import '/objects/server_response.dart';
import '/ui/home/index_controller.dart';
import '/ui/home/index_page.dart';
import '/ui/server_user_manager/key_manager_page.dart';
import '/utils/field_width.dart';
import '/utils/log.dart';
import '/widgets/app_bar.dart';
import '/widgets/card_widget.dart';
import '/widgets/scroll_body.dart';
import '/widgets/error_message_snackbar.dart';
import '/widgets/text_field_widget.dart';
import '/widgets/widgets.dart';

import 'widgets.dart';

/// Routing class to decide which

const _TAG = "ServerUserManagerController";

class ServerUserManagerController extends GetxController {
  MainController mainController = MainController.to;
  ServerResponse serverResponse = ServerResponse();
  late ApiServer apiServer;

  final TextEditingController namingTextController = TextEditingController();
  final TextEditingController credentialTextController = TextEditingController();

  bool get allowManualRegister => namingTextController.text.length > 3 && credentialTextController.text.length > 3;
  bool identityCreated = false;

  void initController(ApiServer apiServer) {
    Log.d(_TAG, "initController");
    this.apiServer = apiServer;
    namingTextController.text = "";
    credentialTextController.text = "";
  }

  ////////////////////////////////////////////////////////
  /// SEND KEY SECTION
  ///

  Future<void> onManualRegistration() async {
    Log.d(_TAG, "onManualSubmit()");
    serverResponse.receivingData = true;
    updateMe();

    MainController mainController = MainController.to;
    Account account = mainController.wallet!.walletAccount!;
    account
      ..userId = namingTextController.text
      ..apiServer =apiServer;

    serverResponse = await account.keySubmission(credential: credentialTextController.text);

    if (serverResponse.isOk("onManualRegistration")) {
      await mainController.onWalletChanged(updateNotification: true);
      mainController.wallet!.walletAccount!.pubKeysOnServerValid = true;
      identityCreated = true;
      IndexController.to.updateMe();
    }
    updateMe();
  }

  void onGoToServerUserManagerPage() {
    Get.to(() => KeyManagerPage(apiServer: apiServer));
  }

  void onInputChanged() {
    updateMe();
  }

  Future<bool> onWillPop() async {
    return true;
  }

  void goToIndexPage() {
    IndexController.to.updateMe();
    Get.offAll(() => IndexPage());
  }

  void updateMe() {
    update();
  }
}


class ServerUserManagerPage extends StatelessWidget {
  final ServerUserManagerController controller = Get.put(ServerUserManagerController());
  final ApiServer apiServer;

  ServerUserManagerPage(this.apiServer, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      controller.initController(apiServer);
    });

    return WillPopScope(
      onWillPop: () => controller.onWillPop(),
      child: OrientationBuilder(
        builder: (_, __) => GetBuilder<ServerUserManagerController>(
            initState: (_) => controller.initController(apiServer),
            builder: (_) => Scaffold(
                  appBar: MyAppBar(
                    title: Text(controller.apiServer.name),
                  ),
                  body: Widgets.loaderBox(loading: controller.serverResponse.receivingData, child: ScrollBody(children: _items)),
                )),
      ),
    );
  }

  List<Widget> get _items {
    List<Widget> items = [];
    if (controller.identityCreated) {
      /// identity  created Widget
      items.add(identityCreatedWidget(server: controller.apiServer, onFinishGoto: controller.goToIndexPage));
    } else {
      items.add(ErrorMessageSnackBar(serverResponse: controller.serverResponse));
      if (controller.apiServer.keyManagerUrl.isNotEmpty) {
        items.add(_keyManager);
      }
      items.add(_manualRegister);
    }
    items.add(_skipRegistration);
    return items;
  }

  Widget get _keyManager {
    return CardWidget(
      // title: "Verifizierung Deiner Zugangsdaten",
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          /// server name
          Text(controller.apiServer.name, style: const TextStyle(fontSize: 20)),

          /// server url
          Text(controller.apiServer.url,
              style: const TextStyle(
                  // fontSize: 14
                  )),
          const Padding(padding: EdgeInsets.all(10.0)),
          Text(
              'In Deinem aktuellen Benutzerprofil ist keine Teilnehmerkennung für den Server ${controller.apiServer.name} vorhanden. '
              'Um eine solche zu erstellen, mußt Du zunächst die Identitätsprüfung durchlaufen, die der Server bereitstellt. Solltest Du bereits auf einem anderen Gerät eine Teilnehmerkennung für '
              'diesen Server erstellt haben, empfiehlt es sich, diesen Vorgang abzubrechen und das Benutzerprofil von dem anderen Gerät auf dieses zu übertragen.'
              '\n\nDer Server "${controller.apiServer.name}" stellt einen automatisierten Verifizierungsprozess bereit.'),
          Center(
              child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: TextButton(
              onPressed: () => controller.onGoToServerUserManagerPage(),
              child: const Text("Identitätsprüfung starten"),
            ),
          ))
        ],
      ),
    );
  }

  Widget get _skipRegistration => CardWidget(
      child: Center(
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: TextButton(
              onPressed: () => Get.back(),
              child: const Text("Ohne Identitätsprüfung fortfahren"),
            ),
          ))
  );


  Widget get _manualRegister {
    return CardWidget(
      // title: "Verifizierung Deiner Zugangsdaten",
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Text("Wenn Du bereits verifiziert wurdest, kannst Du hier die Daten eintragen:"),
          const Padding(padding: EdgeInsets.all(20.0)),

          /// Teilnehmerkennung
          Center(
            child: TextFieldWidget(
              textController: controller.namingTextController,
              labelText: "Teilnehmerkennung",
              hint: "",
              onTap: () => ({}),
              onSubmitted: (_) => ({}),
              onChanged: (_) => controller.onInputChanged(),
              autoCorrect: false,
              isPasswordField: false,
              keyboardType: TextInputType.text,
              textAlign: TextAlign.start,
              maxBoxLength: fieldBoxMaxWidth,
              // obscuringCharacter: "",
              enabled: true,
            ),
          ),

          const Padding(padding: EdgeInsets.all(20.0)),

          /// Verifizierungscode
          Center(
            child: TextFieldWidget(
              textController: controller.credentialTextController,
              labelText: "Verifizierungscode",
              hint: "",
              onTap: () => ({}),
              onSubmitted: (_) => ({}),
              onChanged: (_) => controller.onInputChanged(),
              autoCorrect: false,
              isPasswordField: false,
              keyboardType: TextInputType.text,
              textAlign: TextAlign.start,
              maxBoxLength: fieldBoxMaxWidth,
              // obscuringCharacter: "",
              enabled: true,
            ),
          ),

          const Padding(padding: EdgeInsets.all(20.0)),

          Center(
              child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: TextButton(
              onPressed: controller.allowManualRegister ? () => controller.onManualRegistration() : null,
              child: const Text("weiter"),
            ),
          ))
        ],
      ),
    );
  }
}
