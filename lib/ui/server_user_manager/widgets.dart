

import 'package:flutter/material.dart';
import '/objects/api_server.dart';
import '/widgets/card_widget.dart';

Widget identityCreatedWidget({required ApiServer server, required dynamic onFinishGoto}) {
  return CardWidget(
    title: "Identität erstellt",
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text("Deine Identität wurde erfolgreich überprüft, und es wurden neue Schlüssel für den Zugriff auf ${server.name} erstellt. "
            "Du solltest Dein Benutzerprofil jetzt exportieren, um die erzeugten Schlüssel zu sichern. Falls Du weitere Geräte im Einsatz hast, "
            "empfiehlt es sich, das exportierte Benutzerprofil auf diese zu übertragen"
        ),
        Center(
            child: Padding(
              padding: const EdgeInsets.all(20.0),
              child: TextButton(
                onPressed: () => onFinishGoto(),
                child: const Text("OK, verstanden"),
              ),
            ))
      ],
    ),
  );

}