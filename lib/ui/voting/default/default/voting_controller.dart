// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import '../../../../objects/error_message.dart';
import '/controller/http_controller.dart';
import '/controller/main_controller.dart';
import '/message_type/message_type.dart';
import '/objects/account.dart';
import '/objects/server_response.dart';
import '/objects/voting.dart';
import '/objects/voting_event.dart';
import '/objects/voting_option.dart';
import '/objects/api_server.dart';
import '/objects/voting_type.dart';
import '/utils/log.dart';

import 'vote_popup_page.dart';

const _TAG = "VotingController";
class VotingController extends GetxController {

  static VotingController to = Get.find<VotingController>();

  MainController mainController = MainController.to;

  ServerResponse serverResponse = ServerResponse();
  ErrorMessage? errorMessage;

  late ApiServer server;
  Account? account;
  late Voting voting;
  late VotingEvent votingEvent;
  late bool previewOnly;
  bool dataChanged = false;

  /// for AppBar, as Preview info
  String previewString = "";

  /// For RadioButton on SINGLE-CHOICE
  String _radioGroupValue = "";

  // List<Vote> get votes => voting.votes;

  /// Todo
  bool get allowSubmit => true;

  int get requestedVotesCount {
    int count = 0;

    /// CONSENSUS
    if (voting.votingType.isConsensus) {
      count = voting.votingOptions.length;
    }
    /// YES-NO
    if (voting.votingType.type == VotingTypeEnum.yes_no_rating) {
      count = voting.votingOptions.length;
    }
    /// SINGLE CHOICE
    if (voting.votingType.type == VotingTypeEnum.single_choice) {
      count = 1;
    }
    /// MULTIPLE CHOICE
    else if (voting.votingType.isMultipleChoice) {
      int _count = voting.votingType.multipleChoiceCount;
      count = _count > 900
          ? voting.votingOptions.length
          : _count;
    }
    return count;
  }

  VotingOption listItem(int index) => voting.votingOptions[index];

  bool get hasSpaceForLongButtonsList => Get.width > Get.height || MainController.to.isDesktopOrWeb;

  void initController(ApiServer server, Account? account,
      Voting voting, VotingEvent votingEvent, bool previewOnly) async {
    this.server = server;
    this.account = account;
    this.voting = voting;
    this.votingEvent = votingEvent;
    this.previewOnly = previewOnly;
    dataChanged = false;

    if (previewOnly) {
      previewString = "[Vorschau] ";
    }
    else {
      if (account != null && account.userId == null) {
        await this.server.decryptAccountsUserId(mainController.wallet!.pwdBytes!);
      }
    }
    _initDetailsRequest();
  }


  /// consensus, yes-no
  void setVotingValued(VotingOption option, int vote) {
    option.voteValue = vote;
    updateMe();
    _updateCommentBox(option);

    dataChanged = true;
    Log.d(_TAG, "setVotingValued; id:${option.id}; vote:$vote");
    updateMe();
  }

  /// multiple-choice
  bool setVotingMultiple(VotingOption option, bool vote) {
    Log.d(_TAG, "setVotingMultiple; id:${option.id}; vote:$vote");
    updateMe();
    dataChanged = true;
    return voting.setVoteMulti(option, vote);
  }

  /// single-choice
  void setVotingSingleChoice(VotingOption option) {
    voting.setVoteSingle(option);
    dataChanged = true;
    _radioGroupValue = option.id;
    updateMe();
  }
  String get votingSingleChoiceGroup => _radioGroupValue;


  int? getVote(VotingOption option) {
    return voting.vote(option);
  }


  bool isVoted(VotingOption option) {
    Log.d(_TAG, "isVoted; id:${option.id}; return -> ${option.voteValue != null}");
    return option.voteValue != null;
  }



  //////////////////////////////////////////////
  /// Comment Box

  void _updateCommentBox(VotingOption option) {
    if (option.votingComment != null && !option.votingComment!.commentRequired && option.votingComment!.textController!.text.isEmpty) {
      option.votingComment!.boxIsOpen = false;
    }
  }




  void goToVotePopupPage(VotingOption option) {
    Get.to(
        VotePopupPage(
          voting: voting,
          votingOption: option,
          name: option.name,
          callback: (String votingOptionId, int value) {
            setVotingValued(option, value);
          },
        ),
        duration: Duration(milliseconds: MainController.to.isSlowMachine ? 0 : 200),
        transition: MainController.to.isSlowMachine ? Transition.noTransition : Transition.fadeIn,

        opaque: GetPlatform.isWeb && GetPlatform.isMobile ? true : false)!;
  }

  //////////////////////////////////////////////
  /// SEND VOTES
  Future<void> previewOnlyMessage(BuildContext context) async {
    await showOkAlertDialog(
      context: context,
      message: 'In der Abstimmungs-Vorschau werden keine Daten an der Server gesendet',
      okLabel: 'ok',
      barrierDismissible: true,
    );
  }

  Future<bool> askSendVotes(BuildContext context) async {
    final result = await showOkCancelAlertDialog(
      context: context,
      message: 'Der Stimmzettel ist nicht vollständig ausgefüllt.\n\n'
          'Möchtest Du ihn dennoch absenden?',
      okLabel: 'JA',
      cancelLabel: "NEIN",
      barrierDismissible: false,
    );
    return result == OkCancelResult.ok;
  }


  Future<bool> onWillPop() async {
    // if (votes.length >= 1 && (account != null && ! account!.votedList.contains(voting.id))) {
    if (dataChanged && !previewOnly && account != null && account!.votingAllowed == 1) {

      final OkCancelResult result = await showOkCancelAlertDialog(
        context: Get.context!,
        title: "Stimmzettel verlassen, ohne ihn abzusenden?",

        //message: "Wenn Du Deinen Stimmzettel jetzt noch nicht absenden möchtest, kannst Du das später nachholen. "
        //    "Du kannst ihn aber auch jetzt absenden, später noch einmal überarbeiten und dann erneut absenden. "
        //    "Deine bisherigen Einträge bleiben erhalten, bis Du die Anwendung schließt oder über das Funktionsmenü sperrst.",

        message: "Dein Stimmzettel wurde noch nicht abgesendet. Bist Du sicher, dass Du Deine "
        "bisherige Auswahl verwerfen möchtest? Solange die Abstimmung läuft, kannst Du "
        "jederzeit zurückkommen, um den Stimmzettel erneut auszufüllen und abzusenden. ",

        okLabel: 'JETZT ABSENDEN',
        cancelLabel: "VERWERFEN",
        barrierDismissible: true,
      );

      Log.d(_TAG, "onWillPop; result:$result");
      if (result == OkCancelResult.ok) {
        await sendVotes();
      }
      else {
        return true;
      }
    }
    return true;
  }

  Future<void> sendVotes() async {
    bool doNext = true;
    errorMessage = null;

    /// ask for send votes
    // if (votes.length != requestedVotesCount && !VotingType.isMultipleChoice(voting.type!)) {
    if ((voting.votingOptions.where((element) => element.voteValue != null).length) != requestedVotesCount && !voting.votingType.isMultipleChoice) {
      doNext = await askSendVotes(Get.context!);
    }
    if (!doNext) return;

    /// if previewOnly
    if (previewOnly) {
      await previewOnlyMessage(Get.context!);
      return;
    }

    serverResponse.receivingData = true;
    updateMe();

    final Map<String, dynamic> messageMap = msgTypeBallot(
        event: votingEvent,
        voting: voting
    );

    serverResponse = await HttpController().serverRequestSigned(account!, messageMap, server, loc: "sendVotes");

    if (serverResponse.isOk("sendVotes")) {
      Log.d(_TAG, "------- sendVotes: YES -------");
      // account!.saveVotedByObj(voting);
      votingEvent.addCompletedVoting(votingId: voting.id!);

      /// save wallet in shared prefs
      await mainController.onWalletChanged(updateNotification: false);
      Get.back();
    }
    else {
      errorMessage = ErrorMessage(serverResponse);
      updateMe();
    }
  }

  void _initDetailsRequest() async {
    if (requestedVotesCount == 0) {
      await _votingDetailsRequest();
      updateMe();
    }
  }


  Future<void> _votingDetailsRequest() async {
    errorMessage = null;
    updateMe();

    final Map<String, dynamic> messageMap = msgTypeVotingDetailsRequest(event: votingEvent, voting: voting);
    serverResponse = await HttpController().serverRequestSigned(account!, messageMap, server, loc: "_votingDetailsRequest");

    if (serverResponse.isOk("_votingDetailsRequest")) {
      List<VotingOption> tempList = await _fillDetailsTempList(serverResponse.bodyMap!);
      voting.votingOptions.clear();
      voting.votingOptions.addAll(tempList);
    }

    if (!serverResponse.isOk("_votingDetailsRequest")) {
      errorMessage = ErrorMessage(serverResponse);
    }
    updateMe();
  }


  /// loop over votings
  Future<List<VotingOption>> _fillDetailsTempList(Map<String, dynamic> map) async {

    /// Voting options
    Map<String, dynamic> votingDataMap = map[Voting.VOTING_DATA];

    /// option list
    List<dynamic> optionsList = votingDataMap[VotingOption.OPTIONS];

    List<VotingOption> _tempList = [];

    Log.d(_TAG, "options: ${optionsList.length}");

    for (var i = 0; i< optionsList.length; i++) {
      VotingOption options = VotingOption.fromMap(optionsList[i], voting.votingType);
      _tempList.add(options);
    }

    return _tempList;
  }

  void updateMe() {
    Log.d(_TAG, "updateMe");
    update();
  }

  @override
  void onClose() {
    Log.d(_TAG, "onClose()");
  }
}

