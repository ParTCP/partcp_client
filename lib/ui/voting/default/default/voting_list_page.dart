// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../../objects/error_message.dart';
import '../../../../widgets/card_widget_simple.dart';
import '../../../../widgets/list_tile_simple.dart';
import '../../../home/index_controller.dart';
import '/controller/main_controller.dart';
import '/objects/account.dart';
import '/objects/server_response.dart';
import '/objects/voting.dart';
import '/objects/voting_event.dart';
import '/objects/api_server.dart';
import '/styles/styles.dart';
import 'voting_page.dart';
import 'voting_result_page.dart';
import '/utils/log.dart';
import '/widgets/app_bar.dart';
import '/widgets/card_widget.dart';
import '/widgets/scroll_body.dart';
import '/widgets/error_message_snackbar.dart';
import '/widgets/widgets.dart';

import 'widgets/info_widget.dart';

const _TAG = "VotingListController";

class VotingListController extends GetxController {
  ServerResponse serverResponse = ServerResponse();
  ErrorMessage? errorMessage;

  MainController mainController = MainController.to;

  late ApiServer server;
  Account? account;
  late VotingEvent event;
  late bool previewOnly;
  String previewString = "";

  Voting? lastSelectedVoting;

  late Key scrollBodyKey;

  /// list with status open, idle
  List<dynamic> openList = [];

  /// temp list with status closed
  List<dynamic> closedList = [];

  /// [gotData] is using to prevent to display "no items" on init page
  bool gotData = false;
  bool allowRefresh = true;

  VotingStatusEnum votingStatusEnum = VotingStatusEnum.all;

  void initController(ApiServer server, Account? account, VotingEvent votingEvent, bool previewOnly) async {
    this.server = server;
    this.account = account;
    event = votingEvent;
    this.previewOnly = previewOnly;

    if (previewOnly) {
      previewString = "[Vorschau] ";
    }

    gotData = false;
    scrollBodyKey = ValueKey("voting_list;${DateTime.now().millisecondsSinceEpoch}");
  }

  bool isVoted(Voting voting) {
    // return account != null && account!.votingAllowed == 1 ? account!.isVoted(voting) : false;
    return event.completedVotings.contains(voting.id!);
  }

  Future<void> onGoToVoting(Voting voting) async {
    lastSelectedVoting = voting;
    if (voting.status == Voting.STATUS_CLOSED || voting.status == Voting.STATUS_FINISHED) {
      Get.to(() => VotingResultPage(server: server, account: account, voting: voting, votingEvent: event))!.then((_) => updateMe());
    } else {
      Get.to(() => VotingPage(
                server: server,
                account: account,
                voting: voting,
                votingEvent: event,
                previewOnly: previewOnly,
              ))!
          .then((_) => updateMe());
    }
  }

  void onPostFrameCallback() async {
    Log.d(_TAG, "onPostFrameCallback");
    refreshList();
    _refreshTimer();
  }

  Future<void> _refreshTimer() async {
    if (event.poolingInformation != null && allowRefresh) {
      Future.delayed(Duration(seconds: event.poolingInformation!.pollingInterval)).then((value) async {
        if (allowRefresh) {
          await _requestNewPoolingInformation();
        }
      });
    }
  }

  Future<void> _requestNewPoolingInformation() async {
    bool doRefreshList = await event.poolingInformationRequest();
    if (doRefreshList) {
      await refreshList();
    }
    _refreshTimer();
  }

  Future<void> refreshList() async {
    serverResponse
      ..errorMessage = null
      ..receivingData = true;
    errorMessage = null;
    updateMe();
    serverResponse = await _requestVotings();

    if (!serverResponse.isOk("refreshList")) {
      errorMessage = ErrorMessage(serverResponse);
    }
    gotData = true;
    updateMe();
  }

  Future<ServerResponse> _requestVotings() async {
    serverResponse = await event.votingListRequest(server, account, VotingStatusEnum.all);

    openList.clear();
    closedList.clear();

    for (Voting voting in event.votings) {
      if (previewOnly) {
        voting.status == Voting.STATUS_CLOSED || voting.status == Voting.STATUS_FINISHED ? closedList.add(voting) : openList.add(voting);
      } else {
        if (voting.status == Voting.STATUS_OPEN) {
          openList.add(voting);
        } else if (voting.status == Voting.STATUS_CLOSED || voting.status == Voting.STATUS_FINISHED) {
          closedList.add(voting);
        }
      }
    }

    Log.d(_TAG, "openList: ${openList.length}");
    Log.d(_TAG, "closedList: ${closedList.length}");
    return serverResponse;
  }

  void updateMe() {
    update();
  }
}

class VotingListPage extends StatelessWidget {
  final ApiServer server;
  final Account? account;
  final VotingEvent votingEvent;
  final bool previewOnly;

  VotingListPage({
    Key? key,
    required this.server,
    required this.account,
    required this.votingEvent,
    this.previewOnly = false,
  }) : super(key: key);

  final VotingListController controller = Get.put(VotingListController());

  PreferredSizeWidget get _myAppBar => MyAppBar(
        title: Text("${controller.previewString}${controller.event.name}"),
        actions: [
          /// save wallet
          // saveWalletMenuIcon(onPressed: () {
          //   ExportWallet().download();
          //   controller.updateMe();
          // }),

          /// refresh
          IconButton(
            icon: const Icon(Icons.refresh),
            onPressed: () => controller.refreshList(),
          ),
        ],
      );

  Future<bool> onWillPop() async {
    controller.allowRefresh = false;
    IndexController.to.reRequestEvents();
    return true;
  }

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      controller.onPostFrameCallback();
    });

    return WillPopScope(
      onWillPop: onWillPop,
      child: OrientationBuilder(
        builder: (_, __) => GetBuilder<VotingListController>(
          initState: (_) => controller.initController(server, account, votingEvent, previewOnly),
          builder: (_) => Scaffold(
            appBar: _myAppBar,
            body: Widgets.loaderBox(
              loading: controller.serverResponse.receivingData,
              child: ScrollBody(
                key: controller.scrollBodyKey,
                children: _body,
              ),
            ),
          ),
        ),
      ),
    );
  }

  List<Widget> get _body {
    List<Widget> list = [];

    list.add(ErrorMessageSnackBar(
      errorMessage: controller.errorMessage,
    ));
    // list.add(SaveWalletSnackBarPage());


    list.add(infoWidget(
        title: controller.event.name,
        shortDescription: controller.event.description.shortDescription,
        description: controller.event.description.description));

    if (controller.openList.isEmpty && controller.closedList.isEmpty) {
      if (controller.gotData) {
        list.add(CardWidget(child: _noItems()));
      }
    } else {
      list.add(_votingListActive);
      list.add(_votingListPast);
    }
    return list;
  }

  Widget get _votingListActive {
    List<Widget> items = [];

    items.add(_headlineItem("Aktive Abstimmungen", true));
    if (controller.openList.isNotEmpty) {
      for (var voting in controller.openList) {
        items.add(_votingItem(voting, true));
      }
    } else {
      items.add(_noItems());
    }

    return CardWidget(
        child: Column(
      children: items,
    ));
  }

  Widget get _votingListPast {
    List<Voting> votingItems = [];
    for (var voting in controller.closedList) {
      votingItems.add(voting);
    }
    votingItems.sort((a, b) => b.periodEnd!.millisecondsSinceEpoch.compareTo(a.periodEnd!.millisecondsSinceEpoch));

    List<Widget> items = [];

    if (controller.closedList.isNotEmpty) {
      if (controller.openList.isNotEmpty) {
        items.add(const SizedBox(
          height: 30,
        ));
      }
      items.add(_headlineItem("Beendete Abstimmungen", false));
      for (var voting in votingItems) {
        items.add(_votingItem(voting, false));
      }
    }
    return items.isNotEmpty
        ? CardWidget(
            child: Column(
            children: items,
          ))
        : Container();
  }

  // Widget _noItemsInBoxWidget() {
  //   return CardWidget(
  //     child: _noItems(),
  //   );
  // }

  Widget _noItems() {
    return ListTile(
      leading: Icon(Icons.event_busy),
      title: Text(
        "Zur Zeit sind keine Abstimmungen vorhanden",
      ),
    );
  }

  Widget _votingItem_(Voting voting, bool isActive) {
    return Column(
      children: [
        ListTile(
            tileColor: controller.lastSelectedVoting != null && controller.lastSelectedVoting! == voting ? Styles.selectedTileColor : null,

            /// Colors.black.withOpacity(0.01),
            title: Text(
              voting.title,
              style: TextStyle(fontWeight: voting.status == Voting.STATUS_OPEN ? FontWeight.bold : FontWeight.normal),
            ),
            subtitle: Text(voting.description.shortDescription),
            contentPadding: const EdgeInsets.symmetric(vertical: 5.0, horizontal: 20.0),
            leading: isActive
                ? Icon(Icons.how_to_vote, color: controller.account != null && controller.account!.votingAllowed == 1 ? Colors.green : null)
                : null,
            trailing: _votingItemIcon(voting),
            onTap: () => controller.onGoToVoting(voting)),
        Styles.listDivider
      ],
    );
  }

  Widget _votingItem(Voting voting, bool isActive) {
    return Column(
      children: [
        Material(
          color: controller.lastSelectedVoting != null && controller.lastSelectedVoting! == voting ? Styles.selectedTileColor : null,

          /// Colors.black.withOpacity(0.01),
          child: InkWell(
            onTap: () => controller.onGoToVoting(voting),
            child: Container(
              color: controller.isVoted(voting) ? Color.fromARGB(10, 0, 255, 0) : null,
              child: Padding(
                padding: const EdgeInsets.all(12.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    /// Icon left on active voting
                    Align(
                      alignment: Alignment.center,
                      child: SizedBox(
                        width: isActive ? 60 : 10,
                        child: isActive
                            ? Icon(Icons.how_to_vote,
                                color: controller.account != null && controller.account!.votingAllowed == 1 ? Colors.green : null)
                            : Container(),
                      ),
                    ),

                    /// Text
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Hero(
                            tag: "votMain-${voting.id}",
                            child: Text(
                              voting.title,
                              style: isActive ? Styles.listTileTextMainBold : Styles.listTileTextMain,
                            ),
                          ),
                          voting.description.shortDescription.isNotEmpty
                              ? Hero(
                                  tag: "votSub-${voting.id}",
                                  child: Text(
                                    voting.description.shortDescription,
                                    style: Styles.listTileTextSub,
                                  ),
                                )
                              : Container(),
                        ],
                      ),
                    ),

                    SizedBox(width: 50, child: _votingItemIcon(voting)),
                  ],
                ),
              ),
            ),
          ),
        ),
        Styles.listDivider
      ],
    );
  }

  Widget? _votingItemIcon_(Voting voting) {
    if (voting.status == Voting.STATUS_FINISHED) {
      return voting.statusIcon;
    }

    if (controller.isVoted(voting)) {
      return const Icon(
        Icons.check_circle_outline_rounded,
        color: Colors.green,
      );
    }
    return null;
  }

  Widget _votingItemIcon(Voting voting) {
    if (voting.status == Voting.STATUS_FINISHED) {
      return voting.statusIcon;
    }

    if (controller.isVoted(voting)) {
      return const Icon(
        Icons.check_circle_outline_rounded,
        color: Colors.green,
      );
    }
    return Container();
  }

  Widget _headlineItem(String title, bool isActive) {
    return ListTile(
      title: Text(title,
          textScaleFactor: 1.2,
          style: isActive
              ? const TextStyle(fontWeight: FontWeight.bold, decoration: TextDecoration.underline)
              : const TextStyle(fontWeight: FontWeight.normal, decoration: TextDecoration.underline)),
      // tileColor: Colors.black12,
    );
  }
}
