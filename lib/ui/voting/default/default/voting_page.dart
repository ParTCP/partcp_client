// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../../widgets/card_widget.dart';
import '/objects/account.dart';
import '/objects/voting.dart';
import '/objects/voting_event.dart';
import '/objects/voting_option.dart';
import '/objects/api_server.dart';
import '/widgets/app_bar.dart';
import '/widgets/button_main.dart';
import '/widgets/scroll_body.dart';
import '/widgets/error_message_snackbar.dart';
import '/widgets/button_submit.dart';

import '../../menu_items.dart';
import 'voting_controller.dart';
import 'widgets/comment_box.dart';
import 'widgets/info_widget.dart';
import 'widgets/option_item.dart';

class VotingPage extends StatelessWidget {
  final ApiServer server;
  final Account? account;
  final Voting voting;
  final VotingEvent votingEvent;
  final bool previewOnly;

  VotingPage({
    Key? key,
    required this.server,
    required this.account,
    required this.voting,
    required this.votingEvent,
    this.previewOnly = false,
  }) : super(key: key);

  final VotingController controller = Get.put(VotingController());
  final ScrollController _scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => controller.onWillPop(),
      child: OrientationBuilder(
          builder: (_, __) => GetBuilder<VotingController>(
                initState: (_) => controller.initController(server, account, voting, votingEvent, previewOnly),
                builder: (_) => Scaffold(
                  appBar: MyAppBar(
                    title: Text("${controller.previewString}${voting.title}"),
                    actions: [
                      PopupMenuButton<MenuItems>(
                          icon: const Icon(Icons.menu),
                          onSelected: (MenuItems value) {
                            handleMenuClick(value, controller);
                          },
                          itemBuilder: (BuildContext context) => popupMenuItems(controller)),
                    ],
                  ),
                  body: Padding(
                    padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                    child: Column(
                      mainAxisSize: MainAxisSize.max,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        /// loadingItems / Voting List
                        Expanded(child: ScrollBody(children: [_optionsList])),

                        /// SERVER ERROR && VOTE BUTTON
                        Card(
                          // elevation: MainController.to.isSlowMachine ? 0 : null,
                          child: Center(
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Column(crossAxisAlignment: CrossAxisAlignment.center, children: [
                                /// ERROR MESSAGE
                                ErrorMessageSnackBar(errorMessage: controller.errorMessage),

                                /// Comment Box
                                controller.voting.commentRules != null &&
                                        (controller.voting.commentRules!.acceptance == 1 || controller.voting.commentRules!.acceptance == 3)
                                    ? Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: CommentBoxForVoting(rules: controller.voting.commentRules!, voting: voting)
                                )

                                    : Container(),

                                /// VOTE BUTTON
                                controller.account != null && controller.account!.votingAllowed == 1
                                    ? ButtonSubmit(
                                        submitting: controller.serverResponse.receivingData,
                                        //text:
                                        //    "Stimmzettel absenden (${(controller.voting.votingOptions.where((element) => element.voteValue != null).length)} von ${controller.requestedVotesCount})",

                                        text:
                                          "Stimmzettel absenden",
                                        enabled: true,
                                        onPressed: controller.allowSubmit ? () => controller.sendVotes() : null,
                                        buttonStyle: ButtonMain.styleVoting,
                                      )
                                    : Container(),
                              ]),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              )
          // ),
          ),
    );
  }



  /// OPTIONS LIST
  Widget get _optionsList {
    List<Widget> items = [
      infoWidget(
          title: controller.voting.title,
          shortDescription: controller.voting.description.shortDescription,
          description: controller.voting.description.description)
    ];
    for (VotingOption option in controller.voting.votingOptions) {
      items.add(OptionItem(controller: controller, option: option, votingType: voting.votingType).optionItem());
    }
    return CardWidget(child: Column(children: items));
  }
}
