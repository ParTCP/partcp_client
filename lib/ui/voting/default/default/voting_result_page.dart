// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../../controller/main_controller.dart';
import '../../../../widgets/card_widget.dart';
import '/objects/account.dart';
import '/objects/voting.dart';
import '/objects/voting_event.dart';
import '/objects/voting_option.dart';
import '/objects/api_server.dart';
import '/widgets/app_bar.dart';
import '/widgets/scroll_body.dart';

import '../../menu_items.dart';
import 'widgets/info_widget.dart';
import 'widgets/option_result_item.dart';

class VotingResultController extends GetxController {

  late ApiServer server;
  Account? account;
  late Voting voting;
  late VotingEvent event;

  void initController(ApiServer server, Account? account, VotingEvent event, Voting voting, ) {
    this.server = server;
    this.account = account;
    this.voting = voting;
    this.event = event;
  }

  void onPostFrameCallback() {
    // getVotingResults();
  }

  void updateMe() {
    update();
  }
}

class VotingResultPage extends StatelessWidget {
  final ApiServer server;
  final Account? account;
  final Voting voting;
  final VotingEvent votingEvent;

  VotingResultPage({Key? key,
    required this.server,
    this.account,
    required this.voting,
    required this.votingEvent,
  }) : super(key: key);

  final VotingResultController controller = Get.put(VotingResultController());

  @override
  Widget build(BuildContext context) {

    WidgetsBinding.instance.addPostFrameCallback((_) {
      controller.onPostFrameCallback();
    });

    return OrientationBuilder(
      builder: (_, __) => GetBuilder<VotingResultController>(
          initState: (_) => controller.initController(server, account, votingEvent, voting),
          builder: (_) => Scaffold(
            appBar: MyAppBar(
              title: Text(voting.title),

              actions: [
                PopupMenuButton<MenuItems>(
                    icon: const Icon(Icons.menu),
                    onSelected: (MenuItems value) {
                      handleMenuClick(value, controller);
                    } ,
                    itemBuilder: (BuildContext context) => popupMenuItems(controller)),
              ],

            ),
            body: ScrollBody(children: [_body])),
          ),
    );
  }


  Widget get _body {

    List<Widget> items = [
      infoWidget(
          title: controller.voting.title,
          shortDescription: controller.voting.description.shortDescription,
          description: controller.voting.description.description
      ),
      _resultSummary,
    ];
    for (VotingOption option in controller.voting.votingOptions) {
      items.add(OptionResultItem(controller: controller, option: option, votingType: voting.votingType).optionItem());
    }
    // return _items;
    return CardWidget(
      child: Column(
        children:
          items
      ),
    );
  }

  Widget get _resultSummary => Card(
    // elevation: MainController.to.isSlowMachine ? 0 : 1,
    child: Padding(
      padding: const EdgeInsets.only(left:10, top: 8.0, bottom: 8.0),
      child: ListTile(
        title: const Text("Zusammenfassung der Auswertung"),
        subtitle: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text("Abgegebene Stimmzettel: ${controller.voting.votingResult!.participants}"),
              Text("Ungültige Stimmzettel: ${controller.voting.votingResult!.invalid}")
            ],
          ),
        ),
      ),
    ),
  );

}
