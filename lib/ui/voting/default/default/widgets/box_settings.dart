
import 'package:flutter/material.dart';
import 'package:get/get_utils/get_utils.dart';

//Color tileColor = const Color.fromARGB(255, 248, 248, 248);
Color tileColor = const Color.fromARGB(255, 255, 255, 255);
Color commentBoxColor = const Color.fromARGB(255, 255, 255, 255);

EdgeInsets tilePadding = GetPlatform.isMobile
    ? const EdgeInsets.symmetric(vertical: 5.0, horizontal: 0) //was 10.0
    : const EdgeInsets.symmetric(vertical: 10.0, horizontal: 0); //was 20.0