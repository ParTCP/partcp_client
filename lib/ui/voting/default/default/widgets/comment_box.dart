import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../../objects/comment_rules.dart';
import '../../../../../objects/voting.dart';
import '../../../../../objects/voting_comment.dart';
import '../../../../../objects/voting_option.dart';
import '../../../../../utils/log.dart';
import '../../../../../widgets/card_widget.dart';
import '../../../../../widgets/text_field_widget.dart';
import 'box_settings.dart';

const _TAG = "commentBox";

class CommentBoxController extends GetxController {

  void updateId(dynamic id) {
    update([id]);
  }
}


///
/// The value of [VotingComment.boxIsOpen] will also be updated in [VotingController.setVotingValued]
///



class CommentBoxForOption extends StatelessWidget {
  final CommentRules rules;
  final VotingOption option;

  final CommentBoxController controller = Get.put(CommentBoxController());

  CommentBoxForOption({Key? key, required this.rules, required this.option}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<CommentBoxController>(
        id: option.id,
        builder: (_) {

          String commentText = "Kommentar";

          bool allowCloseBox = true;
          if (option.votingComment!.commentRequired || option.votingComment!.commentRequiredForOption(rules, option)) {
            option.votingComment!.boxIsOpen = true;
            allowCloseBox = false;
          }

          if (!allowCloseBox) {
            commentText += " erwünscht";
          }

          String? errorMessage;

          return _cardBox(controller, option.id, option.votingComment!, commentText, rules.maxLength, commentBoxColor, allowCloseBox, errorMessage);
        });
  }
}


class CommentBoxForVoting extends StatelessWidget {
  final CommentRules rules;
  final Voting voting;

  final CommentBoxController controller = Get.put(CommentBoxController());

  CommentBoxForVoting({Key? key, required this.rules, required this.voting}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<CommentBoxController>(
        id: voting.id,
        builder: (_) {

          String commentText = "Kommentar";

          bool allowCloseBox = true;
          if (voting.votingComment!.commentRequired) {
            voting.votingComment!.boxIsOpen = true;
            allowCloseBox = false;
          }

          if (!allowCloseBox) {
            commentText += " erwünscht";
          }

          String? errorMessage;

          return CardWidget(
              child: _cardBox(controller, voting.id!, voting.votingComment!, commentText, rules.maxLength, commentBoxColor, allowCloseBox, errorMessage)
          );
        });
  }
}


Widget _cardBox(CommentBoxController controller, String id, VotingComment votingComment, String commentText, int maxLength, Color color,
    bool allowCloseBox, String? errorMessage) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      const Padding(
        padding: EdgeInsets.all(8.0),
      ),
      TextButton(
        onPressed: allowCloseBox
            ? () {
                votingComment.boxIsOpen = !votingComment.boxIsOpen;
                controller.updateId(id);
              }
            : null,
        child: votingComment.boxIsOpen ? const Text("Kommentar verwerfen") : const Text("Kommentar verfassen"),
      ),
      votingComment.boxIsOpen
          ? Card(
              // color: commentBoxColor,
              // color: const Color.fromARGB(255, 255, 255, 255),
              color: color,
              /// we can't set the BoxBorder, because the TextField lost focus up update. Flutter bug?
              child: Container(
                // decoration: errorMessage != null ? BoxDecoration(border: Border.all(color: Colors.red, width: 1)) : null,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: ListTile(
                    title: Text(commentText),
                    subtitle: TextFieldWidget(
                      key: ValueKey(id),
                      textController: votingComment.textController!,
                      hint: "Kommentar",
                      showBorder: true,
                      bold: false,
                      autofocus: false,
                      minLines: 1,
                      maxLines: 5,
                      maxLength: maxLength,
                      keyboardType: TextInputType.multiline,
                      errorText: errorMessage,
                      onChanged: (_) => controller.updateId(id),
                    ),
                  ),
                ),
              ),
            )
          : Container(),
    ],
  );
}
