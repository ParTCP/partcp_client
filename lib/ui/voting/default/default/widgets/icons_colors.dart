// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:flutter/material.dart';
import '../../../../../objects/voting.dart';
import '/objects/voting_type.dart';

Color iconsColors(int? value) {
  if (value == null) {
    return Colors.white;
  }
  
  switch (value) {
    case 0: return const Color.fromRGBO(0, 255, 0, 1);
    case 1: return const Color.fromRGBO(51, 255, 0, 1);
    case 2: return const Color.fromRGBO(102, 255, 0, 1);
    case 3: return const Color.fromRGBO(153, 255, 0, 1);
    case 4: return const Color.fromRGBO(204, 255, 0, 1);
    case 5: return const Color.fromRGBO(255, 255, 0, 1);
    case 6: return const Color.fromRGBO(255, 204, 0, 1);
    case 7: return const Color.fromRGBO(255, 153, 0, 1);
    case 8: return const Color.fromRGBO(255, 102, 0, 1);
    case 9: return const Color.fromRGBO(255, 51, 0, 1);
    case 10: return const Color.fromRGBO(255, 0, 0, 1);
    default: return Colors.grey;
  }

}

Color consensusResultColor(double value) {
  return iconsColors(value.round());
}

int? consensuColor(VotingTypeEnum type, int? value) {
  if (type == VotingTypeEnum.consensus_3) {
    return consensu3Color(value);
  }
  else if (type == VotingTypeEnum.consensus_5) {
    return consensu5Color(value);
  }
  else {
    return value;
  }
}

Color yesNoColor(int? value) {
  switch (value) {
    case 1: return Colors.green;
    case 0: return Colors.red;
    default: return iconsColors(Voting.NO_VOTE_VALUE);
  }
}

int consensu3Color(int? value) {
  switch (value) {
    case 0: return 0;
    case 1: return 5;
    case 2: return 10;
    default: return Voting.NO_VOTE_VALUE;
  }
}

int consensu5Color(int? value) {
  switch (value) {
    case 0: return 0;
    case 1: return 2;
    case 2: return 4;
    case 3: return 7;
    case 4: return 10;
    default: return Voting.NO_VOTE_VALUE;
  }
}




