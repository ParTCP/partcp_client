// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../../../widgets/card_widget.dart';
import '/widgets/markdown_view.dart';

Widget infoWidget({required String title, required String shortDescription, required String description}) {
  List<Widget> subtitleList = [];

  if (shortDescription.trim().isNotEmpty) {
    subtitleList.add(Padding(
      padding: const EdgeInsets.only(top: 6),
      child: Text(shortDescription.trim()),
    ));
  }

  if (description.trim().isNotEmpty) {
    Widget w = Align(
      alignment: Alignment.centerRight,
      child: TextButton(
        child: const Text("Details"),
        onPressed: () => Get.to(() => MarkdownView(
              title: title,
              markdownText: description,
            )),
      ),
    );
    subtitleList.add(w);
  }

  Widget? subtitle = subtitleList.isNotEmpty
      ? Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: subtitleList,
        )
      : null;

  return CardWidget(
    child: Padding(
      padding: const EdgeInsets.only(top: 10, bottom: 10),
      child: ListTile(
        title: Text(title, textScaleFactor: 1.2,),
        subtitle: subtitle,
      ),
    ),
  );
}


