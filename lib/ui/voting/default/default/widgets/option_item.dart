// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_linkify/flutter_linkify.dart';
import 'package:get/get.dart';
import '/controller/main_controller.dart';
import '/objects/voting_option.dart';
import '/objects/voting_type.dart';
import 'embedded_voting.dart';
import '/utils/launch_url.dart';

import 'box_settings.dart';
import 'comment_box.dart';
import 'icons_colors.dart';
import 'voting_icon_widget.dart';
import '../voting_controller.dart';

class OptionItem {
  final VotingController controller;
  final VotingOption option;
  final VotingType votingType;

  const OptionItem({required this.controller, required this.option, required this.votingType});

  bool get _showInlineConsensus {
    if (MainController.to.isSlowMachine) return false;
    return Get.width > Get.height || MainController.to.isDesktopOrWeb;
  }

  Widget optionItem() {
    // Color tileColor = const Color.fromARGB(255, 242, 242, 242);
    return Card(
      // elevation: MainController.to.isSlowMachine ? 0 : null,
      color: tileColor,
      margin: const EdgeInsets.fromLTRB(0, 12, 0, 5),
      child: Padding(
        padding: const EdgeInsets.only(left: 30, right: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            /// Option Title + clientData
            ListTile(
              // tileColor: tileColor,

              title: Text(
                option.name,
                style: const TextStyle(fontWeight: FontWeight.bold),
              ),
              subtitle: _mainSubtitle,
              contentPadding: tilePadding,
              // onTap: () => controller.goToVotePopupPage(option),
            ),

            // Divider(),

            /// voting icons
            ListTile(
              tileColor: const Color.fromARGB(0, 255, 255, 255),
              title: Text(
                _votingTileTitle,
                style: const TextStyle(
                  fontStyle: FontStyle.italic,
                ),
              ),
              subtitle: _embeddedVoting,
              contentPadding: tilePadding,
              trailing: _votingResultWidget(controller.voting.votingType.type!, option.voteValue),
            ),

            /// Comment Box
            controller.voting.commentRules != null && (controller.voting.commentRules!.acceptance >= 2)
                ? Padding(padding: const EdgeInsets.only(bottom: 8), child: CommentBoxForOption(rules: controller.voting.commentRules!, option: option))
                : Container(),
          ],
        ),
      ),
    );
  }

  ///////////////////////////////////////////////////////////////////////////
  /// MAIN TILE
  ///
  Widget? get _mainSubtitle {
    List<Widget> _subtitleItems = [];
    Widget w;

    /// subtitle short description
    if (option.description.shortDescription.isNotEmpty) {
      w = Padding(
        padding: const EdgeInsets.only(top: 6),
        child: Text(option.description.shortDescription.trim()),
      );
      _subtitleItems.add(w);
    }

    /// subtitle link
    if (option.description.linkUrl.isNotEmpty) {
      w = Linkify(
        onOpen: (element) {
          launchURL(element.url);
        },
        text: "\nWeitere Informationen: ${option.description.linkUrl}",
      );
      _subtitleItems.add(w);
    }

    return _subtitleItems.isNotEmpty
        ? Padding(
            padding: const EdgeInsets.only(left: 6),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: _subtitleItems,
            ),
          )
        : null;
  }

  ///////////////////////////////////////////////////////////////////////////
  /// VOTING TILE
  ///

  String get _votingTileTitle {
    // if (VotingType.isConsensus(controller.voting.type!)) {
    if (votingType.isConsensus) {
      return "Wie groß ist Dein Widerstand?";
    }
    // if (controller.voting.type == VotingType.YES_NO_RATING) {
    if (votingType.isYesNo) {
      return "Deine Wahl";
    }

    String maxCount = controller.requestedVotesCount < controller.voting.votingOptions.length ? " (max: ${controller.requestedVotesCount})" : "";
    return "Deine Auswahl$maxCount";
  }

  Widget? get _embeddedVoting {
    /// EMBEDDED VOTING
    EmbeddedVoting embeddedVoting = EmbeddedVoting(
        controller: controller, voting: controller.voting, votingOption: option, callback: (id, value) => controller.setVotingValued(option, value));

    double? height = 60;
    if (votingType.type == VotingTypeEnum.yes_no_rating) {
      height = 50;
    }

    /// consensus-10 and mobile in portrait
    else if ((votingType.type == VotingTypeEnum.consensus_10 && !_showInlineConsensus) ||
        (votingType.type == VotingTypeEnum.consensus_5 && MainController.to.isSlowMachine) ||
        votingType.type == VotingTypeEnum.single_choice ||
        votingType.isMultipleChoice) {
      height = null;
    }

    return height != null ? SizedBox(height: height, child: embeddedVoting.createItems()) : null;
  }

  Widget? _votingResultWidget(VotingTypeEnum type, int? value) {
    VotingIconWidget iw = VotingIconWidget.middle(controller.voting.votingType.type);

    Widget w;

    /// CONSENSUS
    if (votingType.isConsensus) {
      w = iw.circleButtonWidget(
          value,
          consensuColor(votingType.type, value),
          votingType.type == VotingTypeEnum.consensus_10 || votingType.type == VotingTypeEnum.consensus_5
              ? () => controller.goToVotePopupPage(option)
              : () => {});
    }

    /// YES-NO
    else if (votingType.type == VotingTypeEnum.yes_no_rating) {
      w = iw.squareButtonWidget(value, null, //() => controller.goToVotePopupPage(option),
          imageOnly: true);
    }

    /// SINGLE CHOICE
    else if (votingType.type == VotingTypeEnum.single_choice) {
      w = Radio(
        value: option.id,
        groupValue: controller.votingSingleChoiceGroup,
        onChanged: (votingOption) => controller.setVotingSingleChoice(option),
      );
    }

    /// MULTIPLE CHOICE
    else if (votingType.isMultipleChoice) {
      w = Checkbox(
        value: controller.isVoted(option),
        onChanged: (value) {
          if (!controller.setVotingMultiple(option, value!)) {
            showOkAlertDialog(
              context: Get.context!,
              message: 'Anzahl der Optionen überschritten. \n\n'
                  'Maximale Anzahl: ${controller.requestedVotesCount}',
              okLabel: 'ok',
              barrierDismissible: true,
            );
          }
        },
      );
    } else {
      w = Container(
        width: 40,
        height: 40,
        color: Colors.cyanAccent,
      );
    }
    return w;
  }
}
