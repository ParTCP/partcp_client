// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:flutter/material.dart';
import 'package:flutter_linkify/flutter_linkify.dart';
import '../../../../../objects/voting.dart';
import '/objects/voting_option.dart';
import '/objects/voting_type.dart';
import '../voting_result_page.dart';
import '/utils/launch_url.dart';
import '/utils/s_utils.dart';

import 'box_settings.dart';
import 'embedded_result.dart';
import 'icons_colors.dart';
import 'voting_icon_widget.dart';


class OptionResultItem {
  final VotingResultController controller;
  final VotingOption option;
  final VotingType votingType;

  const OptionResultItem({required this.controller, required this.option, required this.votingType});

  Widget optionItem() {
    return Card(
      // elevation: MainController.to.isSlowMachine ? 0 : 1,
      color: tileColor,
      margin: const EdgeInsets.fromLTRB(0, 12, 0, 5),
      child: Padding(
        padding: const EdgeInsets.only(left: 30, right: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            /// Option Title + clientData
            ListTile(
              // tileColor: Colors.green,
              title: Text(
                option.name,
                style: const TextStyle(fontWeight: FontWeight.bold),
              ),
              subtitle: _mainSubtitle,
              contentPadding: tilePadding,
            ),

            // const Divider(),

            /// voting icons
            Padding(
              padding: tilePadding,
              child: Container(
                constraints: const BoxConstraints(minWidth: 100, maxWidth: double.infinity),
                // color: Colors.orange,
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(top: 15, bottom: 15, right: 20),
                        child: Text(_votingTileTitle,
                            style: const TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.black87
                            )),
                      ),
                    ),
                    // Container(width: 80, height: 80, child: _votingResultWidget),
                    Container(child: _votingResultWidget),
                  ],
                ),
              ),
            ),

            ListTile(
              tileColor: const Color.fromARGB(0, 255, 255, 255),
              // tileColor: Colors.blue,
              contentPadding: tilePadding,
              subtitle: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("Stimmen: ${controller.voting.votingResult!.votingsCount(option.id)}"),
                  Text("Enthaltungen: ${controller.voting.votingResult!.valueByVote(option.id, Voting.NO_VOTE_VALUE)}"),
                ],
              ),
            ),

            Padding(
              padding: tilePadding,
              child: Center(child: _embeddedResults),
            )
          ],
        ),
      ),
    );
  }

  ///////////////////////////////////////////////////////////////////////////
  /// MAIN TILE
  ///
  Widget? get _mainSubtitle {
    List<Widget> subtitleItems = [];
    Widget w;

    /// subtitle short description
    if (option.description.shortDescription.isNotEmpty) {
      w = Padding(
        padding: const EdgeInsets.only(top: 6),
        child: Text(option.description.shortDescription.trim()),
      );
      subtitleItems.add(w);
    }

    /// subtitle link
    if (option.description.linkUrl.isNotEmpty) {
      w = Linkify(
        onOpen: (element) {
          launchURL(element.url);
        },
        text: "Weitere Informationen: ${option.description.linkUrl}",
      );
      subtitleItems.add(w);
    }

    return subtitleItems.isNotEmpty
        ? Padding(
            padding: const EdgeInsets.only(left: 6),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: subtitleItems,
            ),
          )
        : null;
  }

  ///////////////////////////////////////////////////////////////////////////
  /// VOTING TILE
  ///
  String get _votingTileTitle {
    if (votingType.isConsensus) {
      return "Widerstand-Punkte";
    }
    if (votingType.type == VotingTypeEnum.yes_no_rating) {
      return "Zustimmung";
    }
    if (votingType.type == VotingTypeEnum.single_choice) {
      return "Stimmen";
    }
    return "Auswahl";
  }

  Widget? get _embeddedResults {
    return EmbeddedResult(
            voting: controller.voting,
            option: option,
            highestCount: controller.voting.votingResult!.highestConsensusCount(option.id))
        .createItems();
  }

  ///////////////////////////////////////////////////////////////////////////
  /// VOTING RESULT WIDGET
  ///
  Widget? get _votingResultWidget {
    VotingIconWidget iw = VotingIconWidget.middleResult(votingType.type!);

    /// CONSENSUS
    if (votingType.isConsensus) {
      double value = controller.voting.votingResult!.resistance(option.id);

      return iw.circleButtonWidget(value.round(), consensuColor(votingType.type, value.round()), null,
          namedValue: SUtils.formatDouble(value), selected: true);
    }

    /// YES-NO
    else if (votingType.type == VotingTypeEnum.yes_no_rating) {
      int yes = controller.voting.votingResult!.valueByVote(option.id, 1);
      int no = controller.voting.votingResult!.valueByVote(option.id, 0);
      if (yes == 0 && no == 0) {
        return Container();
      }
      return iw.squareButtonWidget(yes > no ? 1 : 0, null, imageOnly: true);
    }

    /// SINGLE CHOICE & MULTIPLE CHOICE
    else if (votingType.type == VotingTypeEnum.single_choice || votingType.isMultipleChoice) {
      int highestCount = controller.voting.votingResult!.highestOptionCount();
      int thisCount = controller.voting.votingResult!.votingsCount(option.id);
      return Text(
        "$thisCount",
        style: TextStyle(fontWeight: FontWeight.bold, fontSize: 26, color: thisCount == highestCount ? yesNoColor(1) : null),
      );
    } else {
      return null;
    }
  }
}

class LinearSales {
  final int year;
  final int sales;

  LinearSales(this.year, this.sales);
}
