// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:flutter/material.dart';
import '../../../../../controller/main_controller.dart';
import '../../../../../objects/voting.dart';
import '../../../../../objects/voting_type.dart';
import '/utils/log.dart';

import 'icons_colors.dart';

const ICON_COLOR = "ICON_COLOR";
const BOX_COLOR = "BOX_COLOR";
const BOX_ICON = "BOX_ICON";

class VotingIconWidget{
  final double iconSize;
  final double fontSize;
  final double? buttonElevation;
  final VotingTypeEnum votingType;

  static const _TAG = "VotingIconWidget";
  static const double increaseSizeFactor = 0.0; //0.4;

  static const double ICON_SIZE_SMALL = 30;
  static const double ICON_SIZE_MIDDLE = 50;
  static const double ICON_SIZE_LARGE = 60;

  static const double FONT_SIZE_SMALL = 16;
  static const double FONT_SIZE_MIDDLE = 18;
  static const double FONT_SIZE_LARGE = 24;

  int? _value;
  VoidCallback? _callback;

  int? _colorValue;
  late Widget _boxIcon;
  late Color _boxColor;
  late Color _iconTextColor;

  late String valueString;

  /// spacer
  // Widget get circleSpacer => _spacerCircleButton();

  /// CONSENSUS
  Widget circleButtonWidget(int? value, int? colorValue, VoidCallback? callback,
      {String? namedValue, bool? selected}) {
    _value = value;
    _colorValue = colorValue;
    _callback = callback;

    _boxColor = iconsColors(_colorValue);
    _iconTextColor = Colors.black;
    valueString = _valueString(namedValue: namedValue);

    bool decreaseTransparency = !MainController.to.isSlowMachine && selected != null && !selected;

    return selected != null
        ? _circleButton(increaseSize: selected, decreaseTransparency: decreaseTransparency)
        : _circleButton();
  }


  Widget squareButtonWidget(int? value, VoidCallback? callback,
      {String? namedValue, bool? selected, bool imageOnly = false}) {
    _value = value;
    _callback = callback;
    valueString = _valueString(namedValue: namedValue);
    _squareButtonValues(value);

    bool decreaseTransparency = !MainController.to.isSlowMachine && selected != null && !selected;

    return selected != null
        ? _squareButton(increaseSize: selected,
        decreaseTransparency: decreaseTransparency,
        imageOnly: imageOnly)
        : _squareButton(imageOnly: imageOnly);
  }

  String _valueString({String? namedValue}) {
    if (namedValue != null) return namedValue;

    if (_value == null) {
      return "?";
    } else if (_value == Voting.NO_VOTE_VALUE) {
      return "/";
    } else {
      return _value.toString();
    }
  }

  VotingIconWidget({
      required this.iconSize,
      required this.fontSize,
      required this.buttonElevation,
      required this.votingType
  });



  static VotingIconWidget small(VotingTypeEnum votingType) {
    double? elevation = MainController.to.isSlowMachine ? null :4;
    return VotingIconWidget(
        iconSize: ICON_SIZE_SMALL, fontSize: FONT_SIZE_SMALL, buttonElevation: elevation, votingType: votingType);
  }

  static VotingIconWidget middle(VotingTypeEnum votingType) {
    double? elevation = MainController.to.isSlowMachine ? null :6;
    return VotingIconWidget(
        iconSize: ICON_SIZE_MIDDLE, fontSize: FONT_SIZE_MIDDLE, buttonElevation: elevation, votingType: votingType);
  }

  static VotingIconWidget middleResult(VotingTypeEnum votingType) {
    double? elevation = MainController.to.isSlowMachine ? null :4;
    return VotingIconWidget(
        iconSize: ICON_SIZE_LARGE, fontSize: FONT_SIZE_MIDDLE*0.8, buttonElevation: elevation, votingType: votingType);
  }

  static VotingIconWidget large(VotingTypeEnum votingType) {
    double? elevation = MainController.to.isSlowMachine ? null :6;
    return VotingIconWidget(
        iconSize: ICON_SIZE_LARGE, fontSize: FONT_SIZE_LARGE, buttonElevation: elevation, votingType: votingType);
  }

  /// CONSENSUS BUTTON (0-10)
  Widget _circleButton({bool? increaseSize, bool? decreaseTransparency}) {

    double _iconSize = increaseSize != null && increaseSize
      ? iconSize + iconSize * increaseSizeFactor
      : iconSize;

    double _fontSize = increaseSize != null && increaseSize
        ? fontSize + fontSize * increaseSizeFactor
        : fontSize;

    _boxColor = decreaseTransparency != null && decreaseTransparency
      ? _boxColor.withOpacity(0.5)
      : _boxColor;

    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        constraints: BoxConstraints(
            minHeight: 30,
            maxHeight: _iconSize,
            minWidth: 30,
            maxWidth: _iconSize),
        child: ElevatedButton(
          onPressed: _callback != null
            ? _callback
            : () => {},
          style: ElevatedButton.styleFrom(
            shape: const CircleBorder(),
            primary: _boxColor,
            elevation: buttonElevation,
            minimumSize: Size(_iconSize, _iconSize),
            padding: const EdgeInsets.all(2.0),
          ),
          child: Text(
            valueString,
            style: TextStyle(
                color: _iconTextColor,
                fontSize: _fontSize,
                fontWeight: FontWeight.bold),
          ),
        ),
      ),
    );
  }

  /// YES-NO BUTTON
  Widget _squareButton ({bool? increaseSize, bool? decreaseTransparency, required bool imageOnly}) {

    double _fontSize = fontSize;

    _boxColor = decreaseTransparency != null && decreaseTransparency
        ? _boxColor.withOpacity(0.5)
        : _boxColor;

    Log.d(_TAG, "_squareButton; valueString: $valueString");

    Widget button;

    if (imageOnly) {
      button = ElevatedButton(
        onPressed: _callback != null
          ? _callback
          : () => {},
        style: ElevatedButton.styleFrom(
            minimumSize: const Size(50, 50),
            primary: _boxColor,
            elevation: buttonElevation),
        child: _boxIcon,
      );
    }
    else {
      button = ElevatedButton.icon(
        icon: _boxIcon,
        label: Text(
          valueString,
          style: TextStyle(
              color: _iconTextColor,
              fontSize: _fontSize,
              fontWeight: FontWeight.bold),
        ),
        onPressed: _callback != null
            ? _callback
            : () => {},
        style: ElevatedButton.styleFrom(
            minimumSize: const Size(50, 50),
            primary: _boxColor,
            elevation: buttonElevation),
      );
    }

    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        child: button,
      ),
    );
  }

  void _squareButtonValues(int? value) {
    switch (value) {

      /// yes
      case 1:
        _boxIcon = const Icon(Icons.thumb_up_alt_outlined, color: Colors.white);
        _boxColor = yesNoColor(1);
        _iconTextColor = Colors.white;
        break;

      /// no
      case 0:
        _boxIcon = const Icon(Icons.thumb_down_alt_outlined, color: Colors.white);
        _boxColor = yesNoColor(0);
        _iconTextColor = Colors.white;
        break;

      /// abstention
      case Voting.NO_VOTE_VALUE:
        _boxIcon = const Text(
          " / ",
          textScaleFactor: 2,
          style: TextStyle(color: Colors.white),
        );
        _boxColor = Colors.grey;
        _iconTextColor = Colors.white;
        break;

      /// null
      default:
        _boxIcon = const Text(
          " ? ",
          textScaleFactor: 1.7,
          style: TextStyle(color: Colors.black),
        );
        _boxColor = Colors.white;
        _iconTextColor = Colors.black;
        break;
    }
  }

  // Widget _spacerCircleButton () {
  //   double _iconSize = iconSize + iconSize * increaseSizeFactor;
  //
  //   return Padding(
  //     padding: const EdgeInsets.symmetric(vertical: 8),
  //     child: Container(
  //       constraints: BoxConstraints(
  //           minHeight: 30,
  //           maxHeight: _iconSize,
  //           minWidth: 1,
  //           maxWidth: 1),
  //     ),
  //   );
  //
  // }

}
