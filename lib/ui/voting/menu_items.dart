import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../controller/main_controller.dart';
import '../../utils/log.dart';

enum MenuItems {
  slowMachine
}

Future<void> handleMenuClick(MenuItems value, dynamic controller) async {
  switch (value) {
    case MenuItems.slowMachine:
      break;

    default:
      break;
  }
}

const _TAG = "menu_items";

List<PopupMenuItem<MenuItems>> popupMenuItems(dynamic controller) {
  List<PopupMenuItem<MenuItems>> list = [];

  list.add(PopupMenuItem<MenuItems>(
      value: MenuItems.slowMachine,
      child: CheckboxListTile(
        title: const Text("Vereinfachte Darstellung (schneller)"),
        value: MainController.to.isSlowMachine,
        contentPadding: const EdgeInsets.all(0),
        onChanged: (val) {
          Log.d(_TAG, "IndexMenu onClick slowMachine => new Val: $val");
          MainController.to.setSlowMachine(val ??= false);
          controller.updateMe();
          Get.back();
        },
      )));

  return list;
}