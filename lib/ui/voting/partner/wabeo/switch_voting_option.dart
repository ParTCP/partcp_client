import 'package:flutter/material.dart';
import 'package:partcp_client/ui/voting/partner/wabeo/widgets/confirmation_rejection.dart';
import 'package:partcp_client/ui/voting/partner/wabeo/widgets/option_confirmation.dart';
import 'package:partcp_client/ui/voting/partner/wabeo/widgets/option_info.dart';

import '../../../../objects/account.dart';
import '../../../../objects/api_server.dart';
import '../../../../objects/voting_event.dart';
import '../../../../objects/voting_option.dart';
import '../../../../objects/voting_type.dart';
import 'widgets/option_file.dart';
import 'widgets/option_photo.dart';
import 'widgets/option_count.dart';
import 'widgets/option_text.dart';

dynamic switchVotingOption ({
  required VotingEvent votingEvent,
  required VotingOption votingOption,
  required Function(VotingOption resultOption) onUpdate,
  // final bool readOnly = false,
  final dynamic readOnlyValue,
  final Key? key

}) {

  dynamic _wrongSelection = Padding(
    padding: const EdgeInsets.all(8.0),
    child: Container(
      color: Colors.red,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text("Fatal: VotingOption '${votingOption.votingType?.name}' is not implemented"),
      ),
    ),
  );

  switch (votingOption.votingType?.type) {

    case VotingTypeEnum.n_a:
      return _wrongSelection;

    case VotingTypeEnum.yes_no_rating:
      return _wrongSelection;

    case VotingTypeEnum.simple_10_0_10:
      return _wrongSelection;

    case VotingTypeEnum.consensus_3:
      return _wrongSelection;

    case VotingTypeEnum.consensus_5:
      return _wrongSelection;

    case VotingTypeEnum.consensus_10:
      return _wrongSelection;

    case VotingTypeEnum.single_choice:
      return _wrongSelection;

    case VotingTypeEnum.multiple_choice_2:
      return _wrongSelection;

    case VotingTypeEnum.multiple_choice_3:
      return _wrongSelection;

    case VotingTypeEnum.multiple_choice_4:
      return _wrongSelection;

    case VotingTypeEnum.multiple_choice_all:
      return _wrongSelection;

    case VotingTypeEnum.mixed:
      return _wrongSelection;

    case VotingTypeEnum.photo:
      return OptionPhoto(votingEvent: votingEvent, votingOption: votingOption, onUpdate: onUpdate);

    case VotingTypeEnum.count:
      return OptionCount(key: key, votingEvent: votingEvent, votingOption: votingOption, onUpdate: onUpdate, readOnlyValue: readOnlyValue);

    case VotingTypeEnum.text:
      return OptionText(votingEvent: votingEvent, votingOption: votingOption, onUpdate: onUpdate);

    case VotingTypeEnum.file:
      return OptionFile(votingEvent: votingEvent, votingOption: votingOption, onUpdate: onUpdate);

    case VotingTypeEnum.confirmation:
      return OptionConfirmation(votingEvent: votingEvent, votingOption: votingOption, onUpdate: onUpdate);

    case VotingTypeEnum.info:
      return OptionInfo(votingEvent: votingEvent, votingOption: votingOption);

    case VotingTypeEnum.confirmationRejection:
      return ConfirmationRejection(votingOption: votingOption, onUpdate: onUpdate, );

    case null:
      return _wrongSelection;
  }


}