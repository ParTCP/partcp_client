// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'dart:async';

import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:partcp_client/objects/description.dart';
import '../../../../objects/_client_data.dart';
import '../../../../objects/error_message.dart';
import '/controller/http_controller.dart';
import '/controller/main_controller.dart';
import '/message_type/message_type.dart';
import '/objects/account.dart';
import '/objects/server_response.dart';
import '/objects/voting.dart';
import '/objects/voting_event.dart';
import '/objects/voting_option.dart';
import '/objects/api_server.dart';
import '/objects/voting_type.dart';
import '/utils/log.dart';

import 'vote_popup_page.dart';

const _TAG = "VotingController";


enum GetUpdateObjects {
  mainWidget,
  sendProgressWidget
}


class VotingProgressController extends GetxController {
  static VotingProgressController to = Get.find<VotingProgressController>();

  int? uploadProgress;
  CancelToken? cancelToken;

  void updateMe() {
    Log.d("VotingProgressController", "updateMe()");
    update();
  }
}


class VotingController extends GetxController {

  /// war ein Test, um dem upload Progress über stream zu regeln, AL, 2025.01.28
  // StreamController<int> progressStreamController  = StreamController<int>();

  static VotingController to = Get.find<VotingController>();

  final MainController mainController = MainController.to;

  ServerResponse serverResponse = ServerResponse();
  ErrorMessage? errorMessage;

  var uploadProgressObs = 0.obs;
  int? uploadProgress;

  final VotingProgressController progressController = Get.put(VotingProgressController());
  final ScrollController optionsScrollController = ScrollController();

  late ApiServer server;
  Account? account;
  late Voting voting;
  late VotingEvent votingEvent;
  late bool previewOnly;
  bool dataChanged = false;

  bool dataRefreshRequired = false;
  Key? widgetsKey;

  /// //////////////////////////////////////////////////////////////////////////////////////////////////////
  ///  only for [Voting.noneAnonymous] and [Voting.confirmSegmentResults]!=null

  int resultSetCounter = 0;

  bool get votingIsResultConfirmation =>
      votingEvent.isNonAnonymous
          && voting.confirmSegmentResults != null
          && voting.votingResult != null
          && voting.votingResult!.resultSet.isNotEmpty
          && voting.votingResult!.resultSet.length >= (resultSetCounter + 1);

  bool resultsSelfSubmit = false;

  static const String votingOptionConfirmationRejectionId = "///rejection///";
  VotingOption confirmationRejectionOption = VotingOption(description: Description())
    ..id = votingOptionConfirmationRejectionId
    ..votingType = VotingType(VotingTypeEnum.confirmationRejection)
    ..confirmationText = "Die gemeldeten Ergebnisse stimmen NICHT mit meinen Aufzeichnungen überein."
    ..voteValue = 0
  ;
  VotingOption? confirmationRejectionReferenceOption;

  void resetConfirmationRejectionOption () {
    confirmationRejectionOption.voteValue = 0;
  }

  /// //////////////////////////////////////////////////////////////////////////////////////////////////////


  void _setNewWidgetKey () {
    widgetsKey = Key(DateTime.now().millisecondsSinceEpoch.toString());
  }


  // var submitAllowed = false.obs;

  /// for AppBar, as Preview info
  String previewString = "";

  /// For RadioButton on SINGLE-CHOICE
  String _radioGroupValue = "";

  /// confirmation_text
  bool? confirmationValue;

  String get votingSingleChoiceGroup => _radioGroupValue;

  VotingOption listItem(int index) => voting.votingOptions[index];

  bool get hasSpaceForLongButtonsList => Get.width > Get.height || MainController.to.isDesktopOrWeb;

  void initController(ApiServer server, Account? account,
      Voting voting, VotingEvent votingEvent, bool previewOnly) async {
    this.server = server;
    this.account = account;
    this.voting = voting;
    this.votingEvent = votingEvent;
    this.previewOnly = previewOnly;
    dataChanged = false;

    dataRefreshRequired = votingEvent.isNonAnonymous;
    resultSetCounter = 0;

    if (previewOnly) {
      previewString = "[Vorschau] ";
    }
    else {
      if (account != null && account.userId == null) {
        await this.server.decryptAccountsUserId(mainController.wallet!.pwdBytes!);
      }
    }

    _setNewWidgetKey();

  }

  Future<void> onPostFrameCallback() async {
    // serverResponse.receivingData = true;
    updateMe();
    await _initDetailsRequest();
    serverResponse.receivingData = false;
    updateMe();
  }



  /// Todo
  bool get submitAllowed {
    List<bool> failFound = [];
    Log.d(_TAG, "-------------------------------------\n submitAllowed:");

    if (votingIsResultConfirmation && confirmationRejectionOption.voteValue == 1) {
      return true;
    }

    if (votingEvent.votingIsBlocked(voting)) {
      Log.d(_TAG, "votingEvent.votingIsBlocked");
      failFound.add(true);
    }

    for (VotingOption option in voting.votingOptions) {
      switch(option.votingType!.type) {

        case VotingTypeEnum.n_a:
        /// Todo
          failFound.add(true);
          break;

        case VotingTypeEnum.yes_no_rating:
        /// Todo
          failFound.add(true);
          break;

        case VotingTypeEnum.simple_10_0_10:
        /// Todo
          failFound.add(true);
          break;

        case VotingTypeEnum.consensus_3:
        /// Todo
          failFound.add(true);
          break;

        case VotingTypeEnum.consensus_5:
        /// Todo
          failFound.add(true);
          break;

        case VotingTypeEnum.consensus_10:
        /// Todo
          failFound.add(true);
          break;

        case VotingTypeEnum.single_choice:
        /// Todo
          failFound.add(true);
          break;

        case VotingTypeEnum.multiple_choice_2:
        /// Todo
          failFound.add(true);
          break;

        case VotingTypeEnum.multiple_choice_3:
        /// Todo
          failFound.add(true);
          break;

        case VotingTypeEnum.multiple_choice_4:
        /// Todo
          failFound.add(true);
          break;

        case VotingTypeEnum.multiple_choice_all:
        /// Todo
          failFound.add(true);
          break;

        case VotingTypeEnum.mixed:
          failFound.add(true);
          break;

        case VotingTypeEnum.photo:
          bool result = option.mandatory == true && option.voteData == null;
          Log.d(_TAG, "VotingTypeEnum.photo not allowed: $result");
          failFound.add(result);
          break;

        case VotingTypeEnum.text:
          bool result = option.mandatory == true && option.voteValue == 0;
          Log.d(_TAG, "VotingTypeEnum.text not allowed: $result");
          failFound.add(result);
          break;

        case VotingTypeEnum.file:
          bool result = option.mandatory == true && option.voteData == null;
          Log.d(_TAG, "VotingTypeEnum.file not allowed: $result");
          failFound.add(result);
          break;

        case VotingTypeEnum.count:
          bool result = option.mandatory == true && option.voteData == null;
          Log.d(_TAG, "VotingTypeEnum.count not allowed: $result (id: ${option.id}, voteValue: ${option.voteValue}, voteData: ${option.voteData}");
          failFound.add(result);
          break;

        case VotingTypeEnum.confirmation:
          bool result = option.mandatory == true && option.voteValue == 0;
          Log.d(_TAG, "VotingTypeEnum.confirmation not allowed: $result (id: ${option.id}, voteValue: ${option.voteValue}, voteData: ${option.voteData}");
          failFound.add(result);
          break;

        case VotingTypeEnum.info:
          break;

        case VotingTypeEnum.confirmationRejection:
          Log.d(_TAG, "VotingTypeEnum.confirmationRejection not allowed: => false");
      }
    }

    final bool result = !failFound.contains(true);
    // submitAllowed.value = result;
    Log.d(_TAG, "submitAllowed => $result, $failFound\n-------------------------------------");

    return result;

  }


  /// Todo
  bool get _aksForSubmit {

    /// todo
    return false;

    List<bool> failFound = [];
    Log.d(_TAG, "-------------------------------------\n _aksForSubmit:");

    if (votingEvent.votingIsBlocked(voting)) {
      Log.d(_TAG, "votingEvent.votingIsBlocked");
      return false;
    }


    for (VotingOption option in voting.votingOptions) {
      switch(option.votingType!.type) {

        case VotingTypeEnum.n_a:
          return false;

        case VotingTypeEnum.yes_no_rating:
        /// Todo
          return false;

        case VotingTypeEnum.simple_10_0_10:
        /// Todo
          return false;

        case VotingTypeEnum.consensus_3:
        /// Todo
          return false;

        case VotingTypeEnum.consensus_5:
        /// Todo
          return false;

        case VotingTypeEnum.consensus_10:
        /// Todo
          return false;

        case VotingTypeEnum.single_choice:
        /// Todo
          return false;

        case VotingTypeEnum.multiple_choice_2:
        /// Todo
          return false;

        case VotingTypeEnum.multiple_choice_3:
        /// Todo
          return false;

        case VotingTypeEnum.multiple_choice_4:
        /// Todo
          return false;

        case VotingTypeEnum.multiple_choice_all:
        /// Todo
          return false;

        case VotingTypeEnum.mixed:
          return false;

        case VotingTypeEnum.photo:
          bool result = option.voteData != null;
          Log.d(_TAG, "VotingTypeEnum.photo has data: $result");
          failFound.add(result);
          break;

        case VotingTypeEnum.text:
          bool result = option.voteData != null;
          Log.d(_TAG, "VotingTypeEnum.text has data: $result");
          failFound.add(result);
          break;

        case VotingTypeEnum.file:
          bool result = option.voteData != null;
          Log.d(_TAG, "VotingTypeEnum.file has data: $result");
          failFound.add(result);
          break;

        case VotingTypeEnum.count:
          bool result = option.voteData != null;
          Log.d(_TAG, "VotingTypeEnum.count has data: $result");
          failFound.add(result);
          break;

        case VotingTypeEnum.confirmation:
          bool result = option.voteData != null;
          Log.d(_TAG, "VotingTypeEnum.confirmation has data: $result");
          failFound.add(result);
          break;

        case VotingTypeEnum.info:
          break;

        case VotingTypeEnum.confirmationRejection:
          Log.d(_TAG, "VotingTypeEnum.confirmationRejection => N/A");
      }
    }
    final bool result = failFound.contains(false);
    Log.d(_TAG, "_aksForSubmit => $result, $failFound\n-------------------------------------");
    return result;
  }



  bool setVoting(VotingOption option, dynamic vote) {

    bool result;
    switch(option.votingType!.type) {

      case VotingTypeEnum.n_a:
        result = false;
        break;

      case VotingTypeEnum.yes_no_rating:
        // result = _setVotingValued(option, vote);
        result = true;
        break;

      case VotingTypeEnum.simple_10_0_10:
        // result = _setVotingValued(option, vote);
        result = true;
        break;

      case VotingTypeEnum.consensus_3:
        // result = _setVotingValued(option, vote);
        result = true;
        break;

      case VotingTypeEnum.consensus_5:
        // result = _setVotingValued(option, vote);
        result = true;
        break;

      case VotingTypeEnum.consensus_10:
        // result = _setVotingValued(option, vote);
        result = true;
        break;

      case VotingTypeEnum.single_choice:
        result = _setVotingSingleChoice(option);
        break;

      case VotingTypeEnum.multiple_choice_2:
        result = _setVotingMultiple(option, vote);
        break;

      case VotingTypeEnum.multiple_choice_3:
        result = _setVotingMultiple(option, vote);
        break;

      case VotingTypeEnum.multiple_choice_4:
        result = _setVotingMultiple(option, vote);
        break;

      case VotingTypeEnum.multiple_choice_all:
        result = _setVotingMultiple(option, vote);
        break;

      case VotingTypeEnum.mixed:
        result = false;
        break;

      case VotingTypeEnum.photo:
        // result = _setVotingWithData(option, vote);
        result = true;
        break;

      case VotingTypeEnum.text:
        // result = _setVotingWithData(option, vote);
        result = true;
        break;

      case VotingTypeEnum.file:
        // result = _setVotingWithData(option, vote);
        result = true;
        break;

      case VotingTypeEnum.count:
        // result = _setVotingValued(option, vote);
        result = true;
        break;

      case VotingTypeEnum.confirmation:
        // result = _setVotingValued(option, vote);
        result = true;
        break;

      case VotingTypeEnum.info:
        result = true;
        break;

      case VotingTypeEnum.confirmationRejection:
        result = true;
        break;
    };

    dataChanged = true;
    return result;
  }


  /// todo: ist das so noch notwendig? AL, 2025.01.28
  int get requestedVotesCount {
    int count = 0;

    /// CONSENSUS
    if (voting.votingType.isConsensus) {
      count = voting.votingOptions.length;
    }

    /// YES-NO
    if (voting.votingType.type == VotingTypeEnum.yes_no_rating) {
      count = voting.votingOptions.length;
    }

    /// SINGLE CHOICE
    if (voting.votingType.type == VotingTypeEnum.single_choice) {
      count = 1;
    }

    /// MULTIPLE CHOICE
    else if (voting.votingType.isMultipleChoice) {
      int _count = voting.votingType.multipleChoiceCount;
      count = _count > 900
          ? voting.votingOptions.length
          : _count;
    }

    return count;
  }





  /// consensus, yes-no
  /// todo: kann das weg? AL 2025-02-01
  bool _setVotingValued(VotingOption option, dynamic vote) {
    // option.voteValue = vote;
    // updateMe();
    _updateCommentBox(option);
    return true;
  }

  /// multiple-choice
  bool _setVotingMultiple(VotingOption option, bool vote) {
    Log.d(_TAG, "setVotingMultiple; id:${option.id}; vote:$vote");
    return voting.setVoteMulti(option, vote);
  }

  /// single-choice
  bool _setVotingSingleChoice(VotingOption option) {
    voting.setVoteSingle(option);
    _radioGroupValue = option.id;
    return true;
  }

  /// with text or attachment
  /// todo: kann das weg? AL 2025-02-01
  bool _setVotingWithData(VotingOption option, dynamic vote) {
    // option.voteValue = vote != null ? 1 : 0;
    // option.voteData = vote;
    return true;
  }




  int? getVote(VotingOption option) {
    return voting.vote(option);
  }


  bool isVoted(VotingOption option) {
    Log.d(_TAG, "isVoted; id:${option.id}; return -> ${option.voteValue != null}");
    return option.voteValue != null;
  }



  //////////////////////////////////////////////
  /// Comment Box

  void _updateCommentBox(VotingOption option) {
    if (option.votingComment != null && !option.votingComment!.commentRequired && option.votingComment!.textController!.text.isEmpty) {
      option.votingComment!.boxIsOpen = false;
    }
  }


  void goToVotePopupPage(VotingOption option) {
    Get.to(
        VotePopupPage(
          voting: voting,
          votingOption: option,
          name: option.name,
          callback: (String votingOptionId, int value) {
            setVoting(option, value);
          },
        ),
        duration: Duration(milliseconds: MainController.to.isSlowMachine ? 0 : 200),
        transition: MainController.to.isSlowMachine ? Transition.noTransition : Transition.fadeIn,

        opaque: GetPlatform.isWeb && GetPlatform.isMobile ? true : false)!;
  }

  //////////////////////////////////////////////
  /// SEND VOTES
  Future<void> previewOnlyMessage(BuildContext context) async {
    await showOkAlertDialog(
      context: context,
      message: 'In der Abstimmungs-Vorschau werden keine Daten an der Server gesendet',
      okLabel: 'ok',
      barrierDismissible: true,
    );
  }

  Future<bool> askSendVotes(BuildContext context) async {
    final result = await showOkCancelAlertDialog(
      context: context,
      message: 'Der Stimmzettel ist nicht vollständig ausgefüllt.\n\n'
          'Möchtest Du ihn dennoch absenden?',
      okLabel: 'JA',
      cancelLabel: "NEIN",
      barrierDismissible: false,
    );
    return result == OkCancelResult.ok;
  }


  Future<bool> onWillPop() async {
    // if (dataChanged && !previewOnly && account != null && account!.votingAllowed == 1 && !votingEvent.votingIsBlocked(voting)) {
    //
    //   final OkCancelResult result = await showOkCancelAlertDialog(
    //     context: Get.context!,
    //     title: "Stimmzettel verlassen, ohne ihn abzusenden?",
    //
    //     message: "Dein Stimmzettel wurde noch nicht abgesendet. Bist Du sicher, dass Du Deine "
    //     "bisherige Auswahl verwerfen möchtest? Solange die Abstimmung läuft, kannst Du "
    //     "jederzeit zurückkommen, um den Stimmzettel erneut auszufüllen und abzusenden. ",
    //
    //     okLabel: 'JETZT ABSENDEN',
    //     cancelLabel: "VERWERFEN",
    //     barrierDismissible: true,
    //   );
    //
    //   Log.d(_TAG, "onWillPop; result:$result");
    //   if (result == OkCancelResult.ok) {
    //     await sendVotes();
    //   }
    //   else {
    //     return true;
    //   }
    // }
    return true;
  }


  Future<void> sendVotes() async {

    if (votingIsResultConfirmation && confirmationRejectionOption.voteValue == 1) {
      resultSetCounter ++;
      resetConfirmationRejectionOption();
      _setNewWidgetKey();
      await _votingDetailsRequest();
      updateMe();
      optionsScrollController.animateTo(0, duration: Duration(milliseconds: 500), curve: Curves.easeInOut);

    }
    else {
      return await _sendVotes();
    }
  }


  Future<void> _sendVotes() async {
    bool doNext = true;
    errorMessage = null;


    bool keyCheck = await _keyCheck();
    if (!keyCheck) {
      updateMe();
    }

    /// ask for send votes
    // if ((voting.votingOptions.where((element) => element.voteValue != null).length) != requestedVotesCount && !voting.votingType.isMultipleChoice) {
    if (_aksForSubmit) {
      doNext = await askSendVotes(Get.context!);
    }
    if (!doNext) return;

    /// if previewOnly
    if (previewOnly) {
      await previewOnlyMessage(Get.context!);
      return;
    }

    serverResponse.receivingData = true;
    updateMe();


    // /// if [VotingEvent.isNonAnonymous], check public keys on server
    // if (votingEvent.isNonAnonymous) {
    //   bool result = await account!.checkUsersPubKeyOnServer();
    //   /// if fails, submit key-submission again
    //   if (!result) {
    //     serverResponse = await account!.keySubmission();
    //     Log.d(_TAG, "account!.keySubmission: ${serverResponse.object}");
    //     if(!serverResponse.isOk("keySubmission")) {
    //       updateMe();
    //       return;
    //     }
    //   }
    //   // participantData.publicKey != null && (await publicKeysConcat) == participantData.publicKey;
    // }

    final Map<String, dynamic> messageMap = msgTypeBallot(
      event: votingEvent,
      voting: voting,
      dateTime: votingEvent.isNonAnonymous ? DateTime.now() : null
    );


    serverResponse = await HttpController().serverRequestSigned(account!, messageMap, server, loc: "sendVotes", useGetX: false,
      progressCallback: (int? progress, CancelToken? cancelToken) {
        progressController.uploadProgress = progress;
        progressController.cancelToken = cancelToken;
        progressController.updateMe();
      }
    );

    if (serverResponse.canceled) {
     updateMe();
    }
    else {
      if (serverResponse.isOk("sendVotes")) {
        Log.d(_TAG, "------- sendVotes: YES -------");
        // account!.saveVotedByObj(voting);
        votingEvent.addCompletedVoting(votingId: voting.id!);

        /// save wallet in shared prefs
        await mainController.onWalletChanged(updateNotification: false);
        Get.back(result: true);
      }
      else {
        errorMessage = ErrorMessage(serverResponse);
        updateMe();
      }
    }
  }


  Future<void> _initDetailsRequest() async {
      serverResponse.receivingData = true;
      updateMe();

      bool hasErrors = false;

      bool keyCheck = await _keyCheck();
      hasErrors = !keyCheck;

      if (!hasErrors) {
        await _votingDetailsRequest();
        dataRefreshRequired = false;
      }
      serverResponse.receivingData = false;;
      updateMe();
  }


  Future<bool> _keyCheck() async {
    if(account != null) {
      bool keysAreCorrect = await account!.checkUsersPubKeyOnServer();
      Log.d(_TAG, "_checkKeysOnServer => keysAreCorrect: $keysAreCorrect");
      if (!keysAreCorrect) {
        if (votingEvent.isNonAnonymous) {
          serverResponse = await account!.keySubmission();
          if (!serverResponse.isOk("$_TAG ->_initDetailsRequest -> keySubmission")) {
            return false;;
          }
        }
      }
      else {
        return true;
      }
    }
    return true;
  }


  Future<void> _votingDetailsRequest() async {
    serverResponse.receivingData = true;
    errorMessage = null;
    updateMe();

    final Map<String, dynamic> messageMap = msgTypeVotingDetailsRequest(event: votingEvent, voting: voting);
    serverResponse = await HttpController().serverRequestSigned(account!, messageMap, server, loc: "_votingDetailsRequest");

    if (serverResponse.isOk("_votingDetailsRequest")) {
      List<VotingOption> tempList = await _fillDetailsTempList(serverResponse.bodyMap!);
      voting.votingOptions.clear();
      voting.votingOptions.addAll(tempList);

      Voting testVoting = await Voting.fromMap(serverResponse.bodyMap![Voting.VOTING_DATA]);
      voting = testVoting;

    }
    else {
      serverResponse.receivingData = false;
      updateMe();
    }

    if (!serverResponse.isOk("_votingDetailsRequest")) {
      errorMessage = ErrorMessage(serverResponse);
    }
    updateMe();
  }


  /// kann das weg? AL 2025-02-04
  Future<List<VotingOption>> _fillDetailsTempList(Map<String, dynamic> map) async {


    /// Voting options
    Map<String, dynamic> votingDataMap = map[Voting.VOTING_DATA];

    /// option list
    List<dynamic> optionsList = votingDataMap[VotingOption.OPTIONS];

    List<VotingOption> _tempList = [];

    Log.d(_TAG, "options: ${optionsList.length}");

    for (var i = 0; i< optionsList.length; i++) {
      VotingOption options = VotingOption.fromMap(optionsList[i], voting.votingType);
      _tempList.add(options);
    }

    return _tempList;
  }

  void updateMe() {
    Log.d(_TAG, "updateMe");
    update();
  }

  @override
  void onClose() {
    Log.d(_TAG, "onClose()");
  }
}

