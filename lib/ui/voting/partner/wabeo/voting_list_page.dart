// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:partcp_client/objects/storage/app_box.dart';
import 'package:partcp_client/widgets/text_scale_widget.dart';
import '../../../../objects/error_message.dart';
import '../../../../widgets/card_widget_simple.dart';
import '../../../../widgets/list_tile_simple.dart';
import '../../../home/index_controller.dart';
import '/controller/main_controller.dart';
import '/objects/account.dart';
import '/objects/server_response.dart';
import '/objects/voting.dart';
import '/objects/voting_event.dart';
import '/objects/api_server.dart';
import '/styles/styles.dart';
import 'voting_page.dart';
import 'voting_result_page.dart';
import '/utils/log.dart';
import '/widgets/app_bar.dart';
import '/widgets/scroll_body.dart';
import '/widgets/error_message_snackbar.dart';
import '/widgets/widgets.dart';

import 'widgets/info_widget.dart';

const _TAG = "VotingListController";

class VotingListController extends GetxController {
  ServerResponse serverResponse = ServerResponse();
  ErrorMessage? errorMessage;

  MainController mainController = MainController.to;

  late ApiServer server;
  Account? account;
  late VotingEvent event;
  late bool previewOnly;
  String previewString = "";

  Voting? lastSelectedVoting;

  late Key scrollBodyKey;

  /// list with status open, idle
  List<dynamic> openList = [];

  /// temp list with status closed
  List<dynamic> closedList = [];

  /// [gotData] is using to prevent to display "no items" on init page
  bool gotData = false;
  bool allowRefresh = true;

  VotingStatusEnum votingStatusEnum = VotingStatusEnum.all;



  void initController(ApiServer server, Account? account, VotingEvent votingEvent, bool previewOnly) async {
    this.server = server;
    this.account = account;
    event = votingEvent;
    this.previewOnly = previewOnly;

    if (previewOnly) {
      previewString = "[Vorschau] ";
    }

    gotData = false;
    scrollBodyKey = ValueKey("voting_list;${DateTime.now().millisecondsSinceEpoch}");
  }


  void onPostFrameCallback() async {
    Log.d(_TAG, "onPostFrameCallback");
    if (event.isNonAnonymous && account != null) {
      serverResponse = await event.eventDetailsRequest(server, account!, includeCompletedVotings: true);

      updateMe();
      if (account!.participant == null || (account!.participant != null && (account!.participant!.attributes!.lastName!.isEmpty || account!.participant!.attributes!.lastName! == "na")) ) {
        ServerResponse? _serverResponse = await account!.participantDetailsRequest(forPublicKeys: false);
      }
    }


    await refreshList();
    await _refreshTimer();
  }


  bool isVoted(Voting voting) {
    // return account != null && account!.votingAllowed == 1 ? account!.isVoted(voting) : false;
    return event.completedVotings.contains(voting.id);
  }

  Future<void> onGoToVoting(Voting voting) async {
    lastSelectedVoting = voting;
    if (voting.status == Voting.STATUS_CLOSED || voting.status == Voting.STATUS_FINISHED) {
      Get.to(() => VotingResultPage(server: server, account: account, voting: voting, votingEvent: event))!.then((_) => updateMe());
    } else {

      voting.clearOptionsData();

      // Get.to(() => VotingPage(
      //           server: server,
      //           account: account,
      //           voting: voting,
      //           votingEvent: event,
      //           previewOnly: previewOnly,
      //         ))!
      //     .then((_) => updateMe());

      var result = await Get.to(() => VotingPage(
        server: server,
        account: account,
        voting: voting,
        votingEvent: event,
        previewOnly: previewOnly,
      ));

      if (result != null && result is bool && result) {
        await refreshList();
        /// voting-details-request
      }
      updateMe();

    }
  }


  Future<void> _refreshTimer() async {
    if (event.poolingInformation != null && allowRefresh) {
      Future.delayed(Duration(seconds: event.poolingInformation!.pollingInterval)).then((value) async {
        if (allowRefresh) {
          await _requestNewPoolingInformation();
        }
      });
    }
  }


  Future<void> _requestNewPoolingInformation() async {
    bool doRefreshList = await event.poolingInformationRequest();
    if (doRefreshList) {
      await refreshList();
    }
    _refreshTimer();
  }

  Future<void> refreshList() async {
    serverResponse
      ..errorMessage = null
      ..receivingData = true;
    errorMessage = null;
    updateMe();
    serverResponse = await _requestVotings();

    if (!serverResponse.isOk("refreshList")) {
      errorMessage = ErrorMessage(serverResponse);
    }
    gotData = true;

    updateMe();
  }


  Future<ServerResponse> _requestVotings() async {
    serverResponse = await event.votingListRequest(server, account, VotingStatusEnum.all);

    openList.clear();
    closedList.clear();

    for (Voting voting in event.votings) {
      if (previewOnly) {
        voting.status == Voting.STATUS_CLOSED || voting.status == Voting.STATUS_FINISHED ? closedList.add(voting) : openList.add(voting);
      } else {
        if (voting.status == Voting.STATUS_OPEN) {
          openList.add(voting);
        } else if (voting.status == Voting.STATUS_CLOSED || voting.status == Voting.STATUS_FINISHED) {
          closedList.add(voting);
        }
      }
    }

    Log.d(_TAG, "openList: ${openList.length}");
    Log.d(_TAG, "closedList: ${closedList.length}");
    return serverResponse;
  }

  void updateMe() {
    update();
  }
}

class VotingListPage extends StatelessWidget {
  final ApiServer server;
  final Account? account;
  final VotingEvent votingEvent;
  final bool previewOnly;

  VotingListPage({
    Key? key,
    required this.server,
    required this.account,
    required this.votingEvent,
    this.previewOnly = false,
  }) : super(key: key);

  final VotingListController controller = Get.put(VotingListController());

  PreferredSizeWidget get _myAppBar => MyAppBar(
        title: Text("${controller.previewString}${controller.event.name}"),
        actions: [

          if (MainController.to.appBox!.getValue(AppBoxEnum.showTextScaleWidget, defValue: false))
          textScaleWidget(onUpdate: () => controller.updateMe()),

          /// refresh
          IconButton(
            icon: const Icon(Icons.refresh),
            onPressed: () => controller.refreshList(),
          ),
        ],
      );

  Future<bool> onWillPop() async {
    controller.allowRefresh = false;
    IndexController.to.reRequestEvents();
    return true;
  }

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      controller.onPostFrameCallback();
    });

    return WillPopScope(
      onWillPop: onWillPop,
      child: OrientationBuilder(
        builder: (_, __) => GetBuilder<VotingListController>(
          initState: (_) => controller.initController(server, account, votingEvent, previewOnly),
          builder: (_) => Scaffold(
            appBar: _myAppBar,
            body: Widgets.loaderBox(
              loading: controller.serverResponse.receivingData,
              child: ScrollBody(
                key: controller.scrollBodyKey,
                children: _body,
              ),
            ),
          ),
        ),
      ),
    );
  }

  List<Widget> get _body {
    List<Widget> list = [];

    list.add(ErrorMessageSnackBar(
      errorMessage: controller.errorMessage,
    ));
    // list.add(SaveWalletSnackBarPage());


    /// event title + info
    list.add(CardWidgetSimple(
      child: infoWidget(
          title: controller.event.name,
          shortDescription: controller.event.description.shortDescription,
          description: controller.event.description.description),
    ));

    /// participant name
    if (account?.participant?.attributes != null) {
      List placeItems = [];

      if (account!.participant!.attributes!.domainCode.isNotEmpty) {
        placeItems.add("${account!.participant!.attributes!.domainCode},");
      }

      if (account!.participant!.attributes!.placeBuilding.isNotEmpty) {
        placeItems.add("${account!.participant!.attributes!.placeBuilding},");
      }

      if (account!.participant!.attributes!.placeStreet.isNotEmpty) {
        placeItems.add("${account!.participant!.attributes!.placeStreet},");
      }

      if (account!.participant!.attributes!.placePostalCode.isNotEmpty) {
        placeItems.add(account!.participant!.attributes!.placePostalCode);
      }

      if (account!.participant!.attributes!.placeCity.isNotEmpty) {
        placeItems.add(account!.participant!.attributes!.placeCity);
      }

      list.add(CardWidgetSimple(
        child: ListTileSimple(
          title: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text("${account!.participant!.attributes!.firstName} ${account!.participant!.attributes!.lastName}"),
              Text(placeItems.join(" ")),
            ],
          ),
          // subtitle: Text("Wo soll der Name hin?"),
        ),
      ));
    }


    if (controller.openList.isEmpty && controller.closedList.isEmpty) {
      if (controller.gotData) {
        list.add(CardWidgetSimple(child: _noItems()));
      }
    } else {
      list.add(_votingListActive);
    }
    return list;
  }


  Widget get _votingListActive {
    List<Widget> items = [];

    items.add(ListTileSimple(
      title: Text(votingEvent.string(VotingStringEnum.voting_list_title, ifEmpty: "")) ,
      titleStyle: Styles.H1,
    ));

    var list = Padding(
      padding: const EdgeInsets.only(bottom: 16),
      child: ListView.separated(
        itemBuilder: (_, int index) => _votingItem(controller.openList[index], true),
        separatorBuilder: (BuildContext context, int index) => Styles.divider,
        itemCount: controller.openList.length,
        shrinkWrap: true,
        addRepaintBoundaries: false,
        cacheExtent: 999,
      ),
    );
    items.add(list);

    // if (controller.openList.isNotEmpty) {
    //   for (var voting in controller.openList) {
    //     items.add(_votingItem(voting, true));
    //   }
    // } else {
    //   items.add(_noItems());
    // }

    return CardWidgetSimple(
        child: Column(
      children: items,
    ));
  }



  Widget _noItems() {
    return ListTileSimple(
      leading: Icon(Icons.event_busy),
      title: Text(
        "Zur Zeit sind keine Abstimmungen vorhanden",
      ),
    );
  }

  Widget _votingItem(Voting voting, bool isActive) {
    return ListTileSimple(
      isDense: true,
      selected: controller.lastSelectedVoting != null && controller.lastSelectedVoting! == voting,
      leading: _checkWidget(controller.isVoted(voting)),
      title: Text(voting.title),
      disabledStyle: votingEvent.votingIsBlocked(voting),

      subtitle: voting.description.shortDescription.trim().isNotEmpty
          ? Text(voting.description.shortDescription)
          : null,

      // subtitle: Text("required: ${voting.prerequisites}; unmet: ${controller.event.unmetPrerequisites(voting)}"),

      // onTap: controller.event.unmetPrerequisites(voting).isNotEmpty ? null : () => controller.onGoToVoting(voting),
      onTap: votingEvent.votingIsBlocked(voting) ? null : () => controller.onGoToVoting(voting),

    );
  }

  Widget _checkWidget (bool finished) {
    return SizedBox(
      width: 25, height: 25,
      child: Center(
        child: Stack(
          alignment: Alignment.center,
          children: [
            Container(
              decoration: BoxDecoration(
                border: Border.all(color: Colors.black87)
              ),
            ),

            finished
              ? Icon(Icons.check, color: Colors.green)
              : Container()

          ],
        ),
      ),
    );
  }


  /*

  Widget _votingItem_(Voting voting, bool isActive) {
    return Column(
      children: [
        Material(
          color: controller.lastSelectedVoting != null && controller.lastSelectedVoting! == voting ? Styles.selectedTileColor : null,

          /// Colors.black.withOpacity(0.01),
          child: InkWell(
            onTap: () => controller.onGoToVoting(voting),
            child: Container(
              color: controller.isVoted(voting) ? Color.fromARGB(10, 0, 255, 0) : null,
              child: Padding(
                padding: const EdgeInsets.all(12.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    /// Icon left on active voting
                    Align(
                      alignment: Alignment.center,
                      child: SizedBox(
                        width: isActive ? 60 : 10,
                        child: isActive
                            ? Icon(Icons.how_to_vote,
                                color: controller.account != null && controller.account!.votingAllowed == 1 ? Colors.green : null)
                            : Container(),
                      ),
                    ),

                    /// Text
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Hero(
                            tag: "votMain-${voting.id}",
                            child: Text(
                              voting.title,
                              style: isActive ? Styles.listTileTextMainBold : Styles.listTileTextMain,
                            ),
                          ),
                          voting.clientData.shortDescription.isNotEmpty
                              ? Hero(
                                  tag: "votSub-${voting.id}",
                                  child: Text(
                                    voting.clientData.shortDescription,
                                    style: Styles.listTileTextSub,
                                  ),
                                )
                              : Container(),
                        ],
                      ),
                    ),

                    SizedBox(width: 50, child: _votingItemIcon(voting)),
                  ],
                ),
              ),
            ),
          ),
        ),
        Styles.listDivider
      ],
    );
  }




  Widget _votingItemIcon(Voting voting) {
    if (voting.status == Voting.STATUS_FINISHED) {
      return voting.statusIcon;
    }

    if (controller.isVoted(voting)) {
      return const Icon(
        Icons.check_circle_outline_rounded,
        color: Colors.green,
      );
    }
    return Container();
  }

   */
}
