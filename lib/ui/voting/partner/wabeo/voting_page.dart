// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:partcp_client/objects/_client_data.dart';
import 'package:partcp_client/objects/voting_result.dart';
import 'package:partcp_client/styles/styles.dart';
import 'package:partcp_client/ui/voting/partner/wabeo/switch_voting_option.dart';
import 'package:partcp_client/widgets/card_widget_simple.dart';
import '../../../../controller/main_controller.dart';
import '../../../../objects/storage/app_box.dart';
import '../../../../objects/voting_type.dart';
import '../../../../utils/log.dart';
import '../../../../widgets/text_scale_widget.dart';
import '/objects/account.dart';
import '/objects/voting.dart';
import '/objects/voting_event.dart';
import '/objects/voting_option.dart';
import '/objects/api_server.dart';
import '/widgets/app_bar.dart';
import '/widgets/button_main.dart';
import '/widgets/scroll_body.dart';
import '/widgets/error_message_snackbar.dart';
import '/widgets/button_submit.dart';

import '../../menu_items.dart';
import 'voting_controller.dart';
import 'widgets/comment_box.dart';
import 'widgets/info_widget.dart';

const _TAG = "VotingPage";

class VotingPage extends StatelessWidget {
  final ApiServer server;
  final Account? account;
  final Voting voting;
  final VotingEvent votingEvent;
  final bool previewOnly;

  VotingPage({
    Key? key,
    required this.server,
    required this.account,
    required this.voting,
    required this.votingEvent,
    this.previewOnly = false,
  }) : super(key: key);

  final VotingController controller = Get.put(VotingController());

  @override
  Widget build(BuildContext context) {

    WidgetsBinding.instance.addPostFrameCallback((_) {
      controller.onPostFrameCallback();
    });

    return WillPopScope(
      onWillPop: () async => controller.onWillPop(),
      child: OrientationBuilder(
          builder: (_, __) => GetBuilder<VotingController>(
                initState: (_) => controller.initController(server, account, voting, votingEvent, previewOnly),
                builder: (_) => Scaffold(
                  appBar: MyAppBar(
                    title: Text("${controller.previewString}${controller.voting.title}"),
                    actions: [

                      if (MainController.to.appBox!.getValue(AppBoxEnum.showTextScaleWidget, defValue: false))
                        textScaleWidget(onUpdate: () => controller.updateMe()),

                      // PopupMenuButton<MenuItems>(
                      //     icon: const Icon(Icons.menu),
                      //     onSelected: (MenuItems value) {
                      //       handleMenuClick(value, controller);
                      //     },
                      //     itemBuilder: (BuildContext context) => popupMenuItems(controller)),
                    ],
                  ),
                  body: Stack(
                    fit: StackFit.expand,
                    children: [
                      Padding(
                        padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                        child: Column(
                          mainAxisSize: MainAxisSize.max,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            /// ERROR MESSAGE
                            ErrorMessageSnackBar(
                              errorMessage: controller.errorMessage,
                              // onCloseCallback: () => controller.onErrorMessageDismiss(),
                            ),

                            /// VOTING OPTION LIST
                            // Expanded(
                            //     child: ScrollBody(
                            //   children: _optionsList,
                            // )),
                            // Expanded(
                            //   child: ListView(
                            //     children: _optionsList,
                            //     shrinkWrap: true,
                            //     cacheExtent: 999,
                            //     addAutomaticKeepAlives: false,
                            //   ),
                            // ),
                            Expanded(
                              child: SingleChildScrollView(
                                controller: controller.optionsScrollController,
                                child: _optionsWidget,
                                // child: Column(
                                //   children: _optionsList,
                                // ),
                              ),
                            ),


                            /// COMMENT BOX && VOTE BUTTON
                            CardWidgetSimple(
                              child: Center(
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Column(crossAxisAlignment: CrossAxisAlignment.center, children: [
                                    /// Comment Box
                                    controller.voting.commentRules != null &&
                                            (controller.voting.commentRules!.acceptance == 1 || controller.voting.commentRules!.acceptance == 3)
                                        ? Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: CommentBoxForVoting(rules: controller.voting.commentRules!, voting: controller.voting))
                                        : Container(),

                                    /// VOTE BUTTON
                                    controller.account != null && controller.account!.votingAllowed == 1 && !controller.votingEvent.votingIsBlocked(controller.voting)
                                        ? ButtonSubmit(
                                            submitting: controller.serverResponse.receivingData,
                                            text: "Absenden",
                                            enabled: controller.submitAllowed,
                                            onPressed: controller.submitAllowed ? () => controller.sendVotes() : null,
                                            buttonStyle: ButtonMain.styleVoting,
                                          )
                                        : Container(),
                                  ]),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),

                      /// SEND PROGRESS
                      controller.serverResponse.receivingData
                        ? GetBuilder<VotingProgressController>(
                          builder: (context) {
                            return _SendProgressWidget(
                              progress: controller.progressController.uploadProgress,
                              cancelToken: controller.progressController.cancelToken,
                            );
                          }
                        )
                        : Container(),
                    ],
                  ),
                ),
              )
          // ),
          ),
    );
  }





  // Obx(() => Text("Absenden: ${controller.submitAllowed}"))

/*
  Widget get _streamUploadProgress {
    return StreamBuilder<int>(
        stream: controller.progressStreamController.stream,
        builder: (context, snapshot) {
      if (snapshot.connectionState == ConnectionState.waiting) {
        return _SendProgressWidget(progress: null);
      }
      else if (snapshot.hasError) {
        return Text('Error: ${snapshot.error}'); // Display an error message if an error occurs.
      }
      else if (!snapshot.hasData) {
        return Text('No data available'); // Display a message when no data is available.
      }
      else {
        Log.d(_TAG, "_streamUploadProgress: ${snapshot.data}");
        return _SendProgressWidget(progress: snapshot.data!);// Display the latest number when data is available.
      }
    })
    ;
  }

 */



  Widget get _optionsWidget {
    if (controller.dataRefreshRequired) {
      return Container();
    }
    else {
      return Column(
        children: _optionsList,
      );
    }
  }

  /// OPTIONS LIST
  List<Widget> get _optionsList {

    List<Widget> items = [
      /// info
      CardWidgetSimple(
        child: infoWidget(
            title: controller.voting.title,
            shortDescription: controller.voting.description.shortDescription,
            description: controller.voting.description.description),
      )
    ];


    /// info: Count confirmation, if any
    // if (controller.votingIsResultConfirmation) {
    //   items.add(CardWidgetSimple(
    //     child: infoWidget(
    //         title: "Runde ${controller.resultSetCounter + 1}. Ergebnisse eines, der schneller war als Du",
    //         shortDescription: "shortDescription, kurze Beschbeibung ",
    //         description: ""),
    //     )
    //   );
    // }


    for (VotingOption votingOption in controller.voting.votingOptions) {
      bool _readOnly = false;
      var _voteData = null;


      if (controller.votingIsResultConfirmation) {
        // if (votingOption.votingType!.type == VotingTypeEnum.count || votingOption.votingType!.type == VotingTypeEnum.confirmation) {
        if (votingOption.confirmSegmentResults) {
          VoteResult _voteResult = controller.voting.votingResult!.options[votingOption.id]![controller.resultSetCounter];
          // Log.d(_TAG, "_______voteResult => key: ${_voteResult.voteKey}; val: ${_voteResult.value}");

          _readOnly = votingOption.votingType!.type == VotingTypeEnum.count;
          _voteData = _voteResult.value;

          if (_readOnly && votingOption.votingType!.type != VotingTypeEnum.confirmation) {
            votingOption.voteValue = 1;
          }
          votingOption.voteData = _voteData;
        }

        if (votingOption.votingType!.type == VotingTypeEnum.confirmation) {
          controller.confirmationRejectionReferenceOption = votingOption;
        }
      }

      // if (votingOption.visible) {
        items.add(CardWidgetSimple(
            child: switchVotingOption(
                key: controller.widgetsKey,
                votingEvent: votingEvent,
                votingOption: votingOption,
                readOnlyValue: _voteData != null ? _voteData.toString() : null,

                /// Result of option Widget
                onUpdate: (VotingOption resultOption) {
                  /// debug
                  int debugMaxLength = 100;
                  int debugLength = 1;
                  String val = resultOption.voteData.toString();
                  debugLength = val.length < debugMaxLength ? val.length : debugMaxLength;
                  Log.d(_TAG, "onUpdate: voteValue: ${resultOption.voteValue}, voteData: ${val.substring(0, debugLength)} ...");

                  /// debug end

                  if (controller.votingIsResultConfirmation) {
                    /// pseudo-option value on checkbox
                    /// set confirmationRejectionOption.voteValue = 0, when option confirmation value == 1
                    if (controller.confirmationRejectionReferenceOption != null &&
                        resultOption.id == controller.confirmationRejectionReferenceOption!.id) {
                      if (resultOption.voteValue == 1) {
                        controller.confirmationRejectionOption.voteValue = 0;
                        controller.updateMe();
                      }
                    }
                  }
                  else {
                    controller.setVoting(votingOption, resultOption);
                  }
                  controller.updateMe();
                }))
        );
      // }
    }

    if (controller.votingIsResultConfirmation && !controller.resultsSelfSubmit) {


      items.add(CardWidgetSimple(
          child: switchVotingOption(
              key: controller.widgetsKey,
              votingEvent: votingEvent,
              votingOption: controller.confirmationRejectionOption,

              /// Result of option Widget
              onUpdate: (VotingOption resultOption) {
                /// debug
                int debugMaxLength = 100;
                int debugLength = 1;
                String val = resultOption.voteData.toString();
                debugLength = val.length < debugMaxLength ? val.length : debugMaxLength;
                Log.d(_TAG, "onUpdate: voteValue: ${resultOption.voteValue}, voteData: ${val.substring(0, debugLength)} ...");
                /// debug end

                /// pseudo-option value on checkbox
                /// set option confirmation.voteValue = 0, when this value == 1
                if (resultOption.voteValue == 1 && controller.confirmationRejectionReferenceOption != null) {
                  controller.confirmationRejectionReferenceOption!.voteValue = 0;
                  controller.updateMe();
                }

                controller.updateMe();
              }))
      );
    }

    return items;
  }
}


class _SendProgressWidget extends StatelessWidget {
  final int? progress;
  final CancelToken? cancelToken;

  _SendProgressWidget({required this.progress, this.cancelToken, Key? key} ): super(key: key);

  @override
  Widget build(BuildContext context) {

    Widget _title = progress == 100
      ? Text("Warte auf Antwort.",style: Styles.H1)
      : Text("Sende Daten.",style: Styles.H1);

    Log.d(_TAG, "_SendProgressWidget: $progress} %");
    return Stack(
      alignment: Alignment.center,
      children: [
        /// transparent Background
        Center(
          child: Container(
            color: Color.fromRGBO(200, 200, 200, 0.8),
          ),
        ),

        Container(
          width: Get.width * 0.7,
          height: 300,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: Colors.white,
          ),
          child: Padding(
            padding: const EdgeInsets.all(32.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: [

                _title,

                const SizedBox(height: 16),

                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: LinearProgressIndicator(
                    backgroundColor: Colors.grey,
                    value: progress == 100 || progress == null ? null : progress! / 100,
                    minHeight: 18,
                  ),
                ),

                const SizedBox(height: 16),

                SizedBox(
                  height: 20,
                  child: _cancelButton
                )
              ],
            ),
          ),
        )
      ],
    );
  }

  Widget get _cancelButton {
    if (progress != null && progress! < 100) {
      return TextButton(
        onPressed: () {
          if (cancelToken != null) {
            cancelToken!.cancel();
          }
        },
        child: Text("Abbrechen"));
    }
    return Container();
  }

}
