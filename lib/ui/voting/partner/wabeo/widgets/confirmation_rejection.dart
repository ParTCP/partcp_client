import 'package:flutter/material.dart';
import 'package:partcp_client/objects/voting_type.dart';

import '../../../../../objects/_client_data.dart';
import '../../../../../widgets/list_tile_simple.dart';


import '../../../../../objects/abstract_handle_error.dart';
import '../../../../../objects/account.dart';
import '../../../../../objects/api_server.dart';
import '../../../../../objects/error_message.dart';
import '../../../../../objects/voting_event.dart';
import '../../../../../objects/voting_option.dart';
import '../../../../../styles/styles.dart';
import '../../../../../utils/file_action.dart';
import '../../../../../widgets/error_message_snackbar.dart';
import '../../../widgets/voting_option_head.dart';




class ConfirmationRejection extends StatefulWidget {
  final VotingOption votingOption;
  final Function(VotingOption resultOption) onUpdate;


  const ConfirmationRejection({
    required this.votingOption,
    required this.onUpdate,
    Key? key
  }) : super(key: key);

  @override
  _ConfirmationRejectionState createState() => _ConfirmationRejectionState();
}

class _ConfirmationRejectionState extends State<ConfirmationRejection> implements HandleError {
  final _textEditingController = TextEditingController();

  ErrorMessage? errorMessage;
  FilePickerResultHolder? resultHolder;
  bool resultIsPhoto = true;
  // String photoResult = '';

  @override
  void dispose() {
    _textEditingController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {


    return Column(
      children: [

        ErrorMessageSnackBar(
          errorMessage: errorMessage,
          onCloseCallback: () => onErrorMessageDismiss(),
        ),

        _widget

      ],
    );
  }

  Widget get _widget {

    return ListTileSimple(
        title: Text(widget.votingOption.confirmationText!),
        leading: Checkbox(
            value: widget.votingOption.voteValue == 1 ? true : false,
            onChanged: (bool? value) {
              if (value != null && value) {
                widget.votingOption.voteValue = 1;
                widget.votingOption.voteData = widget.votingOption.confirmationText!;
              }
              else {
                widget.votingOption.voteValue = 0;
                widget.votingOption.voteData = null;
              }
              widget.onUpdate(widget.votingOption);
            }
        )

    );
  }


  // void _setState (String value) {
  //   String? _val = value.trim().isEmpty ? null : value.trim();
  //   widget.onUpdate(_val);
  //   setState(() {});
  // }


  @override
  void onErrorMessageDismiss() {
    errorMessage = null;
    setState(() {
      errorMessage = null;
    });
  }
}
