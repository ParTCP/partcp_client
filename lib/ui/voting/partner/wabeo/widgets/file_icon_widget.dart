
import 'package:flutter/material.dart';

import '../../../../../utils/file_action.dart';

Widget fileIconWidget (FilePickerResultHolder holder) {
  String? ext = holder.filePickerResult!.files.first.extension;
  Widget w;
  if (holder.isPhoto) {
    w = Image.memory(holder.result);
  }
  else if (ext != null && ext.toLowerCase() == "pdf") {
    w = Transform.scale(
        scale: 1.5,
        child: Icon(Icons.picture_as_pdf)
    );
  }
  else if (ext != null) {
    w = Text("${ext}");
  }
  else {
    w = Text("");
  }

  return SizedBox(
      width: 80,
      child: Center(child: w)
  );
}
