import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../../utils/log.dart';
import '../../../../../widgets/list_tile_simple.dart';


import '../../../../../objects/abstract_handle_error.dart';
import '../../../../../objects/account.dart';
import '../../../../../objects/api_server.dart';
import '../../../../../objects/error_message.dart';
import '../../../../../objects/voting_event.dart';
import '../../../../../objects/voting_option.dart';
import '../../../../../styles/styles.dart';
import '../../../../../utils/file_action.dart';
import '../../../../../widgets/error_message_snackbar.dart';
import '../../../widgets/voting_option_head.dart';




class OptionConfirmation extends StatefulWidget {
  final VotingEvent votingEvent;
  final VotingOption votingOption;
  final Function(VotingOption resultOption) onUpdate;
  // final String? confirmationText;


  const OptionConfirmation({
    required this.votingEvent,
    required this.votingOption,
    required this.onUpdate,
    // this.confirmationText,
    Key? key
  }) : super(key: key);

  @override
  _OptionConfirmationState createState() => _OptionConfirmationState();
}

class _OptionConfirmationState extends State<OptionConfirmation> implements HandleError {
  final _textEditingController = TextEditingController();

  ErrorMessage? errorMessage;
  FilePickerResultHolder? resultHolder;
  bool resultIsPhoto = true;
  // String photoResult = '';

  @override
  void dispose() {
    _textEditingController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {


    // WidgetsBinding.instance.addPostFrameCallback((_) {
    // });

    return Builder(
      builder: (context) {
        return Column(
          children: [

            ErrorMessageSnackBar(
              errorMessage: errorMessage,
              onCloseCallback: () => onErrorMessageDismiss(),
            ),

            /// head
            VotingOptionHead(votingEvent: widget.votingEvent, votingOption: widget.votingOption),

            _widget

          ],
        );
      }
    );

  }

  Widget get _widget {

    return ListTileSimple(
      // title: Text(widget.confirmationText ?? widget.votingOption.confirmationText!),
        title: Text(widget.votingOption.voteData ?? widget.votingOption.confirmationText),
      leading: Checkbox(
        value: widget.votingOption.voteValue == 1 ? true : false ,
        onChanged: (bool? value) => _sendData(value)
     )

    );
  }

  void _sendData(bool? value) {
    if (value != null && value) {
      widget.votingOption.voteValue = 1;
    }
    else {
      widget.votingOption.voteValue = 0;
    }
    widget.votingOption.voteData = widget.votingOption.voteData ?? widget.votingOption.confirmationText!;
    widget.onUpdate(widget.votingOption);

    // setState(() {
    //   if (value != null && value) {
    //     widget.votingOption.voteValue = 1;
    //     // widget.votingOption.voteData = widget.votingOption.confirmationText!;
    //   }
    //   else {
    //     widget.votingOption.voteValue = 0;
    //     // widget.votingOption.voteData = null;
    //   }
    //   widget.votingOption.voteData = widget.votingOption.confirmationText!;
    //   widget.onUpdate(widget.votingOption);
    //
    // });
  }



  // void _setState (String value) {
  //   String? _val = value.trim().isEmpty ? null : value.trim();
  //   widget.onUpdate(_val);
  //   setState(() {});
  // }


  @override
  void onErrorMessageDismiss() {
    errorMessage = null;
    setState(() {
      errorMessage = null;
    });
  }
}
