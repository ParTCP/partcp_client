import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:partcp_client/controller/main_controller.dart';

import '../../../../../objects/abstract_handle_error.dart';
import '../../../../../objects/account.dart';
import '../../../../../objects/api_server.dart';
import '../../../../../objects/error_message.dart';
import '../../../../../objects/voting_event.dart';
import '../../../../../objects/voting_option.dart';
import '../../../../../styles/styles.dart';
import '../../../../../utils/file_action.dart';
import '../../../../../widgets/error_message_snackbar.dart';
import '../../../widgets/voting_option_head.dart';



class OptionCount extends StatefulWidget {
  final VotingEvent votingEvent;
  final VotingOption votingOption;
  final Function(VotingOption resultOption) onUpdate;
  final String? readOnlyValue;

  const OptionCount({
    required this.votingEvent,
    required this.votingOption,
    required this.onUpdate,
    this.readOnlyValue,
    Key? key
  }) : super(key: key);

  @override
  _OptionCountState createState() => _OptionCountState();
}

class _OptionCountState extends State<OptionCount> implements HandleError {
  final _textEditingController = TextEditingController();


  ErrorMessage? errorMessage;
  FilePickerResultHolder? resultHolder;

  /// todo, dynamic
  int _maxDigits = 5;


  @override
  void dispose() {
    _textEditingController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    if (widget.readOnlyValue != null) {
      _textEditingController.text = widget.readOnlyValue!;
    }

    return Column(
      children: [

        ErrorMessageSnackBar(
          errorMessage: errorMessage,
          onCloseCallback: () => onErrorMessageDismiss(),
        ),

        /// head
        // VotingOptionHead(votingEvent: widget.votingEvent, votingOption: widget.votingOption, leading: SizedBox(width: 10), trailing: _textField,),
        VotingOptionHead(votingEvent: widget.votingEvent, votingOption: widget.votingOption, trailing: _textField, ),

      ],
    );
  }

  Widget get _textField {

    return Padding(
      padding: EdgeInsets.only(right: 0 / MainController.to.screenScaleFactor),
      child: SizedBox(
        width: _maxDigits * 15 * MainController.to.screenScaleFactor,
        child: TextFormField(
          readOnly: widget.readOnlyValue!= null,
          enabled: widget.readOnlyValue == null,
          controller: _textEditingController,
          onChanged: (value) => _setState(value),
          keyboardType: TextInputType.number,
          textAlign: TextAlign.right,
          enableSuggestions: false,
          style: Styles.H1,
          inputFormatters: <TextInputFormatter>[
            FilteringTextInputFormatter.digitsOnly
          ],

          decoration: InputDecoration(
            // labelText: 'Enter the Value',
            errorText: !_validate ? "Zu lang" : null,
          ),
        ),
      ),
    );
  }



  bool get _validate  {
    String value = _textEditingController.text;
    if (value.length > _maxDigits) {
      setState(() {});
      return false;
    }
    setState(() {});
    return true;
  }

  void _setState (String value) {
    var test = int.tryParse(value);
    if (test != null && test.toString().length == value.length && value.trim().length >= 0 && value.trim().length <= 5) {
      widget.votingOption.voteData = test;
      widget.votingOption.voteValue = 1;
    }
    else {
      widget.votingOption.voteValue = 0;
    }
    widget.onUpdate(widget.votingOption);
    setState(() {});
  }


  @override
  void onErrorMessageDismiss() {
    errorMessage = null;
    setState(() {
      errorMessage = null;
    });
  }
}
