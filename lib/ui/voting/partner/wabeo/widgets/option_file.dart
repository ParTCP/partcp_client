import 'dart:convert';
import 'dart:math';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:partcp_client/utils/s_util__convert.dart';

import 'package:simple_web_camera/simple_web_camera.dart';

import '../../../../../objects/abstract_handle_error.dart';
import '../../../../../objects/account.dart';
import '../../../../../objects/api_server.dart';
import '../../../../../objects/error_message.dart';
import '../../../../../objects/voting_event.dart';
import '../../../../../objects/voting_option.dart';
import '../../../../../styles/styles.dart';
import '../../../../../utils/file_action.dart';
import '../../../../../widgets/error_message_snackbar.dart';
import '../../../../../widgets/list_tile_simple.dart';
import '../../../widgets/voting_option_head.dart';
import 'file_icon_widget.dart';


class OptionFile extends StatefulWidget {
  final VotingEvent votingEvent;
  final VotingOption votingOption;
  final Function(VotingOption resultOption) onUpdate;


  const OptionFile({required this.votingEvent, required this.votingOption, required this.onUpdate, Key? key}) : super(key: key);

  @override
  _OptionFileState createState() => _OptionFileState();
}

class _OptionFileState extends State<OptionFile> implements HandleError {

  ErrorMessage? errorMessage;
  List<FilePickerResultHolder> files = [];

  bool get _newFileAllowed => _filesSize < widget.votingOption.maxSize!;

  double get _filesSize {
    double size = 0;
    for (FilePickerResultHolder holder in files) {
      size = size + holder.sizeInMb;
    }
    return size;
  }

  double get _fileSizeLeft =>  widget.votingOption.maxSize! - _filesSize;

  /// onUpdate file list
  List<String> get _result {
    List<String> items = [];
    for (FilePickerResultHolder holder in files) {
      // items.add(holder.fileAsString!);
      items.add(holder.toDataUrl);
    }
    return items;
  }

  @override
  void dispose() {
    // _textEditingController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [

        ErrorMessageSnackBar(
          errorMessage: errorMessage,
          onCloseCallback: () => onErrorMessageDismiss(),
        ),

        /// head
        VotingOptionHead(votingEvent: widget.votingEvent, votingOption: widget.votingOption),

        /// Anhang Liste
        ListView.separated(
          itemCount: files.length,
          itemBuilder: (_, int index) => _fileWidget(index),
          separatorBuilder: (_, int index) => Styles.divider,
          shrinkWrap: true,
        ),


        Styles.divider,

        /// Select File Button
        _selectFileButton,

        Padding(
          padding: const EdgeInsets.only(left: 12),
          child: _fileSizeLeft > 0
            ? _fileSizeLeftWidgetOk
            : _fileSizeLeftWidgetToBig,
        ),

        const SizedBox(height: 8),

      ],
    );
  }


  Widget get _fileSizeLeftWidgetOk {
    return Text("(noch ${SUtilConvert.decimalRound(_fileSizeLeft, places: 1)} MB möglich)",
      style: Styles.listTileTextSub,
    );
  }

  Widget get _fileSizeLeftWidgetToBig {
    return Text("${widget.votingOption.name} ist zu gross, maximal ${SUtilConvert.decimalRound(widget.votingOption.maxSize!.toDouble(), places: 1)} MB",
      style: Styles.listTileTextSubError,
    );
  }


  Widget get _selectFileButton {
    return TextButton(
        onPressed: !_newFileAllowed
            ? null
            : () async {
      FilePickerResultHolder? res = await FileActions.filePicker(pageTitle: "Datei auswählen", fileType: FileType.any, pickBinary: true, maxFileSizeInMb: widget.votingOption.maxSize);
      setState(() {
        errorMessage = res?.errorMessage;

        if (res != null && res.errorMessage == null) {
          files.add(res);
          widget.votingOption.voteValue = 1;
          widget.votingOption.voteData = _result;
          widget.onUpdate(widget.votingOption);
        }

        if (errorMessage == null) {
          // widget.onUpdate(resultHolder!.resultB64);
        }
      });

        }, child: Text("Datei auswählen"),
    );
  }


  Widget _fileWidget (int index) {
    FilePickerResultHolder holder = files[index];
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          constraints: BoxConstraints(maxWidth: 1000),
          child: ListTileSimple(
            onTap: () => {},
            leading: fileIconWidget(holder),
            title: Text("${holder.filePickerResult!.files.first.name}"),
            // subtitle: Text("${SUtilConvert.byteToString(holder.resultB64!.length)}"),
            subtitle: Text("${SUtilConvert.byteToString(holder.bytes!.length)}"),
            trailing: IconButton(
              icon: Icon(Icons.delete_forever, color: Colors.red,),
              onPressed: () {
                files.removeAt(index);
                widget.votingOption.clearClientVote();
                if (files.isNotEmpty) {
                  widget.votingOption.voteValue = 1;
                  widget.votingOption.voteData = _result;
                }
                widget.onUpdate(widget.votingOption);
                setState(() {

                });
              },
            ),
          ),
        ),
      ],
    );
  }

  @override
  void onErrorMessageDismiss() {
    errorMessage = null;
    setState(() {
      errorMessage = null;
    });
  }
}

