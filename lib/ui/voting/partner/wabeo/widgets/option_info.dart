import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../../utils/log.dart';
import '../../../../../widgets/list_tile_simple.dart';


import '../../../../../objects/abstract_handle_error.dart';
import '../../../../../objects/account.dart';
import '../../../../../objects/api_server.dart';
import '../../../../../objects/error_message.dart';
import '../../../../../objects/voting_event.dart';
import '../../../../../objects/voting_option.dart';
import '../../../../../styles/styles.dart';
import '../../../../../utils/file_action.dart';
import '../../../../../widgets/error_message_snackbar.dart';
import '../../../widgets/voting_option_head.dart';




class OptionInfo extends StatefulWidget {
  final VotingEvent votingEvent;
  final VotingOption votingOption;



  const OptionInfo({
    required this.votingEvent,
    required this.votingOption,
    Key? key
  }) : super(key: key);

  @override
  _OptionInfoState createState() => _OptionInfoState();
}

class _OptionInfoState extends State<OptionInfo> implements HandleError {
  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {


    // WidgetsBinding.instance.addPostFrameCallback((_) {
    // });

    return Builder(
        builder: (context) {
          return Column(
            children: [

              // ErrorMessageSnackBar(
              //   errorMessage: errorMessage,
              //   onCloseCallback: () => onErrorMessageDismiss(),
              // ),

              /// head
              VotingOptionHead(votingEvent: widget.votingEvent, votingOption: widget.votingOption),

              _widget

            ],
          );
        }
    );

  }

  Widget get _widget {

    return widget.votingOption.voteData != null
        ? ListTileSimple(
            title: Text(widget.votingOption.voteData),
          )
        : Container()
    ;
  }

  // void _sendData(bool? value) {
  //   if (value != null && value) {
  //     widget.votingOption.voteValue = 1;
  //     // widget.votingOption.voteData = widget.votingOption.confirmationText!;
  //   }
  //   else {
  //     widget.votingOption.voteValue = 0;
  //     // widget.votingOption.voteData = null;
  //   }
  //   widget.votingOption.voteData = widget.votingOption.confirmationText!;
  //   widget.onUpdate(widget.votingOption);
  //
  //   // setState(() {
  //   //   if (value != null && value) {
  //   //     widget.votingOption.voteValue = 1;
  //   //     // widget.votingOption.voteData = widget.votingOption.confirmationText!;
  //   //   }
  //   //   else {
  //   //     widget.votingOption.voteValue = 0;
  //   //     // widget.votingOption.voteData = null;
  //   //   }
  //   //   widget.votingOption.voteData = widget.votingOption.confirmationText!;
  //   //   widget.onUpdate(widget.votingOption);
  //   //
  //   // });
  // }
  //





  @override
  void onErrorMessageDismiss() {

  }
}
