import 'dart:convert';
import 'dart:math';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:simple_web_camera/simple_web_camera.dart';

import 'package:get/get_utils/src/platform/platform.dart';
import 'package:intl/intl.dart';

import '../../../../../objects/abstract_handle_error.dart';
import '../../../../../objects/account.dart';
import '../../../../../objects/api_server.dart';
import '../../../../../objects/error_message.dart';
import '../../../../../objects/voting_event.dart';
import '../../../../../objects/voting_option.dart';
import '../../../../../utils/file_action.dart';
import '../../../../../utils/log.dart';
import '../../../../../utils/s_util__convert.dart';
import '../../../../../widgets/error_message_snackbar.dart';
import '../../../../../widgets/list_tile_simple.dart';
import '../../../widgets/voting_option_head.dart';
import 'file_icon_widget.dart';

const _TAG = "OptionPhoto";

class OptionPhoto extends StatefulWidget {
  final VotingEvent votingEvent;
  final VotingOption votingOption;
  final Function(VotingOption resultOption) onUpdate;


  const OptionPhoto({required this.votingEvent, required this.votingOption, required this.onUpdate, Key? key}) : super(key: key);

  @override
  _OptionPhotoState createState() => _OptionPhotoState();
}

class _OptionPhotoState extends State<OptionPhoto> implements HandleError {
  final _textEditingController = TextEditingController();

  ErrorMessage? errorMessage;
  FilePickerResultHolder? resultHolder;
  bool resultIsPhoto = true;

  List<FilePickerResultHolder> files = [];

  @override
  void dispose() {
    _textEditingController.dispose();
    super.dispose();
  }


  /// onUpdate file list
  List<String> get _result {
    List<String> items = [];
    for (FilePickerResultHolder holder in files) {
      items.add(holder.toDataUrl);
    }
    return items;
  }


  @override
  Widget build(BuildContext context) {
    
    Widget _resWidget;
    if (resultHolder != null && resultIsPhoto) {
      Log.d(_TAG, "_resWidget is PHOTO");
      _resWidget = _photo;
    }
    else if (resultHolder != null) {
      Log.d(_TAG, "_resWidget is DOCUMENT");
      _resWidget = _fileWidget;
    }
    else {
      _resWidget = Container();
    }
    
    return Column(
      children: [

        ErrorMessageSnackBar(
          errorMessage: errorMessage,
          onCloseCallback: () => onErrorMessageDismiss(),
        ),

        /// head
        VotingOptionHead(votingEvent: widget.votingEvent, votingOption: widget.votingOption),

        /// Camera / Image Buttons
        Padding(
          padding: const EdgeInsets.only(bottom: 8),
          child: _buttons,
        ),

        _resWidget,
      ],
    );
  }


  Widget get _photo {
    double width = Get.width;
    double height = Get.height;

    double imgSize = min(width, height) * 0.7;

    return Center(
      child: SizedBox(
        width: imgSize,
        height: imgSize,
        child: Image.memory(resultHolder!.result),
      ),
    );
  }


  Widget get _fileWidget {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          constraints: BoxConstraints(maxWidth: 1000),
          child: ListTileSimple(
            // onTap: () => {},
            leading: fileIconWidget(resultHolder!),
            title: Text("${resultHolder!.filePickerResult!.files.first.name}"),
            subtitle: Text("${SUtilConvert.byteToString(resultHolder!.bytes!.length)}"),
            trailing: IconButton(
              icon: Icon(Icons.delete_forever, color: Colors.red,),
              onPressed: () {
                resultHolder = null;
                widget.votingOption.voteData = null;
                widget.votingOption.voteValue = 0;
                widget.onUpdate(widget.votingOption);
              },
            ),
          ),
        ),
      ],
    );
  }


  Widget get _buttons {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [

        /// /////////////////////////////
        /// Photo
        // TextButton(
        //   onPressed: GetPlatform.isWeb || GetPlatform.isMobile
        //     ? () async {
        //       var res = await Navigator.push(
        //         context,
        //         MaterialPageRoute(
        //           builder: (context) => const SimpleWebCameraPage(appBarTitle: "Photo machen", centerTitle: true),
        //         ),
        //       );
        //       setState(() {
        //         if (res is String) {
        //           resultHolder = FilePickerResultHolder(result: base64Decode(res));
        //           resultIsPhoto = true;
        //           widget.onUpdate(resultHolder!.resultB64);
        //         }
        //         else {
        //           widget.onUpdate(null);
        //         }
        //       });
        //     }
        //   : null,
        //   child: const Text("Photo machen"),
        // ),

        /// /////////////////////////////
        /// file select
        TextButton(onPressed: () async {
          // FilePickerResultHolder? res = await FileActions.filePicker(pageTitle: "Datei auswählen", fileType: FileType.custom, pickBinary: true, extensions: ["pdf", "jpg", "jpeg", "png"], maxFileSizeInMb: widget.votingOption.maxSize);
          FilePickerResultHolder? res = await FileActions.filePicker(pageTitle: "Datei auswählen", fileType: FileType.any, pickBinary: true, maxFileSizeInMb: widget.votingOption.maxSize);
          setState(() {
            resultHolder = res;
            errorMessage = res?.errorMessage;
            resultIsPhoto = resultHolder != null && resultHolder!.isPhoto;
            if (resultIsPhoto) {
              resultHolder!.result = resultHolder!.bytes;
            }

            if (res != null && errorMessage == null) {
              // widget.votingOption.voteValue = 1;
              // widget.votingOption.voteData = resultHolder!.fileAsString;
              // widget.onUpdate(widget.votingOption);
              files.add(res);
              widget.votingOption.voteValue = 1;
              widget.votingOption.voteData = _result;
              widget.onUpdate(widget.votingOption);

            }
            else {
              widget.votingOption.clearClientVote();
              widget.votingOption.voteValue = 0;
              widget.onUpdate(widget.votingOption);
            }

          });
        }, child: Text("Datei auswählen")),

      ],
    );
  }

  @override
  void onErrorMessageDismiss() {
    errorMessage = null;
    setState(() {
      errorMessage = null;
    });
  }
}
