import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:partcp_client/controller/main_controller.dart';
import 'package:partcp_client/widgets/list_tile_simple.dart';

import '../../../../../objects/abstract_handle_error.dart';
import '../../../../../objects/account.dart';
import '../../../../../objects/api_server.dart';
import '../../../../../objects/error_message.dart';
import '../../../../../objects/voting_event.dart';
import '../../../../../objects/voting_option.dart';
import '../../../../../styles/styles.dart';
import '../../../../../utils/file_action.dart';
import '../../../../../widgets/error_message_snackbar.dart';
import '../../../widgets/voting_option_head.dart';



class OptionText extends StatefulWidget {
  final VotingEvent votingEvent;
  final VotingOption votingOption;
  final Function(VotingOption resultOption) onUpdate;


  const OptionText({required this.votingEvent, required this.votingOption, required this.onUpdate, Key? key}) : super(key: key);

  @override
  _OptionTextState createState() => _OptionTextState();
}

class _OptionTextState extends State<OptionText> implements HandleError {
  final _textEditingController = TextEditingController();

  ErrorMessage? errorMessage;
  FilePickerResultHolder? resultHolder;
  bool resultIsPhoto = true;
  // String photoResult = '';

  @override
  void dispose() {
    _textEditingController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {


    return Column(
      children: [

        ErrorMessageSnackBar(
          errorMessage: errorMessage,
          onCloseCallback: () => onErrorMessageDismiss(),
        ),

        /// head
        VotingOptionHead(votingEvent: widget.votingEvent, votingOption: widget.votingOption),

        _textField

      ],
    );
  }

  Widget get _textField {

    return ListTileSimple(
      title: TextFormField(
        controller: _textEditingController,
        onChanged: (value) => _setState(value),
        minLines: widget.votingOption.rows,
        maxLines: widget.votingOption.rows,
        maxLength: widget.votingOption.maxLength,
        enableSuggestions: true,
        style: Styles.listTileTextSub,

        decoration: InputDecoration(
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(8.0),
            borderSide: BorderSide(
              color: Styles.buttonColor,
            ),
          ),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(8.0),
            borderSide: BorderSide(
              color: Styles.textColorDefault,
              // width: 2.0,
            ),
          ),
        ),
      ),
    );
  }

  // bool get _validate  {
  //   String value = _textEditingController.text;
  //   if (value.length > 5) {
  //     setState(() {});
  //     return false;
  //   }
  //   setState(() {});
  //   return true;
  // }

  void _setState (String value) {
    // String? _val = value.trim().isEmpty ? null : value.trim();
    widget.votingOption.voteValue = value.trim().isEmpty ? 0 :1;
    widget.votingOption.voteData = value.trim();
    widget.onUpdate(widget.votingOption);
    // setState(() {});
  }


  @override
  void onErrorMessageDismiss() {
    errorMessage = null;
    setState(() {
      errorMessage = null;
    });
  }
}
