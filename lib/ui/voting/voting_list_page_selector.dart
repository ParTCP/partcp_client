
import 'package:flutter/material.dart';

import '../../objects/account.dart';
import '../../objects/api_server.dart';
import '../../objects/voting_event.dart';
import 'default/default/voting_list_page.dart' as def;
import 'partner/wabeo/voting_list_page.dart' as wabeo;

class VotingListPageSelector extends StatelessWidget {
  final ApiServer server;
  final Account? account;
  final VotingEvent votingEvent;
  final bool previewOnly;

  VotingListPageSelector({
    Key? key,
    required this.server,
    required this.account,
    required this.votingEvent,
    this.previewOnly = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {

    if (votingEvent.isNonAnonymous) {
      return wabeo.VotingListPage(server: server, account: account, votingEvent: votingEvent);
    }

    return def.VotingListPage(server: server, account: account, votingEvent: votingEvent);
  }
}