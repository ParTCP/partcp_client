import 'package:flutter/material.dart';
import 'package:flutter_linkify/flutter_linkify.dart';
import 'package:partcp_client/styles/styles.dart';

import '../../../objects/voting_event.dart';
import '../../../objects/voting_option.dart';
import '../../../utils/launch_url.dart';
import '../../../widgets/list_tile_simple.dart';

class VotingOptionHead extends StatelessWidget {
  final VotingEvent votingEvent;
  final VotingOption votingOption;
  final Widget? leading;
  final Widget? trailing;

  const VotingOptionHead({required this.votingEvent, required this.votingOption, this.leading, this.trailing, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return votingOption.visible
        ? ListTileSimple(
            title: Text(votingOption.name),
            titleStyle: Styles.H2,
            subtitle: _subtitle,
            leading: leading,
            trailing: trailing,
        )
     : Container();

  }

  Widget? get _subtitle {
    List<Widget> _subtitleItems = [];
    Widget w;

    String descriptionTxt = votingOption.description.shortDescription;
    if (votingOption.description.shortDescription.isEmpty && votingOption.description.description.isNotEmpty) {
      descriptionTxt = votingOption.description.description;
    }

    /// subtitle short description
    if (descriptionTxt.isNotEmpty) {
      w = Padding(
        padding: const EdgeInsets.only(top: 6),
        child: Text(descriptionTxt),
      );
      _subtitleItems.add(w);
    }

    /// subtitle link
    if (votingOption.description.linkUrl.isNotEmpty) {
      w = Linkify(
        onOpen: (element) {
          launchURL(element.url);
        },
        text: "\nWeitere Informationen: ${votingOption.description.linkUrl}",
      );
      _subtitleItems.add(w);
    }

    return _subtitleItems.isNotEmpty
        ? Padding(
            padding: const EdgeInsets.only(left: 6),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: _subtitleItems,
            ),
          )
        : null;
  }
}
