//CreateWalletByShortCodePage

// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '/controller/main_controller.dart';
import '/objects/account.dart';
import '/objects/wallet.dart';
import '/ui/home/index_controller.dart';
import '/ui/home/index_page.dart';
import '/utils/log.dart';
import '/widgets/app_bar.dart';
import '/widgets/button_main.dart';
import '/widgets/card_widget.dart';
import '/widgets/text_field_widget.dart';

import '../api_server/api_server_page.dart';


class CreateWalletShortCodeController extends GetxController {
  final _TAG = "CreateWalletController";

  MainController mainController = MainController.to;

  TextEditingController nameController = TextEditingController();
  TextEditingController pwd1Controller = TextEditingController();
  TextEditingController pwd2Controller = TextEditingController();

  bool _keysSaved = false;
  bool isEncrypting = false;
  bool walletCreated = false;

  Wallet get wallet => mainController.wallet!;
  set wallet (Wallet _wallet) => mainController.wallet = _wallet;

  /// on some browsers we can not create keys
  bool fatalErrorCreateKeys = false;
  String fatalErrorMessage = "";

  bool get allowEncrypt => pwd1Controller.text == pwd2Controller.text &&
      pwd1Controller.text.length >= MainController.MIN_KEY_PWD_LEN &&
      !_keysSaved && !isEncrypting && nameController.text.length >= 2;

  // bool get allowFinish => _keysSaved;
  bool get pwdFieldEnabled => !_keysSaved;

  void initController() {
    nameController.text = "";
    pwd1Controller.text = "";
    pwd2Controller.text = "";
    _keysSaved = false;
    walletCreated = false;
  }

  Future<void> onClickEncryptKeys() async {
    if (allowEncrypt) {
      isEncrypting = true;
      updateMe();
      await _createAndEncryptWallet(pwd1Controller);
      isEncrypting = false;
      updateMe();
    }
  }

  void onInputChanged()  {
    updateMe();
  }


  /// ENCRYPT KEYS + CREATE FILE CONTENT
  Future<void> _createAndEncryptWallet(TextEditingController textEditingController) async {
    String _pass = "";
    try {
      _pass += "Create Wallet..";
      wallet = Wallet();
      _pass += "\nCreate Wallet: OK";
      wallet.pwdBytes = textEditingController.text.codeUnits;

      /// create wallet keys
      _pass += "\nCreate Account..";
      wallet.walletAccount = Account(Mode.wallet);
      wallet.walletAccount!.name = nameController.text;
      _pass += "\nCreate Account: OK";
      _pass += "\nCreate Keys...";

      if (await wallet.walletAccount!.createNewKeys()) {
        _pass += "\nCreate Keys: OK";
        _pass += "\nEncrypt Keys...";
        await wallet.walletAccount!.encryptKeys(wallet.pwdBytes!);
        _pass += "\nEncrypt Keys: OK";

        _pass += "\nEncrypt onWalletChanged...";
        await mainController.onWalletDecrypted(wallet);
        await mainController.onWalletChanged(updateNotification: false);
        _pass += "\nEncrypt onWalletChanged: OK";

        _pass += "\nGo to Server Page";
        // _goToServerPage();
        walletCreated = true;
      }
      else {
        _pass += "\nCreate Keys: FAILS";
        fatalErrorCreateKeys = true;
      }
    }
    catch(e, s) {
      _pass += "\n\nerror: $e; \nstack: $s";
      Log.e(_TAG, "_createAndEncryptWallet: $_pass");
      fatalErrorCreateKeys = true;
      fatalErrorMessage = "$_pass";
    }

    updateMe();
  }

  void onFinish() {
    // _goToServerPage();
    IndexController.to.updateMe();
    Route<dynamic> route = Get.rawRoute!;
    Get.to(() => IndexPage());
    Get.removeRoute(route);
  }

  void _goToServerPage() {
    IndexController.to.updateMe();
    Route<dynamic> route = Get.rawRoute!;

    Get.to(() => ApiServerPage());

    /// remove this route
    Get.removeRoute(route);
  }


  void updateMe() {
    update();
  }
}


class CreateWalletByShortCodePage extends StatelessWidget {
  final _TAG = "CreateWalletPage";
  final CreateWalletShortCodeController controller = Get.put(CreateWalletShortCodeController());

  final spacer = Container(height: 20,);

  final String createWalletTitle = "Benutzerprofil einrichten";
  final String walletCreatedTitle = "Benutzerprofil eingerichtet";

  @override
  Widget build(BuildContext context) {
    return OrientationBuilder(
      builder: (_, __) => GetBuilder<CreateWalletShortCodeController>(
          initState: (_) => controller.initController(),
          builder: (_) => Scaffold(
            appBar: MyAppBar(
              title: Text(controller.walletCreated
                  ? walletCreatedTitle
                  : createWalletTitle),
            ),
            body: SingleChildScrollView(
                child: controller.walletCreated
                    ? _bodyWalletCreated
                    : _bodyCreateWallet
            ),
          )),
    );
  }


  /// Body create Wallet
  Widget get _bodyCreateWallet => Column(
    mainAxisSize: MainAxisSize.max,
    crossAxisAlignment: CrossAxisAlignment.center,
    children: [

      const CardWidget(
        // title: "Ein Benutzerprofil für Deine persönlichen Zugangsdaten",
        childNeedsExtraTopPadding: true,
        child: Text(
            "Bitte lege ein Passwort fest, das nur Dir bekannt und schwer zu erraten ist. Es dient dazu, Dein Benutzerprofil auf diesem Gerät zu schützen und "
                "damit zu verhindern, dass jemand in Deinem Namen an Abstimmungen teilnimmt oder herausfindet, wie Du abgestimmt hast.\n\n"
                "Bitte beachte, dass es keine Möglichkeit gibt, ein verlorenes Passwort wiederherzustellen. "
                "Darum merke es Dir gut oder notiere es und bewahre es an einem sicheren Ort auf."),
      ),

      CardWidget(
        // title: "Passwort",

        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            // Text("Bitte geben Sie ein Passwort zur Verschlüsselung Ihres Benutzerprofiles ein. "),
            Center(
              child: Column(
                children: [

                  /// NAME
                  TextFieldWidget(
                    textController: controller.nameController,
                    labelText: "Name",
                    hint: "Name",
                    // onTap: () => (_),
                    onSubmitted: (_) => (_),
                    onChanged: (_) => controller.onInputChanged(),
                    autoCorrect: false,
                    isPasswordField: false,
                    keyboardType: TextInputType.visiblePassword,
                    textAlign: TextAlign.start,
                    maxBoxLength: 350,
                    enabled: controller.pwdFieldEnabled,
                  ),

                  spacer,
                  spacer,

                  /// PWD
                  TextFieldWidget(
                    textController: controller.pwd1Controller,
                    labelText: "Passwort",
                    hint: "Passwort",
                    // onTap: () => (_),
                    onSubmitted: (_) => (_),
                    onChanged: (_) => controller.onInputChanged(),
                    autoCorrect: false,
                    isPasswordField: true,
                    keyboardType: TextInputType.visiblePassword,
                    textAlign: TextAlign.start,
                    maxBoxLength: 350,
                    enabled: controller.pwdFieldEnabled,
                  ),

                  spacer,

                  /// PWD REPEAT
                  Center(
                    child: TextFieldWidget(
                      textController: controller.pwd2Controller,
                      labelText: "Passwort wiederholen",
                      hint: "Passwort",
                      // onTap: () => (_),
                      onSubmitted: controller.allowEncrypt
                          ? (_) => controller.onClickEncryptKeys()
                          : null,
                      onChanged: (_) => controller.onInputChanged(),
                      autoCorrect: false,
                      isPasswordField: true,
                      keyboardType: TextInputType.visiblePassword,
                      textAlign: TextAlign.start,
                      maxBoxLength: 350,
                      enabled: controller.pwdFieldEnabled,
                    ),
                  ),

                  Center(
                    child: _fatalErrorCreateKeys(),
                  ),

                  spacer,
                  spacer,

                  Center(
                    child: ButtonMain(
                        onPressed: controller.allowEncrypt
                            ? () => controller.onClickEncryptKeys()
                            : null,
                        child: const Text("Fertigstellen")
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    ],
  );


  /// Body create Wallet
  Widget get _bodyWalletCreated => Column(
    mainAxisSize: MainAxisSize.max,
    crossAxisAlignment: CrossAxisAlignment.center,
    children: [

      CardWidget(
        title: "Benutzerprofil eingerichtet",
        childNeedsExtraTopPadding: true,
        child: Column(
          children: [
            const Text("Dein Passwort wurde erfolgreich gespeichert und wird ab sofort dafür verwendet, "
                "Deine persönlichen Zugangs- und Abstimmungsdaten auf diesem Gerät zu schützen.\n\n"
                "Bitte notiere das Passwort an einem sicheren Ort, damit Du es gegebenenfalls nachschlagen kannst. "
                "Wenn Du das Passwort verlierst, gibt es keine Möglichkeit, das Benutzerprofil wiederherzustellen. "
                "Der Zugriff auf bereits eingelöste Teilnahmecodes und die Teilnahme an den betreffenden Abstimmungen ist dann nicht mehr möglich."),

            spacer,
            spacer,

            Center(
              child: ButtonMain(
                  onPressed: () => controller.onFinish(),
                  child: const Text("Verstanden")
              ),
            )

          ],
        ),
      ),

    ],
  );


  Widget _fatalErrorCreateKeys() {
    if (controller.fatalErrorCreateKeys) {
      return Text("Fatal Error: Can not create keys"
          "\n\n"
          "${controller.fatalErrorMessage}",
        textScaleFactor: 1.1,
        style: const TextStyle(
            color: Colors.red
        ),
      );
    }
    else {
      return Container();
    }
  }
}