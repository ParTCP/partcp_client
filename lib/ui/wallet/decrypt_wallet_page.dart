// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '/controller/main_controller.dart';
import '/objects/wallet.dart';
import '/utils/log.dart';
import '/widgets/app_bar.dart';
import '/widgets/card_widget.dart';
import '/widgets/button_submit.dart';
import '/widgets/text_field_widget.dart';

const _TAG = "DecryptKeysController";

class DecryptWalletController extends GetxController {

  MainController mainController = MainController.to;

  TextEditingController pwdController = TextEditingController();

  late Wallet wallet;
  String? decryptionError;
  bool submitting = false;

  bool _keysDecrypted = false;

  bool get allowDecrypt => pwdController.text.length >= MainController.MIN_KEY_PWD_LEN &&
      !_keysDecrypted;

  bool get pwdFieldEnabled => !_keysDecrypted;

  void initController(Wallet wallet) async {
    this.wallet = wallet;

    pwdController.text = "";
    decryptionError = null;
    _keysDecrypted = false;
    updateMe();
  }

  void onInputChanged()  {
    updateMe();
  }

  /// Benutzerprofil entsperren
  void onClickDecryptWallet() {
    Log.d(_TAG, "onClickDecryptWallet");
    if (allowDecrypt) {
      submitting = true;
      updateMe();
      _decryptWallet(pwdController);
    }
  }

  /// ENCRYPT KEYS + CREATE FILE CONTENT
  Future<void> _decryptWallet(TextEditingController textEditingController) async {
    decryptionError = null;
    bool isSuccess = await wallet.decryptAccounts(textEditingController.text.codeUnits);

    /// Pop decrypted wallet
    if (isSuccess) {
      Get.back(result: wallet);
    }
    else {
      Log.e(_TAG, "_decryptKeys: FAILS");
      decryptionError = "Passwort falsch";
      submitting = false;
      updateMe();
    }
  }

  void updateMe() {
    update();
  }
}

typedef CallbackWallet = void Function(Wallet callbackWallet);

class DecryptWalletPage extends StatelessWidget {
  final Wallet wallet;
  // final CallbackWallet callbackWallet;

  const DecryptWalletPage({Key? key,
    required this.wallet,
    // @required this.callbackWallet
  }) : super(key: key);

  Future<bool> _onWillPop() async {
    return true;
  }

  @override
  Widget build(BuildContext context) {
    final DecryptWalletController controller = Get.put(DecryptWalletController());

    final spacer = Container(height: 20,);


    return OrientationBuilder(
      builder: (_, __) => GetBuilder<DecryptWalletController>(
          initState: (_) => controller.initController(wallet),
          builder: (_) => WillPopScope(
            onWillPop: _onWillPop,
            child: Scaffold(
              appBar: MyAppBar(
                title: Text("Passwort eingeben (${wallet.walletAccount!.title})"),
              ),
              body: SingleChildScrollView(
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [

                    CardWidget(
                      //title: "Benutzerprofil entsperren",
                      childNeedsExtraTopPadding: true,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          // const Text("Bitte gib das Passwort ein, das Du für Dein Benutzerprofil festgelegt hast.\n\n"
                          //     "Solltest Du Dein Passwort vergessen haben, navigiere zurück zur Startseite und aktiviere im Funktionsmenü oben rechts (≡) den erweiterten Modus."
                          //     "Du kannst dann ein neues Benutzerprofil erstellen. Beachte aber, dass Du dann nicht mehr an Abstimmungen teilnehmen kannst, deren Teilnahmecodes"
                          //     "unter dem alten Benutzerprofil eingelöst wurden."),

                          ListTile(
                            title: CardWidget.titleStyled("Bitte gib das Passwort ein, das Du für Dein Benutzerprofil festgelegt hast."),

                            subtitle: Text("Solltest Du Dein Passwort vergessen haben, navigiere zurück zur Startseite und aktiviere im Funktionsmenü oben rechts (≡) den Mehr-Benutzer-Modus. "
                                "Du kannst dann ein neues Benutzerprofil erstellen. Beachte aber, dass Du dann nicht mehr an Abstimmungen teilnehmen kannst, deren Teilnahmecodes "
                                "unter dem alten Benutzerprofil eingelöst wurden."),

                          ),
                          spacer,

                          /// PWD
                          Center(
                            child: TextFieldWidget(
                              textController: controller.pwdController,
                              labelText: "Passwort",
                              hint: "Passwort",
                              onTap: () => (_),
                              onSubmitted: controller.allowDecrypt
                                  ? (_) => controller.onClickDecryptWallet()
                                  : null,
                              onChanged: (_) => controller.onInputChanged(),
                              autoCorrect: false,
                              isPasswordField: true,
                              keyboardType: TextInputType.visiblePassword,
                              textAlign: TextAlign.start,
                              maxBoxLength: 250,
                              enabled: controller.pwdFieldEnabled,
                              errorText: controller.decryptionError,
                            ),
                          ),

                          spacer,

                          ButtonSubmit(
                              onPressed: () =>  controller.onClickDecryptWallet(),
                              text: "Weiter",
                              submitting: controller.submitting,
                              enabled: controller.allowDecrypt),
                        ],
                      ),
                    ),

                  ],
                ),
              ),
            ),
          )),
    );
  }

}