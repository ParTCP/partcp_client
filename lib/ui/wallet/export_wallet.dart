// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved



import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '/constants.dart';
import '/controller/main_controller.dart';
import '/utils/log.dart';
import '/utils/save_file.dart';

const _TAG = "ExportWallet";

class ExportWallet {
  Future<bool> download() async {
    MainController mainController = MainController.to;
    await mainController.onWalletChanged(updateNotification: false);

    final String walletYaml = (await mainController.getYamlWallet(walletId: await mainController.wallet!.walletAccount!.walledId))!;
    // Log.d(_TAG, "download; walletYaml:$walletYaml");
    Log.d(_TAG, "download()");

    SaveFile saveFile = SaveFile(
        content: walletYaml,
        fileName: mainController.walletFileName,
        subDir: null);

    await saveFile.save();

    final snackBar = SnackBar(
      content: Text('Die Datei wurde gespeichert in: ${saveFile.filePathLazy}'),
      duration: Constants.SNACK_BAR_DURATION_LONG,
    );
    ScaffoldMessenger.of(Get.context!).showSnackBar(snackBar);


    mainController.onWalletDownloaded();
    return true;
  }

}