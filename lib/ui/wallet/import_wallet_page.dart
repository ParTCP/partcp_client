// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:partcp_client/widgets/card_widget_simple.dart';
import '/constants.dart';
import '/controller/main_controller.dart';
import '/objects/wallet.dart';
import '/ui/wallet/decrypt_wallet_page.dart';
import '/utils/file_action.dart';
import '/utils/log.dart';
import '/widgets/app_bar.dart';
import '/widgets/button_main.dart';
import '/widgets/card_widget.dart';
import 'package:permission_handler/permission_handler.dart';

class ImportController extends GetxController {
  final _TAG = "ImportController";

  MainController mainController = MainController.to;

  Wallet? wallet;

  String? keyFileName;
  String? errorMessage;

  void initController() {
    keyFileName = null;
    errorMessage = null;
  }

  Future<void> loadWalletFile() async {
    if (GetPlatform.isMobile && !GetPlatform.isWeb) {
      Log.d(_TAG, "loadWalletFile => check permission on mobile");
      if (await Permission.storage.request().isGranted) {
        Log.d(
            _TAG, "loadWalletFile => check permission on mobile => isGranted");
        _loadWalletFile();
      }
    } else {
      _loadWalletFile();
    }
  }


  Future<void> _loadWalletFile() async {
    errorMessage = null;
    wallet = null;
    updateMe();
    String? fileExtension = mainController.walletFileName.split(".").last;
    Log.d(_TAG, "_loadWalletFile; fileExtension: $fileExtension");

    Log.d(_TAG, "wallet 1");

    try {

      FilePickerResultHolder? resultHolder = await FileActions.pickFileFromStorage( pageTitle: "Wallet" );
      String? fileContent = resultHolder != null && resultHolder.errorMessage == null
        ? resultHolder.result
        : null;

      Log.d(_TAG, "fileContent: $fileContent");

      if (fileContent == null) return;

      Log.d(_TAG, "fileContent 2: $fileContent");
      wallet = Wallet.fromYaml(fileContent);

      Log.d(_TAG, "wallet 2: $wallet");

      if (wallet != null) {
        // var result = await Get.to(() => DecryptWalletPage(wallet: wallet!));
        // await _onWalletDecrypted(result);

        if (wallet!.walletAccount != null && wallet!.walletAccount!.protection == WalletProtection.simple) {
          bool isSuccess = await wallet!.decryptAccounts(MainController.to.unsecureWalletPwd.codeUnits);

          /// Pop decrypted wallet
          if (isSuccess) {
            await _onWalletDecrypted(wallet!);
          }
          else {
            Log.e(_TAG, "_decryptKeys: FAILS");
            // decryptionError = "Passwort falsch";
            updateMe();
          }
        }
        else {
          var result = await Get.to(() => DecryptWalletPage(wallet: wallet!));
          await _onWalletDecrypted(result);
        }

      } else {
        errorMessage = "Wrong wallet file";
        updateMe();
      }

    } catch (e, s) {
      Log.e(_TAG, "_loadWalletFile error: $e");
      errorMessage = "Wrong wallet file";
      Log.e(_TAG, s);
      updateMe();
    }

  }

  Future<void> _onWalletDecrypted(Wallet wallet) async {
    await MainController.to.onWalletDecrypted(wallet);
    await MainController.to.onWalletChanged();

    Log.s(
        _TAG,
        "onDecryptionCallback; "
            "pwdBytes: ${wallet.pwdBytes}; "
            "callbackWallet:${await wallet.toYaml()}");

    Get.back(result: wallet.pwdBytes != null ? wallet : null);
  }

  void updateMe() {
    update();
  }
}

class ImportWalletPage extends StatelessWidget {
  final ImportController controller = Get.put(ImportController());

  final spacer = Container(
    height: 20,
  );

  String get _test {
    return "\n"
        "isMobile: ${GetPlatform.isMobile}\n"
        "isWeb: ${GetPlatform.isWeb}\n"
        "isAndroid: ${GetPlatform.isAndroid}\n"
        "isDesktop: ${GetPlatform.isDesktop}\n"
        "isLinux: ${GetPlatform.isLinux}\n"
        "isMacOS: ${GetPlatform.isMacOS}\n"
        "isWindows: ${GetPlatform.isWindows}\n"
    ;
  }

  @override
  Widget build(BuildContext context) {
    return OrientationBuilder(
      builder: (_, __) => GetBuilder<ImportController>(
          initState: (_) => controller.initController(),
          builder: (_) => Scaffold(
            appBar: MyAppBar(
              title: const Text("Benutzerprofil importieren"),
            ),
            body: SingleChildScrollView(
              child: Column(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  CardWidgetSimple(
                    //title: "Benutzerprofil importieren",
                    child: Column(
                      children: [
                        const Text(
                            "Wenn Du zu einem früheren Zeitpunkt ein Benutzerprofil exportiert hast, kannst Du es an dieser Stelle wieder importieren. "
                                "Klicke auf „Datei auswählen“ und wähle dann die Datei aus, die beim Export erstellt wurde."),
                        spacer,
                        _errorMessage(),
                        ElevatedButton(
                            onPressed: () => controller.loadWalletFile(),
                            child: const Text("Datei auswählen")),
                        spacer,

                      ],
                    ),
                  ),
                ],
              ),
            ),
          )),
    );
  }

  Widget _errorMessage() {
    Widget w = Container();
    if (controller.errorMessage != null) {
      w = Padding(
        padding: const EdgeInsets.only(top: 0, left: 0, right: 0),
        child: Container(
          child: Text(
            controller.errorMessage!,
            style: const TextStyle(color: Colors.red),
          ),
        ),
      );
    }
    return w;
  }
}