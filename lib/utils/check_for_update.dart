// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:get/get.dart';
import 'package:package_info_plus/package_info_plus.dart';
import '/objects/app_update.dart';
import '/transferable/file_read.dart';
import '/utils/yaml_converter.dart';
import 'package:version/version.dart';

import '../constants.dart';
import 'log.dart';

class CheckForUpdate {
  static const _TAG = "CheckForUpdate";

  AppUpdate? appUpdate;
  PackageInfo? packageInfo;

  Future<void> check({required Function(bool avialable) onResponse}) async {


    if (GetPlatform.isAndroid || (GetPlatform.isLinux && GetPlatform.isDesktop)) {
      packageInfo = await PackageInfo.fromPlatform();

      try {
        FileRead(
          fileUrl: Constants.APP_REV_URL,
          onError: (int status, String message) {
            Log.e(_TAG, "check; onError: $message");
            onResponse(false);
          },
          onResponse: (dynamic response) {
            Log.d(_TAG, "check; response: $response");
            try {
              Map<String, dynamic> responseMap = YamlConverter().toMap(response);
              appUpdate = AppUpdate.fromMap(responseMap);
              Log.d(_TAG, "check; App-Revision: ${appUpdate!.version}");
              Log.d(_TAG, "check; App-Filename: ${appUpdate!.appFilename}");
              Log.d(_TAG, "check; App-Dir: ${appUpdate!.appDir}");
              onResponse(isNewRev(packageInfo, appUpdate));
            } catch (e) {
              Log.e(_TAG, "check: $e");
              onResponse(false);
            }
          },
        );
      }
      catch(e){
        onResponse(false);
      }
    }

    else {
      onResponse(false);
      return;
    }
  }

  static isNewRev(PackageInfo? packageInfo, AppUpdate? appUpdate) {
    if (appUpdate == null || packageInfo == null) return false;

    /// Android version have at the end String a
    /// "d" -> debug
    /// "r" -> release

    Version currentVersion = Version.parse(packageInfo.version.replaceAll(RegExp(r'[^0-9.]'), ''));
    Version latestVersion = Version.parse(appUpdate.version.replaceAll(RegExp(r'[^0-9.]'), ''));

    Log.d(_TAG, "isNewRev-> currentVersion: $currentVersion; latestVersion: $latestVersion");
    var currentSplitted = packageInfo.version.split("");
    var latestSplitted = appUpdate.version.split("");

    if (latestVersion == currentVersion && currentSplitted.last == "d" && latestSplitted.last == "r") {
      return true;
    }
    return latestVersion > currentVersion;

    /*
    Version betaVersion = new Version(2, 1, 0, preRelease: ["beta"]);
    // Note: this test will return false, as pre-release versions are considered
    // lesser then a non-pre-release version that otherwise has the same numbers.
    if (betaVersion > latestVersion) {
      print("More recent beta available");
    }
 */
  }

}