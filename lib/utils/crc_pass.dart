// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'dart:convert';

import 'package:crclib/catalog.dart';

import 'log.dart';

bool crcPass({
  required String text,
  required int finalLength,
  required int crcLength,
}) {
  final _TAG = "crcPass";

  Log.d(_TAG, "text: $text (text length: ${text.length}, id length: ${finalLength})");
  if (text.length == 0 || finalLength == 0) {
    return false;
  }
  bool crcPass = false;

  if (text.length == finalLength) {
    final crc = Crc32();
    final data = utf8.encode(text.substring(0, finalLength - 1));
    String crcValue = crc.convert(data).toString();
    Log.d(_TAG, "crcLength:$crcLength; crcValue: $crcValue");

    final String textLastDigits = text.substring(finalLength - crcLength);
    final String crcLastDigits = crcValue.substring(crcValue.length - crcLength);
    Log.d(_TAG, "textLastDigits: $textLastDigits; crcLastDigits: $crcLastDigits");
    if (textLastDigits == crcLastDigits) {
      Log.d(_TAG, "crcVal: true");
      crcPass = true;
    }
  }


  return crcPass;
}