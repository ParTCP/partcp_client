// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'dart:convert';

import 'package:cryptography/cryptography.dart' as ecLib;
import 'package:crypto/crypto.dart' as cryptoLib;
import '../log.dart';

class MyEcCrypt {
  static const _TAG = "MyEcCrypt";

  static final _ecInstanceX = ecLib.Cryptography.instance.x25519();
  static final _ecInstanceEd = ecLib.Cryptography.instance.ed25519();

  static Future<ecLib.SimpleKeyPair?> createKeyPairX() async {
    try {
      return await _ecInstanceX.newKeyPair();
    }
    catch(e) {
      Log.e(_TAG, "createKeyPairX; error: $e");
      return null;
    }
  }

  static Future<ecLib.SimpleKeyPair?> createKeyPairEd() async {
    try {
      return await _ecInstanceEd.newKeyPair();
    }
    catch(e){
      Log.e(_TAG, "createKeyPairEd; error: $e");
      return null;
    }
  }


  static Future<ecLib.SimpleKeyPair> createKeyPairXFromSeed(List<int> seed) async {
    return await _ecInstanceX.newKeyPairFromSeed(seed);
  }

  static Future<ecLib.SimpleKeyPair> createKeyPairEdFromSeed(List<int> seed) async {
    return await _ecInstanceEd.newKeyPairFromSeed(seed);
  }

  static Future<ecLib.SecretKey> sharedSecretWithKeyPairX(ecLib.KeyPair keyPair, ecLib.PublicKey remotePubKeyX) async {
    return await _ecInstanceX.sharedSecretKey(
        keyPair: keyPair,
        remotePublicKey: remotePubKeyX
    );
  }

  /// hash SHA-265
  static Future<ecLib.Hash> hashSha256(List<int> bytes) async {
    final pwdHashAlgorithm = ecLib.Sha256();

    final ecLib.Hash hash = await pwdHashAlgorithm.hash(bytes);
    // Log.d(_TAG, "createPwdHash: ${pwdHash}");
    return hash;
  }

  static Future<String> hashSha256AsB64 (List<int> bytes) async {
    ecLib.Hash hash = await hashSha256(bytes);
    return base64Encode(hash.bytes);
  }


  static String concatPublicKeys ({
    required ecLib.SimplePublicKey pubKeyEd,
    required ecLib.SimplePublicKey pubKeyX}) {
    List<int> list = [];
    list.addAll(pubKeyEd.bytes);
    list.addAll(pubKeyX.bytes);
    return base64Encode(list);
  }

  static Map<String, dynamic> createPublicKeysFromConcat(List<int> concatKeyBytes) {
    List<int> list = concatKeyBytes;
    List<int> seedEd = []..addAll(list.sublist(0, 32));
    List<int> seedX = []..addAll(list.sublist(32));

    Log.s(_TAG, "createPublicKeysFromConcat list: $list");
    Log.s(_TAG, "createPublicKeysFromConcat seedEd: $seedEd (${seedEd.length})");
    Log.s(_TAG, "createPublicKeysFromConcat seedX: $seedX (${seedX.length})");

    ecLib.SimplePublicKey pkX = createPublicKeyXFromSeed(seedX);
    ecLib.SimplePublicKey pkEd = createPublicKeyEdFromSeed(seedEd);

    Map<String, dynamic> map = {
      "X": pkX,
      "Ed": pkEd
    };
    return map;
  }
  

  static ecLib.SimplePublicKey createPublicKeyXFromSeed(List<int> pubKeyBytes) {
    return ecLib.SimplePublicKey(
        pubKeyBytes,
        type: ecLib.KeyPairType.x25519);
  }

  static ecLib.SimplePublicKey createPublicKeyEdFromSeed(List<int> pubKeyBytes) {
    return ecLib.SimplePublicKey(
        pubKeyBytes,
        type: ecLib.KeyPairType.ed25519);
  }


  static Future<ecLib.SecretKey> sharedSecretWithPubKeyXB64(ecLib.KeyPair keyPair, String remotePubKeyXB64) async {
    return await _ecInstanceX.sharedSecretKey(
      keyPair: keyPair,
      remotePublicKey: createPublicKeyXFromSeed(base64Decode(remotePubKeyXB64))
    );
  }

  static Future<ecLib.SecretKey> sharedSecretWithPubKey(ecLib.KeyPair keyPair, ecLib.PublicKey remotePubKeyX) async {
    return await _ecInstanceX.sharedSecretKey(
        keyPair: keyPair,
        remotePublicKey: remotePubKeyX
    );
  }

  static Future<String> createSignatureString(String message, ecLib.SimpleKeyPair localKeyPairEd) async {
    final signature = await _ecInstanceEd.sign(
      // message.codeUnits,
      utf8.encode(message),
      keyPair: localKeyPairEd,
    );
    return base64Encode(signature.bytes);
  }


  static Future<bool> verifySignature(String message, String remoteSignatureB64, ecLib.SimplePublicKey remotePublicKey) async {
    final sig2 = ecLib.Signature(
        base64Decode(remoteSignatureB64),
        publicKey: remotePublicKey
    );

    final isSignatureCorrect = await _ecInstanceEd.verify(
      // message.codeUnits,
      utf8.encode(message),
      signature: sig2,
    );

    return isSignatureCorrect;
  }

  static String hashFromPublicKeyX (ecLib.SimplePublicKey publicKeyX) {
    cryptoLib.Hash hasher = cryptoLib.md5;
    var bytes = publicKeyX.bytes;
    cryptoLib.Digest digest = hasher.convert(bytes);
    return digest.toString();
  }



}