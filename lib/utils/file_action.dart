// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:mime/mime.dart';


import 'package:file_picker/file_picker.dart';
// import 'package:filesystem_picker/filesystem_picker.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:partcp_client/objects/error_message.dart';
import 'package:partcp_client/objects/server_response.dart';
import '/controller/main_controller.dart';
import 'package:permission_handler/permission_handler.dart';

import "package:universal_html/html.dart" as webFile;

import '../constants.dart';
import 'log.dart';

const _TAG = "file_action";

class FilePickerResultHolder {
  ErrorMessage? errorMessage;
  FilePickerResult? filePickerResult;

  dynamic result;
  
  static const List<String> photoExtensions = ["jpg", "jpeg", "png"];

  FilePickerResultHolder({
    this.errorMessage,
    this.filePickerResult,
    this.result,
  });


  Uint8List? get bytes => filePickerResult?.files.first.bytes;
  bool get isPhoto => filePickerResult != null && filePickerResult?.files.first.extension != null && photoExtensions.contains(filePickerResult?.files.first.extension!);


  double get sizeInMb {
    if (filePickerResult == null || filePickerResult!.files.isEmpty) {
      return 0;
    }
    return (filePickerResult!.files.first.size / 1024 / 1024);
  }

  String? get mimeType {
    String? mimeRes = lookupMimeType(filePickerResult!.files.first.name, headerBytes: filePickerResult!.files.first.bytes!);
    if (mimeRes == null) {
      mimeRes = mimeTypeUnknown;
    }
    return mimeRes;
  }


  String get toDataUrl {
    return "data:${mimeType};base64,$toBase64";
  }

  String get toBase64 => base64Encode(bytes!);


  bool get fileIsText {
    try {
      String test = utf8.decode(bytes!);
      return true;
    }
    catch(e) {
      return false;
    }
  }

  String get mimeTypeUnknown {
    return fileIsText
      ? "text/plain"
      : "application/octet-stream";
  }


  String? get fileAsString {
    String? test;

    bool _isTextFile = true;

    try {
      test = utf8.decode(bytes!);
    }
    catch (e) {
      Log.e(_TAG, "fileAsText (fileAsString): $e");
      _isTextFile = false;
    }
    
    if (!_isTextFile) {
      try {
        test =  base64Encode(bytes!);
      }
      catch (e) {
        Log.e(_TAG, "fileAsString (base64Encode): $e");
      }
    }

    // if (test == null) {
    //   test = "ERROR";
    // }

    return test;
  } 

}

class FileActions {

  static Future<FilePickerResultHolder?> filePicker({required String pageTitle, List<String>? extensions, bool pickBinary = false, FileType? fileType, int? maxFileSizeInMb}) async {
    if (GetPlatform.isMobile && !GetPlatform.isWeb) {
      Log.d(_TAG, "loadWalletFile => check permission on mobile");
      if (await Permission.storage.request().isGranted) {
        Log.d(
            _TAG, "loadWalletFile => check permission on mobile => isGranted");
        return _filePicker(pageTitle: pageTitle, extensions: extensions, pickBinary: pickBinary, fileType: fileType, maxFileSizeInMb: maxFileSizeInMb);
      }
    } else {
      return _filePicker(pageTitle: pageTitle, extensions: extensions, pickBinary: pickBinary, fileType: fileType, maxFileSizeInMb: maxFileSizeInMb);
    }
    return null;
  }
  
  static Future<FilePickerResultHolder?> _filePicker({required String pageTitle, required List<String>? extensions, required bool pickBinary, FileType? fileType, int? maxFileSizeInMb}) async {
    return await FileActions.pickFileFromStorage(
      pageTitle: pageTitle,
      extensions: extensions,
      pickBinary: pickBinary,
      fileType: fileType,
      maxFileSizeInMb: maxFileSizeInMb
    );
  }
  
  
  static Future<void> downloadFileAction(String fileName, String fileContent) async {
    Log.d(_TAG, "downloadFileAction; fileContent:$fileContent");
    Log.d(_TAG, "downloadFileAction; fileName:$fileName");

    final bytes = utf8.encode(fileContent);
    // final blob = webFile.Blob([bytes]);
    final blob = webFile.Blob([bytes], "text/plain", 'native');
    final url = webFile.Url.createObjectUrlFromBlob(blob);

    final anchor = webFile.document.createElement('a') as webFile.AnchorElement
      ..href = url
      ..style.display = 'none'
      ..title = "$fileName"
      ..download = "$fileName";
    webFile.document.body!.children.add(anchor);
    anchor.click();

    Log.d(_TAG, "downloadFileAction: FINISH");
  }

  static Future<FilePickerResultHolder?> pickFileFromStorage({required String pageTitle, List<String>? extensions, bool pickBinary = false, FileType? fileType, int? maxFileSizeInMb}) async {
    FilePickerResult? filePickerResult = await _openFileAction(extensions, fileType);

    if (filePickerResult == null) {
      return null;
    }

      Log.d(_TAG, "pickFileFromStorage maxFileSizeInMb: $maxFileSizeInMb");
      Log.d(_TAG, "pickFileFromStorage extensions: $extensions");

      Log.d(_TAG, "pickFileFromStorage file: ${filePickerResult.files.first.name}");
      Log.d(_TAG, "pickFileFromStorage size: ${filePickerResult.files.first.size}");
      Log.d(_TAG, "pickFileFromStorage extension: ${filePickerResult.files.first.extension}");

      ServerResponse serverResponse = ServerResponse();

    /// todo maxFileSizeInKb
    if (maxFileSizeInMb != null && maxFileSizeInMb > 0) {
      if (filePickerResult.files.first.size > (maxFileSizeInMb * 1024 * 1024) ) {
        serverResponse.errorMessage = "Die Datei ist zu groß. Maximale Dateigröße: $maxFileSizeInMb MB";
        ErrorMessage errorMessage = ErrorMessage(serverResponse);
        Log.w(_TAG, serverResponse.errorMessage);
        return FilePickerResultHolder(filePickerResult: filePickerResult, errorMessage: errorMessage);
      }
    }
    if (extensions != null) {
      if (!extensions.contains(filePickerResult.files.first.extension) ) {
        serverResponse.errorMessage = "Dateityp nicht zulässig. Erlaubte Dateitypen: ${extensions.join(", ")}";
        ErrorMessage errorMessage = ErrorMessage(serverResponse);
        Log.w(_TAG, serverResponse.errorMessage);
        return FilePickerResultHolder(filePickerResult: filePickerResult, errorMessage: errorMessage);
      }
    }

    try {
      var resultConverted =  pickBinary
          ? filePickerResult.files.first.bytes! // base64Encode(filePickerResult.files.first.bytes!)
          : utf8.decode(filePickerResult.files.first.bytes!);

      /// var resultConverted =  filePickerResult.files.first.bytes;
      // var resultConverted = utf8.decode(filePickerResult.files.first.bytes!);

      return FilePickerResultHolder(filePickerResult: filePickerResult, result: resultConverted );

    } catch (e) {
      Log.e(_TAG, "pickFileFromStorage (Web); error: $e");
      serverResponse.errorMessage = "$e";
      ErrorMessage errorMessage = ErrorMessage(serverResponse);
      return FilePickerResultHolder(filePickerResult: filePickerResult, errorMessage: errorMessage);
    }

  }

  static Future<FilePickerResult?> _openFileAction(List<String>? extensions, FileType? fileType) async {
    Log.d(_TAG, "_openFileAction");

    try {
      FilePickerResult? filePickerResult = await FilePicker.platform.pickFiles(
        type: fileType ?? FileType.any,
        allowedExtensions: extensions != null ? extensions : null,
        withData: true,
      );
      return filePickerResult;
    } catch (e) {
      Log.e(_TAG, "_openFileAction; filePickerCross error: $e; extensions: $extensions, fileType: $fileType");
      return null;
    }
  }
}

