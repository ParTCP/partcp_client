// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:markdown/markdown.dart' as md;

/*
  Todo. More info:
  https://pub.dev/packages/markdown
 */

String markdownToHtml(String markdownText) {
  const _TAG = "markdownToHtml";

  final String htmlContent = md.markdownToHtml(markdownText,
    inlineSyntaxes: [md.InlineHtmlSyntax()]
  );

  return htmlContent;
}