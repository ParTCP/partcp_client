import 'package:get/get.dart';
import '/constants.dart';
import 'package:path_provider/path_provider.dart' as pathProvider;
import 'package:path/path.dart' as path;

import 'log.dart';

class PathProvider {
  final _TAG = "PathProvider";

  String? appDataPath;
  Future<String?> getAppDataPath() async {
    try {
      /// Linux
      if (GetPlatform.isDesktop && GetPlatform.isLinux) {
        String applicationSupportPath = (await pathProvider.getApplicationSupportDirectory()).path;
        appDataPath = "${applicationSupportPath.split(".").first}.${Constants.CLIENT_APPDATA_DIR_NAME}";
      }

      /// Windows
      else if (GetPlatform.isDesktop && GetPlatform.isWindows) {
        String applicationSupportPath = (await pathProvider.getApplicationSupportDirectory()).path;
        var lastPos = applicationSupportPath.lastIndexOf("\\");
        appDataPath = path.join(applicationSupportPath.substring(0, lastPos), Constants.CLIENT_APPDATA_DIR_NAME);
      }

      /// MacOS
      else if (GetPlatform.isDesktop && GetPlatform.isMacOS) {
        // todo
      }

      /// Android
      else if (GetPlatform.isAndroid) {
        /// must be null
      }
    } catch (e) {
      Log.e(_TAG, "initController error: $e");
    }
    Log.d(_TAG, "initController() -> appDataPath: $appDataPath");

    return appDataPath;
  }
}
