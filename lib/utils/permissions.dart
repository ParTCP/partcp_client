// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:permission_handler/permission_handler.dart';

class Permissions {

  static List<Permission>? group (MyPermissionGroups myPermissions) {
    switch (myPermissions) {
      case MyPermissionGroups.Gallery:
        return [
          Permission.accessMediaLocation,
          Permission.mediaLibrary,
          Permission.photos,
          Permission.storage,
        ];

      case MyPermissionGroups.Camera:
        return [Permission.camera];

      case MyPermissionGroups.ExternalStorage:
        return [Permission.storage];

    }
  }

  static Future<bool> requestPermission(List<Permission>? permissions) async {
    if (permissions != null) {
      for (Permission permission in permissions) {
        if (await permission.request().isDenied) {
          return false;
        }
      }
    }
    return true;
  }

}

enum MyPermissionGroups {
  Gallery,
  Camera,
  ExternalStorage,
}