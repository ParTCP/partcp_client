import 'dart:convert';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:get/get.dart';
import 'package:uni_links_desktop/uni_links_desktop.dart';
import '/constants.dart';
import 'package:package_info_plus/package_info_plus.dart';

import 'log.dart';
import 'path_provider.dart';

// https://askubuntu.com/questions/62585/how-do-i-set-a-new-xdg-open-setting

class RegisterApp {
  final _TAG = "RegisterApp";

  Future<String> register() async {
    String result = "0";

    bool isReleaseMode = kReleaseMode;
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    String packageName = packageInfo.packageName;

    bool registerApp = isReleaseMode;
    registerApp = true; /// for tests only
    
    try {
      /// Linux
      if (GetPlatform.isDesktop && GetPlatform.isLinux) {

        String? xdgDesktopPath;

        /// App Path
        String appPath = Directory.current.path;
        Log.d(_TAG, "appPath: $appPath");

        String? appDataPath = await PathProvider().getAppDataPath();  /// -> /home/user/.PartcpClient
        Log.d(_TAG, "appDataPath: $appDataPath");

        /// XDG .desktop
        if (appDataPath != null) {

          String homePath = (Directory(appDataPath).parent).path;

          /// only if we are in in correct app dir
          if (await checkWorkingDir(appPath, packageName)) {

            /// XDG .desktop Filename
            String xdgDesktopFileName = "${Constants.XDG_DESKTOP_NAME}-extension-${Constants.XDG_DESKTOP_NAME}.desktop";

            xdgDesktopPath = "$homePath/.local/share/applications/$xdgDesktopFileName";

            Log.d(_TAG, "xdgDesktopPath: $xdgDesktopPath");

            File appFile = File(xdgDesktopPath);

            String fileContents = fileData(
                appPath: appPath,
                packageName: packageName,
                xdgDesktopName: Constants.XDG_DESKTOP_NAME);

            if (registerApp) {
              await appFile.writeAsString(fileContents);

              final contents = await appFile.readAsLines();
              Log.d(_TAG, "partcp.desktop: $contents");

              /// register app with xdg-mime
              await Process.run('xdg-mime', ['install', '$xdgDesktopFileName']).then((ProcessResult results) {
                Log.d(_TAG, "xdg-mime install: ${results.stdout}");
              });

              await Process.run('xdg-mime', ['default', '$xdgDesktopFileName', 'x-scheme-handler/${Constants.XDG_DESKTOP_NAME}']).then((
                  ProcessResult results) {
                Log.d(_TAG, "xdg-mime default: ${results.stdout}");
              });
            }
          }
          else if (GetPlatform.isDesktop && (GetPlatform.isWindows || GetPlatform.isMacOS)) {
            registerProtocol(Constants.XDG_DESKTOP_NAME);
          }


          else {
            Log.w(_TAG, "wrong Dir, app not registered");
          }

        }
      }
    }
    catch (e) {
      /// ignore
      Log.e(_TAG, e);
    }

    return result;
  }


  String fileData({required String appPath, required String packageName, required String xdgDesktopName}) {
    String fileData = "[Desktop Entry]\n"
        // "Exec=$appPath/./$packageName %u\n"
        "Exec=$appPath/./$packageName %U\n"
        "MimeType=application/$xdgDesktopName\n"
        "Name=$xdgDesktopName\n"
        "NoDisplay=true\n"
        "Type=Application";

    return fileData;
  }

  Future<bool> checkWorkingDir(String currentDir, String packageName) async{
    /// Directory.current.path returns wrong path when app is executed via xdg-open or menu starter

    /// check app file
    File appFile = File("$currentDir/$packageName");
    if (! await appFile.exists()) {
      return false;
    }

    /// check data dir
    Directory dataDir = Directory("$currentDir/data");
    if (! await dataDir.exists()) {
      return false;
    }

    /// check data dir
    Directory libDir = Directory("$currentDir/lib");
    if (! await libDir.exists()) {
      return false;
    }

    return true;
  }

}