// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:uuid/uuid.dart';

class SUtil {
  static String createUuid() {
    var uuid = Uuid();
    return uuid.v4();
  }

}
