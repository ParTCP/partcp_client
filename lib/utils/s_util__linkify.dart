// // Author: Aleksander Lorenz
// // partcp-client@ogx.de
// // Copyright 2022. All rights reserved
//
// import 'package:flutter/material.dart';
// import 'package:url_launcher/url_launcher.dart';
//
// enum LinkifyHandleController {
//   chatController
// }
//
// class SUtilLinkify {
//
//
//
//   static const String _urlPattern = r'(?:(?:https?|ftp):\/\/)?[\w/\-?=%.,&:]+\.[\w/\-?=%.,&:]+';
//   static const String _emailPattern = r'\S+@\S+';
//   static const String _phonePattern = r'\+?[\d-]{9,}';
//
//   static final RegExp _linkRegExp = RegExp('($_urlPattern)|($_emailPattern)|($_phonePattern)', caseSensitive: false);
//
//   static Future<void> openUrl(String url) async {
//     if (url.contains(".") && !url.startsWith("http") && !url.startsWith("mailto") && !url.startsWith("tel")) {
//       url = "http://" + url;
//     }
//
//     if (await canLaunch(url)) {
//       await launch(url);
//     } else {
//       throw 'Could not launch $url';
//     }
//   }
//
//   static List<InlineSpan> linkify(String text, TextStyle textStyle, TextStyle linkStyle, String messageId, LinkifyHandleController handleController) {
//     final List<InlineSpan> list = <InlineSpan>[];
//     final RegExpMatch? match = _linkRegExp.firstMatch(text);
//     if (match == null) {
//       list.add(TextSpan(
//           text: text,
//           style: textStyle
//       ));
//       return list;
//     }
//
//     if (match.start > 0) {
//       list.add(
//           TextSpan(
//               text: text.substring(0, match.start),
//               style: textStyle
//           ));
//     }
//
//     final String linkText = match.group(0)!;
//     if (linkText.contains(RegExp(_urlPattern, caseSensitive: false))) {
//       list.add(_buildLinkComponent(linkText, linkText, linkStyle, messageId, handleController));
//     }
//     else if (linkText.contains(RegExp(_emailPattern, caseSensitive: false))) {
//       list.add(_buildLinkComponent(linkText, 'mailto:$linkText', linkStyle, messageId, handleController));
//     }
//     else if (linkText.contains(RegExp(_phonePattern, caseSensitive: false))) {
//       list.add(_buildLinkComponent(linkText, 'tel:$linkText', linkStyle, messageId, handleController));
//     } else {
//       throw 'Unexpected match: $linkText';
//     }
//
//     list.addAll(linkify(text.substring(match.start + linkText.length), textStyle, linkStyle, messageId, handleController));
//
//     return list;
//   }
//
//   /// not the best way, but works
//   /// provide a controller handle name, which handle the onTap action
//   /// when controller is null, onTap will be executed immediately
//   static WidgetSpan _buildLinkComponent(String text, String linkToOpen, TextStyle style, String messageId, LinkifyHandleController handleController) {
//     return WidgetSpan(
//         child: InkWell(
//             child: Text(
//               text,
//               style: style,
//             ),
//             //onTap: () => openUrl(linkToOpen),
//             onTap: () => handleController == null || messageId == null
//                 ? openUrl(linkToOpen)
//                 : handleOnTap(handleController, messageId, linkToOpen)
//         )
//     );
//   }
//
//   static handleOnTap(LinkifyHandleController handleController, String messageId, String linkToOpen) {
//     // if (handleController == LinkifyHandleController.chatController) {
//     //   var controller = Get.find<ChatController>();
//     //   controller.onLinkTap(messageId, linkToOpen);
//     // }
//
//   }
//
//
//
// }
