// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:get/get.dart';
import 'package:intl/intl.dart';

import 'log.dart';

class SUtils {
  static const _TAG = "SUtils";

  //static dynamic mapValue(Map<String, dynamic> map, String key, dynamic defaultVal) {
  static dynamic mapValue(dynamic map, String key, dynamic defaultVal) {
    try {
      if (map is Map) {
        var data = (map.containsKey(key))
            ? map[key]
            : defaultVal;

        if (data != null && data is String) {
          data = data.trim();
        }
        if (data == null) {
          data = defaultVal;
        }
        return data;
      }
      else {
        Log.w(_TAG, "mapValue; map is not Map<StringDynamic> ($map)");
        return defaultVal;
      }
    }
    catch(e) {
      Log.e(_TAG, "mapValue; error: $e");
      return defaultVal;
    }
  }

  static mapValueBool(dynamic map, String key, dynamic defaultVal) {
    var value = mapValue(map, key, defaultVal);
    if (value is bool) return value;
    if (value is int) {
      return value == 1;
    }
    return false;
  }

  static dynamic mapValueUrlDecoded(Map<String, dynamic> map, String key, dynamic defaultVal) {
    return map.containsKey(key)
        ? Uri.decodeComponent(map[key])
        : defaultVal;
  }


  static dynamic mapParseDateTime(Map<String, dynamic> map, String key, dynamic defaultVal) {
    dynamic data =  (map.containsKey(key))
        ? map[key]
        : defaultVal;

    if (data is int) {
      data = data.toString();
    }

    return data != null && DateTime.tryParse(data) != null
      ? DateTime.tryParse(data)
      : defaultVal;
  }


  static String dateTimeToServer(DateTime? date) {
    return  date != null
        ? DateFormat('yyyy-MM-dd HH:mm').format(date)
        : "null";
  }


  static String dateTimeToYaml(DateTime? date) {
    return  date != null
        ? DateFormat('yyyy-MM-dd – HH:mm').format(date)
        : "";
  }


  static String dateToYaml(DateTime? date) {
    return  date != null
        ? DateFormat('yyyy-MM-dd').format(date)
        : "";
  }


  static String formatDate(DateTime? date) {
    return  date != null
        ? DateFormat('dd.MM.yyyy').format(date)
        : "";
  }


  static String formatDateShort(DateTime? date) {
    return  date != null
        ? DateFormat('dd.MM.yy').format(date)
        : "";
  }


  static String formatDateTime(DateTime? date) {
    return  date != null
        ? DateFormat('dd.MM.yyyy HH:mm').format(date)
        : "";
  }


  static String formatDateTimeShort(DateTime? date) {
    return  date != null
        ? DateFormat('dd.MM.yy HH:mm').format(date)
        : "";
  }


  static String formatTime(DateTime? date) {
    return  date != null
        ? DateFormat('HH:mm').format(date)
        : "";
  }


  static bool isInt(String? s) {
    if (s == null || s.trim().isEmpty) {
      return false;
    }
    return int.tryParse(s) != null;
  }


  static String formatDouble(double value) {
    var f = NumberFormat("##0.0#", "de_DE");
    return f.format(value);
  }

}