// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:get/get_utils/src/platform/platform.dart';
import '/utils/permissions.dart';
import 'package:path_provider/path_provider.dart' as pathProvLib;
import 'package:path/path.dart' as pathLib;

import 'file_action.dart';
import 'log.dart';

class SaveFile {
  static const _TAG = "SaveFile";

  final String fileName;
  final String? subDir;
  final String content;

  String? filePathLazy; /// exists after file was saved

  SaveFile({
    required this.content,
    required this.fileName,
    required this.subDir,
  });

  Future<void> save() async {

    Log.d(_TAG, "save; filePathLazy: $filePathLazy");

    /// WEB
    if (GetPlatform.isWeb) {
      Log.d(_TAG, "Device is WEB");
      await FileActions.downloadFileAction(
          fileName, content);
    }

    /// DESKTOP
    else if(GetPlatform.isDesktop) {

      final directory = await pathProvLib.getApplicationDocumentsDirectory();
      final path = directory.path;

      String? outputFile = await FilePicker.platform.saveFile(
        initialDirectory: path,
        dialogTitle: 'Please select an output file:',
        fileName: fileName,
      );

      if (outputFile != null) {
        File file = File(outputFile);
        file.writeAsString(content);
        filePathLazy = file.path;
      }
    }

    /// MOBILE
    else {
      Log.d(_TAG, "Device is MOBILE");

        if (await Permissions.requestPermission(
            Permissions.group(MyPermissionGroups.ExternalStorage))) {

          String? selectedDirectory = await FilePicker.platform.getDirectoryPath();

          if (selectedDirectory != null) {
            print("selectedDirectory: $selectedDirectory");
            filePathLazy = pathLib.join(selectedDirectory, fileName);
            File file = File(filePathLazy!);
            await file.writeAsString(content);
          }
        }
    }
  }

}




