import 'package:flutter/services.dart';
import 'package:barcode_scan2/barcode_scan2.dart';

Future<ScanResult> scanner({
  String cancel = "",
  // List<BarcodeFormat> selectedFormats,
  bool autoEnableFlash = false,
  double aspectTolerance = 0.5,
  bool useAutoFocus = true
}) async{
  ScanResult? result;
  try {
    result = await BarcodeScanner.scan(
      options: ScanOptions(
        strings: {
          'cancel': cancel,
          'flash_on': "",
          'flash_off': "",
        },
        // restrictFormat: selectedFormats,
        // useCamera: _selectedCamera,
        autoEnableFlash: autoEnableFlash,
        android: AndroidOptions(
          aspectTolerance: aspectTolerance,
          useAutoFocus: useAutoFocus,
        ),
      ),
    );

  } on PlatformException catch (e) {
    result = ScanResult(
      type: ResultType.Error,
      rawContent: e.code == BarcodeScanner.cameraAccessDenied
          ? 'The user did not grant the camera permission!'
          : 'Unknown error: $e',
    );
  }
  return result;
}
