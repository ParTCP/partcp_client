import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:qrcode_reader_web/qrcode_reader_web.dart';

import '../widgets/app_bar.dart';
import 'log.dart';

class ScannerWebController extends GetxController {
  static const _TAG = "ScannerMobilController";

  void onPostFrameCallback() {
  }

  void onQrCodeFound(QRCodeCapture barcodes) {
    String? code = barcodes.raw;
    Get.back(result: code ??= "");
  }

  void updateMe() {
    update();
  }
}

class ScannerWeb extends StatelessWidget {
  final String title;
  const ScannerWeb(this.title, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ScannerWebController controller = Get.put(ScannerWebController());

    WidgetsBinding.instance.addPostFrameCallback((_) {
      controller.onPostFrameCallback();
    });

    return GetBuilder<ScannerWebController>(builder: (context) {
      return Scaffold(
        appBar: MyAppBar(
          title: Text(title),
        ),
        body: QRCodeReaderSquareWidget(
            onDetect: (QRCodeCapture capture) => controller.onQrCodeFound(capture),
            size: 300,
          ),
      );
    });
  }
}

