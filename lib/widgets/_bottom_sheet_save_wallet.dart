// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:flutter/material.dart';
import 'package:get/get.dart';

class BottomSheetSaveWallet  {
  void open(BuildContext context) {

    // showModalBottomSheet(
    //   context: context,
    //   builder: (BuildContext context) {
    //     return Container(
    //       child: new Wrap(
    //         children: <Widget>[
    //           new ListTile(
    //             leading: new Icon(Icons.access_alarms),
    //             title: new Text('Neuer Alarm'),
    //             onTap: () => {},
    //           ),
    //         ],
    //       ),
    //     );
    //   },
    // );

    Get.bottomSheet(
        Container(
          child: Wrap(
            children: <Widget>[
              ListTile(
                  leading: Icon(Icons.music_note),
                  title: Text('Music'),
                  onTap: () {}
              ),
              ListTile(
                leading: Icon(Icons.videocam),
                title: Text('Video'),
                onTap: () {},
              ),
            ],
          ),
        )
    );

  }
}