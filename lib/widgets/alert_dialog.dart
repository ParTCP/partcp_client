// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved



import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:flutter/material.dart';

/*
AlertDialog alertDialogWaitingForServerResponse() {
  return AlertDialog(
    // title: titleText,
    content: Center(child: CircularProgressIndicator()),
    // actions: actions.convertToMaterialDialogActions(
    //   onPressed: pop,
    //   destructiveColor: colorScheme.error,
    //   fullyCapitalized: fullyCapitalizedForMaterial,
    // ),
    // actionsOverflowDirection: actionsOverflowDirection,
  );
}
*/

Future<OkCancelResult> alertDialogErrorBox(BuildContext context, String text) async {
  return await showOkAlertDialog(
    context: context,
    title: "Fehler",
    message: text,
    okLabel: 'OK',
    barrierDismissible: false,
  );
}


