// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ButtonMain extends StatelessWidget {

  final Function? onPressed;
  final Widget child;
  final ButtonStyle? buttonStyle;

  const ButtonMain({
    Key? key,
    required this.onPressed,
    required this.child,
    this.buttonStyle,
  }) : super(key: key);

  static ButtonStyle get styleDefault => OutlinedButton.styleFrom(
      padding: const EdgeInsets.only(top: 12, bottom: 12, left: 40, right: 40),
      side: BorderSide(width: 0.5, color: Colors.black.withOpacity(0.3)),
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(3.0)));

  static ButtonStyle get styleVoting {
    double horPadding = GetPlatform.isDesktop ? 16 : 12;
    return OutlinedButton.styleFrom(
        primary: Colors.white,
        backgroundColor: Colors.blueAccent,
        padding: EdgeInsets.only(top: horPadding, bottom: horPadding, left: 40, right: 40),
        side: BorderSide(width: 1.5, color: Colors.black.withOpacity(0.3)),
        textStyle: const TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0)));
  }


  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: OutlinedButton(
        onPressed: onPressed != null
            ? () => onPressed!()
            : null,

        style: buttonStyle == null ? styleDefault : buttonStyle,
        child: child,
      ),
    );
  }

}