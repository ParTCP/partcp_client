// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:flutter/material.dart';

class ButtonSub extends StatelessWidget {
  final Function? onPressed;
  final Widget child;

  const ButtonSub({
    Key? key,
    this.onPressed,
    required this.child
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return OutlinedButton(
      onPressed: onPressed != null
        ? () => onPressed!()
        : null,
      style: OutlinedButton.styleFrom(
          padding: const EdgeInsets.only(top: 0, bottom: 0, left: 14, right: 14),
          side: BorderSide(width: 0.5, color: Colors.black.withOpacity(0.3)),
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(3.0))),
      child: child,
    );
  }

}