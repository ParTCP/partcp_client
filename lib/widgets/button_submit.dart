// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:flutter/material.dart';
import '/widgets/widgets.dart';

import '../utils/log.dart';
import 'button_main.dart';

class ButtonSubmit extends StatelessWidget {
  final bool enabled;
  final Function? onPressed;
  final String text;
  final bool submitting;
  final ButtonStyle? buttonStyle;

  const ButtonSubmit({this.enabled = true, this.onPressed, required this.text, required this.submitting, this.buttonStyle});

  @override
  Widget build(BuildContext context) {

    return ButtonMain(
      onPressed: enabled && !submitting && onPressed != null
          ? () => onPressed!()
          : null,
      child: submitting
          ? Stack(
              alignment: AlignmentDirectional.center,
              children: [
                Widgets.loaderS,
                Text(text),
              ],
            )
          : Text(text),
      buttonStyle: buttonStyle,
    );
  }
}

