// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:flutter/material.dart';
import '/utils/field_width.dart';

class CardWidget extends StatelessWidget {
  final String? title;
  final Widget? titleWidget;
  final Widget? titleListTile;
  final bool childNeedsExtraTopPadding;
  final Widget? child;
  final bool expandChild;
  final Function()? onWidgetTap;

  const CardWidget({
    Key? key,
    this.title,
    this.titleWidget,
    this.titleListTile,
    this.childNeedsExtraTopPadding = false,
    this.child,
    this.expandChild = false,
    this.onWidgetTap,
  }) : super(key: key);

  static titleStyled(dynamic title) {
    return title is String
        ? Text(
            title,
            textScaleFactor: 1.1,
            style: const TextStyle(fontWeight: FontWeight.normal, color: Colors.black87),
          )
        : title;
  }

  static listTileTitle({
    Widget? leading,
    dynamic title,
    Widget? subtitle,
    Widget? trailing,
    bool isThreeLine = false,
  }) {
    return ListTile(
      leading: leading,
      title: titleStyled(title),
      subtitle: subtitle,
      trailing: trailing,
      isThreeLine: isThreeLine,
      contentPadding: const EdgeInsets.all(0),
    );
  }

  @override
  Widget build(BuildContext context) {
    double childPadding = 0;

    if (childNeedsExtraTopPadding) {
      childPadding = 10;
    }

    Widget _titleWidget() {
      if (titleWidget != null) {
        return titleWidget!;
      }
      if (titleListTile != null) {
        return titleListTile!;
      }
      return title == null ? Container() : titleStyled(title);
    }

    EdgeInsetsGeometry padding =
        titleListTile != null ? const EdgeInsets.fromLTRB(8, 8, 0, 8) : const EdgeInsets.fromLTRB(8, 16, 8, 16);

    Widget _child() {
      Widget w;
      if (child == null) return Container();
      if (expandChild) {
        w = Expanded(
          child: Padding(
              padding: EdgeInsets.only(top: childPadding),
              child: SizedBox(
                width: fieldBoxMaxWidth,
                child: child,
              )),
        );
      } else {
        w = Padding(
            padding: EdgeInsets.only(top: childPadding),
            child: SizedBox(
              width: fieldBoxMaxWidth,
              child: child,
            ));
      }
      return w;
    }

    return Card(
      // elevation: MainController.to.isSlowMachine ? 0 : 1,
      child: InkWell(
        onTap: onWidgetTap,
        child: Padding(
          padding: padding,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Center(
                child: SizedBox(
                  width: fieldBoxMaxWidth,
                  child: _titleWidget(),
                ),
              ),
              _child(),
            ],
          ),
        ),
      ),
    );

  }
}
