// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:flutter/material.dart';
import 'package:partcp_client/widgets/list_tile_simple.dart';
import '/utils/field_width.dart';

class CardWidgetSimple extends StatelessWidget {
  final Widget? child;
  final bool expandChild;
  final Function()? onWidgetTap;

  const CardWidgetSimple({
    Key? key,
    this.child,
    this.expandChild = false,
    this.onWidgetTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {

    EdgeInsetsGeometry padding = const EdgeInsets.fromLTRB(8, 0, 8, 0);

    return Card(
      elevation: .5,
      child: InkWell(
        onTap: onWidgetTap,
        child: Padding(
          padding: padding,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Center(
                child: SizedBox(
                  width: fieldBoxMaxWidth,
                  child: child,
                ),
              ),
              // _child(),
            ],
          ),
        ),
      ),
    );

  }
}
