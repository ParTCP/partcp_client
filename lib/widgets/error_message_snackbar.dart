// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import '/objects/error_message.dart';
import '/objects/server_response.dart';
import '/utils/log.dart';
import '/widgets/button_sub.dart';

import '../constants.dart';

const _TAG = "ServerErrorMessage";

class ErrorMessageSnackBarController extends GetxController {
  static const _TAG = "ErrorMessageSnackBarController";

  ServerResponse? serverResponse;
  ErrorMessage? errorMessage;
  String uuid = "";
  bool messageShown = false;

  bool controllerInitialized = false;
  void initController(ServerResponse? serverResponse, ErrorMessage? errorMessage) {

    this.serverResponse = serverResponse;
    this.errorMessage = errorMessage;

    if (serverResponse != null && serverResponse.uuid != uuid) {
      uuid = serverResponse.uuid;
      messageShown = false;
    }
    else if(errorMessage != null && errorMessage.serverResponse.uuid != uuid) {
      uuid = errorMessage.serverResponse.uuid;
      messageShown = false;
    }
    // Log.d(_TAG, "initController => INIT, $uuid");
  }

  void onSnackBar() {
    Log.d(_TAG, "onSnackBar");
    messageShown = true;
  }
}



class ErrorMessageSnackBar extends StatelessWidget{
  final ServerResponse? serverResponse;
  final ErrorMessage? errorMessage;
  final bool useCardWidget;
  final Function()? onCloseCallback;

  ErrorMessageSnackBar({
    this.errorMessage,
    this.serverResponse,
    this.useCardWidget = true,
    this.onCloseCallback,
    Key? key,
  }) : super(key: key);

  final ErrorMessageSnackBarController controller = Get.put(ErrorMessageSnackBarController());

  String? get _message => errorMessage != null
      ? errorMessage!.message
      : serverResponse != null
        ? serverResponse!.errorMessage
        : null;

  String get _messageLong {
    String msg;
    if (errorMessage != null && errorMessage!.rawBody.isNotEmpty) {
      msg = errorMessage!.rawBody;
    }
    else if (serverResponse != null && serverResponse!.body != null) {
      msg = serverResponse!.body!;
    }
    else {
      msg = _message!;
    }
    return msg;
  }

  void _onClose() {
    ScaffoldMessenger.of(Get.context!).hideCurrentSnackBar();
    if (onCloseCallback != null) {
      onCloseCallback!();
    }
  }

  @override
  Widget build(BuildContext context) {

    controller.initController(serverResponse, errorMessage);

    WidgetsBinding.instance.addPostFrameCallback((_) {
      if (_message != null) {
        Log.d(_TAG, "show errorMessage");

        String copyDataRequest = "";
        if (serverResponse != null && serverResponse!.request != null) {
          copyDataRequest = "Request:\n${serverResponse!.request!}\n\n=>\n";
        }

        final snackBar = SnackBar(
          // backgroundColor: const Color.fromARGB(255, 240, 240, 240),
          // onVisible: () => controller.clearMessage(),
          showCloseIcon: false,
          content: ListView(
            shrinkWrap: true,
            children: [
              Row(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                    child: SelectableText(
                      "$_message\n",
                      textScaleFactor: 1.2,
                      style: const TextStyle(
                          color: Colors.red
                      ),
                    ),
                  ),

                  Align(
                    alignment: Alignment.topRight,
                    child: SizedBox(
                      width: 20,
                      height: 20,
                      child: InkWell(
                          onTap: () => _onClose(),
                          child: const Icon(Icons.cancel_outlined, color: Colors.white54,
                          )
                      ),
                    ),
                  ),

                ],
              ),

              ButtonSub(
                child: const Text(
                  "Fehlermeldung in die Zwischenablage kopieren"),
                onPressed: () => Clipboard.setData(ClipboardData(text: _messageLong))
              )
            ],
          ),
          duration: Constants.SNACK_BAR_DURATION_SERVER_ERROR,
        );

        if (!controller.messageShown) {
          controller.onSnackBar();
          ScaffoldMessenger.of(Get.context!).showSnackBar(snackBar);
        }
      }
    });
    return Container();
  }

}

