import 'package:flutter/material.dart';

import '../styles/styles.dart';

class ListTileSimple extends StatelessWidget {
  final Widget? leading;
  final Widget? title;
  final Widget? subtitle;
  final Widget? trailing;
  final bool isThreeLine;
  final EdgeInsetsGeometry? contentPadding;

  final bool selected;
  final bool isDense;

  final TextStyle? titleStyle;
  final TextStyle? subTitleStyle;

  final bool disabledStyle;

  final Function()? onTap;

  ListTileSimple(
      {this.leading,
      this.title,
      this.subtitle,
      this.trailing,
      this.isThreeLine = false,
      this.contentPadding,
      this.titleStyle,
      this.subTitleStyle,
      this.selected = false,
      this.onTap,
      this.isDense = false,
      this.disabledStyle = false
    });

  @override
  Widget build(BuildContext context) {
    final BoxBorder? _boxBorder = onTap != null
        ? null // Border.all(color: Colors.black12)
        : null;

    return Padding(
      padding: EdgeInsets.only(top: isDense ? 0 : 8, bottom: isDense ? 0 : 8),
      child: Container(
        decoration: BoxDecoration(
          border: _boxBorder,
          borderRadius: BorderRadius.circular(10),
          color: selected
              ? Styles.selectedTileColor
              : onTap != null
                  ? null //Color.fromARGB(150, 240, 240, 240)
                  : null,
        ),
        child: InkWell(
          borderRadius: BorderRadius.circular(10),
          onTap: onTap,
          child: Padding(
            padding: const EdgeInsets.only(left: 8, top: 8, bottom: 8, right: 8),
            child: Row(
              crossAxisAlignment: subtitle != null ? CrossAxisAlignment.start : CrossAxisAlignment.center,
              children: _children,
            ),
          ),
        ),
      ),
    );
  }


  List<Widget> get _children {
    List<Widget> items = [];

    if (leading != null) {
      items.add(_leading);
    }

    if (_content != null) {
      items.add(_content!);
    }

    if (trailing != null) {
      items.add(_trailing);
    }

    return items;
  }

  Widget? get _content {

    TextStyle _titleUsedStyle = titleStyle ?? Styles.listTileTextMain;
    TextStyle _titleStyle = disabledStyle
      ? _titleUsedStyle.copyWith(color: Styles.textColorDisabled)
      : _titleUsedStyle;

    Widget? wTitle;
    if (title != null) {
      wTitle = DefaultTextStyle(
        style: _titleStyle,
        child: title!,
      );
    }

    Widget? _wSubTitle;

    TextStyle _subTitleUsedStyle = subTitleStyle ?? Styles.listTileTextSub;
    TextStyle _subTitleStyle = disabledStyle
        ? _subTitleUsedStyle.copyWith(color: Styles.textColorDisabled)
        : _subTitleUsedStyle;

    if (subtitle != null) {
      _wSubTitle = Padding(
        padding: EdgeInsets.only(top: isDense ? 0 : 0),
        child: DefaultTextStyle(
          style: _subTitleStyle,
          child: subtitle!,
        ),
      );
    }

    List<Widget> items = [];
    if (wTitle != null) {
      items.add(wTitle);
    }
    if (_wSubTitle != null) {
      items.add(_wSubTitle);
    }

    if (items.isNotEmpty) {
      return Expanded(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: items,
        ),
      );

    } else {
      return null;
    }
  }

  double get _topPadding => subtitle != null ? 8 : 0;

  Widget get _leading {
    return Container(
      constraints: BoxConstraints(minWidth: 50, maxWidth: 150),
      child: Padding(
        padding: EdgeInsets.fromLTRB(8, _topPadding, 16, 0),
        child: leading,
      ),
    );
  }

  Widget get _trailing {
    return Container(
      constraints: BoxConstraints(minWidth: 50, maxWidth: 150),
      child: Padding(
        padding: EdgeInsets.fromLTRB(16, _topPadding, 16, 0),
        child: trailing,
      ),
    );
  }
}
