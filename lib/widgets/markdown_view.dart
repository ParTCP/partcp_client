// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../utils/log.dart';
import '/widgets/app_bar.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import '/utils/mark_down_to_html.dart';
import '/widgets/scroll_body.dart';

class MarkdownView extends StatelessWidget {
  final _TAG = "MarkdownView";
  final String markdownText;
  final String title;

  const MarkdownView({Key? key, required this.title, required this.markdownText}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final String htmlContent = markdownToHtml(markdownText);

    Log.d(_TAG, "title: $title");
    Log.d(_TAG, "markdownText: $markdownText");

    Widget backButton = GetPlatform.isWeb
        ? Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextButton(
              onPressed: () => Get.back(),
              child: const Text("Zurück"),
            ),
          )
        : Container(height: 8,);

    return Scaffold(
      appBar: MyAppBar(
        title: Text(title),
      ),
      body: Builder(
        builder: (context) {
          return Padding(
            padding: const EdgeInsets.all(8.0),
            child: ScrollBody(children: [
              backButton,
              HtmlWidget(
                htmlContent,
                textStyle: const TextStyle(color: Colors.black87, fontSize: 14),
                customStylesBuilder: (element) {
                  if (element.classes.contains('h1')) {
                    return {'color': 'red'};
                  }
                  return null;
                },
              ),
              backButton,
            ]),
          );
        },
      ),
    );
  }
}
