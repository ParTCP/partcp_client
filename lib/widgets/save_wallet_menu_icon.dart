// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '/controller/main_controller.dart';

Widget saveWalletMenuIcon({required Function() onPressed}) {
  return GetPlatform.isWeb
    ? IconButton(icon: _icon(MainController.to.walletWasChanged), onPressed: () => onPressed())
    : Container();
}

Icon _icon(bool isActive) {
  IconData _iconData = Icons.save_outlined;
  return isActive ? Icon(_iconData) : Icon(_iconData, color: Colors.black26);
}
