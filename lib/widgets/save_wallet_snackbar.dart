// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:flutter/material.dart';
import 'package:get/get_utils/get_utils.dart';
import '/controller/main_controller.dart';
import '/ui/wallet/export_wallet.dart';

class SaveWalletSnackBarPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) async {

      if (GetPlatform.isWeb) {
        MainController controller = MainController.to;

        if (controller.walletNotification) {
          controller.walletNotification = false;

          final result = await showModalActionSheet<String>(
            context: context,
            title: 'Benutzerprofil',
            message: 'Dein Benutzerprofil wurde geändert. Vergesse nicht, ihn zu sichern',
            actions: [
              SheetAction(
                icon: Icons.save_outlined,
                label: 'Benutzerprofil speichern',
                key: 'saveWallet',
              ),
            ],
          );
          if (result != null && result == "saveWallet") {
            await ExportWallet().download();
            controller.walletWasChanged = false;
          }
        }
      }

    });
    return Container();
  }
}

