// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ScrollBody extends StatelessWidget{
  final List<Widget> children;
  final ScrollPhysics? physics;
  final bool shrinkWrap;
  final bool enabled;

  ScrollBody({Key? key,
    required this.children,
    this.physics,
    this.shrinkWrap = false,
    this.enabled = true,
  }) : super(key: key);


  final ScrollController _scrollController = ScrollController();
  @override
  Widget build(BuildContext context) {

    return Scrollbar(
      controller: _scrollController,
      // thickness: 15,
      child: CustomScrollView(
        controller: _scrollController,
        // physics: const ClampingScrollPhysics(),
        cacheExtent: 50,
        slivers: [
          SliverList(
            delegate: SliverChildBuilderDelegate(
              (context, index) => children[index],
              childCount: children.length ,
            ),
          )
        ],
      ),
    );
  }



}