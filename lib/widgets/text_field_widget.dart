// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class TextFieldWidget extends StatefulWidget {
  final TextEditingController textController;
  final FocusNode? focusNode;
  final String? hint;
  final String? errorText;
  final VoidCallback? onTap;
  final Function(String text)? onSubmitted;
  final Function(String text)? onChanged;
  final double fontSize;
  final Color borderColor;
  final double? maxBoxLength;
  final bool isPasswordField;
  final bool readOnly;
  final bool? enabled;
  final bool? showBorder;
  final int minLines;
  final int maxLines;
  final bool autoCorrect;
  final List<TextInputFormatter>? inputFormatters;
  final TextInputType keyboardType;
  final String? labelText;
  final TextAlign textAlign;
  final String obscuringCharacter;
  final TextCapitalization textCapitalization;
  final TextInputAction? textInputAction;
  final int? maxLength;
  final MaxLengthEnforcement? maxLengthEnforcement;
  final bool bold; // for (main) title when multiline
  final double? labelFontSize;
  final Widget? suffixIcon;
  final bool autofocus;

  const TextFieldWidget({
      Key? key,
      required this.textController,
      this.focusNode,
      required this.hint,
      this.onTap,
      this.onSubmitted,
      this.onChanged,
      this.errorText,
      this.fontSize = 18.0,
      this.borderColor = Colors.blueAccent,
      this.maxBoxLength = 300,
      this.isPasswordField = false,
      this.readOnly = false,
      this.enabled,
      this.showBorder = true,
      this.minLines = 1,
      this.maxLines = 1,
      this.autoCorrect = false,
      this.inputFormatters,
      this.keyboardType = TextInputType.text,
      this.labelText,
      this.textAlign = TextAlign.start,
      this.obscuringCharacter = "*",
      this.textCapitalization = TextCapitalization.none,
      this.textInputAction,
      this.maxLength,
      this.maxLengthEnforcement,
      this.bold = false,
      this.labelFontSize,
      this.suffixIcon,
      this.autofocus = false
  }) : super(key: key);

  @override
  _TextFieldWidgetState createState() => _TextFieldWidgetState();
}

class _TextFieldWidgetState extends State<TextFieldWidget> {
  bool obscureText = false;

  @override
  void initState() {
    super.initState();
    obscureText = widget.isPasswordField;
  }

  @override
  Widget build(BuildContext context) {
    // print("obscureText: $obscureText");

    double _maxBoxLength =
        widget.maxBoxLength == null ? 1500 : widget.maxBoxLength!;

    ScrollController _scrollController = ScrollController();

    return Container(
      constraints: BoxConstraints(minWidth: 20, maxWidth: _maxBoxLength),
      child: TextField(
        key: widget.key,
        focusNode: widget.focusNode,
        controller: widget.textController,
        scrollPadding: const EdgeInsets.all(0.0),
        enabled: widget.enabled,
        readOnly: widget.readOnly,
        keyboardType: widget.keyboardType,
        obscureText: obscureText,
        textAlign: widget.textAlign,
        showCursor: true,
        minLines: widget.minLines,
        maxLines: widget.maxLines,
        onTap: () => widget.onTap != null ? widget.onTap!() : {},
        onSubmitted: (String text) => widget.onSubmitted != null ? widget.onSubmitted!(text) : null,
        onChanged: (String text) => widget.onChanged != null ? widget.onChanged!(text) : null,
        inputFormatters: widget.inputFormatters,
        obscuringCharacter: widget.obscuringCharacter,
        textCapitalization: widget.textCapitalization,
        textInputAction: widget.textInputAction,
        maxLength: widget.maxLength,
        maxLengthEnforcement: widget.maxLengthEnforcement,
        autofocus: widget.autofocus,

        style: widget.maxLines == 1 || widget.bold
            ? TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.black87,
                fontSize: widget.fontSize,
              )
            : const TextStyle(),

        decoration: InputDecoration(
          labelText: widget.labelText,
          errorText: widget.errorText,
          suffixIcon: widget.suffixIcon ?? _obscureTextIcon(),

          hintText: widget.hint,

          hintStyle: const TextStyle(
              // fontSize: widget.fontSize,
              // fontWeight: FontWeight.w700,
              color: Colors.black26),

          labelStyle: TextStyle(color: Colors.black26, fontSize: widget.labelFontSize),

          border: widget.showBorder! ? const UnderlineInputBorder() :InputBorder.none,

          focusedBorder: UnderlineInputBorder(
            borderRadius: BorderRadius.circular(5.0),
            borderSide: BorderSide(width: 2.0, color: widget.borderColor),
          ),

          contentPadding: const EdgeInsets.only(bottom: 4),
        ),

      ),
    );
  }

  Widget? _obscureTextIcon() {
    Widget? w;
    if (widget.isPasswordField) {
      w = IconButton(
        icon: Icon(
          // Based on passwordVisible state choose the icon
          obscureText ? Icons.visibility_off : Icons.visibility,
          color: Theme.of(context).primaryColorDark,
        ),
        onPressed: () {
          // Update the state i.e. toogle the state of passwordVisible variable
          setState(() {
            obscureText = !obscureText;
          });
        },
      );
    }
    return w;
  }
}
