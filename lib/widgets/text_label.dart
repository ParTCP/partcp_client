// Author: Aleksander Lorenz
// partcp-client@ogx.de
// Copyright 2022. All rights reserved

import 'package:flutter/material.dart';

class TextLabel extends StatelessWidget {
  final String label;
  final bool labelIsBold;
  final String? text;
  final bool textIsBold;
  final Widget? child;

  const TextLabel({
    required this.label,
    this.labelIsBold = true,
    this.text,
    this.textIsBold = false,
    this.child,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          /// label
          formatLabel(label: label, labelIsBold: labelIsBold),

          /// text
          Padding(
            padding: const EdgeInsets.only(top: 3),
            child: child != null
                ? child
                : Text(
                    text!,
                    style: TextStyle(
                        fontWeight: textIsBold ? FontWeight.bold : FontWeight.normal, color: Colors.black87, fontSize: 18),
                  ),
          ),
        ],
      ),
    );
  }

  static Widget formatLabel({required String label, bool labelIsBold = true}) {
    return Text(
      label,
      style: TextStyle(fontWeight: labelIsBold ? FontWeight.bold : FontWeight.normal, color: Colors.black45, fontSize: 15),
    );
  }
}

