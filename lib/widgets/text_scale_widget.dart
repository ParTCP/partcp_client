import 'package:flutter/material.dart';

import '../controller/main_controller.dart';


Row textScaleWidget ({required Function() onUpdate}) {
  return Row(
    mainAxisSize: MainAxisSize.min,
    children: [

      IconButton(
        icon: const Icon(Icons.remove),
        onPressed: () {
          MainController.to.changeScreenScaleFactor(-0.1);
          onUpdate();
        },
      ),

      IconButton(
        icon: const Icon(Icons.format_size),
        onPressed: () {
          MainController.to.resetScreenScaleFactor();
          onUpdate();
        },
      ),


      IconButton(
        icon: const Icon(Icons.add),
        onPressed: () {
          MainController.to.changeScreenScaleFactor(0.1);
          onUpdate();
        },
      ),

    ],
  );

}